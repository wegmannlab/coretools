cmake_minimum_required(VERSION 3.14)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()


# Special build types
if("${CMAKE_BUILD_TYPE}" STREQUAL "Asan")
  set(SAN "-O1;-g;-fsanitize=address;-fno-omit-frame-pointer")
  set(SAN_LINK "-fsanitize=address")
  message("Compiling and linking with adress sanitizer")
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "Msan")
  set(SAN "-O1;-g;-fsanitize=memory;-fno-omit-frame-pointer;-fsanitize-memory-track-origins")
  set(SAN_LINK "-fsanitize=memory")
  message("Compiling and linking with memory sanitizer")
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "UBsan")
  set(SAN "-O1;-g;-fsanitize=undefined;-fno-omit-frame-pointer")
  set(SAN_LINK "-fsanitize=undefined")
  message("Compiling and linking with undefined behavior sanitizer")
else()
  set(SAN "")
  set(SAN_LINK "")
endif()

# Project
project(coretools LANGUAGES CXX)
add_library(${PROJECT_NAME} STATIC)

# Packages
include(FetchContent)
find_package(ZLIB REQUIRED)

option(CONDA "Compiling with/for conda" OFF)
if(CONDA)
  # conda install cmake ninja gcc gxx armadillo fmt nlohmann_json openmp zlib sysroot_linux-64=2.17
  # CC=$(which gcc) CXX=$(which g++) cmake .. -GNinja -DCONDA=ON -DCMAKE_LIBRARY_PATH=$CONDA_PREFIX/lib
  find_package(Armadillo REQUIRED)
  find_package(fmt REQUIRED)
  find_package(nlohmann_json REQUIRED)
else()
  option(FETCHARMA "Fetching armadillo from git" ON)
  if(FETCHARMA)
	message("Fetching Armadillo from git")
	FetchContent_Declare(armadillo
			GIT_REPOSITORY https://gitlab.com/conradsnicta/armadillo-code.git
			GIT_TAG        11.4.x
			SOURCE_DIR     "${CMAKE_CURRENT_SOURCE_DIR}/armadillo"
			)
	FetchContent_MakeAvailable(armadillo)
  else()
	message("Using locally installed Armadillo")
	find_package(Armadillo REQUIRED)
	set(armadillo ${ARMADILLO_LIBRARIES})
  endif ()

  FetchContent_Declare(fmt
	GIT_REPOSITORY https://github.com/fmtlib/fmt.git
	GIT_TAG        11.0.2
	SOURCE_DIR     "${CMAKE_CURRENT_SOURCE_DIR}/fmt"
  )

  FetchContent_Declare(
	json
	GIT_REPOSITORY https://github.com/nlohmann/json.git
	GIT_TAG        v3.11.3
	SOURCE_DIR     "${CMAKE_CURRENT_SOURCE_DIR}/json"
  )

  FetchContent_MakeAvailable(fmt json)

  # Rcpp
  option(RCPP "Using Rcpp" OFF)
  if(RCPP)
	message(STATUS "Compiling in Rcpp mode")
	# Find R
	list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")
	find_package(R)
	if(NOT ${R_FOUND})
	  message(FATAL_ERROR "No R found!")
	endif()
	# Find Rcpp (credits: https://gist.github.com/wush978/4662585)
	execute_process(
			COMMAND ${R_EXECUTABLE} "--slave" "-e" "stopifnot(require('Rcpp', quietly=T));cat(Rcpp:::Rcpp.system.file('include'))"
			OUTPUT_VARIABLE RCPP_INCLUDE_DIRS
	)
	target_include_directories(${PROJECT_NAME} PUBLIC ${R_INCLUDE_DIRS} ${RCPP_INCLUDE_DIRS})
	set(USE_RCPP "-DUSE_RCPP -DCOMPILING_RCPP")
  else()
	message(STATUS "Compiling in command-line (non-Rcpp) mode")
	set(USE_RCPP "")
  endif()
endif()

option(PARALLEL "Parallelize with OpenMP" ON)
if(PARALLEL AND NOT RCPP) # only allow parallel if Rcpp is not used
  find_package(OpenMP)
  if(OpenMP_CXX_FOUND)
	target_link_libraries(${PROJECT_NAME} PUBLIC OpenMP::OpenMP_CXX)
  endif()
endif()

# This is header-only and therefore no problem with conda
FetchContent_Declare(
  fast_float
  GIT_REPOSITORY https://github.com/fastfloat/fast_float.git
  GIT_TAG        v6.1.6
  SOURCE_DIR     "${CMAKE_CURRENT_SOURCE_DIR}/fast_float"
)
FetchContent_MakeAvailable(fast_float)

file(GLOB_RECURSE CORETOOLS_SOURCES CONFIGURE_DEPENDS core/*.h core/*.cpp)
execute_process(COMMAND git rev-parse HEAD OUTPUT_VARIABLE GIT_HEAD OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
option(CHECK_INTERVALS "Check for valid weakType intervals" OFF)

target_sources(${PROJECT_NAME} PRIVATE ${CORETOOLS_SOURCES})
target_include_directories(${PROJECT_NAME}
		PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/core"
		SYSTEM INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/core")
target_link_libraries(${PROJECT_NAME} PUBLIC ${SAN_LINK} armadillo fast_float fmt::fmt nlohmann_json::nlohmann_json ${ZLIB_LIBRARIES} ${R_LIBRARY})
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_EXTENSIONS OFF)
target_compile_options(${PROJECT_NAME} PUBLIC ${SAN} ${USE_RCPP} PRIVATE -Wall -Wextra)
target_compile_definitions(${PROJECT_NAME} PUBLIC GITVERSION=\"${GIT_HEAD}\"
  PRIVATE "$<$<BOOL:${CHECK_INTERVALS}>:CHECK_INTERVALS>" DEVTOOLS)

# Test
add_executable("${PROJECT_NAME}_unitTests" EXCLUDE_FROM_ALL)

file(GLOB_RECURSE TEST_SOURCES tests/*.h tests/*.cpp)

FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        main
)
FetchContent_MakeAvailable(googletest)

set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
option(gtest_disable_pthreads "Disable uses of pthreads in gtest." ON)

target_sources("${PROJECT_NAME}_unitTests" PRIVATE ${TEST_SOURCES})
target_include_directories("${PROJECT_NAME}_unitTests" PRIVATE tests)
target_link_libraries("${PROJECT_NAME}_unitTests" PRIVATE ${PROJECT_NAME} gtest_main gmock_main ${SAN_LINK})
target_compile_definitions("${PROJECT_NAME}_unitTests" PRIVATE CHECK_INTERVALS DEVTOOLS)
target_compile_options(${PROJECT_NAME}_unitTests PRIVATE -Wall -Wextra ${SAN} ${USE_RCPP})

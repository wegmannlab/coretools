//
// Created by madleina on 26.07.21.
//

#ifndef BANGOLIN_WEAKTYPESWITHLOGEXP_H
#define BANGOLIN_WEAKTYPESWITHLOGEXP_H

#include "coretools/Types/intervals.h"
#include "coretools/Types/old/weakTypes.h"

namespace coretools {

//---------------------------------------------------------------------
// Purpose of classes in this file:
//  - keep track of _value as well as log(_value) and/or exp(_value)
//  - useful if log/exp of a value are used a lot, but value does not change in between
//  - classes update log/exp whenever _value changes -> do not need to re-compute it all the time!
//---------------------------------------------------------------------

//------------------------------------------------
// WeakTypeWithLog
// Weak type that keeps track of its value and its logarithm
//------------------------------------------------

template<class Type>
class WeakTypeWithLog : public WeakTypeWithInterval<Type, WeakTypeWithLog<Type>, intervals::Positive> {
private:
	using Base = WeakTypeWithInterval<Type, WeakTypeWithLog<Type>, intervals::Positive>;
	// Constructor without check, only use internally
	WeakTypeWithLog(tags::NoCheck, Type Value) : Base(tags::NoCheck{}, Value) { _updateLogValue(); };

	double _logValue;

	void _updateLogValue() noexcept { _logValue = std::log(this->_value); }

public:
	// constructors
	WeakTypeWithLog()
	    : WeakTypeWithInterval<Type, WeakTypeWithLog<Type>, intervals::Positive>(tags::NoCheck{}, 0.0) {
		_updateLogValue();
	};

	WeakTypeWithLog(Type Value)
	    : WeakTypeWithInterval<Type, WeakTypeWithLog<Type>, intervals::Positive>(Value) { // not explicit
		_updateLogValue();
	};
	explicit WeakTypeWithLog(tags::NoCheck, std::string_view Value)
	    : WeakTypeWithInterval<Type, WeakTypeWithLog<Type>, intervals::Positive>(tags::NoCheck{}, Value) {
		_updateLogValue();
	};
	explicit WeakTypeWithLog(std::string_view Value)
	    : WeakTypeWithInterval<Type, WeakTypeWithLog<Type>, intervals::Positive>(Value) {
		_updateLogValue();
	};

	// assign
	WeakTypeWithLog &operator=(Type Val) {
		this->_set(Val);
		_updateLogValue();
		return *this;
	};

	WeakTypeWithLog &operator=(std::string_view ValueString) {
		this->_set(str::fromString<Type, true>(ValueString));
		_updateLogValue();
		return *this;
	};

	//-----------------------------
	// manipulate with WeakTypeWithLog
	// Returns WeakTypeWithLog
	//-----------------------------
	// multiplication always works
	[[nodiscard]] constexpr WeakTypeWithLog operator*(WeakTypeWithLog other) const noexcept {
		return WeakTypeWithLog(tags::NoCheck{}, this->_value * other._value);
	};

	constexpr WeakTypeWithLog &operator*=(WeakTypeWithLog other) noexcept {
		this->_value *= other._value;
		_updateLogValue();
		return *this;
	};

	// division always works
	[[nodiscard]] constexpr WeakTypeWithLog operator/(WeakTypeWithLog other) const noexcept {
		return WeakTypeWithLog(tags::NoCheck{}, this->_value / other._value);
	};

	constexpr WeakTypeWithLog &operator/=(WeakTypeWithLog other) noexcept {
		this->_value /= other._value;
		_updateLogValue();
		return *this;
	};

	// additions always works
	[[nodiscard]] constexpr WeakTypeWithLog operator+(WeakTypeWithLog other) const noexcept {
		return WeakTypeWithLog(tags::NoCheck{}, this->_value + other._value);
	};

	constexpr WeakTypeWithLog &operator+=(WeakTypeWithLog other) noexcept {
		this->_value += other._value;
		_updateLogValue();
		return *this;
	};

	// subtractions may or may not be permissible: check range!
	[[nodiscard]] constexpr WeakTypeWithLog operator-(WeakTypeWithLog other) const {
		// constructor will check range
		return WeakTypeWithLog(this->_value - other._value);
	};

	constexpr WeakTypeWithLog &operator-=(WeakTypeWithLog other) {
		*this = *this - other;
		return *this;
	};

	friend double log(WeakTypeWithLog Obj) noexcept { return Obj._logValue; }
};

//------------------------------------------------
// WeakTypeWithExp
// Weak type that keeps track of its exponential
//------------------------------------------------

template<class Type> class WeakTypeWithExp : public WeakTypeBase<Type, WeakTypeWithExp<Type>> {
private:
	double _expValue;

	void _updateExpValue() noexcept { _expValue = std::exp(this->_value); }

public:
	// constructors
	WeakTypeWithExp() : WeakTypeBase<Type, WeakTypeWithExp<Type>>(0.0) { _updateExpValue(); };

	WeakTypeWithExp(Type Value) : WeakTypeBase<Type, WeakTypeWithExp<Type>>(Value) { // not explicit
		_updateExpValue();
	};
	explicit WeakTypeWithExp(std::string_view Value) : WeakTypeBase<Type, WeakTypeWithExp<Type>>(Value) {
		_updateExpValue();
	};
	explicit WeakTypeWithExp(tags::NoCheck, std::string_view Value)
	    : WeakTypeBase<Type, WeakTypeWithExp<Type>>(tags::NoCheck{}, Value) {
		_updateExpValue();
	};

	// assign
	constexpr WeakTypeWithExp &operator=(Type Val) noexcept {
		this->_value = Val;
		_updateExpValue();
		return *this;
	};

	WeakTypeWithExp &operator=(std::string_view ValueString) {
		this->_value = str::fromString<Type, true>(ValueString);
		_updateExpValue();
		return *this;
	};

	//-----------------------------
	// manipulate with WeakTypeWithExp
	// Returns WeakTypeWithExp
	//-----------------------------
	// multiplication always works
	constexpr WeakTypeWithExp &operator*=(WeakTypeWithExp other) noexcept {
		this->_value *= other._value;
		_updateExpValue();
		return *this;
	};

	// division always works
	constexpr WeakTypeWithExp &operator/=(WeakTypeWithExp other) noexcept {
		this->_value /= other._value;
		_updateExpValue();
		return *this;
	};

	// additions always works
	constexpr WeakTypeWithExp &operator+=(WeakTypeWithExp other) noexcept {
		this->_value += other._value;
		_updateExpValue();
		return *this;
	};

	// subtractions always works
	constexpr WeakTypeWithExp &operator-=(WeakTypeWithExp other) noexcept {
		this->_value -= other._value;
		_updateExpValue();
		return *this;
	};

	friend double exp(WeakTypeWithExp Obj) noexcept { return Obj._expValue; }
};

//------------------------------------------------
// WeakTypeWithLogAndExp
// Weak type that keeps track of its logarithm and exponential
//------------------------------------------------

template<class Type>
class WeakTypeWithLogAndExp : public WeakTypeWithInterval<Type, WeakTypeWithLogAndExp<Type>, intervals::Positive> {
private:
	using Base = WeakTypeWithInterval<Type, WeakTypeWithLogAndExp<Type>, intervals::Positive>;
	// Constructor without check, only use internally
	WeakTypeWithLogAndExp(tags::NoCheck, Type Value) : Base(tags::NoCheck{}, Value) { _updateLogAndExpValue(); };

	double _logValue;
	double _expValue;

	void _updateLogAndExpValue() noexcept {
		_logValue = log(this->_value);
		_expValue = exp(this->_value);
	}

public:
	// constructors
	WeakTypeWithLogAndExp()
	    : WeakTypeWithInterval<Type, WeakTypeWithLogAndExp<Type>, intervals::Positive>(tags::NoCheck{}, 0.0) {
		_updateLogAndExpValue();
	};

	constexpr WeakTypeWithLogAndExp(Type Value)
	    : WeakTypeWithInterval<Type, WeakTypeWithLogAndExp<Type>, intervals::Positive>(Value) { // not explicit
		_updateLogAndExpValue();
	};
	constexpr explicit WeakTypeWithLogAndExp(tags::NoCheck, std::string_view Value)
	    : WeakTypeWithInterval<Type, WeakTypeWithLogAndExp<Type>, intervals::Positive>(tags::NoCheck{}, Value) {
		_updateLogAndExpValue();
	};
	constexpr explicit WeakTypeWithLogAndExp(std::string_view Value)
	    : WeakTypeWithInterval<Type, WeakTypeWithLogAndExp<Type>, intervals::Positive>(Value) {
		_updateLogAndExpValue();
	};

	// assign
	constexpr WeakTypeWithLogAndExp &operator=(Type Val) {
		this->_set(Val);
		_updateLogAndExpValue();
		return *this;
	};

	WeakTypeWithLogAndExp &operator=(std::string_view ValueString) {
		this->_set(str::fromString<Type, true>(ValueString));
		_updateLogAndExpValue();
		return *this;
	};

	//-----------------------------
	// manipulate with WeakTypeWithLogAndExp
	// Returns WeakTypeWithLogAndExp
	//-----------------------------
	// multiplication always works
	[[nodiscard]] constexpr WeakTypeWithLogAndExp operator*(WeakTypeWithLogAndExp other) const noexcept {
		return WeakTypeWithLogAndExp(tags::NoCheck{}, this->_value * other._value);
	};

	constexpr WeakTypeWithLogAndExp &operator*=(WeakTypeWithLogAndExp other) noexcept {
		this->_value *= other._value;
		_updateLogAndExpValue();
		return *this;
	};

	// division always works
	[[nodiscard]] constexpr WeakTypeWithLogAndExp operator/(WeakTypeWithLogAndExp other) const noexcept {
		return WeakTypeWithLogAndExp(tags::NoCheck{}, this->_value / other._value);
	};

	constexpr WeakTypeWithLogAndExp &operator/=(WeakTypeWithLogAndExp other) noexcept {
		this->_value /= other._value;
		_updateLogAndExpValue();
		return *this;
	};

	// additions always works
	[[nodiscard]] constexpr WeakTypeWithLogAndExp operator+(WeakTypeWithLogAndExp other) const noexcept {
		return WeakTypeWithLogAndExp(tags::NoCheck{}, this->_value + other._value);
	};

	constexpr WeakTypeWithLogAndExp &operator+=(WeakTypeWithLogAndExp other) noexcept {
		this->_value += other._value;
		_updateLogAndExpValue();
		return *this;
	};

	// subtractions may or may not be permissible: check range!
	[[nodiscard]] constexpr WeakTypeWithLogAndExp operator-(WeakTypeWithLogAndExp other) const {
		// constructor will check range
		return WeakTypeWithLogAndExp(this->_value - other._value);
	};

	constexpr WeakTypeWithLogAndExp &operator-=(WeakTypeWithLogAndExp other) {
		*this = *this - other;
		return *this;
	};

	friend double log(WeakTypeWithLogAndExp Obj) noexcept { return Obj._logValue; }
	friend double exp(WeakTypeWithLogAndExp Obj) noexcept { return Obj._expValue; }
};

}; // namespace coretools

#endif // BANGOLIN_WEAKTYPESWITHLOGEXP_H

/*
 * simpleTypes.h
 *
 *  Created on: Jun 24, 2021
 *      Author: phaentu
 */

#ifndef CORETOOLS_CORE_WEAKTYPES_H_
#define CORETOOLS_CORE_WEAKTYPES_H_

#include <cassert>
#include <type_traits>

#include "coretools/Main/TError.h"
#include "coretools/Strings/fromString.h"
#include "coretools/traits.h"

namespace coretools {

//---------------------------------------------------------------------
// Weak types are wrappers around (fundamental) types:
//  - they are implicitly convertible to the underlying type
//  - they can be used just as the underlying type, but then decay to underlying types
//  - but they offer operators and functions specific to that type
//---------------------------------------------------------------------

//------------------------------------------------
// WeakType
// Simple wrapper
// No interval checks
//------------------------------------------------

template<typename Type> struct WeakTypeNumericLimitsInterval {
	static constexpr Type min            = std::numeric_limits<Type>::lowest();
	static constexpr Type max            = std::numeric_limits<Type>::max();
	inline static const std::string info = "[" + str::toString(min) + ", " + str::toString(max) + "]";
};

template<typename Type, typename Tag> class WeakTypeBase {
protected:
	Type _value = 0;

public:
	using value_type = Type;

	// constructors
	explicit constexpr WeakTypeBase() = default;
	constexpr WeakTypeBase(Type val) : _value(val){}; // not explicit
	explicit WeakTypeBase(tags::NoCheck, std::string_view ValueString)
	    : _value(str::fromString<Type, false>(ValueString)) {}
	explicit WeakTypeBase(std::string_view ValueString) : _value(str::fromString<Type, true>(ValueString)) {}

	// convert
	[[nodiscard]] constexpr operator Type() const noexcept { return _value; };
	[[nodiscard]] explicit operator std::string() const { return str::toString<Type>(_value); };
	[[nodiscard]] constexpr Type get() const noexcept { return _value; };

	//-----------------------------
	// manipulate with WeakType
	// Returns WeakType
	//-----------------------------
	[[nodiscard]] constexpr Tag operator*(Tag other) const noexcept { return Tag(_value * other._value); };
	[[nodiscard]] constexpr Tag operator/(Tag other) const noexcept { return Tag(_value / other._value); };
	[[nodiscard]] constexpr Tag operator+(Tag other) const noexcept { return Tag(_value + other._value); };
	[[nodiscard]] constexpr WeakTypeBase operator-(Tag other) const noexcept { return Tag(_value - other._value); };

	//---------------
	// calculate
	// these functions return the underlying type
	// need to write both left/right combinations out to prevent ambiguous overload errors
	//---------------

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator*(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First * (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator*(Tag First, Type Second) noexcept {
		// template specialization for multiplying with underlying type
		return (Type)First * Second;
	};

	[[nodiscard]] friend constexpr Type operator*(Type First, Tag Second) noexcept {
		// template specialization for multiplying with underlying type
		return First * (Type)Second;
	};

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator/(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First / (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator/(Tag First, Type Second) noexcept {
		// template specialization for dividing by underlying type
		return (Type)First / Second;
	};

	[[nodiscard]] friend constexpr Type operator/(Type First, Tag Second) noexcept {
		// template specialization for dividing by underlying type
		return First / (Type)Second;
	};

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator+(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First + (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator+(Tag First, Type Second) noexcept {
		// template specialization for adding underlying type
		return (Type)First + Second;
	};

	[[nodiscard]] friend constexpr Type operator+(Type First, Tag Second) noexcept {
		// template specialization for adding underlying type
		return First + (Type)Second;
	};

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator-(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First - (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator-(Tag First, Type Second) noexcept {
		// template specialization for subtracting underlying type
		return (Type)First - Second;
	};

	[[nodiscard]] friend constexpr Type operator-(Type First, Tag Second) noexcept {
		// template specialization for subtracting underlying type
		return First - (Type)Second;
	};

	//--------------------
	// stream
	//--------------------
	friend std::ostream &operator<<(std::ostream &os, Tag Other) {
		if constexpr (std::is_integral_v<Type> && !std::is_signed_v<Type>) {
			// unsigned integers: are printed as ASCII chars by default. Cast in order to print properly!
			os << static_cast<unsigned int>(Other.get());
		} else {
			os << Other.get();
		}
		return os;
	}

	//-----------------
	// range information
	// Needed for compatibility with WeakTypeWithInterval
	//-----------------
	static constexpr Tag min() noexcept { return Tag(std::numeric_limits<Type>::lowest()); };
	static constexpr Tag max() noexcept { return Tag(std::numeric_limits<Type>::max()); };
	static constexpr WeakTypeNumericLimitsInterval<Type> interval() noexcept {
		return WeakTypeNumericLimitsInterval<Type>();
	};

	void setMin(Type) { DEVERROR("Should never get here! Weak type does not have interval that can be changed."); }

	void setMax(Type) { DEVERROR("Should never get here! Weak type does not have interval that can be changed."); }
};



//------------------------------------------------
// WeakTypeWithInterval
// Weak type that is bounded
//------------------------------------------------

template<typename Type, class Tag, template<typename, typename> class Interval, class TagInterval = void>
// Tag and TagInterval are phantom types
// TagInterval is used as a Tag for the interval class. It must be defined for intervals where min/max can be changed
// Since most intervals have fixed min/max and do not require a Tag, TagInterval has a default (void)
class WeakTypeWithInterval {
protected:
	Type _value;
#ifdef CHECK_INTERVALS
	static constexpr bool _checkIntervals = true;
#else // CHECK_INTERVALS
	static constexpr bool _checkIntervals = false;
#endif

	constexpr void _ensureInterval() const noexcept(!_checkIntervals) {
		if constexpr (_checkIntervals) {
			if (!isInsideInterval()) {
				DEVERROR("value ", _value, " outside range ", Interval<Type, TagInterval>::info, "!");
			}
		} else {
			assert(isInsideInterval()); // always check in debug-mode
		}
	};

	constexpr void _set(Type Value) noexcept(!_checkIntervals) {
		_value = Value;
		_ensureInterval();
	};

	// keep constructors protected so the base class can not be used
	explicit constexpr WeakTypeWithInterval(tags::NoCheck, Type val) : _value(val){};
	explicit constexpr WeakTypeWithInterval(Type val) : _value(val) { _ensureInterval(); };
	explicit constexpr WeakTypeWithInterval(tags::NoCheck, std::string_view ValueString)
	    : _value(str::fromString<Type, false>(ValueString)) {
		// only check for intervals if CHECK_INTERVALS is true
		_ensureInterval();
	};
	explicit constexpr WeakTypeWithInterval(std::string_view ValueString)
	    : _value(str::fromString<Type, true>(ValueString)) {
		// always check for intervals when constructed from string -> this is at the interface with the user
		if (!isInsideInterval()) {
			UERROR("Value ", _value, " is outside range ", Interval<Type, TagInterval>::info, "!");
		}
	};

public:
	using value_type = Type;

	// convert
	[[nodiscard]] constexpr operator Type() const noexcept { return _value; }; // not explicit
	[[nodiscard]] explicit operator std::string() const { return str::toString<Type>(_value); };
	[[nodiscard]] constexpr Type get() const noexcept { return _value; };

	// compare
	constexpr bool operator==(WeakTypeWithInterval other) const noexcept { return _value == other._value; };
	constexpr bool operator!=(WeakTypeWithInterval other) const noexcept { return _value != other._value; };
	constexpr bool operator<(WeakTypeWithInterval other) const noexcept { return _value < other._value; };
	constexpr bool operator>(WeakTypeWithInterval other) const noexcept { return _value > other._value; };

	//---------------
	// calculate with underlying type
	// these functions return the underlying type, no interval checks happen here
	// need to write both left/right combinations out to prevent ambiguous overload errors
	//---------------
	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator*(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First * (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator*(Tag First, Type Second) noexcept {
		// template specialization for multiplying with underlying type
		return (Type)First * Second;
	};

	[[nodiscard]] friend constexpr Type operator*(Type First, Tag Second) noexcept {
		// template specialization for multiplying with underlying type
		return First * (Type)Second;
	};

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator/(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First / (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator/(Tag First, Type Second) noexcept {
		// template specialization for dividing by underlying type
		return (Type)First / Second;
	};

	[[nodiscard]] friend constexpr Type operator/(Type First, Tag Second) noexcept {
		// template specialization for dividing by underlying type
		return First / (Type)Second;
	};

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator+(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First + (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator+(Tag First, Type Second) noexcept {
		// template specialization for adding underlying type
		return (Type)First + Second;
	};

	[[nodiscard]] friend constexpr Type operator+(Type First, Tag Second) noexcept {
		// template specialization for adding underlying type
		return First + (Type)Second;
	};

	template<typename OtherType, typename = std::enable_if_t<isCastable_v<OtherType, Type>>>
	[[nodiscard]] friend constexpr Type operator-(Tag First, OtherType Second) noexcept {
		// general template
		return (Type)First - (Type)Second;
	};

	[[nodiscard]] friend constexpr Type operator-(Tag First, Type Second) noexcept {
		// template specialization for subtracting underlying type
		return (Type)First - Second;
	};

	[[nodiscard]] friend constexpr Type operator-(Type First, Tag Second) noexcept {
		// template specialization for subtracting underlying type
		return First - (Type)Second;
	};

	// stream
	friend std::ostream &operator<<(std::ostream &os, Tag Other) {
		if constexpr (std::is_integral_v<Type> && !std::is_signed_v<Type>) {
			// unsigned integers: are printed as ASCII chars by default. Cast in order to print properly!
			os << static_cast<unsigned int>(Other.get());
		} else {
			os << Other.get();
		}
		return os;
	}

	// interval information
	constexpr bool isInsideInterval() const noexcept {
		// if compiled without CHECK_INTERVALS, the interval check is off -> in critical cases, this function can be
		// used to check for valid values
		return _value >= Interval<Type, TagInterval>::min && _value <= Interval<Type, TagInterval>::max;
	}

	static constexpr Tag min() noexcept { return Tag(Interval<Type, TagInterval>::min); };
	static constexpr Tag max() noexcept { return Tag(Interval<Type, TagInterval>::max); };
	static constexpr Interval<Type, TagInterval> interval() noexcept { return Interval<Type, TagInterval>(); };

	// change interval
	static constexpr void setMin(Type Min) noexcept { Interval<Type, TagInterval>::min = Min; }

	void setMinAndCheck(Type Min) const {
		// only execute if min of Interval is non-const
		if constexpr (!std::is_const_v<decltype(Interval<Type, TagInterval>::min)>) {
			Interval<Type, TagInterval>::min = Min;
			_ensureInterval();
		} else
			DEVERROR("Should never get here! Min of interval is const.");
	}

	static constexpr void setMax(Type Max) noexcept { Interval<Type, TagInterval>::max = Max; }

	void setMaxAndCheck(Type Max) const {
		// only execute if max of Interval is non-const
		if constexpr (!std::is_const_v<decltype(Interval<Type, TagInterval>::max)>) {
			Interval<Type, TagInterval>::max = Max;
			_ensureInterval();
		} else
			DEVERROR("Should never get here! Max of interval is const.");
	}
};

}; // end namespace coretools

#endif /* CORETOOLS_CORE_WEAKTYPES_H_ */

#include "gtest/gtest.h"

#include "coretools/Math/mathFunctions.h"

using namespace testing;
using namespace coretools;

TEST(mathFunctionsTests, uPow) {
	static_assert(uPow<0>(1.32) == 1.);
	static_assert(uPow<1>(1.32) == 1.32);
	static_assert(uPow<2>(2.) == 2. * 2.);

	EXPECT_EQ(uPow(3.14, 0), 1.);
	EXPECT_EQ(uPow(3.14, 1), 3.14);
	EXPECT_EQ(uPow(77.7, 2), 77.7 * 77.7);
	EXPECT_EQ(uPow(1.23, 3), 1.23 * 1.23 * 1.23);
	EXPECT_FLOAT_EQ(uPow(3.3, 4), 3.3 * 3.3 * 3.3 * 3.3);

	double r = 1.;
	for (size_t _ = 0; _ < 17; ++_) r *= 1.2;
	EXPECT_FLOAT_EQ(uPow(1.2, 17), r);
}

// log to log10 and back

TEST(mathFunctionsTests, logToLog10) {
	std::vector<double> vec = {0.00001, 0.55, 1.05, 9.967, 655456};
	for (auto &x : vec) {
		double natural = log(x);
		double ten     = log10(x);
		EXPECT_FLOAT_EQ(natural, log10ToLog(ten));
		EXPECT_FLOAT_EQ(ten, logToLog10(natural));
	}
}

// gammaln

TEST(mathFunctionsTests, gammaLog) {
	// gammaln is known to be unprecise around values of 1-2
	// quoting: gives an accuracy of about
	// 10-12 significant decimal digits except for small regions around X = 1 and
	// X = 2, where the function goes to zero.
	EXPECT_NEAR(gammaLog(1.), 0., 10e-15);
	EXPECT_NEAR(gammaLog(2.), 0., 10e-15);
	EXPECT_FLOAT_EQ(gammaLog(3.), 0.6931472);
	EXPECT_FLOAT_EQ(gammaLog(100.), 359.1342);
}

// betaLog

TEST(mathFunctionsTests, betaLog) {
	// both small
	EXPECT_FLOAT_EQ(betaLog(1e-10, 1e-10), 23.71899811);
	EXPECT_FLOAT_EQ(betaLog(0.2, 0.7), 1.718554829);
	EXPECT_NEAR(betaLog(1.0, 1.0), 0.0, 10e-15);
	EXPECT_FLOAT_EQ(betaLog(2.0, 2.0), -1.791759469);

	// a is large
	EXPECT_FLOAT_EQ(betaLog(12.0, 2.0), -5.049856007);
	EXPECT_FLOAT_EQ(betaLog(2e20, 2.0), -93.48969808);

	// b is large
	EXPECT_FLOAT_EQ(betaLog(2.0, 12.0), -5.049856007);
	EXPECT_FLOAT_EQ(betaLog(2.0, 2e20), -93.48969808);

	// a and b are large
	EXPECT_FLOAT_EQ(betaLog(12.0, 12.0), -16.60205988);
	EXPECT_FLOAT_EQ(betaLog(2e10, 2e10), -27725887233.0);
}

// diffGammaLog

TEST(mathFunctionsTests, diffGammaLog) {
	// check against difference of log-Beta-functions where one argument is the same in R to prevent numeric issues:
	// e.g. for first test: lbeta(1e-10, 1e-10) - lbeta(1e-11, 1e-10)
	// both small
	EXPECT_FLOAT_EQ(diffGammaLog(1e-10, 1e-10) - diffGammaLog(1e-11, 1e-10), -1.704748092);
	EXPECT_FLOAT_EQ(diffGammaLog(0.2, 0.7) - diffGammaLog(0.21, 0.7), 0.04416376716);
	EXPECT_FLOAT_EQ(diffGammaLog(1.0, 1.0) - diffGammaLog(1.001, 1.), 0.0009995003331);
	EXPECT_FLOAT_EQ(diffGammaLog(2.0, 2.0) - diffGammaLog(5.0, 2.0), 1.609437912);

	// a is large
	EXPECT_FLOAT_EQ(diffGammaLog(12.0, 2.0) - diffGammaLog(15.0, 2.0), 0.4307829161);
	EXPECT_FLOAT_EQ(diffGammaLog(2e20, 2.0) - diffGammaLog(2e19, 2.0), -4.605170186);

	// b is large
	EXPECT_FLOAT_EQ(diffGammaLog(2.0, 12.0) - diffGammaLog(2.0, 15.0), 8.119696);
	EXPECT_FLOAT_EQ(diffGammaLog(2.0, 2e20) - diffGammaLog(2.0, 2e19), -8.280125e+21);

	// a and b are large
	EXPECT_FLOAT_EQ(diffGammaLog(12.0, 12.0) - diffGammaLog(15.0, 12.0), 1.966112856);
	EXPECT_FLOAT_EQ(diffGammaLog(2e10, 2e10) - diffGammaLog(2e11, 2e10), 39294054195.0);
}

// digamma

TEST(mathFunctionsTests, digamma) {
	// tested against R digamma() function

	// negative x (only small values give reasonable results)
	EXPECT_FLOAT_EQ(TDigamma::digamma(-0.5), 0.03648998);

	// x between 0 and 6
	EXPECT_FLOAT_EQ(TDigamma::digamma(0.0000001), -1e+07);
	EXPECT_FLOAT_EQ(TDigamma::digamma(1.), -0.5772157);
	EXPECT_FLOAT_EQ(TDigamma::digamma(2.), 0.4227843);
	EXPECT_FLOAT_EQ(TDigamma::digamma(3.), 0.9227843);
	EXPECT_FLOAT_EQ(TDigamma::digamma(4.), 1.256118);
	EXPECT_FLOAT_EQ(TDigamma::digamma(5.), 1.506118);
	EXPECT_FLOAT_EQ(TDigamma::digamma(6.), 1.706118);

	// very large x
	EXPECT_FLOAT_EQ(TDigamma::digamma(100000000), 18.42068);
}

// dirichlet distribution

/*
TEST(mathFunctionsTests, ddirichletThrow) {
    // invalid: alpha <= 0
    EXPECT_ANY_THROW(probdist::TDirichletDistr::density({0.5, 0.25, 0.25}, {0.1, 0.2, 0.}));
    EXPECT_ANY_THROW(probdist::TDirichletDistr::density({0.5, 0.25, 0.25}, {0.1, 0.2, -0.3}));
    // invalid: x < 0
    EXPECT_ANY_THROW(probdist::TDirichletDistr::density({-0.5, 0.5, 0.5}, {0.1, 0.2, 0.3}));
    // invalid: x > 1
    EXPECT_ANY_THROW(probdist::TDirichletDistr::density({1.5, 0.5, 0.5}, {0.1, 0.2, 0.3}));
    // invalid: x does not sum to 1
#ifdef _DEBUG
    EXPECT_DEATH(probdist::TDirichletDistr::density({0.5, 0.5, 0.5}, {0.1, 0.2, 0.3}), "");
    // invalid: size of x and alpha doesn't match
    EXPECT_DEATH(probdist::TDirichletDistr::density({0.5, 0.5}, {0.1, 0.2, 0.3}), "");
#endif
    // invalid: x == 0
    EXPECT_ANY_THROW(probdist::TDirichletDistr::density({0., 0.5, 0.5}, {0.1, 0.2, 0.3}));
    // invalid: x == 1
    EXPECT_ANY_THROW(probdist::TDirichletDistr::density({1.}, {0.1}));
}

TEST(mathFunctionsTests, ddirichlet) {
    std::vector<ZeroOneOpen> x = {
        0.181818181818182,  0.163636363636364,  0.145454545454545,  0.127272727272727,  0.109090909090909,
        0.0909090909090909, 0.0727272727272727, 0.0545454545454545, 0.0363636363636364, 0.0181818181818182};
    std::vector<StrictlyPositive> alpha = {0.1,
                                                   0.188888888888889,
                                                   0.277777777777778,
                                                   0.366666666666667,
                                                   0.455555555555556,
                                                   0.544444444444445,
                                                   0.633333333333333,
                                                   0.722222222222222,
                                                   0.811111111111111,
                                                   0.9};
    EXPECT_FLOAT_EQ(TDirichletDistr::density(x, alpha), 458.0038);
    EXPECT_FLOAT_EQ(TDirichletDistr::logDensity(x, alpha), 6.126878);

    x     = {0.139495918701045,  0.143278688150917,  0.0490180575439956, 0.0479300393857425, 0.127627647333716,
         0.0912743516047731, 0.0711770008483544, 0.01044347693951,   0.169024252259974,  0.150730567231972};
    alpha = {0.715417139410271,  1.18555226917834,  0.70766649614947,  2.02720824248138,    0.0529360190437087,
             0.0316477520391345, 0.200610389001667, 0.694285509256988, 0.00192753784358501, 0.339209412224591};
    EXPECT_FLOAT_EQ(TDirichletDistr::density(x, alpha), 0.09994159);
    EXPECT_FLOAT_EQ(TDirichletDistr::logDensity(x, alpha), -2.303169);
}

// dirichlet distribution - reusable

TEST(mathFunctionsTests, ddirichletReUsableThrow) {
    // invalid: alpha <= 0
    EXPECT_ANY_THROW(TDirichletDistrReUsable dirichlet({0.1, 0.2, 0.}));
    EXPECT_ANY_THROW(TDirichletDistrReUsable dirichlet({0.1, 0.2, -0.3}));

    // this is all what we can test over the constructor - now test if x is checked inside density function
    TDirichletDistrReUsable dirichlet({0.1, 0.2, 0.3});
    // invalid: x < 0
    EXPECT_ANY_THROW(dirichlet.density({-0.5, 0.5, 0.5}));
    // invalid: x > 1
    EXPECT_ANY_THROW(dirichlet.density({1.5, 0.5, 0.5}));
    // invalid: x does not sum to 1

#ifdef _DEBUG
    EXPECT_DEATH(dirichlet.density({0.5, 0.5, 0.5}), "");
    // invalid: size of x and alpha doesn't match
    EXPECT_DEATH(dirichlet.density({0.3, 0.7}), "");
#endif
    // invalid: x == 0
    EXPECT_ANY_THROW(dirichlet.density({0., 0.5, 0.5}));
    // invalid: x == 1
    TDirichletDistrReUsable dirichlet2({0.1});
    EXPECT_ANY_THROW(dirichlet2.density({1.}));
}

TEST(mathFunctionsTests, ddirichletReUsable) {
    std::vector<StrictlyPositive> alpha = {0.1,
                                                   0.188888888888889,
                                                   0.277777777777778,
                                                   0.366666666666667,
                                                   0.455555555555556,
                                                   0.544444444444445,
                                                   0.633333333333333,
                                                   0.722222222222222,
                                                   0.811111111111111,
                                                   0.9};
    TDirichletDistrReUsable dirichlet(alpha);

    std::vector<ZeroOneOpen> x = {
        0.181818181818182,  0.163636363636364,  0.145454545454545,  0.127272727272727,  0.109090909090909,
        0.0909090909090909, 0.0727272727272727, 0.0545454545454545, 0.0363636363636364, 0.0181818181818182};
    EXPECT_FLOAT_EQ(dirichlet.density(x), 458.0038);
    EXPECT_FLOAT_EQ(dirichlet.logDensity(x), 6.126878);

    // other values -> reset
    alpha = {0.715417139410271,  1.18555226917834,  0.70766649614947,  2.02720824248138,    0.0529360190437087,
             0.0316477520391345, 0.200610389001667, 0.694285509256988, 0.00192753784358501, 0.339209412224591};
    dirichlet.resetParameters(alpha);
    x = {0.139495918701045,  0.143278688150917,  0.0490180575439956, 0.0479300393857425, 0.127627647333716,
         0.0912743516047731, 0.0711770008483544, 0.01044347693951,   0.169024252259974,  0.150730567231972};
    EXPECT_FLOAT_EQ(dirichlet.density(x), 0.09994159);
    EXPECT_FLOAT_EQ(dirichlet.logDensity(x), -2.303169);
}

// gamma distribution

TEST(mathFunctionsTests, dgammaThrow) {
    // invalid x
    EXPECT_ANY_THROW(TGammaDistr::density(-1., 0.1, 0.1));
    EXPECT_ANY_THROW(TGammaDistr::density(0., 0.1, 0.1));
    // invalid alpha
    EXPECT_ANY_THROW(TGammaDistr::density(0.5, -1., 0.1));
    EXPECT_ANY_THROW(TGammaDistr::density(0.5, 0., 0.1));
    // invalid beta
    EXPECT_ANY_THROW(TGammaDistr::density(0.5, 0.1, -1.));
    EXPECT_ANY_THROW(TGammaDistr::density(0.5, 0.1, 0.));
}

TEST(mathFunctionsTests, dgammaNoThrow) {
    // valid x
    EXPECT_NO_THROW(TGammaDistr::density(0.1, 0.1, 0.1));
    EXPECT_NO_THROW(TGammaDistr::density(2., 0.1, 0.1));
    // valid alpha and beta
    EXPECT_NO_THROW(TGammaDistr::density(0.5, 2., 0.1));
    EXPECT_NO_THROW(TGammaDistr::density(0.5, 2., 100.));
}

TEST(mathFunctionsTests, pgammaThrow) {
    // invalid x
    EXPECT_ANY_THROW(TGammaDistr::cumulativeDistrFunction(-1., 0.1, 0.1));
    EXPECT_ANY_THROW(TGammaDistr::cumulativeDistrFunction(0., 0.1, 0.1));
    // invalid alpha
    EXPECT_ANY_THROW(TGammaDistr::cumulativeDistrFunction(0.5, -1., 0.1));
    EXPECT_ANY_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 0., 0.1));
    // invalid beta
    EXPECT_ANY_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 0.1, -1.));
    EXPECT_ANY_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 0.1, 0.));
}

TEST(mathFunctionsTests, pgammaNoThrow) {
    // valid x
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(0.1, 0.1, 0.1));
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(2., 0.1, 0.1));
    // valid alpha and beta
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 2., 0.1));
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 2., 100.));
}

TEST(mathFunctionsTests, dgamma) {
    EXPECT_FLOAT_EQ(TGammaDistr::density(0.1, 0.7, 0.7), 1.116541);
    EXPECT_FLOAT_EQ(TGammaDistr::density(0.00000001, 0.1, 2.6), 1832975);
    EXPECT_FLOAT_EQ(TGammaDistr::density(2.354, 0.1, 1), 0.004620663);
}

TEST(mathFunctionsTests, dgammaLog) {
    EXPECT_FLOAT_EQ(TGammaDistr::logDensity(0.1, 0.7, 0.7), 0.1102358);
    EXPECT_FLOAT_EQ(TGammaDistr::logDensity(0.00000001, 0.1, 2.6), 14.42145);
    EXPECT_FLOAT_EQ(TGammaDistr::logDensity(2.354, 0.1, 1), -5.377217);
}

TEST(mathFunctionsTests, pgamma) {
    EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDistrFunction(0.1, 0.7, 0.7), 0.1662473);
    EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDistrFunction(0.00000001, 0.1, 2.6), 0.1832975);
    EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDistrFunction(2.354, 0.1, 1), 0.996445);
}

// generalized pareto distribution

TEST(mathFunctionsTests, dparetoThrow) {
    // invalid x
    EXPECT_ANY_THROW(TParetoDistr::logDensity(-1., 0., 0.1, 0.1));
    EXPECT_ANY_THROW(TParetoDistr::logDensity(3., 0., 1., -1.));
    // invalid sd
    EXPECT_ANY_THROW(TParetoDistr::logDensity(1., 0., -1., 0.1));

    // not in log
    EXPECT_ANY_THROW(TParetoDistr::logDensity(1., 0., -1., 0.1));
}

TEST(mathFunctionsTests, dparetoNoThrow) {
    // valid x
    EXPECT_NO_THROW(TParetoDistr::logDensity(0., 0., 0.1, 0.1));
    EXPECT_NO_THROW(TParetoDistr::logDensity(3., 0., 1., 1.));
}

TEST(mathFunctionsTests, pparetoThrow) {
    // invalid x
    EXPECT_ANY_THROW(TParetoDistr::cumulativeDistrFunction(-1., 0., 0.1, 0.1));
    EXPECT_ANY_THROW(TParetoDistr::cumulativeDistrFunction(3., 0., 1., -1.));
    // invalid sd
    EXPECT_ANY_THROW(TParetoDistr::cumulativeDistrFunction(1., 0., -1., 0.1));
}

TEST(mathFunctionsTests, pparetoNoThrow) {
    // valid x
    EXPECT_NO_THROW(TParetoDistr::cumulativeDistrFunction(0., 0., 0.1, 0.1));
    EXPECT_NO_THROW(TParetoDistr::cumulativeDistrFunction(3., 0., 1., 1.));
}

TEST(mathFunctionsTests, dpareto) {
    EXPECT_FLOAT_EQ(TParetoDistr::logDensity(0.1, 0., 1., 0.), -0.1);
    EXPECT_FLOAT_EQ(TParetoDistr::logDensity(0.9, -1, 0.4, 1.2), -2.570906);
    EXPECT_FLOAT_EQ(TParetoDistr::logDensity(-9, -10, 11, 0.2), -2.506006);
}

TEST(mathFunctionsTests, ppareto) {
    EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDistrFunction(0.1, 0., 1., 0.), 0.09516258);
    EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDistrFunction(0.9, -1, 0.4, 1.2), 0.7950706);
    EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDistrFunction(-9, -10, 11, 0.2), 0.08615337);
}
*/

// incomplete gamma functions

TEST(mathFunctionsTests, lowerIncompleteGamma) {
	// compared to: Rgamma(x, a, b, lower = T) from R package zipfR (our implementation is the regularized incomplete
	// gamma function!)
	EXPECT_FLOAT_EQ(TIncompleteGamma::lower(1.8, 0.1), 0.008867713);
	EXPECT_FLOAT_EQ(TIncompleteGamma::lower(0.7, 1.92), 0.9166503);
}

TEST(mathFunctionsTests, upperIncompleteGamma) {
	// compared to: Rbeta(x, a, b, lower = F) from R package zipfR (our implementation is the regularized incomplete
	// beta function!)
	EXPECT_FLOAT_EQ(TIncompleteGamma::upper(1.8, 0.1), 0.9911323);
	EXPECT_FLOAT_EQ(TIncompleteGamma::upper(0.7, 1.92), 0.08334971);
}

// factorials

TEST(mathFunctionsTests, factorialThrow) {
	EXPECT_ANY_THROW(TFactorial::factorial(-1));
	EXPECT_ANY_THROW(TFactorial::factorial(171));
}

TEST(mathFunctionsTests, factorial) {
	EXPECT_EQ(TFactorial::factorial(0), 1);
	EXPECT_EQ(TFactorial::factorial(1), 1);
	EXPECT_EQ(TFactorial::factorial(2), 2);
	EXPECT_EQ(TFactorial::factorial(3), 6);
	EXPECT_EQ(TFactorial::factorial(20), 2432902008176640000);
}

TEST(mathFunctionsTests, factorialLogThrow) { EXPECT_ANY_THROW(TFactorial::factorialLog(-1)); }

TEST(mathFunctionsTests, factorialLog) {
	EXPECT_NEAR(TFactorial::factorialLog(0), 0., 10e-15);
	EXPECT_NEAR(TFactorial::factorialLog(1), 0., 10e-15);
	EXPECT_FLOAT_EQ(TFactorial::factorialLog(2), 0.6931472);
	EXPECT_FLOAT_EQ(TFactorial::factorialLog(3), 1.791759);
	EXPECT_FLOAT_EQ(TFactorial::factorialLog(170), 706.5731);
}

// falling factorials

TEST(mathFunctionsTests, fallingFactorial) {
	// n = 0
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(0, 0), 1.);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(1, 0), 1.);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(100, 0), 1.);

	// n = 1
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(0, 1), 0.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(1, 1), 1.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(2, 1), 2.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(100, 1), 100.0);

	// n = 5
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(0, 5), 0.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(4, 5), 0.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(5, 5), 120.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(8, 5), 6720);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(15, 5), 360360);

	// n = 10
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(0, 10), 0.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(9, 10), 0.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(10, 10), 3628800.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(11, 10), 39916800.0);
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorial(20, 10), 670442572800.0);
}

TEST(mathFunctionsTests, fallingFactorialLog) {
	// n = 0
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(0, 0), log(1.));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(1, 0), log(1.));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(100, 0), log(1.));

	// n = 1
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(0, 1), std::numeric_limits<double>::lowest());
	EXPECT_NEAR(TFallingFactorial::fallingFactorialLog(1, 1), log(1.0), 10e-15); // gammaLog not exactly 0
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(2, 1), log(2.0));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(100, 1), log(100.0));

	// n = 5
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(0, 5), std::numeric_limits<double>::lowest());
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(4, 5), std::numeric_limits<double>::lowest());
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(5, 5), log(120.0));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(8, 5), log(6720));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(15, 5), log(360360));

	// n = 10
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(0, 10), std::numeric_limits<double>::lowest());
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(9, 10), std::numeric_limits<double>::lowest());
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(10, 10), log(3628800.0));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(11, 10), log(39916800.0));
	EXPECT_FLOAT_EQ(TFallingFactorial::fallingFactorialLog(20, 10), log(670442572800.0));
}

// choose
TEST(mathFunctionsTests, chooseLog) {
	EXPECT_NEAR(chooseLog(0, 0), 0., 10e-15);
	EXPECT_NEAR(chooseLog(1, 0), 0., 10e-15);
	EXPECT_FLOAT_EQ(chooseLog(170, 27), 71.9278);
	EXPECT_FLOAT_EQ(chooseLog(89, 2), 8.272826);
}

TEST(mathFunctionsTests, choose) {
	EXPECT_NEAR(choose(0, 0), 1, 10e-15);
	EXPECT_EQ(choose(1, 0), 1);
	EXPECT_EQ(choose(17, 10), 19448);
	EXPECT_EQ(choose(60, 2), 1770);
}

// binomial p value

TEST(mathFunctionsTests, binomPValueClass) {
	EXPECT_NEAR(TBinomPValue::binomPValue(2, 10), 0.05469, 10e-2);
	EXPECT_NEAR(TBinomPValue::binomPValue(2, 20), 0.0002012, 10e-2);
	EXPECT_NEAR(TBinomPValue::binomPValue(53, 100), 0.7579, 10e-2);
}

// incomplete beta function

TEST(mathFunctionsTests, incompleteBetaThrow) {
	// invalid x
	EXPECT_ANY_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, P(-1.)));
	EXPECT_ANY_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, P(2.)));
	// invalid alpha
	EXPECT_ANY_THROW(TIncompleteBeta::incompleteBeta(-1., 0.1, P(0.5)));
	EXPECT_ANY_THROW(TIncompleteBeta::incompleteBeta(0., 0.1, P(0.5)));
	// invalid beta
	EXPECT_ANY_THROW(TIncompleteBeta::incompleteBeta(0.1, 0., P(0.5)));
	EXPECT_ANY_THROW(TIncompleteBeta::incompleteBeta(0.1, -1., P(0.5)));
}

TEST(mathFunctionsTests, incompleteBetaNoThrow) {
	// valid x
	EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, P(0.)));
	EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, P(0.5)));
	EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, P(1.)));
	// valid alpha and beta
	EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 2., P(0.5)));
	EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(2., 0.1, P(0.5)));
}

TEST(mathFunctionsTests, incompleteBeta) {
	// compared to: Rbeta(x, a, b) from R package zipfR (our implementation is the regularized incomplete beta
	// function!)
	EXPECT_FLOAT_EQ(TIncompleteBeta::incompleteBeta(0.5, 0.5, P(0.1)), 0.2048328);
	EXPECT_FLOAT_EQ(TIncompleteBeta::incompleteBeta(5., 0.5, P(0.8)), 0.1449276);
}

TEST(mathFunctionsTests, inverseIncompleteBeta) {
	// compared to: Rbeta.inv(x, a, b) from R package zipfR
	EXPECT_FLOAT_EQ(TIncompleteBeta::inverseIncompleteBeta(P(0.1), 0.5, 0.5), 0.02447174);
	EXPECT_FLOAT_EQ(TIncompleteBeta::inverseIncompleteBeta(P(0.8), 5., 0.5), 0.9932759);
}

TEST(mathFunctionsTests, logit) {
	// 0
	auto prob         = Probability(0.);
	double logit_prob = logit(prob);
	EXPECT_FLOAT_EQ(prob.get(), logistic(logit_prob).get());

	// something in between
	prob       = Probability(0.74983746);
	logit_prob = logit(prob);
	EXPECT_FLOAT_EQ(prob.get(), logistic(logit_prob).get());
}

//------------------------------------------------
// types and numeric limits
//------------------------------------------------

// addition
TEST(mathFunctionsTests, checkForNumericOverflow_addition_double) {
	// overflow
	double limit = std::numeric_limits<double>::max();
	double nextSmallerValue =
	    std::nextafter(limit, 0.); // double precision for very large values is different than for small values -> this
	                               // gives us the next-smaller value below the limit
	double dist = limit - nextSmallerValue; // distance between second largest and largest number

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, std::nextafter(dist, 0.)));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, dist));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, 2. * dist));

	// underflow
	limit            = std::numeric_limits<double>::lowest();
	nextSmallerValue = std::nextafter(limit, 0.);
	dist             = nextSmallerValue - limit; // distance between second smallest and smallest number

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, -std::nextafter(dist, 0.)));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, -dist));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, -2. * dist));
}

TEST(mathFunctionsTests, checkForNumericOverflow_addition_int) {
	// overflow
	int limit            = std::numeric_limits<int>::max();
	int nextSmallerValue = limit - 1;

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue - 1, 1));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, 1));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, 2));

	// underflow
	limit            = std::numeric_limits<int>::lowest();
	nextSmallerValue = limit + 1;

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue + 1, -1));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, -1));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, -2));
}

TEST(mathFunctionsTests, checkForNumericOverflow_addition_uint8) {
	// overflow
	uint8_t limit            = std::numeric_limits<uint8_t>::max();
	uint8_t nextSmallerValue = limit - 1;

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue - 1, 1));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, 1));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, 2));

	// underflow
	limit            = std::numeric_limits<uint8_t>::lowest();
	nextSmallerValue = limit + 1;

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue + 1, -1));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, -1));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, -2));
}

// subtraction
TEST(mathFunctionsTests, checkForNumericOverflow_subtraction_double) {
	// overflow
	double limit = std::numeric_limits<double>::max();
	double nextSmallerValue =
	    std::nextafter(limit, 0.); // double precision for very large values is different than for small values -> this
	                               // gives us the next-smaller value below the limit
	double dist = limit - nextSmallerValue; // distance between second largest and largest number

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, -std::nextafter(dist, 0.)));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, -dist));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, -2. * dist));

	// underflow
	limit            = std::numeric_limits<double>::lowest();
	nextSmallerValue = std::nextafter(limit, 0.);
	dist             = nextSmallerValue - limit; // distance between second smallest and smallest number

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, std::nextafter(dist, 0.)));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, dist));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, 2. * dist));
}

TEST(mathFunctionsTests, checkForNumericOverflow_subtraction_int) {
	// overflow
	int limit            = std::numeric_limits<int>::max();
	int nextSmallerValue = limit - 1;

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue - 1, -1));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, -1));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, -2));

	// underflow
	limit            = std::numeric_limits<int>::lowest();
	nextSmallerValue = limit + 1;

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue + 1, 1));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, 1));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, 2));
}

TEST(mathFunctionsTests, checkForNumericOverflow_subtraction_uint8) {
	// overflow
	uint8_t limit            = std::numeric_limits<uint8_t>::max();
	uint8_t nextSmallerValue = limit - 1;

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue - 1, -1));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, -1));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, -2));

	// underflow
	limit            = std::numeric_limits<uint8_t>::lowest();
	nextSmallerValue = limit + 1;

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue + 1, 1));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, 1));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, 2));
}

// multiplication
TEST(mathFunctionsTests, checkForNumericOverflow_multiplication_double) {
	// overflow
	double limit = std::numeric_limits<double>::max();
	double nextSmallerValue =
	    std::nextafter(limit, 0.); // double precision for very large values is different than for small values -> this
	                               // gives us the next-smaller value below the limit

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_multiplication(1., nextSmallerValue));
	EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., -nextSmallerValue));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_multiplication(1., limit));
	EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., -limit));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_multiplication(1., 2. * (limit + nextSmallerValue)));
	EXPECT_FALSE(checkForNumericOverflow_multiplication(-1., -2. * (limit + nextSmallerValue)));

	// underflow
	limit            = std::numeric_limits<double>::lowest();
	nextSmallerValue = std::nextafter(limit, 0.);

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_multiplication(1., -nextSmallerValue));
	EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., nextSmallerValue));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_multiplication(1., limit));
	EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., limit));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_multiplication(-1., 2. * (limit + nextSmallerValue)));
	EXPECT_FALSE(checkForNumericOverflow_multiplication(1., -2. * (limit + nextSmallerValue)));
}

TEST(mathFunctionsTests, checkForNumericOverflow_multiplication_int) {
	// overflow
	int limit            = std::numeric_limits<int>::max();
	int nextSmallerValue = limit - 1;

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, nextSmallerValue));
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-1, -nextSmallerValue));
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-nextSmallerValue, -1));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, limit));
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-1, -limit));
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-limit, -1));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(3, limit));

	// underflow
	limit            = std::numeric_limits<int>::lowest();
	nextSmallerValue = limit + 1;

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, -nextSmallerValue));
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-1, nextSmallerValue)); // = max
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(nextSmallerValue, -1)); // = max
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, limit));
	// jump across min
	EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(-1, limit)); // = abs(min) -> bigger than max
	EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(limit, -1)); // = abs(min) -> bigger than max
	EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(3, limit));
}

TEST(mathFunctionsTests, checkForNumericOverflow_multiplication_uint8) {
	// overflow
	uint8_t limit            = std::numeric_limits<uint8_t>::max();
	uint8_t nextSmallerValue = limit - 1;

	// jump below max
	EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, nextSmallerValue));
	// jump exactly on max
	EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, limit));
	// jump across max
	EXPECT_FALSE(checkForNumericOverflow_multiplication<uint8_t>(2, limit));

	// underflow -> won't happen, because second argument can't be negative -> will never be < 0
	limit            = std::numeric_limits<uint8_t>::lowest();
	nextSmallerValue = limit + 1;

	// jump above min
	EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, nextSmallerValue));
	// jump exactly on min
	EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, 0));
	// jump across min -> won't happen, because second argument can't be negative -> will never be < 0
}

#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Math/TAcceptOddsRation.h"
#include "coretools/Math/mathFunctions.h"

using namespace coretools;

constexpr uint64_t numPoints = 1000000;
TEST(TAcceptOddsRatio, acceptRandomSampling) {
	size_t sum = 0;
	auto seed  = coretools::instances::randomGenerator().getSeed();
	coretools::instances::randomGenerator().setSeed((long)seed, true);
	for (size_t r = 0; r < numPoints; ++r) {
		const double logitQ = coretools::logit(coretools::instances::randomGenerator().getRand());
		sum += TAcceptOddsRatio::accept(logitQ);
	}

	size_t sum_naive = 0;
	coretools::instances::randomGenerator().setSeed(seed, true);
	for (size_t r = 0; r < numPoints; ++r) {
		const double logitQ = logit(coretools::instances::randomGenerator().getRand());
		const double logitX = logit(coretools::instances::randomGenerator().getRand());
		sum_naive += (logitX <= logitQ);
	}

	EXPECT_EQ(sum, sum_naive);
}

#include "gtest/gtest.h"

#include "coretools/Math/TMeanVarVector.h"

using namespace coretools;

//--------------------------------------------
// TMeanVarVector
//--------------------------------------------

TEST(TMeanVarVectorTest, all) {
	TMeanVarVector<double> meanVarVec;

	// add
	meanVarVec.add(0, 1.);
	meanVarVec.add(0, 2.);
	meanVarVec.add(0, 0.83);
	meanVarVec.add(2, -102.9);
	meanVarVec.add(2, 0.);
	meanVarVec.add(2, 100.293);

	// size()
	EXPECT_EQ(meanVarVec.size(), 3);
	// sum()
	EXPECT_FLOAT_EQ(meanVarVec.sum(), 1.223);
	// exists()
	EXPECT_EQ(meanVarVec.exists(0), true);
	EXPECT_EQ(meanVarVec.exists(1), true);
	EXPECT_EQ(meanVarVec.exists(2), true);
	EXPECT_EQ(meanVarVec.exists(3), false);

	// []
	EXPECT_FLOAT_EQ(meanVarVec[0].mean(), 1.276667);
	EXPECT_FLOAT_EQ(meanVarVec[1].mean(), 0);
	EXPECT_FLOAT_EQ(meanVarVec[2].mean(), -0.869);
	EXPECT_ANY_THROW(meanVarVec[3]);

	// clear
	meanVarVec.clear();
	EXPECT_EQ(meanVarVec.exists(0), false);
	EXPECT_EQ(meanVarVec.exists(1), false);
	EXPECT_EQ(meanVarVec.exists(2), false);
}

// just test for uint8_t, as for other types, it takes ages to reach min/max if we can only add sqrt(limit) per entry in
// the vector
TEST(TMeanVarVectorTest, uint8_t_overflow) {
	TMeanVarVector<uint8_t> meanVarVec;

	// add
	uint8_t limit = std::numeric_limits<uint8_t>::max();
	for (uint8_t i = 0; i < 17; i++) {
		meanVarVec.add(i, sqrt(limit)); // sqrt(limit) = 15 -> 15*17 = 255 = numeric_max
	}

	// sum is exactly numeric max -> ok
	EXPECT_NO_THROW(meanVarVec.sum());

	// add more -> sum is above numeric max -> throw
	meanVarVec.add(17, sqrt(limit));
	EXPECT_ANY_THROW(meanVarVec.sum());
}

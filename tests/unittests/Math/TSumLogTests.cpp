//
// Created by caduffm on 5/23/22.
//
#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Math/TSumLog.h"

using namespace coretools;

constexpr size_t numRep = 10000;

TEST(TSumLogTests, binWidth1) {
	// if bin width = 1: should give exactly the same results as with std::log
	TSumLog<1> sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_EQ(sumLog.getSum(), sum);
}

TEST(TSumLogTests, binWidth2) {
	TSumLog<2> sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(TSumLogTests, binWidth3) {
	// numRep is not a multiple of 3
	TSumLog<3> sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(TSumLogTests, binWidth119) {
	// numRep is not a multiple of 119
	TSumLog<119> sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(TSumLogTests, binWidth1000) {
	TSumLog<1000> sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(getSumOfLogTests, binnned) {
	std::vector<double> vals(numRep);
	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		vals[i]  = r;
		sum += log(r);
	}

	double s1    = getSumOfLog<1>(vals);
	double s2    = getSumOfLog<2>(vals);
	double s3    = getSumOfLog<3>(vals);
	double s119  = getSumOfLog<119>(vals);
	double s1000 = getSumOfLog<1000>(vals);

	EXPECT_EQ(s1, sum);
	EXPECT_FLOAT_EQ(s2, sum);
	EXPECT_FLOAT_EQ(s3, sum);
	EXPECT_FLOAT_EQ(s119, sum);
	EXPECT_FLOAT_EQ(s1000, sum);
}

TEST(TSumLogTests, getExponentOfDouble) {
	for (int exponent = -1022; exponent < 1023; exponent++) {
		for (size_t i = 0; i < numRep; i++) {
			const double frac = 1. + (double)i / numRep;
			const double val  = frac * std::pow(2, exponent);
			EXPECT_EQ(impl::getExponentOfDouble(val), exponent);
		}
	}
}

TEST(TSumLogTests, dynamic_extremes) {
	TSumLog<> sumLog;

	double sum = 0.0;
	for (int exponent = -1022; exponent < 1023; exponent++) {
		for (size_t i = 0; i < 1000; i++) {
			const double frac = 1. + (double)i / numRep;
			const double val  = frac * std::pow(2, exponent);
			sumLog.add(val);
			sum += log(val);
		}
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(TSumLogTests, dynamic_random) {
	TSumLog<> sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(TSumLogProbabilityTests, random_probability) {
	TSumLogProbability sumLog;

	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		while (r <= 0.0 || r > 1.0) { r = instances::randomGenerator().getExponentialRandom(1); }
		sumLog.add(r);
		sum += log(r);
	}

	EXPECT_FLOAT_EQ(sumLog.getSum(), sum);
}

TEST(getSumOfLogTests, dynamic_extremes) {
	std::vector<double> vals;
	vals.reserve(2045000);
	double sum = 0.0;
	for (int exponent = -1022; exponent < 1023; exponent++) {
		for (size_t i = 0; i < 1000; i++) {
			const double frac = 1. + (double)i / numRep;
			const double val  = frac * std::pow(2, exponent);
			vals.push_back(val);
			sum += log(val);
		}
	}

	double sumLog = getSumOfLog(vals);

	EXPECT_FLOAT_EQ(sumLog, sum);
}

TEST(getSumOfLogTests, dynamic_random) {
	std::array<double, numRep> vals{};
	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		sum += log(r);
		vals[i] = r;
	}

	double sumLog = getSumOfLog(vals);
	EXPECT_FLOAT_EQ(sumLog, sum);
}

TEST(getSumOfLogProbabilityTests, getSumOfLogProbability) {
	std::array<double, numRep> vals{};
	double sum = 0;
	for (size_t i = 0; i < numRep; i++) {
		double r = instances::randomGenerator().getExponentialRandom(1);
		while (r <= 0.0 || r > 1.0) { r = instances::randomGenerator().getExponentialRandom(1); }
		sum += log(r);
		vals[i] = r;
	}

	double sumLog = getSumOfLogProbability(vals);
	EXPECT_FLOAT_EQ(sumLog, sum);
}

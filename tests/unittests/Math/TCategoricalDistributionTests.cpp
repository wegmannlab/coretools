#include <memory>
#include "gtest/gtest.h"

#include "coretools/Math/TCategoricalDistribution.h"

using namespace coretools;
using namespace probdist;

TEST(TCategoricalDistribution, ValueStorage) {
	std::unique_ptr<TValueStorage_base<double>> base = std::make_unique<TValueStorageFixed<double>>(10.); 
	EXPECT_EQ(base->size(), 1);
	EXPECT_EQ(base->min(), 10.);
	EXPECT_EQ(base->max(), 10.);

	TValueStorageDispersed<double> dispersed({0,34,77});
	TValueStorageRange<double> range(0., 10.);
	// TODO...
}

TEST(TCategoricalDistribution, Distribution) {
	TCategoricalDistribution<uint16_t> dist("exponential(0.5)[1,20]");
	const auto s = dist.sample();
	EXPECT_GE(s, 1);
	EXPECT_LE(s, 20);
}

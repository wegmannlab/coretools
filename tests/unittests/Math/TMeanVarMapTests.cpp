#include "gtest/gtest.h"
#include "coretools/Math/TMeanVarMap.h"

using namespace coretools;

TEST(TMeanVarMapTest, all) {
	TMeanVarMap<uint64_t, double> meanVarMap;

	// add
	meanVarMap.add(0, 1.);
	meanVarMap.add(0, 2.);
	meanVarMap.add(0, 0.83);
	meanVarMap.add(2, -102.9);
	meanVarMap.add(2, 0.);
	meanVarMap.add(2, 100.293);

	// size()
	EXPECT_EQ(meanVarMap.size(), 2);
	// sum()
	EXPECT_FLOAT_EQ(meanVarMap.sum(), 1.223);
	// exists()
	EXPECT_EQ(meanVarMap.exists(0), true);
	EXPECT_EQ(meanVarMap.exists(1), false);
	EXPECT_EQ(meanVarMap.exists(2), true);
	EXPECT_EQ(meanVarMap.exists(3), false);

	// []
	EXPECT_FLOAT_EQ(meanVarMap[0].mean(), 1.276667);
	EXPECT_FLOAT_EQ(meanVarMap[2].mean(), -0.869);
	EXPECT_ANY_THROW(meanVarMap[1]);
	EXPECT_ANY_THROW(meanVarMap[3]);

	// clear
	meanVarMap.clear();
	EXPECT_EQ(meanVarMap.exists(0), false);
	EXPECT_EQ(meanVarMap.exists(1), false);
	EXPECT_EQ(meanVarMap.exists(2), false);
}

// just test for uint8_t, as for other types, it takes ages to reach min/max if we can only add sqrt(limit) per entry in
// the vector
TEST(TMeanVarMapTest, uint8_t_overflow) {
	TMeanVarMap<uint64_t, uint8_t> meanVarVec;

	// add
	uint8_t limit = std::numeric_limits<uint8_t>::max();
	for (uint8_t i = 0; i < 17; i++) {
		meanVarVec.add(i, sqrt(limit)); // sqrt(limit) = 15 -> 15*17 = 255 = numeric_max
	}

	// sum is exactly numeric max -> ok
	EXPECT_NO_THROW(meanVarVec.sum());

	// add more -> sum is above numeric max -> throw
	meanVarVec.add(17, sqrt(limit));
	EXPECT_ANY_THROW(meanVarVec.sum());
}

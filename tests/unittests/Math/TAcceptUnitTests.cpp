#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Math/TAccept.h"

using namespace testing;
using namespace coretools;

constexpr uint64_t numPoints = 1000000;
TEST(TAccept, acceptRandomSampling) {

	size_t sum = 0;
	auto seed  = coretools::instances::randomGenerator().getSeed();
	coretools::instances::randomGenerator().setSeed((long)seed, true);
	for (size_t r = 0; r < numPoints; ++r) {
		const double logQ = log(coretools::instances::randomGenerator().getRand());
		sum += TAccept::accept(logQ);
	}

	size_t sum_naive = 0;
	coretools::instances::randomGenerator().setSeed((long)seed, true);
	for (size_t r = 0; r < numPoints; ++r) {
		const double logQ = log(coretools::instances::randomGenerator().getRand());
		const double logX = log(coretools::instances::randomGenerator().getRand());
		sum_naive += (logX < logQ);
	}

	EXPECT_EQ(sum, sum_naive);
}


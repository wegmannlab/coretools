/*
 * TRangeUnitTests.cpp
 *
 *  Created on: May 14, 2021
 *      Author: Daniel Wegmann
 */
#include "gtest/gtest.h"

#include "coretools/Main/TParameters.h"
#include "coretools/Math/TNumericRange.h"

using namespace coretools;

//------------------------------------------------
// Default constructor
//------------------------------------------------
TEST(TRangeTests, defaultConstructor) {
	// unit8_t
	TNumericRange<uint8_t> u8;
	EXPECT_TRUE(u8.within(4));
	EXPECT_TRUE(u8.within(std::numeric_limits<uint8_t>::max()));
	EXPECT_TRUE(u8.within(std::numeric_limits<uint8_t>::lowest()));

	// double
	TNumericRange<double> d;
	EXPECT_TRUE(d.within(4.0));
	EXPECT_TRUE(d.within(std::numeric_limits<double>::max()));
	EXPECT_TRUE(d.within(std::numeric_limits<double>::lowest()));
};

//------------------------------------------------
// range constructor
//------------------------------------------------

TEST(TRangeTests, rangeConstructorWithinWithin) {
	// unit8_t
	TNumericRange<uint8_t> u8(10, true, 100, true);
	EXPECT_TRUE(u8.within(10));
	EXPECT_TRUE(u8.within(40));
	EXPECT_TRUE(u8.within(100));

	EXPECT_FALSE(u8.within(4));
	EXPECT_FALSE(u8.within(104));

	// double
	TNumericRange<double> d(-9.99, true, 100.01, true);
	EXPECT_TRUE(d.within(-9.99));
	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));
	EXPECT_TRUE(d.within(100.01));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(100.011));
};

TEST(TRangeTests, rangeConstructorWithinOutside) {
	// unit8_t
	TNumericRange<uint8_t> u8(10, true, 100, false);
	EXPECT_TRUE(u8.within(10));
	EXPECT_TRUE(u8.within(40));

	EXPECT_FALSE(u8.within(9));
	EXPECT_FALSE(u8.within(100));
	EXPECT_FALSE(u8.within(101));

	EXPECT_TRUE(u8.larger(100));
	EXPECT_TRUE(u8.smaller(9));
	EXPECT_FALSE(u8.larger(9));
	EXPECT_FALSE(u8.smaller(10));

	// double
	TNumericRange<double> d(-9.99, true, 100.01, false);
	EXPECT_TRUE(d.within(-9.99));
	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(100.01));
	EXPECT_FALSE(d.within(100.011));

	EXPECT_TRUE(d.larger(100.01));
	EXPECT_TRUE(d.smaller(-10.));
	EXPECT_FALSE(d.larger(100.0));
	EXPECT_FALSE(d.smaller(-9.99));
};

TEST(TRangeTests, rangeConstructorOutsideWithin) {
	// unit8_t
	TNumericRange<uint8_t> u8(10, false, 100, true);
	EXPECT_TRUE(u8.within(40));
	EXPECT_TRUE(u8.within(100));

	EXPECT_FALSE(u8.within(4));
	EXPECT_FALSE(u8.within(10));
	EXPECT_FALSE(u8.within(104));

	// double
	TNumericRange<double> d(-9.99, false, 100.01, true);
	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));
	EXPECT_TRUE(d.within(100.01));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(-9.99));
	EXPECT_FALSE(d.within(100.011));

	EXPECT_TRUE(d.larger(100.02));
	EXPECT_TRUE(d.smaller(-9.99));
	EXPECT_FALSE(d.larger(100.01));
	EXPECT_FALSE(d.smaller(-9.989));
};

TEST(TRangeTests, rangeConstructorOutsideOutside) {
	// unit8_t
	TNumericRange<uint8_t> u8(10, false, 100, false);
	EXPECT_TRUE(u8.within(40));

	EXPECT_FALSE(u8.within(4));
	EXPECT_FALSE(u8.within(10));
	EXPECT_FALSE(u8.within(100));
	EXPECT_FALSE(u8.within(104));

	// double
	TNumericRange<double> d(-9.99, false, 100.01, false);
	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(-9.990));
	EXPECT_FALSE(d.within(100.01));
	EXPECT_FALSE(d.within(100.011));
};

//------------------------------------------------
// string constructor
//------------------------------------------------

TEST(TRangeTests, stringConstructorWithinWithin) {
	// unit8_t
	TNumericRange<uint8_t> u8("[10,100]");
	EXPECT_TRUE(u8.minIncluded());
	EXPECT_TRUE(u8.maxIncluded());
	EXPECT_EQ(u8.min(), 10);
	EXPECT_EQ(u8.max(), 100);

	EXPECT_TRUE(u8.within(10));
	EXPECT_TRUE(u8.within(40));
	EXPECT_TRUE(u8.within(100));

	EXPECT_FALSE(u8.within(9));
	EXPECT_FALSE(u8.within(101));

	// double
	TNumericRange<double> d("[-9.99,100.01]");
	EXPECT_TRUE(d.minIncluded());
	EXPECT_TRUE(d.maxIncluded());
	EXPECT_DOUBLE_EQ(d.min(), -9.99);
	EXPECT_DOUBLE_EQ(d.max(), 100.01);

	EXPECT_TRUE(d.within(-9.99));
	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));
	EXPECT_TRUE(d.within(100.01));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(100.011));
};

TEST(TRangeTests, stringConstructorWithinOutside) {
	// unit8_t
	TNumericRange<uint8_t> u8("[10,100)");
	EXPECT_TRUE(u8.minIncluded());
	EXPECT_FALSE(u8.maxIncluded());

	EXPECT_TRUE(u8.within(10));
	EXPECT_TRUE(u8.within(40));

	EXPECT_FALSE(u8.within(9));
	EXPECT_FALSE(u8.within(100));
	EXPECT_FALSE(u8.within(101));

	// double
	TNumericRange<double> d("[-9.99,100.01)");
	EXPECT_TRUE(d.minIncluded());
	EXPECT_FALSE(d.maxIncluded());

	EXPECT_TRUE(d.within(-9.99));
	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(100.01));
	EXPECT_FALSE(d.within(100.011));
};

TEST(TRangeTests, stringConstructorOutsideWithin) {
	// unit8_t
	TNumericRange<uint8_t> u8("(10 ,100]");
	EXPECT_FALSE(u8.minIncluded());
	EXPECT_TRUE(u8.maxIncluded());

	EXPECT_TRUE(u8.within(40));
	EXPECT_TRUE(u8.within(100));

	EXPECT_FALSE(u8.within(4));
	EXPECT_FALSE(u8.within(10));
	EXPECT_FALSE(u8.within(104));

	// double
	TNumericRange<double> d("(-9.99, 100.01]");
	EXPECT_FALSE(d.minIncluded());
	EXPECT_TRUE(d.maxIncluded());

	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));
	EXPECT_TRUE(d.within(100.01));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(-9.99));
	EXPECT_FALSE(d.within(100.011));
};

TEST(TRangeTests, stringConstructorOutsideOutside) {
	// unit8_t
	TNumericRange<uint8_t> u8(" (10,100)");
	EXPECT_FALSE(u8.minIncluded());
	EXPECT_FALSE(u8.maxIncluded());

	EXPECT_TRUE(u8.within(40));

	EXPECT_FALSE(u8.within(4));
	EXPECT_FALSE(u8.within(10));
	EXPECT_FALSE(u8.within(100));
	EXPECT_FALSE(u8.within(104));

	// double
	TNumericRange<double> d("(-9.99,100.01) ");
	EXPECT_FALSE(d.minIncluded());
	EXPECT_FALSE(d.maxIncluded());

	EXPECT_TRUE(d.within(-4.0));
	EXPECT_TRUE(d.within(40.0));

	EXPECT_FALSE(d.within(-9.991));
	EXPECT_FALSE(d.within(-9.990));
	EXPECT_FALSE(d.within(100.01));
	EXPECT_FALSE(d.within(100.011));
};

TEST(TRangeTests, stringConstructorOmissions) {
	// unit8_t
	TNumericRange<uint8_t> u8("10,100)");
	EXPECT_TRUE(u8.minIncluded());
	EXPECT_FALSE(u8.maxIncluded());

	TNumericRange<uint16_t> u16("(10,");
	EXPECT_FALSE(u16.minIncluded());
	EXPECT_TRUE(u16.maxIncluded());
	EXPECT_TRUE(u16.within(65335));

	// double
	TNumericRange<double> d(" [ , 100.01) ");
	EXPECT_TRUE(d.minIncluded());
	EXPECT_FALSE(d.maxIncluded());

	EXPECT_TRUE(d.within(-10000.0));
	EXPECT_FALSE(d.within(100.01));
};

TEST(TRangeTests, stringConstructorFail) {
	EXPECT_ANY_THROW(TNumericRange<double>("10;100"));
	EXPECT_ANY_THROW(TNumericRange<double>("4,10,100"));
	EXPECT_ANY_THROW(TNumericRange<double>("10,100X"));
	EXPECT_ANY_THROW(TNumericRange<double>("(10;100)"));
	EXPECT_ANY_THROW(TNumericRange<double>("(10F0,10R)"));
};

TEST(TRangeTests, constuctfromParameters) {
	TParameters params;
	params.add("range", "[10, 100]");

	TNumericRange<uint8_t> u8 = params.get<TNumericRange<uint8_t>>("range");

	EXPECT_TRUE(u8.minIncluded());
	EXPECT_TRUE(u8.maxIncluded());
	EXPECT_EQ(u8.min(), 10);
	EXPECT_EQ(u8.max(), 100);

	EXPECT_TRUE(u8.within(10));
	EXPECT_TRUE(u8.within(40));
	EXPECT_TRUE(u8.within(100));

	EXPECT_FALSE(u8.within(9));
	EXPECT_FALSE(u8.within(101));
};

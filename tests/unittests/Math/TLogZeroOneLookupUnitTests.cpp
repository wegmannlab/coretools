//
// Created by caduffm on 12/6/21.
//

#include "gtest/gtest.h"
#include "coretools/Math/TLogZeroOneLookup.h"

using namespace testing;
using namespace coretools;

TEST(TLogZeroOneLookupTest, approxLog) {
	TLogZeroOneLookup logZeroOneLookup;

	uint64_t numPoints = 1000000;
	double start       = 10e-10;
	double end         = 1.0;
	double step        = (end - start) / (double)numPoints;
	for (uint64_t i = 0; i < numPoints; i++) {
		double x = start + (double)i * step;
		EXPECT_NEAR(log(x), logZeroOneLookup.approxLog01(x), 7.5e-05);
	}
	EXPECT_EQ(log(1.0), logZeroOneLookup.approxLog01(1.0));
}
#include "gtest/gtest.h"

#include "coretools/Math/TMatrix.h"

using namespace coretools;

void fillComplicatedBandMat(TBandMatrix<double> &mat, double addTo = 0) {
	mat(0, 0) = 0.1 + addTo;
	mat(0, 1) = 0.2 + addTo;
	mat(0, 2) = 0;
	mat(0, 3) = 0;
	mat(1, 0) = 0.3 + addTo;
	mat(1, 1) = 0.4 + addTo;
	mat(1, 2) = 0.5 + addTo;
	mat(1, 3) = 0;
	mat(2, 0) = 0;
	mat(2, 1) = 0.6 + addTo;
	mat(2, 2) = 0.7 + addTo;
	mat(2, 3) = 0.8 + addTo;
	mat(3, 0) = 0;
	mat(3, 1) = 0;
	mat(3, 2) = 0.9 + addTo;
	mat(3, 3) = 1.0 + addTo;
}

//-------------------------------------------
// TMatrix
//-------------------------------------------

TEST(TMatrixTests, constructor_noArgs) {
	TMatrix<double> mat;

	EXPECT_EQ(mat.rows(), 0);
	EXPECT_EQ(mat.cols(), 0);
}

TEST(TMatrixTests, constructor_size) {
	TMatrix<double> mat(2);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TMatrixTests, constructor_size_initVal) {
	TMatrix<double> mat(2, 0.1);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.1); }
	}
}

TEST(TMatrixTests, constructor_rows_cols) {
	TMatrix<double> mat(2, 3);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TMatrixTests, constructor_rows_cols_initVal) {
	TMatrix<double> mat(2, 3, 0.1);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.1); }
	}
}

TEST(TMatrixTests, copyConstructor) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(mat);

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.1); }
	}
}

TEST(TMatrixTests, copyConstructor_scale) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(mat, 5.);

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.5); }
	}
}

TEST(TMatrixTests, moveAssignment) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(4, 5, 0.5);

	mat2 = std::move(mat);

	EXPECT_EQ(mat.rows(), 0);
	EXPECT_EQ(mat.cols(), 0);
	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.1); }
	}
}

TEST(TMatrixTests, move_swap) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(4, 5, 0.5);

	std::swap(mat, mat2);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 5);
	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.5); }
	}
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.1); }
	}
}

TEST(TMatrixTests, resize_size) {
	// not initialized
	TMatrix<double> mat;
	mat.resize(2);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TMatrixTests, resize_size_value) {
	// not initialized
	TMatrix<double> mat;
	mat.resize(2, 0.1);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.1); }
	}
}

TEST(TMatrixTests, resize_rows_cols) {
	// not initialized
	TMatrix<double> mat;
	mat.resize(2, 3);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}

	// change rows
	mat.resize(3, 3);
	EXPECT_EQ(mat.rows(), 3);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}

	// change cols
	mat.resize(3, 2);
	EXPECT_EQ(mat.rows(), 3);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TMatrixTests, resize_rows_cols_value) {
	// not initialized
	TMatrix<double> mat;
	mat.resize(2, 3, 0.1);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.1); }
	}

	// change rows
	mat.resize(3, 3, 0.2);
	EXPECT_EQ(mat.rows(), 3);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.2); }
	}

	// change cols
	mat.resize(3, 2, 0.3);
	EXPECT_EQ(mat.rows(), 3);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.3); }
	}
}

TEST(TMatrixTests, set) {
	TMatrix<double> mat(2, 3, 0.1);

	mat.set(0.89);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.89); }
	}
}

TEST(TMatrixTests, operator_equal) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(4, 6, 0.2);

	mat2 = mat;

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.1); }
	}
}

TEST(TMatrixTests, operator_plus) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(2, 3, 0.2);

	TMatrix<double> mat3 = mat + mat2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.3); }
	}

	mat3 = mat2 + mat;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.3); }
	}
}

TEST(TMatrixTests, operator_plus_itself) {
	TMatrix<double> mat(2, 3, 0.1);

	TMatrix<double> mat3 = mat + mat;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.2); }
	}
}

TEST(TMatrixTests, operator_plus_throw) {
	TMatrix<double> mat1(2, 3, 0.1);

	TMatrix<double> mat2(1, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TMatrix<double> mat = mat1 + mat2, "");
#endif
	TMatrix<double> mat3(2, 4, 0.2);

#ifdef _DEBUG
	EXPECT_DEATH(TMatrix<double> mat = mat1 + mat3, "");
#endif
}

TEST(TMatrixTests, operator_plusEqual) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(2, 3, 0.2);

	mat2 += mat;

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.3); }
	}
}

TEST(TMatrixTests, operator_plusEqual_itself) {
	TMatrix<double> mat(2, 3, 0.1);

	mat += mat;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.2); }
	}
}

TEST(TMatrixTests, operator_plusEqual_throw) {
	TMatrix<double> mat1(2, 3, 0.1);
	TMatrix<double> mat2(1, 3, 0.2);

#ifdef _DEBUG
	EXPECT_DEATH(mat2 += mat1, "");
#endif
	TMatrix<double> mat3(2, 4, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat3 += mat1, "");
#endif
}

TEST(TMatrixTests, operator_minus) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(2, 3, 0.2);

	TMatrix<double> mat3 = mat - mat2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), -0.1); }
	}
}

TEST(TMatrixTests, operator_minus_itself) {
	TMatrix<double> mat(2, 3, 0.1);

	TMatrix<double> mat3 = mat - mat;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.); }
	}
}

TEST(TMatrixTests, operator_minus_throw) {
	TMatrix<double> mat1(2, 3, 0.1);

	TMatrix<double> mat2(1, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TMatrix<double> mat = mat1 - mat2, "");
#endif
	TMatrix<double> mat3(2, 4, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TMatrix<double> mat = mat1 - mat3, "");
#endif
}

TEST(TMatrixTests, operator_minusEqual) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(2, 3, 0.2);

	mat2 -= mat;

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.1); }
	}
}

TEST(TMatrixTests, operator_minusEqual_itself) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(mat);

	mat -= mat2;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TMatrixTests, operator_minusEqual_throw) {
	TMatrix<double> mat1(2, 3, 0.1);

	TMatrix<double> mat2(1, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat1 -= mat2, "");
#endif
	TMatrix<double> mat3(2, 4, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat1 -= mat3, "");
#endif
}

TEST(TMatrixTests, operator_multiplication) {
	// fill with some values
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}
	TMatrix<double> mat2(3, 2, 0.2);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) {
			mat2(i, j) = val;
			val += 0.05;
		}
	}

	TMatrix<double> mat3 = mat * mat2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 2);
	std::vector<double> vals = {0.235, 0.2575, 0.460, 0.5050};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, operator_multiplication_itself) {
	// fill with some values
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	TMatrix<double> mat3 = mat * mat.transpose();

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 2);
	std::vector<double> vals = {0.0725, 0.140, 0.1400, 0.275};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, operator_multiplication_throw) {
	TMatrix<double> mat1(2, 3, 0.1);

	TMatrix<double> mat2(1, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TMatrix<double> mat = mat1 * mat2, "");
#endif
	TMatrix<double> mat3(4, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TMatrix<double> mat = mat3 * mat1, "");
#endif
}

TEST(TMatrixTests, operator_multiplicationEqual) {
	// fill with some values
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}
	TMatrix<double> mat2(3, 2, 0.2);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) {
			mat2(i, j) = val;
			val += 0.05;
		}
	}

	mat *= mat2;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	std::vector<double> vals = {0.235, 0.2575, 0.460, 0.5050};
	int k                    = 0;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			EXPECT_FLOAT_EQ(mat(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, operator_multiplicationEqual_itself) {
	// fill with some values
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	mat *= mat.transpose();

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	std::vector<double> vals = {0.0725, 0.140, 0.1400, 0.275};
	int k                    = 0;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			EXPECT_FLOAT_EQ(mat(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, operator_multiplicationEqual_throw) {
	TMatrix<double> mat1(2, 3, 0.1);

	TMatrix<double> mat2(1, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat1 *= mat2, "");
#endif
	TMatrix<double> mat3(4, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat1 *= mat3, "");
#endif
}

TEST(TMatrixTests, operator_transpose) {
	TMatrix<double> mat(2, 3, 0.);
	// fill with some values
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	TMatrix<double> mat2 = mat.transpose();

	EXPECT_EQ(mat2.rows(), 3);
	EXPECT_EQ(mat2.cols(), 2);
	val = 0.1;
	for (unsigned int j = 0; j < mat2.cols(); j++) {
		for (unsigned int i = 0; i < mat2.rows(); i++) {
			EXPECT_FLOAT_EQ(mat2(i, j), val);
			val += 0.05;
		}
	}
}

TEST(TMatrixTests, fillRowsFromVector) {
	TMatrix<double> mat(2, 8, 0.1);
	std::vector<double> values = {0.5, 0.8, 0.3};

	mat.fillRowsFromVector(values, 4);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), values[j]); }
	}
}

TEST(TMatrixTests, fillFromMatrix) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(4, 6, 0.2);

	mat2.fillFromMatrix(mat);

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.1); }
	}
}

TEST(TMatrixTests, fillFromMatrix_scale) {
	TMatrix<double> mat(2, 3, 0.1);
	TMatrix<double> mat2(4, 6, 0.2);

	mat2.fillFromMatrix(mat, 5.1);

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 3);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) { EXPECT_FLOAT_EQ(mat2(i, j), 0.51); }
	}
}

TEST(TMatrixTests, fillFromProduct) {
	// fill with some values
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}
	TMatrix<double> mat2(3, 2, 0.2);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) {
			mat2(i, j) = val;
			val += 0.05;
		}
	}

	TMatrix<double> mat3;
	mat3.fillFromProduct(mat, mat2);

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 2);
	std::vector<double> vals = {0.235, 0.2575, 0.460, 0.5050};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, fillFromProduct_throw) {
	TMatrix<double> mat1(2, 3, 0.1);

	TMatrix<double> mat2(1, 3, 0.2);
	TMatrix<double> mat;
#ifdef _DEBUG
	EXPECT_DEATH(mat.fillFromProduct(mat1, mat2), "");
#endif
	TMatrix<double> mat3(4, 3, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat.fillFromProduct(mat3, mat1), "");
#endif
}

TEST(TMatrixTests, fillFromSquare) {
	// fill with some values
	TMatrix<double> mat(2);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	TMatrix<double> mat3;
	mat3.fillFromSquare(mat);

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 2);
	std::vector<double> vals = {0.04, 0.0525, 0.07, 0.0925};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, fillFromMatrix_bandmatrix) {
	TBandMatrix<double> mat(4, 1, 0.89);
	TMatrix<double> mat2(2, 6, 0.2);

	mat2.fillFromMatrix(mat);

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2(0, 0), 0.89);
	EXPECT_EQ(mat2(0, 1), 0.89);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.89);
	EXPECT_EQ(mat2(1, 1), 0.89);
	EXPECT_EQ(mat2(1, 2), 0.89);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.89);
	EXPECT_EQ(mat2(2, 2), 0.89);
	EXPECT_EQ(mat2(2, 3), 0.89);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.89);
	EXPECT_EQ(mat2(3, 3), 0.89);
}

TEST(TMatrixTests, fillFromMatrix_bandmatrix_scale) {
	TBandMatrix<double> mat(4, 1, 0.1);
	TMatrix<double> mat2(2, 6, 0.2);

	mat2.fillFromMatrix(mat, 8.9);

	EXPECT_FLOAT_EQ(mat2.rows(), 4);
	EXPECT_FLOAT_EQ(mat2.cols(), 4);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.89);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.89);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.89);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.89);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.89);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.89);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.89);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.89);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.89);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.89);
}

TEST(TMatrixTests, dumba) {
	TBandMatrix<double> mat(5, 1, 0.0);

	mat(0, 0) = 1;
	mat(0, 1) = 2;
	mat(1, 0) = 3;
	mat(1, 1) = 4;
	mat(1, 2) = 5;
	mat(2, 1) = 6;
	mat(2, 2) = 7;
	mat(2, 3) = 8;
	mat(3, 2) = 9;
	mat(3, 3) = 10;
	mat(3, 4) = 11;
	mat(4, 3) = 12;
	mat(4, 4) = 13;

	TMatrix<double> mat2(5, 5, 0.0);
	mat2.fillFromMatrix(mat, 1.0);
}

TEST(TMatrixTests, fillFromProduct_bandmatrix) {
	// fill with some values
	TBandMatrix<double> mat(4, 1);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat2(4, 1);
	fillComplicatedBandMat(mat2, 0.5);

	TMatrix<double> mat3(2, 9, 0.1);
	mat3.fillFromProduct(mat, mat2);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	std::vector<double> vals = {0.22, 0.25, 0.20, 0.00, 0.50, 1.12, 1.00, 0.65,
	                            0.48, 1.31, 2.56, 2.11, 0.00, 0.99, 2.48, 2.67};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, fillFromProduct_bandmatrix_throw) {
	TBandMatrix<double> mat1(4, 1);
	TBandMatrix<double> mat2(5, 1);

	TMatrix<double> mat;
	EXPECT_ANY_THROW(mat.fillFromProduct(mat1, mat2));
	TBandMatrix<double> mat3(3, 1);
	EXPECT_ANY_THROW(mat.fillFromProduct(mat3, mat1));
}

TEST(TMatrixTests, fillFromSquare_bandmatrix) {
	TBandMatrix<double> mat(4, 1);
	fillComplicatedBandMat(mat);

	TMatrix<double> mat3(2, 9, 0.1);
	mat3.fillFromSquare(mat);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	std::vector<double> vals = {0.07, 0.10, 0.10, 0.00, 0.15, 0.52, 0.55, 0.40,
	                            0.18, 0.66, 1.51, 1.36, 0.00, 0.54, 1.53, 1.72};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TMatrixTests, operator_plus_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	TMatrix<double> mat3 = mat + 0.2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.3); }
	}
}

TEST(TMatrixTests, operator_plusEqual_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	mat += 0.2;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.3); }
	}
}

TEST(TMatrixTests, operator_minus_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	TMatrix<double> mat3 = mat - 0.2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), -0.1); }
	}
}

TEST(TMatrixTests, operator_minusEqual_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	mat -= 0.2;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), -0.1); }
	}
}

TEST(TMatrixTests, operator_multiplication_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	TMatrix<double> mat3 = mat * 0.2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.02); }
	}
}

TEST(TMatrixTests, operator_multiplicationEqual_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	mat *= 0.2;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.02); }
	}
}

TEST(TMatrixTests, operator_division_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	TMatrix<double> mat3 = mat / 0.2;

	EXPECT_EQ(mat3.rows(), 2);
	EXPECT_EQ(mat3.cols(), 3);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.5); }
	}
}

TEST(TMatrixTests, operator_divisionEqual_scalar) {
	TMatrix<double> mat(2, 3, 0.1);

	mat /= 0.2;

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.5); }
	}
}

TEST(TMatrixTests, operator_multiplication_vector) {
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}
	std::vector<double> vec  = {0.3, 0.4, 0.5};
	std::vector<double> vec2 = mat * vec;

	EXPECT_EQ(vec2.size(), 2);

	std::vector<double> vals = {0.19, 0.37};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }
}

TEST(TMatrixTests, operator_multiplication_vector_throw) {
	TMatrix<double> mat(2, 3, 0.1);
	std::vector<double> vec = {0.3, 0.4, 0.5, 0.6};

#ifdef _DEBUG
	EXPECT_DEATH(std::vector<double> vec2 = mat * vec, "");
#endif
}

TEST(TMatrixTests, addToDiag) {
	TMatrix<double> mat(3);
	mat.set(0.1);

	mat.addToDiag(5.);

	std::vector<double> vals = {5.1, 0.1, 0.1, 0.1, 5.1, 0.1, 0.1, 0.1, 5.1};

	int k = 0;
	EXPECT_EQ(mat.rows(), 3);
	EXPECT_EQ(mat.cols(), 3);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			EXPECT_FLOAT_EQ(mat(i, j), vals[k]);
			k++;
		}
	}

	// throw if not square matrix
	TMatrix<double> mat2(3, 2, 0.1);
#ifdef _DEBUG
	EXPECT_DEATH(mat2.addToDiag(0.1), "");
#endif
}

TEST(TMatrixTests, diag_vec) {
	TMatrix<double> mat(3);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}
	std::vector<double> vec2 = mat.diag_vec();

	EXPECT_EQ(vec2.size(), 3);

	std::vector<double> vals = {0.1, 0.3, 0.5};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// throw if not square matrix
	TMatrix<double> mat2(3, 2, 0.1);
#ifdef _DEBUG
	EXPECT_DEATH(mat2.diag_vec(), "");
#endif
}

TEST(TMatrixTests, numDiffEntries) {
	// fill with some values
	TMatrix<double> mat(2, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	val = 0.1;
	TMatrix<double> mat2(2, 3, 0.);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) {
			mat2(i, j) = val;
			val += 0.1;
		}
	}

	int numDiff = mat.numDiffEntries(mat2);
	EXPECT_EQ(numDiff, 5);
}

TEST(TMatrixTests, numDiffEntries_throw) {
	TMatrix<double> mat(2, 3, 0.);
	TMatrix<double> mat2(3, 2, 0.);
#ifdef _DEBUG
	EXPECT_DEATH(mat.numDiffEntries(mat2), "");
#endif
}

TEST(TMatrixTests, row) {
	TMatrix<double> mat(3, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	// row 0
	std::vector<double> vec2 = mat.row(0);
	EXPECT_EQ(vec2.size(), 3);
	std::vector<double> vals = {0.1, 0.15, 0.2};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// row 1
	vec2 = mat.row(1);
	EXPECT_EQ(vec2.size(), 3);
	vals = {0.25, 0.3, 0.35};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// row2
	vec2 = mat.row(2);
	EXPECT_EQ(vec2.size(), 3);
	vals = {0.4, 0.45, 0.5};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// invalid row
#ifdef _DEBUG
	EXPECT_DEATH(vec2 = mat.row(3), "");
#endif
}

TEST(TMatrixTests, rowSum) {
	TMatrix<double> mat(3, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	EXPECT_FLOAT_EQ(mat.rowSum(0), 0.45);
	EXPECT_FLOAT_EQ(mat.rowSum(1), 0.9);
	EXPECT_FLOAT_EQ(mat.rowSum(2), 1.35);
}

TEST(TMatrixTests, col) {
	TMatrix<double> mat(3, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	// row 0
	std::vector<double> vec2 = mat.col(0);
	EXPECT_EQ(vec2.size(), 3);
	std::vector<double> vals = {0.1, 0.25, 0.4};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// col 1
	vec2 = mat.col(1);
	EXPECT_EQ(vec2.size(), 3);
	vals = {0.15, 0.3, 0.45};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// col 2
	vec2 = mat.col(2);
	EXPECT_EQ(vec2.size(), 3);
	vals = {0.2, 0.35, 0.5};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }

	// invalid col
#ifdef _DEBUG
	EXPECT_DEATH(vec2 = mat.col(3), "");
#endif
}

TEST(TMatrixTests, colSum) {
	TMatrix<double> mat(3, 3, 0.);
	double val = 0.1;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			mat(i, j) = val;
			val += 0.05;
		}
	}

	EXPECT_FLOAT_EQ(mat.colSum(0), 0.75);
	EXPECT_FLOAT_EQ(mat.colSum(1), 0.9);
	EXPECT_FLOAT_EQ(mat.colSum(2), 1.05);
}

TEST(TMatrixTests, normalize_rows) {
	TMatrix<double> mat(3, 3, 0.);
	mat.fillUniformRandom();
	mat.normalize_rows();

	EXPECT_FLOAT_EQ(mat.rowSum(0), 1.0);
	EXPECT_FLOAT_EQ(mat.rowSum(1), 1.0);
	EXPECT_FLOAT_EQ(mat.rowSum(2), 1.0);
}

TEST(TMatrixTests, normalize_cols) {
	TMatrix<double> mat(3, 3, 0.);
	mat.fillUniformRandom();
	mat.normalize_cols();

	EXPECT_FLOAT_EQ(mat.colSum(0), 1.0);
	EXPECT_FLOAT_EQ(mat.colSum(1), 1.0);
	EXPECT_FLOAT_EQ(mat.colSum(2), 1.0);
}

TEST(TMatrixTests, operator_brackets_throw) {
	TMatrix<double> mat(2, 3, 0.1);

#ifdef _DEBUG
	EXPECT_DEATH(double val = mat(2, 3), "");
#endif
}

TEST(TMatrixTests, fillAsExponential) {
	TBandMatrix<double> mat(4, 1, 0);
	fillComplicatedBandMat(mat);

	TMatrix<double> mat3(5, 1, 0.8);
	mat3.fillAsExponential(mat);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	std::vector<double> vals = {1.14319741, 0.2763107,  0.08318345, 0.02474405, 0.41446610, 1.8072138,
	                            1.05167537, 0.44408199, 0.14973020, 1.2620104,  3.18765657, 2.20073255,
	                            0.05010669, 0.5995107,  2.47582412, 3.67986979};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_NEAR(mat3(i, j), vals[k], 0.01);
			k++;
		}
	}
}
//-------------------------------------------
// TBandMatrix
//-------------------------------------------

TEST(TBandMatrixTests, constructor_noArgs) {
	TBandMatrix<double> mat;

	EXPECT_EQ(mat.rows(), 0);
	EXPECT_EQ(mat.cols(), 0);
	EXPECT_EQ(mat.bandwidth(), 0);
	EXPECT_EQ(mat.numDiag(), 0);
	EXPECT_EQ(mat.size(), 0);
}

TEST(TBandMatrixTests, constructor_size_bandwidth) {
	TBandMatrix<double> mat(2, 0);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
	EXPECT_EQ(mat.bandwidth(), 0);
	EXPECT_EQ(mat.numDiag(), 1);
	EXPECT_EQ(mat.size(), 2);
}

TEST(TBandMatrixTests, constructor_size_bandwidth_initVal) {
	TBandMatrix<double> mat(2, 0, 0.1);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			if (i == j)
				EXPECT_FLOAT_EQ(mat(i, j), 0.1);
			else
				EXPECT_FLOAT_EQ(mat(i, j), 0.);
		}
	}
	EXPECT_EQ(mat.bandwidth(), 0);
	EXPECT_EQ(mat.numDiag(), 1);
	EXPECT_EQ(mat.size(), 2);
}

TEST(TBandMatrixTests, copyConstructor) {
	TBandMatrix<double> mat(2, 0, 0.1);
	TBandMatrix<double> mat2(mat);

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 2);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) {
			if (i == j)
				EXPECT_FLOAT_EQ(mat2(i, j), 0.1);
			else
				EXPECT_FLOAT_EQ(mat2(i, j), 0.);
		}
	}
	EXPECT_EQ(mat.bandwidth(), 0);
	EXPECT_EQ(mat.numDiag(), 1);
	EXPECT_EQ(mat.size(), 2);
}

TEST(TBandMatrixTests, copyConstructor_scale) {
	TBandMatrix<double> mat(2, 0, 0.1);
	TBandMatrix<double> mat2(mat, 5.);

	EXPECT_EQ(mat2.rows(), 2);
	EXPECT_EQ(mat2.cols(), 2);
	for (unsigned int i = 0; i < mat2.rows(); i++) {
		for (unsigned int j = 0; j < mat2.cols(); j++) {
			if (i == j)
				EXPECT_FLOAT_EQ(mat2(i, j), 0.5);
			else
				EXPECT_FLOAT_EQ(mat2(i, j), 0.);
		}
	}
	EXPECT_EQ(mat.bandwidth(), 0);
	EXPECT_EQ(mat.numDiag(), 1);
	EXPECT_EQ(mat.size(), 2);
}

TEST(TBandMatrixTests, resize_size_bandwidth) {
	// not initialized
	TBandMatrix<double> mat;
	mat.resize(2, 0);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TBandMatrixTests, resize_size_bandwidth_initial) {
	// not initialized
	TBandMatrix<double> mat;
	mat.resize(2, 0, 5.);

	EXPECT_EQ(mat.rows(), 2);
	EXPECT_EQ(mat.cols(), 2);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			if (i == j)
				EXPECT_EQ(mat(i, j), 5.);
			else
				EXPECT_FLOAT_EQ(mat(i, j), 0.);
		}
	}
}

TEST(TBandMatrixTests, set) {
	TBandMatrix<double> mat(4, 1, 0.1);

	mat.set(0.89);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);

	EXPECT_EQ(mat(0, 0), 0.89);
	EXPECT_EQ(mat(0, 1), 0.89);
	EXPECT_EQ(mat(0, 2), 0);
	EXPECT_EQ(mat(0, 3), 0);
	EXPECT_EQ(mat(1, 0), 0.89);
	EXPECT_EQ(mat(1, 1), 0.89);
	EXPECT_EQ(mat(1, 2), 0.89);
	EXPECT_EQ(mat(1, 3), 0);
	EXPECT_EQ(mat(2, 0), 0);
	EXPECT_EQ(mat(2, 1), 0.89);
	EXPECT_EQ(mat(2, 2), 0.89);
	EXPECT_EQ(mat(2, 3), 0.89);
	EXPECT_EQ(mat(3, 0), 0);
	EXPECT_EQ(mat(3, 1), 0);
	EXPECT_EQ(mat(3, 2), 0.89);
	EXPECT_EQ(mat(3, 3), 0.89);
}

TEST(TBandMatrixTests, operator_equal) {
	TBandMatrix<double> mat(4, 1, 0.89);
	TBandMatrix<double> mat2(5, 2, 0.2);

	mat2 = mat;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);

	EXPECT_EQ(mat2(0, 0), 0.89);
	EXPECT_EQ(mat2(0, 1), 0.89);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.89);
	EXPECT_EQ(mat2(1, 1), 0.89);
	EXPECT_EQ(mat2(1, 2), 0.89);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.89);
	EXPECT_EQ(mat2(2, 2), 0.89);
	EXPECT_EQ(mat2(2, 3), 0.89);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.89);
	EXPECT_EQ(mat2(3, 3), 0.89);
}

TEST(TBandMatrixTests, operator_plus) {
	TBandMatrix<double> mat(4, 1, 0.89);
	TBandMatrix<double> mat2(4, 0, 0.1); // different bandwidth is also ok

	TBandMatrix<double> mat3 = mat + mat2;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3(0, 0), 0.99);
	EXPECT_EQ(mat3(0, 1), 0.89);
	EXPECT_EQ(mat3(0, 2), 0);
	EXPECT_EQ(mat3(0, 3), 0);
	EXPECT_EQ(mat3(1, 0), 0.89);
	EXPECT_EQ(mat3(1, 1), 0.99);
	EXPECT_EQ(mat3(1, 2), 0.89);
	EXPECT_EQ(mat3(1, 3), 0);
	EXPECT_EQ(mat3(2, 0), 0);
	EXPECT_EQ(mat3(2, 1), 0.89);
	EXPECT_EQ(mat3(2, 2), 0.99);
	EXPECT_EQ(mat3(2, 3), 0.89);
	EXPECT_EQ(mat3(3, 0), 0);
	EXPECT_EQ(mat3(3, 1), 0);
	EXPECT_EQ(mat3(3, 2), 0.89);
	EXPECT_EQ(mat3(3, 3), 0.99);

	mat3 = mat2 + mat; // other way around is also ok

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3(0, 0), 0.99);
	EXPECT_EQ(mat3(0, 1), 0.89);
	EXPECT_EQ(mat3(0, 2), 0);
	EXPECT_EQ(mat3(0, 3), 0);
	EXPECT_EQ(mat3(1, 0), 0.89);
	EXPECT_EQ(mat3(1, 1), 0.99);
	EXPECT_EQ(mat3(1, 2), 0.89);
	EXPECT_EQ(mat3(1, 3), 0);
	EXPECT_EQ(mat3(2, 0), 0);
	EXPECT_EQ(mat3(2, 1), 0.89);
	EXPECT_EQ(mat3(2, 2), 0.99);
	EXPECT_EQ(mat3(2, 3), 0.89);
	EXPECT_EQ(mat3(3, 0), 0);
	EXPECT_EQ(mat3(3, 1), 0);
	EXPECT_EQ(mat3(3, 2), 0.89);
	EXPECT_EQ(mat3(3, 3), 0.99);
}

TEST(TBandMatrixTests, operator_plus_itself) {
	TBandMatrix<double> mat(4, 1, 0.1);

	TBandMatrix<double> mat3 = mat + mat;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3(0, 0), 0.2);
	EXPECT_EQ(mat3(0, 1), 0.2);
	EXPECT_EQ(mat3(0, 2), 0);
	EXPECT_EQ(mat3(0, 3), 0);
	EXPECT_EQ(mat3(1, 0), 0.2);
	EXPECT_EQ(mat3(1, 1), 0.2);
	EXPECT_EQ(mat3(1, 2), 0.2);
	EXPECT_EQ(mat3(1, 3), 0);
	EXPECT_EQ(mat3(2, 0), 0);
	EXPECT_EQ(mat3(2, 1), 0.2);
	EXPECT_EQ(mat3(2, 2), 0.2);
	EXPECT_EQ(mat3(2, 3), 0.2);
	EXPECT_EQ(mat3(3, 0), 0);
	EXPECT_EQ(mat3(3, 1), 0);
	EXPECT_EQ(mat3(3, 2), 0.2);
	EXPECT_EQ(mat3(3, 3), 0.2);
}

TEST(TBandMatrixTests, operator_plus_throw) {
	TBandMatrix<double> mat1(2, 0, 0.1);

	TBandMatrix<double> mat2(1, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TBandMatrix<double> mat = mat1 + mat2, "");
#endif
	TBandMatrix<double> mat3(4, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TBandMatrix<double> mat = mat1 + mat3, "");
#endif
}

TEST(TBandMatrixTests, operator_plusEqual) {
	TBandMatrix<double> mat(4, 1, 0.1);
	TBandMatrix<double> mat2(4, 1, 0.1);

	mat2 += mat;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2(0, 0), 0.2);
	EXPECT_EQ(mat2(0, 1), 0.2);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.2);
	EXPECT_EQ(mat2(1, 1), 0.2);
	EXPECT_EQ(mat2(1, 2), 0.2);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.2);
	EXPECT_EQ(mat2(2, 2), 0.2);
	EXPECT_EQ(mat2(2, 3), 0.2);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.2);
	EXPECT_EQ(mat2(3, 3), 0.2);
}

TEST(TBandMatrixTests, operator_plusEqual_itself) {
	TBandMatrix<double> mat2(4, 1, 0.1);

	mat2 += mat2;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2(0, 0), 0.2);
	EXPECT_EQ(mat2(0, 1), 0.2);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.2);
	EXPECT_EQ(mat2(1, 1), 0.2);
	EXPECT_EQ(mat2(1, 2), 0.2);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.2);
	EXPECT_EQ(mat2(2, 2), 0.2);
	EXPECT_EQ(mat2(2, 3), 0.2);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.2);
	EXPECT_EQ(mat2(3, 3), 0.2);
}

TEST(TBandMatrixTests, operator_plusEqual_throw) {
	TBandMatrix<double> mat1(2, 0, 0.1);
	TBandMatrix<double> mat2(1, 0, 0.2);

#ifdef _DEBUG
	EXPECT_DEATH(mat2 += mat1, "");
#endif
	TBandMatrix<double> mat3(4, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat3 += mat1, "");
#endif
}

TEST(TBandMatrixTests, operator_minus) {
	TBandMatrix<double> mat(4, 1, 0.2);
	TBandMatrix<double> mat2(4, 0, 0.1);

	TBandMatrix<double> mat3 = mat - mat2;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3(0, 0), 0.1);
	EXPECT_EQ(mat3(0, 1), 0.2);
	EXPECT_EQ(mat3(0, 2), 0);
	EXPECT_EQ(mat3(0, 3), 0);
	EXPECT_EQ(mat3(1, 0), 0.2);
	EXPECT_EQ(mat3(1, 1), 0.1);
	EXPECT_EQ(mat3(1, 2), 0.2);
	EXPECT_EQ(mat3(1, 3), 0);
	EXPECT_EQ(mat3(2, 0), 0);
	EXPECT_EQ(mat3(2, 1), 0.2);
	EXPECT_EQ(mat3(2, 2), 0.1);
	EXPECT_EQ(mat3(2, 3), 0.2);
	EXPECT_EQ(mat3(3, 0), 0);
	EXPECT_EQ(mat3(3, 1), 0);
	EXPECT_EQ(mat3(3, 2), 0.2);
	EXPECT_EQ(mat3(3, 3), 0.1);

	// other way around
	mat3 = mat2 - mat;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3(0, 0), 0.1);
	EXPECT_EQ(mat3(0, 1), 0.2);
	EXPECT_EQ(mat3(0, 2), 0);
	EXPECT_EQ(mat3(0, 3), 0);
	EXPECT_EQ(mat3(1, 0), 0.2);
	EXPECT_EQ(mat3(1, 1), 0.1);
	EXPECT_EQ(mat3(1, 2), 0.2);
	EXPECT_EQ(mat3(1, 3), 0);
	EXPECT_EQ(mat3(2, 0), 0);
	EXPECT_EQ(mat3(2, 1), 0.2);
	EXPECT_EQ(mat3(2, 2), 0.1);
	EXPECT_EQ(mat3(2, 3), 0.2);
	EXPECT_EQ(mat3(3, 0), 0);
	EXPECT_EQ(mat3(3, 1), 0);
	EXPECT_EQ(mat3(3, 2), 0.2);
	EXPECT_EQ(mat3(3, 3), 0.1);
}

TEST(TBandMatrixTests, operator_minus_itself) {
	TBandMatrix<double> mat(4, 1, 0.1);

	TBandMatrix<double> mat3 = mat - mat;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) { EXPECT_FLOAT_EQ(mat3(i, j), 0.); }
	}
}

TEST(TBandMatrixTests, operator_minus_throw) {
	TBandMatrix<double> mat1(2, 0, 0.1);

	TBandMatrix<double> mat2(1, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TBandMatrix<double> mat = mat1 - mat2, "");
#endif
	TBandMatrix<double> mat3(4, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(TBandMatrix<double> mat = mat1 - mat3, "");
#endif
}

TEST(TBandMatrixTests, operator_minusEqual) {
	TBandMatrix<double> mat(4, 0, 0.1);
	TBandMatrix<double> mat2(4, 1, 0.2);

	mat2 -= mat;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2(0, 0), 0.1);
	EXPECT_EQ(mat2(0, 1), 0.2);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.2);
	EXPECT_EQ(mat2(1, 1), 0.1);
	EXPECT_EQ(mat2(1, 2), 0.2);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.2);
	EXPECT_EQ(mat2(2, 2), 0.1);
	EXPECT_EQ(mat2(2, 3), 0.2);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.2);
	EXPECT_EQ(mat2(3, 3), 0.1);
}

TEST(TBandMatrixTests, operator_minusEqual_itself) {
	TBandMatrix<double> mat(4, 1, 0.1);
	TBandMatrix<double> mat2(mat);

	mat -= mat2;

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) { EXPECT_FLOAT_EQ(mat(i, j), 0.); }
	}
}

TEST(TBandMatrixTests, operator_minusEqual_throw) {
	TBandMatrix<double> mat1(2, 0, 0.1);

	TBandMatrix<double> mat2(1, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat1 -= mat2, "");
#endif
	TBandMatrix<double> mat3(4, 0, 0.2);
#ifdef _DEBUG
	EXPECT_DEATH(mat1 -= mat3, "");
#endif
}

TEST(TBandMatrixTests, operator_multiplication_sameBandwidth) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat2(4, 1, 0.2);
	fillComplicatedBandMat(mat2, 0.5);

	TBandMatrix<double> mat3 = mat * mat2;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3.bandwidth(), 2);
	std::vector<double> vals = {0.22, 0.25, 0.20, 0.00, 0.50, 1.12, 1.00, 0.65,
	                            0.48, 1.31, 2.56, 2.11, 0.00, 0.99, 2.48, 2.67};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, operator_multiplication_differentBandwithds) {
	// fill with some values
	TBandMatrix<double> mat(5, 1, 0.);
	mat(0, 0) = 1.;
	mat(0, 1) = 2.;
	mat(0, 2) = 0;
	mat(0, 3) = 0;
	mat(0, 4) = 0;
	mat(1, 0) = 3.;
	mat(1, 1) = 4.;
	mat(1, 2) = 5.;
	mat(1, 3) = 0;
	mat(1, 4) = 0;
	mat(2, 0) = 0;
	mat(2, 1) = 6.;
	mat(2, 2) = 7.;
	mat(2, 3) = 8.;
	mat(2, 4) = 0;
	mat(3, 0) = 0;
	mat(3, 1) = 0;
	mat(3, 2) = 9.;
	mat(3, 3) = 10.;
	mat(3, 4) = 11.;
	mat(4, 0) = 0;
	mat(4, 1) = 0;
	mat(4, 2) = 0.;
	mat(4, 3) = 12.;
	mat(4, 4) = 13.;

	TBandMatrix<double> mat2(5, 2, 0.2);
	mat2(0, 0) = 1.;
	mat2(0, 1) = 2.;
	mat2(0, 2) = 3.;
	mat2(0, 3) = 0;
	mat2(0, 4) = 0;
	mat2(1, 0) = 4.;
	mat2(1, 1) = 5.;
	mat2(1, 2) = 6.;
	mat2(1, 3) = 7.;
	mat2(1, 4) = 0;
	mat2(2, 0) = 8.;
	mat2(2, 1) = 9.;
	mat2(2, 2) = 10.;
	mat2(2, 3) = 11.;
	mat2(2, 4) = 12.;
	mat2(3, 0) = 0;
	mat2(3, 1) = 13.;
	mat2(3, 2) = 14.;
	mat2(3, 3) = 15.;
	mat2(3, 4) = 16.;
	mat2(4, 0) = 0;
	mat2(4, 1) = 0;
	mat2(4, 2) = 17.;
	mat2(4, 3) = 18.;
	mat2(4, 4) = 19.;

	TBandMatrix<double> mat3 = mat * mat2;

	EXPECT_EQ(mat3.rows(), 5);
	EXPECT_EQ(mat3.cols(), 5);
	EXPECT_EQ(mat3.bandwidth(), 3);
	std::vector<double> vals = {9,   12,  15, 14,  0,   59,  71,  83, 83,  60,  80,  197, 218,
	                            239, 212, 72, 211, 417, 447, 477, 0,  156, 389, 414, 439};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, operator_multiplication_itself) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat3 = mat * mat;

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3.bandwidth(), 2);
	std::vector<double> vals = {0.07, 0.10, 0.10, 0.00, 0.15, 0.52, 0.55, 0.40,
	                            0.18, 0.66, 1.51, 1.36, 0.00, 0.54, 1.53, 1.72};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, operator_multiplication_throw) {
	TBandMatrix<double> mat1(2, 0, 0.1);

	TBandMatrix<double> mat2(1, 0, 0.2);
	EXPECT_ANY_THROW(TBandMatrix<double> mat = mat1 * mat2);
	TBandMatrix<double> mat3(4, 0, 0.2);
	EXPECT_ANY_THROW(TBandMatrix<double> mat = mat3 * mat1);
}

TEST(TBandMatrixTests, operator_multiplicationEqual) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat2(4, 1, 0.2);
	fillComplicatedBandMat(mat2, 0.5);

	mat *= mat2;

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);
	EXPECT_EQ(mat.bandwidth(), 2);
	std::vector<double> vals = {0.22, 0.25, 0.20, 0.00, 0.50, 1.12, 1.00, 0.65,
	                            0.48, 1.31, 2.56, 2.11, 0.00, 0.99, 2.48, 2.67};
	int k                    = 0;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			EXPECT_FLOAT_EQ(mat(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, operator_multiplicationEqual_itself) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	mat *= mat;

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);
	EXPECT_EQ(mat.bandwidth(), 2);
	std::vector<double> vals = {0.07, 0.10, 0.10, 0.00, 0.15, 0.52, 0.55, 0.40,
	                            0.18, 0.66, 1.51, 1.36, 0.00, 0.54, 1.53, 1.72};
	int k                    = 0;
	for (unsigned int i = 0; i < mat.rows(); i++) {
		for (unsigned int j = 0; j < mat.cols(); j++) {
			EXPECT_FLOAT_EQ(mat(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, operator_multiplicationEqual_throw) {
	TBandMatrix<double> mat1(2, 0, 0.1);

	TBandMatrix<double> mat2(1, 0, 0.2);
	EXPECT_ANY_THROW(mat1 *= mat2);
	TBandMatrix<double> mat3(4, 0, 0.2);
	EXPECT_ANY_THROW(mat1 *= mat3);
}

TEST(TBandMatrixTests, transpose) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat2 = mat.transpose();

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	// check if we get the same
	EXPECT_EQ(mat2(0, 0), 0.1);
	EXPECT_EQ(mat2(0, 1), 0.3);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.2);
	EXPECT_EQ(mat2(1, 1), 0.4);
	EXPECT_EQ(mat2(1, 2), 0.6);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.5);
	EXPECT_EQ(mat2(2, 2), 0.7);
	EXPECT_EQ(mat2(2, 3), 0.9);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.8);
	EXPECT_EQ(mat2(3, 3), 1.0);
}

TEST(TBandMatrixTests, operator_plus_scalar) {
	TBandMatrix<double> mat(4, 1, 0.1);

	TBandMatrix<double> mat2 = mat + 0.2;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_plusEqual_scalar) {
	TBandMatrix<double> mat2(4, 1, 0.1);

	mat2 += 0.2;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_minus_scalar) {
	TBandMatrix<double> mat(4, 1, 0.5);

	TBandMatrix<double> mat2 = mat - 0.2;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_minusEqual_scalar) {
	TBandMatrix<double> mat2(4, 1, 0.5);

	mat2 -= 0.2;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_multiplication_scalar) {
	TBandMatrix<double> mat(4, 1, 0.1);

	TBandMatrix<double> mat2 = mat * 3.;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_multiplicationEqual_scalar) {
	TBandMatrix<double> mat2(4, 1, 0.1);

	mat2 *= 3.;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_division_scalar) {
	TBandMatrix<double> mat(4, 1, 0.6);

	TBandMatrix<double> mat2 = mat / 2.;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_divisionEqual_scalar) {
	TBandMatrix<double> mat2(4, 1, 0.6);

	mat2 /= 2.;

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2.bandwidth(), 1);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.3);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.3);
}

TEST(TBandMatrixTests, operator_multiplication_vector) {
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	std::vector<double> vec  = {0.3, 0.4, 0.5, 0.6};
	std::vector<double> vec2 = mat * vec;

	EXPECT_EQ(vec2.size(), 4);

	std::vector<double> vals = {0.11, 0.50, 1.07, 1.05};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }
}

TEST(TBandMatrixTests, operator_multiplication_vector_throw) {
	TBandMatrix<double> mat(4, 1, 0.);
	std::vector<double> vec = {0.3, 0.4, 0.5, 0.6, 0.7};

#ifdef _DEBUG
	EXPECT_DEATH(std::vector<double> vec2 = mat * vec, "");
#endif
}

TEST(TBandMatrixTests, fillFromMatrix) {
	TBandMatrix<double> mat(4, 1, 0.89);
	TBandMatrix<double> mat2(6, 2, 0.2);

	mat2.fillFromMatrix(mat);

	EXPECT_EQ(mat2.rows(), 4);
	EXPECT_EQ(mat2.cols(), 4);
	EXPECT_EQ(mat2(0, 0), 0.89);
	EXPECT_EQ(mat2(0, 1), 0.89);
	EXPECT_EQ(mat2(0, 2), 0);
	EXPECT_EQ(mat2(0, 3), 0);
	EXPECT_EQ(mat2(1, 0), 0.89);
	EXPECT_EQ(mat2(1, 1), 0.89);
	EXPECT_EQ(mat2(1, 2), 0.89);
	EXPECT_EQ(mat2(1, 3), 0);
	EXPECT_EQ(mat2(2, 0), 0);
	EXPECT_EQ(mat2(2, 1), 0.89);
	EXPECT_EQ(mat2(2, 2), 0.89);
	EXPECT_EQ(mat2(2, 3), 0.89);
	EXPECT_EQ(mat2(3, 0), 0);
	EXPECT_EQ(mat2(3, 1), 0);
	EXPECT_EQ(mat2(3, 2), 0.89);
	EXPECT_EQ(mat2(3, 3), 0.89);
}

TEST(TBandMatrixTests, fillFromMatrix_scale) {
	TBandMatrix<double> mat(4, 1, 0.1);
	TBandMatrix<double> mat2(6, 2, 0.2);

	mat2.fillFromMatrix(mat, 8.9);

	EXPECT_FLOAT_EQ(mat2.rows(), 4);
	EXPECT_FLOAT_EQ(mat2.cols(), 4);
	EXPECT_FLOAT_EQ(mat2(0, 0), 0.89);
	EXPECT_FLOAT_EQ(mat2(0, 1), 0.89);
	EXPECT_FLOAT_EQ(mat2(0, 2), 0);
	EXPECT_FLOAT_EQ(mat2(0, 3), 0);
	EXPECT_FLOAT_EQ(mat2(1, 0), 0.89);
	EXPECT_FLOAT_EQ(mat2(1, 1), 0.89);
	EXPECT_FLOAT_EQ(mat2(1, 2), 0.89);
	EXPECT_FLOAT_EQ(mat2(1, 3), 0);
	EXPECT_FLOAT_EQ(mat2(2, 0), 0);
	EXPECT_FLOAT_EQ(mat2(2, 1), 0.89);
	EXPECT_FLOAT_EQ(mat2(2, 2), 0.89);
	EXPECT_FLOAT_EQ(mat2(2, 3), 0.89);
	EXPECT_FLOAT_EQ(mat2(3, 0), 0);
	EXPECT_FLOAT_EQ(mat2(3, 1), 0);
	EXPECT_FLOAT_EQ(mat2(3, 2), 0.89);
	EXPECT_FLOAT_EQ(mat2(3, 3), 0.89);
}

TEST(TBandMatrixTests, fillFromProduct) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat2(4, 1, 0.2);
	fillComplicatedBandMat(mat2, 0.5);

	TBandMatrix<double> mat3(6, 2, 0.2);
	mat3.fillFromProduct(mat, mat2);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3.bandwidth(), 2);
	std::vector<double> vals = {0.22, 0.25, 0.20, 0.00, 0.50, 1.12, 1.00, 0.65,
	                            0.48, 1.31, 2.56, 2.11, 0.00, 0.99, 2.48, 2.67};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, fillFromProduct_throw) {
	TBandMatrix<double> mat1(4, 1);
	TBandMatrix<double> mat2(5, 1);

	TBandMatrix<double> mat;
	EXPECT_ANY_THROW(mat.fillFromProduct(mat1, mat2));
	TBandMatrix<double> mat3(3, 1);
	EXPECT_ANY_THROW(mat.fillFromProduct(mat3, mat1));
}

TEST(TBandMatrixTests, fillFromSquare) {
	TBandMatrix<double> mat(4, 1);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat3(5, 1);
	mat3.fillFromSquare(mat);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3.bandwidth(), 2);
	std::vector<double> vals = {0.07, 0.10, 0.10, 0.00, 0.15, 0.52, 0.55, 0.40,
	                            0.18, 0.66, 1.51, 1.36, 0.00, 0.54, 1.53, 1.72};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_FLOAT_EQ(mat3(i, j), vals[k]);
			k++;
		}
	}
}

TEST(TBandMatrixTests, fillAsExponential_bandwidth0) {
	TBandMatrix<double> mat(4, 0, 1.);

	TBandMatrix<double> mat3(5, 1, 0.8);
	mat3.fillAsExponential(mat);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3.bandwidth(), 0);
	std::vector<double> vals = {2.718282, 0, 0, 0, 0, 2.718282, 0, 0, 0, 0, 2.718282, 0, 0, 0, 0, 2.718282};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_NEAR(mat3(i, j), vals[k], 0.01);
			k++;
		}
	}
}

TEST(TBandMatrixTests, fillAsExponential) {
	TBandMatrix<double> mat(4, 1, 0);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat3(5, 1, 0.8);
	mat3.fillAsExponential(mat);

	EXPECT_EQ(mat3.rows(), 4);
	EXPECT_EQ(mat3.cols(), 4);
	EXPECT_EQ(mat3.bandwidth(), 4);
	std::vector<double> vals = {1.14319741, 0.2763107,  0.08318345, 0.02474405, 0.41446610, 1.8072138,
	                            1.05167537, 0.44408199, 0.14973020, 1.2620104,  3.18765657, 2.20073255,
	                            0.05010669, 0.5995107,  2.47582412, 3.67986979};
	int k                    = 0;
	for (unsigned int i = 0; i < mat3.rows(); i++) {
		for (unsigned int j = 0; j < mat3.cols(); j++) {
			EXPECT_NEAR(mat3(i, j), vals[k], 0.01);
			k++;
		}
	}
}

TEST(TBandMatrixTests, diag_vec) {
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);
	std::vector<double> vec2 = mat.diag_vec();

	EXPECT_EQ(vec2.size(), 4);

	std::vector<double> vals = {0.1, 0.4, 0.7, 1.0};
	for (unsigned int i = 0; i < vec2.size(); i++) { EXPECT_FLOAT_EQ(vec2[i], vals[i]); }
}

TEST(TBandMatrixTests, numDiffEntries) {
	// fill with some values
	TBandMatrix<double> mat(4, 1, 0.);
	fillComplicatedBandMat(mat);

	TBandMatrix<double> mat2(4, 0, 0.1); // different bandwidth is ok

	int numDiff = mat.numDiffEntries(mat2);
	EXPECT_EQ(numDiff, 9); // first element is same
}

TEST(TBandMatrixTests, numDiffEntries_throw) {
	TBandMatrix<double> mat(4, 1, 0.);
	TBandMatrix<double> mat2(5, 1, 0.);
#ifdef _DEBUG
	EXPECT_DEATH(mat.numDiffEntries(mat2), "");
#endif
}

TEST(TBandMatrixTests, operator_brackets) {
	// not initialized
	TBandMatrix<double> mat(4, 1);

	// fill
	fillComplicatedBandMat(mat);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);

	// check if we get the same
	EXPECT_EQ(mat(0, 0), 0.1);
	EXPECT_EQ(mat(0, 1), 0.2);
	EXPECT_EQ(mat(0, 2), 0);
	EXPECT_EQ(mat(0, 3), 0);
	EXPECT_EQ(mat(1, 0), 0.3);
	EXPECT_EQ(mat(1, 1), 0.4);
	EXPECT_EQ(mat(1, 2), 0.5);
	EXPECT_EQ(mat(1, 3), 0);
	EXPECT_EQ(mat(2, 0), 0);
	EXPECT_EQ(mat(2, 1), 0.6);
	EXPECT_EQ(mat(2, 2), 0.7);
	EXPECT_EQ(mat(2, 3), 0.8);
	EXPECT_EQ(mat(3, 0), 0);
	EXPECT_EQ(mat(3, 1), 0);
	EXPECT_EQ(mat(3, 2), 0.9);
	EXPECT_EQ(mat(3, 3), 1.0);

	// throw (const)
#ifdef _DEBUG
	EXPECT_DEATH(mat(4, 0), "");
#endif // invalid row
#ifdef _DEBUG
	EXPECT_DEATH(mat(0, 4), "");
#endif // invalid col
       // throw (non-const)
#ifdef _DEBUG
	EXPECT_DEATH(mat(4, 0) = 0., "");
#endif
#ifdef _DEBUG
	EXPECT_DEATH(mat(0, 4) = 0., "");
#endif
}

TEST(TBandMatrixTests, row) {
	// not initialized
	TBandMatrix<double> mat(4, 1);

	// fill
	fillComplicatedBandMat(mat);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);

	// check if we get the same
	std::vector<double> row = mat.row(0);
	EXPECT_EQ(row[0], 0.1);
	EXPECT_EQ(row[1], 0.2);
	EXPECT_EQ(row[2], 0);
	EXPECT_EQ(row[3], 0);
	row = mat.row(1);
	EXPECT_EQ(row[0], 0.3);
	EXPECT_EQ(row[1], 0.4);
	EXPECT_EQ(row[2], 0.5);
	EXPECT_EQ(row[3], 0);
	row = mat.row(2);
	EXPECT_EQ(row[0], 0);
	EXPECT_EQ(row[1], 0.6);
	EXPECT_EQ(row[2], 0.7);
	EXPECT_EQ(row[3], 0.8);
	row = mat.row(3);
	EXPECT_EQ(row[0], 0);
	EXPECT_EQ(row[1], 0);
	EXPECT_EQ(row[2], 0.9);
	EXPECT_EQ(row[3], 1.0);

	// throw
#ifdef _DEBUG
	EXPECT_DEATH(mat.row(4), "");
#endif
}

TEST(TBandMatrixTests, rowSum) {
	// not initialized
	TBandMatrix<double> mat(4, 1);

	// fill
	fillComplicatedBandMat(mat);

	EXPECT_FLOAT_EQ(mat.rowSum(0), 0.3);
	EXPECT_FLOAT_EQ(mat.rowSum(1), 1.2);
	EXPECT_FLOAT_EQ(mat.rowSum(2), 2.1);
	EXPECT_FLOAT_EQ(mat.rowSum(3), 1.9);
}

TEST(TBandMatrixTests, col) {
	// not initialized
	TBandMatrix<double> mat(4, 1);

	// fill
	fillComplicatedBandMat(mat);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);

	// check if we get the same
	std::vector<double> col = mat.col(0);
	EXPECT_EQ(col[0], 0.1);
	EXPECT_EQ(col[1], 0.3);
	EXPECT_EQ(col[2], 0);
	EXPECT_EQ(col[3], 0);
	col = mat.col(1);
	EXPECT_EQ(col[0], 0.2);
	EXPECT_EQ(col[1], 0.4);
	EXPECT_EQ(col[2], 0.6);
	EXPECT_EQ(col[3], 0);
	col = mat.col(2);
	EXPECT_EQ(col[0], 0);
	EXPECT_EQ(col[1], 0.5);
	EXPECT_EQ(col[2], 0.7);
	EXPECT_EQ(col[3], 0.9);
	col = mat.col(3);
	EXPECT_EQ(col[0], 0);
	EXPECT_EQ(col[1], 0);
	EXPECT_EQ(col[2], 0.8);
	EXPECT_EQ(col[3], 1.0);

	// throw
#ifdef _DEBUG
	EXPECT_DEATH(mat.col(4), "");
#endif
}

TEST(TBandMatrixTests, colSum) {
	// not initialized
	TBandMatrix<double> mat(4, 1);

	// fill
	fillComplicatedBandMat(mat);

	EXPECT_FLOAT_EQ(mat.colSum(0), 0.4);
	EXPECT_FLOAT_EQ(mat.colSum(1), 1.2);
	EXPECT_FLOAT_EQ(mat.colSum(2), 2.1);
	EXPECT_FLOAT_EQ(mat.colSum(3), 1.8);
}

TEST(TBandMatrixTests, atDiag) {
	// not initialized
	TBandMatrix<double> mat(4, 1);

	// fill
	fillComplicatedBandMat(mat);

	EXPECT_EQ(mat.rows(), 4);
	EXPECT_EQ(mat.cols(), 4);

	// check if we get the same
	// diag 0
	EXPECT_EQ(mat.atDiag(0, 0), 0.3);
	EXPECT_EQ(mat.atDiag(0, 1), 0.6);
	EXPECT_EQ(mat.atDiag(0, 2), 0.9);
	// diag 1
	EXPECT_EQ(mat.atDiag(1, 0), 0.1);
	EXPECT_EQ(mat.atDiag(1, 1), 0.4);
	EXPECT_EQ(mat.atDiag(1, 2), 0.7);
	EXPECT_EQ(mat.atDiag(1, 3), 1.0);
	// diag 2
	EXPECT_EQ(mat.atDiag(2, 0), 0.2);
	EXPECT_EQ(mat.atDiag(2, 1), 0.5);
	EXPECT_EQ(mat.atDiag(2, 2), 0.8);

	// throw
#ifdef _DEBUG
	EXPECT_DEATH *(mat.atDiag(3, 0), "");
#endif // invalid diagonal
#ifdef _DEBUG
	EXPECT_DEATH(mat.atDiag(0, 3), "");
#endif // invalid index
#ifdef _DEBUG
	EXPECT_DEATH(mat.atDiag(1, 4), "");
#endif // invalid index
}

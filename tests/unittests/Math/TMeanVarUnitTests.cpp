/*
 * TMeanVarUnitTests.cpp
 *
 *  Created on: Oct 24, 2022
 *      Author: phaentu
 */

#include "gtest/gtest.h"

#include "coretools/Math/TMeanVar.h"        // for TCountDistribution, TCountDistrib...

using namespace coretools;

TEST(TMeanVarTest, all) {
	TMeanVar<double> meanVar;

	// add
	meanVar.add(1.);
	meanVar.add(2.);
	meanVar.add(0.83);
	meanVar.add(-102.9);
	meanVar.add(0.);
	meanVar.add(100.293);

	EXPECT_EQ(meanVar.counts(), 6);
	EXPECT_FLOAT_EQ(meanVar.sum(), 1.223);
	EXPECT_FLOAT_EQ(meanVar.mean(), 0.2038333);
	EXPECT_FLOAT_EQ(meanVar.sd(), 58.66932);
	EXPECT_FLOAT_EQ(meanVar.variance(), 3442.089);

	// add from other
	TMeanVar<double> meanVar2;
	meanVar2.add(meanVar);

	EXPECT_EQ(meanVar.counts(), 6);
	EXPECT_FLOAT_EQ(meanVar.sum(), 1.223);
	EXPECT_FLOAT_EQ(meanVar.mean(), 0.2038333);
	EXPECT_FLOAT_EQ(meanVar.sd(), 58.66932);
	EXPECT_FLOAT_EQ(meanVar.variance(), 3442.089);

	// clear
	meanVar.clear();
	EXPECT_EQ(meanVar.counts(), 0);
	EXPECT_EQ(meanVar.sum(), 0.);
	EXPECT_EQ(meanVar.mean(), 0.);
	EXPECT_EQ(meanVar.sd(), 0.);
	EXPECT_EQ(meanVar.variance(), 0.);
}

TEST(TMeanVarTest, double_overflow) {
	TMeanVar<double> meanVar;

	// overflow
	double limit = std::numeric_limits<double>::max();
	// add x = sqrt(limit) such that x^2 = sumOfSquares = limit
	meanVar.add(sqrt(limit));
	// add one thing more -> too big in order to be stored -> throw
	EXPECT_ANY_THROW(meanVar.add(std::nextafter(limit, 0.)));
}

TEST(TMeanVarTest, int_overflow) {
	TMeanVar<int> meanVar;

	// overflow
	size_t limit = std::numeric_limits<int>::max();
	// add x = sqrt(limit) such that x^2 = sumOfSquares = limit (not exactly, as sqrt(limit) is floating-point and cut
	// off to an integer
	// -> distance to limit is then 88047 -> sqrt(88047) = 296.72)
	meanVar.add(sqrt(limit));
	// add one thing more -> too big in order to be stored -> throw
	EXPECT_ANY_THROW(meanVar.add(297));
}

TEST(TMeanVarTest, uint8_t_overflow) {
	TMeanVar<uint8_t> meanVar;

	// overflow
	uint8_t limit = std::numeric_limits<uint8_t>::max();
	// add x = sqrt(limit) such that x^2 = sumOfSquares = limit (not exactly, as sqrt(limit) is floating-point and cut
	// off to an integer
	// -> distance to limit is then 30 -> sqrt(30) = 5.47)
	meanVar.add(sqrt(limit));
	// add one thing more -> too big in order to be stored -> throw
	EXPECT_ANY_THROW(meanVar.add(6));
}


#include "gtest/gtest.h"

#include "coretools/Distributions/TGammaDistr.h"

using namespace coretools;
using namespace probdist;

TEST(TGammaDistrTests, dgamma) {
	EXPECT_FLOAT_EQ(TGammaDistr::density(0.1, 0.1, 0.1), 0.6566234);
	EXPECT_FLOAT_EQ(TGammaDistr::density(5, 0.1, 0.1), 0.011897044);
	EXPECT_FLOAT_EQ(TGammaDistr::density(0.5, 10, 0.1), 5.119792e-19);
	EXPECT_FLOAT_EQ(TGammaDistr::density(0.5, 0.1, 10), 0.001663849);
	EXPECT_FLOAT_EQ(TGammaDistr::density(0.0000001, 2.5, 1000), 0.0007521776);

	TGammaDistr test(0.1, 0.1);
	EXPECT_FLOAT_EQ(test.density(0.1), 0.6566234);
	EXPECT_FLOAT_EQ(test.density(5), 0.011897044);
	TGammaDistr test1(10, 0.1);
	EXPECT_FLOAT_EQ(test1.density(0.5), 5.119792e-19);
	TGammaDistr test2(0.1, 10);
	EXPECT_FLOAT_EQ(test2.density(0.5), 0.001663849);
	TGammaDistr test3(2.5, 1000);
	EXPECT_FLOAT_EQ(test3.density(0.0000001), 0.0007521776);
}

TEST(TGammaDistrTests, dgammaLog) {
	EXPECT_FLOAT_EQ(TGammaDistr::logDensity(0.1, 0.1, 0.1), log(0.6566234));
	EXPECT_FLOAT_EQ(TGammaDistr::logDensity(5, 0.1, 0.1), log(0.011897044));
	EXPECT_FLOAT_EQ(TGammaDistr::logDensity(0.5, 10, 0.1), log(5.119792e-19));
	EXPECT_FLOAT_EQ(TGammaDistr::logDensity(0.5, 0.1, 10), log(0.001663849));
	EXPECT_FLOAT_EQ(TGammaDistr::logDensity(0.0000001, 2.5, 1000), log(0.0007521776));

	TGammaDistr test(0.1, 0.1);
	EXPECT_FLOAT_EQ(test.logDensity(0.1), log(0.6566234));
	EXPECT_FLOAT_EQ(test.logDensity(5), log(0.011897044));
	TGammaDistr test1(10, 0.1);
	EXPECT_FLOAT_EQ(test1.logDensity(0.5), log(5.119792e-19));
	TGammaDistr test2(0.1, 10);
	EXPECT_FLOAT_EQ(test2.logDensity(0.5), log(0.001663849));
	TGammaDistr test3(2.5, 1000);
	EXPECT_FLOAT_EQ(test3.logDensity(0.0000001), log(0.0007521776));
}

TEST(TGammaDistrTests, pgamma) {
	EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDensity(0.1, 0.1, 0.1), 0.6626213);
	EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDensity(5, 0.1, 0.1), 0.9414024);
	EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDensity(0.5, 10, 0.1), 2.57158e-20);
	EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDensity(0.5, 0.1, 10), 0.9998561);
	EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDensity(0.0000001, 2.5, 1000), 3.008796e-11);

	TGammaDistr test(0.1, 0.1);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.1), 0.6626213);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(5), 0.9414024);
	TGammaDistr test1(10, 0.1);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(0.5), 2.57158e-20);
	TGammaDistr test2(0.1, 10);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(0.5), 0.9998561);
	TGammaDistr test3(2.5, 1000);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(0.0000001), 3.008796e-11);
}

TEST(TGammaDistrTests, gammaMean) {
	EXPECT_FLOAT_EQ(TGammaDistr::mean(0.1, 0.1), 1);
	EXPECT_FLOAT_EQ(TGammaDistr::mean(10, 0.1), 100);
	EXPECT_FLOAT_EQ(TGammaDistr::mean(0.1, 10), 0.01);
	EXPECT_FLOAT_EQ(TGammaDistr::mean(2.5, 1000), 0.0025);

	TGammaDistr test(0.1, 0.1);
	EXPECT_FLOAT_EQ(test.mean(), 1);
	TGammaDistr test1(10, 0.1);
	EXPECT_FLOAT_EQ(test1.mean(), 100);
	TGammaDistr test2(0.1, 10);
	EXPECT_FLOAT_EQ(test2.mean(), 0.01);
	TGammaDistr test3(2.5, 1000);
	EXPECT_FLOAT_EQ(test3.mean(), 0.0025);
}

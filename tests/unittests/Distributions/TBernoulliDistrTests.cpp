#include "gtest/gtest.h"
#include "coretools/Distributions/TBernoulliDistr.h"

using namespace coretools;
using namespace probdist;

TEST(TBernoulliDistrTests, dbernoulliThrow) {
	// invalid pi
	EXPECT_ANY_THROW(TBernoulliDistr::density(true, 1.1_P));
	EXPECT_ANY_THROW(TBernoulliDistr test1(1.1_P));
}

TEST(TBernoulliDistrTests, dbernoulli) {
	EXPECT_FLOAT_EQ(TBernoulliDistr::density(false, 0._P), 1.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::density(false, 1._P), 0.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::density(true, 0._P), 0.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::density(true, 1._P), 1.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::density(true, 0.44_P), 0.44);
	EXPECT_FLOAT_EQ(TBernoulliDistr::density(false, 0.44_P), 0.56);

	TBernoulliDistr test(0._P);
	EXPECT_FLOAT_EQ(test.density(false), 1.);
	TBernoulliDistr test1(1._P);
	EXPECT_FLOAT_EQ(test1.density(false), 0.);
	EXPECT_FLOAT_EQ(test.density(true), 0.);
	EXPECT_FLOAT_EQ(test1.density(true), 1.);
	TBernoulliDistr test2(0.44_P);
	EXPECT_FLOAT_EQ(test2.density(true), 0.44);
	EXPECT_FLOAT_EQ(test2.density(false), 0.56);
}

TEST(TBernoulliDistrTests, dbernoulliLog) {
	EXPECT_FLOAT_EQ(TBernoulliDistr::logDensity(false, 0.2_P), -0.2231436);
	EXPECT_FLOAT_EQ(TBernoulliDistr::logDensity(true, 0.9_P), -0.1053605);
	EXPECT_FLOAT_EQ(TBernoulliDistr::logDensity(true, 0.44_P), -0.8209806);
	EXPECT_FLOAT_EQ(TBernoulliDistr::logDensity(false, 0.44_P), -0.5798185);

	TBernoulliDistr test(0.2_P);
	EXPECT_FLOAT_EQ(test.logDensity(false), -0.2231436);
	TBernoulliDistr test1(0.9_P);
	EXPECT_FLOAT_EQ(test1.logDensity(true), -0.1053605);
	TBernoulliDistr test2(0.44_P);
	EXPECT_FLOAT_EQ(test2.logDensity(true), -0.8209806);
	EXPECT_FLOAT_EQ(test2.logDensity(false), -0.5798185);
}

TEST(TBernoulliDistrTests, pbernoulli) {
	EXPECT_FLOAT_EQ(TBernoulliDistr::cumulativeDensity(false, 0._P), 1.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::cumulativeDensity(false, 1._P), 0.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::cumulativeDensity(true, 0._P), 1.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::cumulativeDensity(true, 1._P), 1.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::cumulativeDensity(true, 0.44_P), 1);
	EXPECT_FLOAT_EQ(TBernoulliDistr::cumulativeDensity(false, 0.44_P), 0.56);

	TBernoulliDistr test(0._P);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(false), 1.);
	TBernoulliDistr test1(1._P);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(false), 0.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(true), 1.);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(true), 1.);
	TBernoulliDistr test2(0.44_P);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(true), 1.);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(false), 0.56);
}

TEST(TBernoulliDistrTests, bernMean) {
	EXPECT_FLOAT_EQ(TBernoulliDistr::mean(0._P), 0.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::mean(1._P), 1.);
	EXPECT_FLOAT_EQ(TBernoulliDistr::mean(0.4_P), 0.4);

	TBernoulliDistr test(0._P);
	EXPECT_FLOAT_EQ(test.mean(), 0.);
	TBernoulliDistr test1(1._P);
	EXPECT_FLOAT_EQ(test1.mean(), 1.);
	TBernoulliDistr test2(0.4_P);
	EXPECT_FLOAT_EQ(test2.mean(), 0.4);
}

TEST(TBernoulliDistrTests, bernBools) {
	EXPECT_TRUE(TBernoulliDistr::isDiscrete());
	EXPECT_FALSE(TBernoulliDistr::isMultiVariate());
}

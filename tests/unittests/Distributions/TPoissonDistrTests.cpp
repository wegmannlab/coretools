#include "gtest/gtest.h"
#include "coretools/Distributions/TPoissonDistr.h"

using namespace coretools;
using namespace probdist;
// Poisson distribution

TEST(TPoissonDistrTests, dpois) {
	EXPECT_FLOAT_EQ(TPoissonDistr::density(0, 1.), 0.3678794);
	EXPECT_FLOAT_EQ(TPoissonDistr::density(0, 1.473), 0.2292367);
	EXPECT_FLOAT_EQ(TPoissonDistr::density(0, 8.3872), 0.0002277641);
	EXPECT_FLOAT_EQ(TPoissonDistr::density(1, 8.3872), 0.001910303);
	EXPECT_FLOAT_EQ(TPoissonDistr::density(2, 8.3872), 0.008011048);
	EXPECT_FLOAT_EQ(TPoissonDistr::density(100, 99.), 0.03966086);

	TPoissonDistr test(1.);
	EXPECT_FLOAT_EQ(test.density(0), 0.3678794);
	TPoissonDistr test1(1.473);
	EXPECT_FLOAT_EQ(test1.density(0), 0.2292367);
	TPoissonDistr test2(8.3872);
	EXPECT_FLOAT_EQ(test2.density(0), 0.0002277641);
	EXPECT_FLOAT_EQ(test2.density(1), 0.001910303);
	EXPECT_FLOAT_EQ(test2.density(2), 0.008011048);
	TPoissonDistr test3(99.);
	EXPECT_FLOAT_EQ(test3.density(100), 0.03966086);
}

TEST(TPoissonDistrTests, ppois) {
	EXPECT_FLOAT_EQ(TPoissonDistr::cumulativeDensity(0, 1.), 0.3678794);
	EXPECT_FLOAT_EQ(TPoissonDistr::cumulativeDensity(0, 1.473), 0.2292367);
	EXPECT_FLOAT_EQ(TPoissonDistr::cumulativeDensity(0, 8.3872), 0.0002277641);
	EXPECT_FLOAT_EQ(TPoissonDistr::cumulativeDensity(1, 8.3872), 0.002138067);
	EXPECT_FLOAT_EQ(TPoissonDistr::cumulativeDensity(2, 8.3872), 0.01014911522);
	EXPECT_FLOAT_EQ(TPoissonDistr::cumulativeDensity(100, 99.), 0.5663565);

	TPoissonDistr test(1.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0), 0.3678794);
	TPoissonDistr test1(1.473);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(0), 0.2292367);
	TPoissonDistr test2(8.3872);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(0), 0.0002277641);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(1), 0.002138067);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(2), 0.01014911522);
	TPoissonDistr test3(99.);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(100), 0.5663565);
}

TEST(TPoissonDistrTests, dpoisLog) {
	EXPECT_FLOAT_EQ(TPoissonDistr::logDensity(0, 1.), log(0.3678794));
	EXPECT_FLOAT_EQ(TPoissonDistr::logDensity(0, 1.473), log(0.2292367));
	EXPECT_FLOAT_EQ(TPoissonDistr::logDensity(0, 8.3872), log(0.0002277641));
	EXPECT_FLOAT_EQ(TPoissonDistr::logDensity(1, 8.3872), log(0.001910303));
	EXPECT_FLOAT_EQ(TPoissonDistr::logDensity(2, 8.3872), log(0.008011048));
	EXPECT_FLOAT_EQ(TPoissonDistr::logDensity(100, 99.), log(0.03966086));

	TPoissonDistr test(1.);
	EXPECT_FLOAT_EQ(test.logDensity(0), log(0.3678794));
	TPoissonDistr test1(1.473);
	EXPECT_FLOAT_EQ(test1.logDensity(0), log(0.2292367));
	TPoissonDistr test2(8.3872);
	EXPECT_FLOAT_EQ(test2.logDensity(0), log(0.0002277641));
	EXPECT_FLOAT_EQ(test2.logDensity(1), log(0.001910303));
	EXPECT_FLOAT_EQ(test2.logDensity(2), log(0.008011048));
	TPoissonDistr test3(99.);
	EXPECT_FLOAT_EQ(test3.logDensity(100), log(0.03966086));
}

TEST(TPoissonDistrTests, mean) {
	EXPECT_FLOAT_EQ(TPoissonDistr::mean(8.3872), 8.3872);

	TPoissonDistr test(8.3872);
	EXPECT_FLOAT_EQ(test.mean(), 8.3872);
}

TEST(TPoissonDistrTests, poisBools) {
	EXPECT_TRUE(TPoissonDistr::isDiscrete());
	EXPECT_FALSE(TPoissonDistr::isMultiVariate());
}

#include "gtest/gtest.h"

#include "coretools/Distributions/TBinomialDistr.h"
#include "coretools/Distributions/TBinomialDistrVariableN.h"

using namespace coretools;
using namespace probdist;

// binomial distribution variable N

TEST(TBinomialDistrTests, dbinomVariableNThrow) {
	// k > n
	EXPECT_ANY_THROW(TBinomialDistrVariableN::density(5, 6, 0.6_P));
	TBinomialDistrVariableN test1(0.6_P);
	EXPECT_ANY_THROW((void)test1.density(5, 6));
}

TEST(TBinomialDistrTests, dbinomVariableN) {
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::density(10, 8, 0.9_P), 0.1937102);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::density(1, 1, 0.9_P), 0.9);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::density(19, 0, 0.11_P), 0.10924716);

	TBinomialDistrVariableN test(0.9_P);
	EXPECT_FLOAT_EQ(test.density(10, 8), 0.1937102);
	EXPECT_FLOAT_EQ(test.density(1, 1), 0.9);
	TBinomialDistrVariableN test1(0.11_P);
	EXPECT_FLOAT_EQ(test1.density(19, 0), 0.10924716);
}

TEST(TBinomialDistrTests, dbinomVariableNLog) {
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::logDensity(10, 8, 0.9_P), -1.641392);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::logDensity(1, 1, 0.9_P), -0.1053605);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::logDensity(19, 0, 0.11_P), -2.214143);

	TBinomialDistrVariableN test(0.9_P);
	EXPECT_FLOAT_EQ(test.logDensity(10, 8), -1.641392);
	EXPECT_FLOAT_EQ(test.logDensity(1, 1), -0.1053605);
	TBinomialDistrVariableN test1(0.11_P);
	EXPECT_FLOAT_EQ(test1.logDensity(19, 0), -2.214143);
}

TEST(TBinomialDistrTests, pbinomVariableNThrow) {
	// k > n
	EXPECT_ANY_THROW(TBinomialDistrVariableN::cumulativeDensity(5, 6, 0.6_P));
	TBinomialDistrVariableN test(0.6_P);
	EXPECT_ANY_THROW((void)test.cumulativeDensity(5, 6));
}

TEST(TBinomialDistrTests, pbinomVariableN) {
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(1, 0, 0.3_P), 0.7);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(10, 0, 0.3_P), 0.02824752);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 90, 0.9_P), 0.5487098);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 90, 0.1_P), 1.);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 10, 0.1_P), 0.5831555);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 50, 0.6_P), 0.0270992);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 100, 0.6_P), 1.);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 100, 1._P), 1.);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::cumulativeDensity(100, 100, 0._P), 1.);

	TBinomialDistrVariableN test(0.3_P);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(1, 0), 0.7);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(10, 0), 0.02824752);
	TBinomialDistrVariableN test1(0.9_P);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(100, 90), 0.5487098);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(100, 100), 1.);
	TBinomialDistrVariableN test2(0.1_P);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(100, 90), 1.);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(100, 10), 0.5831555);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(100, 100), 1.);
	TBinomialDistrVariableN test3(0.6_P);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(100, 50), 0.0270992);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(100, 100), 1.);
}

TEST(TBinomialDistrTests, qbinomVariableN) {
	std::vector<double> ps = {0.01, 0.03, 0.05, 0.07, 0.09, 0.11, 0.13, 0.15, 0.17, 0.19, 0.21, 0.23, 0.25,
	                          0.27, 0.29, 0.31, 0.33, 0.35, 0.37, 0.39, 0.41, 0.43, 0.45, 0.47, 0.49, 0.51,
	                          0.53, 0.55, 0.57, 0.59, 0.61, 0.63, 0.65, 0.67, 0.69, 0.71, 0.73, 0.75, 0.77,
	                          0.79, 0.81, 0.83, 0.85, 0.87, 0.89, 0.91, 0.93, 0.95, 0.97, 0.99};

	std::vector<size_t> expected_n1_p05    = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	                                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
	std::vector<size_t> expected_n100_p05  = {38, 41, 42, 43, 43, 44, 44, 45, 45, 46, 46, 46, 47, 47, 47, 48, 48,
	                                          48, 48, 49, 49, 49, 49, 50, 50, 50, 50, 51, 51, 51, 51, 52, 52, 52,
	                                          52, 53, 53, 53, 54, 54, 54, 55, 55, 56, 56, 57, 57, 58, 59, 62};
	std::vector<size_t> expected_n100_p099 = {96,  97,  97,  97,  98,  98,  98,  98,  98,  98,  98,  98,  98,
	                                          99,  99,  99,  99,  99,  99,  99,  99,  99,  99,  99,  99,  99,
	                                          99,  99,  99,  99,  99,  99,  100, 100, 100, 100, 100, 100, 100,
	                                          100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
	std::vector<size_t> expected_n100_p001 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	                                          0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	                                          1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4};

	for (size_t i = 0; i < ps.size(); i++) {
		EXPECT_EQ(TBinomialDistrVariableN::invCumulativeDensity(ps[i], 0, 0.5_P), 0);
		EXPECT_EQ(TBinomialDistrVariableN::invCumulativeDensity(ps[i], 1, 0.5_P), expected_n1_p05[i]);
		EXPECT_EQ(TBinomialDistrVariableN::invCumulativeDensity(ps[i], 100, 0.5_P), expected_n100_p05[i]);
		EXPECT_EQ(TBinomialDistrVariableN::invCumulativeDensity(ps[i], 100, 0.99_P), expected_n100_p099[i]);
		EXPECT_EQ(TBinomialDistrVariableN::invCumulativeDensity(ps[i], 100, 0.01_P), expected_n100_p001[i]);
	}
}

TEST(TBinomialDistrTests, binomMeanVariableN) {
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::mean(20, 0._P), 0.);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::mean(1000, 0.2_P), 200);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::mean(10, 0.9_P), 9);
	EXPECT_FLOAT_EQ(TBinomialDistrVariableN::mean(0, 1._P), 0.);

	TBinomialDistrVariableN test(0._P);
	EXPECT_FLOAT_EQ(test.mean(20), 0.);
	TBinomialDistrVariableN test1(0.2_P);
	EXPECT_FLOAT_EQ(test1.mean(1000), 200);
	TBinomialDistrVariableN test2(0.9_P);
	EXPECT_FLOAT_EQ(test2.mean(10), 9);
	TBinomialDistrVariableN test3(1._P);
	EXPECT_FLOAT_EQ(test3.mean(0), 0.);
}

TEST(TBinomialDistrTests, binomVariableNBools) {
	EXPECT_TRUE(TBinomialDistrVariableN::isDiscrete());
	EXPECT_FALSE(TBinomialDistrVariableN::isMultiVariate());
}

// binomial distribution fixed N

TEST(TBinomialDistrTests, dbinomThrow) {
	// k > n
	EXPECT_ANY_THROW(TBinomialDistr::density(5, 6, 0.6_P));
	TBinomialDistr test1(5, 0.6_P);
	EXPECT_ANY_THROW((void)test1.density(6));
}

TEST(TBinomialDistrTests, dbinom) {
	EXPECT_FLOAT_EQ(TBinomialDistr::density(10, 8, 0.9_P), 0.1937102);
	EXPECT_FLOAT_EQ(TBinomialDistr::density(1, 1, 0.9_P), 0.9);
	EXPECT_FLOAT_EQ(TBinomialDistr::density(19, 0, 0.11_P), 0.10924716);

	TBinomialDistr test(10, 0.9_P);
	EXPECT_FLOAT_EQ(test.density(8), 0.1937102);
	TBinomialDistr test1(1, 0.9_P);
	EXPECT_FLOAT_EQ(test1.density(1), 0.9);
	TBinomialDistr test2(19, 0.11_P);
	EXPECT_FLOAT_EQ(test2.density(0), 0.10924716);
}

TEST(TBinomialDistrTests, dbinomLog) {
	EXPECT_FLOAT_EQ(TBinomialDistr::logDensity(10, 8, 0.9_P), -1.641392);
	EXPECT_FLOAT_EQ(TBinomialDistr::logDensity(1, 1, 0.9_P), -0.1053605);
	EXPECT_FLOAT_EQ(TBinomialDistr::logDensity(19, 0, 0.11_P), -2.214143);

	TBinomialDistr test(10, 0.9_P);
	EXPECT_FLOAT_EQ(test.logDensity(8), -1.641392);
	TBinomialDistr test1(1, 0.9_P);
	EXPECT_FLOAT_EQ(test1.logDensity(1), -0.1053605);
	TBinomialDistr test2(19, 0.11_P);
	EXPECT_FLOAT_EQ(test2.logDensity(0), -2.214143);
}

TEST(TBinomialDistrTests, pbinomThrow) {
	// k > n
	EXPECT_ANY_THROW(TBinomialDistr::cumulativeDensity(5, 6, 0.6_P));
	TBinomialDistr test(5, 0.6_P);
	EXPECT_ANY_THROW((void)test.cumulativeDensity(6));
}

TEST(TBinomialDistrTests, pbinom) {
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(1, 0, 0.3_P), 0.7);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(10, 0, 0.3_P), 0.02824752);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 90, 0.9_P), 0.5487098);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 90, 0.1_P), 1.);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 10, 0.1_P), 0.5831555);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 50, 0.6_P), 0.0270992);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 100, 0.6_P), 1.);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 100, 1._P), 1.);
	EXPECT_FLOAT_EQ(TBinomialDistr::cumulativeDensity(100, 100, 0._P), 1.);

	TBinomialDistr test(1, 0.3_P);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0), 0.7);
	TBinomialDistr test1(10, 0.3_P);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(0), 0.02824752);
	TBinomialDistr test2(100, 0.9_P);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(90), 0.5487098);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(100), 1.);
	TBinomialDistr test3(100, 0.1_P);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(90), 1.);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(10), 0.5831555);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(100), 1.);
	TBinomialDistr test4(100, 0.6_P);
	EXPECT_FLOAT_EQ(test4.cumulativeDensity(50), 0.0270992);
	EXPECT_FLOAT_EQ(test4.cumulativeDensity(100), 1.);
}

TEST(TBinomialDistrTests, TBinomialDistr_qbinom) {
	std::vector<double> ps = {0.01, 0.03, 0.05, 0.07, 0.09, 0.11, 0.13, 0.15, 0.17, 0.19, 0.21, 0.23, 0.25,
	                          0.27, 0.29, 0.31, 0.33, 0.35, 0.37, 0.39, 0.41, 0.43, 0.45, 0.47, 0.49, 0.51,
	                          0.53, 0.55, 0.57, 0.59, 0.61, 0.63, 0.65, 0.67, 0.69, 0.71, 0.73, 0.75, 0.77,
	                          0.79, 0.81, 0.83, 0.85, 0.87, 0.89, 0.91, 0.93, 0.95, 0.97, 0.99};

	std::vector<size_t> expected_n1_p05    = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	                                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
	std::vector<size_t> expected_n100_p05  = {38, 41, 42, 43, 43, 44, 44, 45, 45, 46, 46, 46, 47, 47, 47, 48, 48,
	                                          48, 48, 49, 49, 49, 49, 50, 50, 50, 50, 51, 51, 51, 51, 52, 52, 52,
	                                          52, 53, 53, 53, 54, 54, 54, 55, 55, 56, 56, 57, 57, 58, 59, 62};
	std::vector<size_t> expected_n100_p099 = {96,  97,  97,  97,  98,  98,  98,  98,  98,  98,  98,  98,  98,
	                                          99,  99,  99,  99,  99,  99,  99,  99,  99,  99,  99,  99,  99,
	                                          99,  99,  99,  99,  99,  99,  100, 100, 100, 100, 100, 100, 100,
	                                          100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
	std::vector<size_t> expected_n100_p001 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	                                          0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	                                          1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4};

	TBinomialDistr n1(0, 0.5_P);
	TBinomialDistr n2(1, 0.5_P);
	TBinomialDistr n3(100, 0.5_P);
	TBinomialDistr n4(100, 0.99_P);
	TBinomialDistr n5(100, 0.01_P);

	for (size_t i = 0; i < ps.size(); i++) {
		EXPECT_EQ(n1.invCumulativeDensity(ps[i], 0, 0.5_P), 0);
		EXPECT_EQ(n2.invCumulativeDensity(ps[i], 1, 0.5_P), expected_n1_p05[i]);
		EXPECT_EQ(n3.invCumulativeDensity(ps[i], 100, 0.5_P), expected_n100_p05[i]);
		EXPECT_EQ(n4.invCumulativeDensity(ps[i], 100, 0.99_P), expected_n100_p099[i]);
		EXPECT_EQ(n5.invCumulativeDensity(ps[i], 100, 0.01_P), expected_n100_p001[i]);
	}
}

TEST(TBinomialDistrTests, binomMean) {
	EXPECT_FLOAT_EQ(TBinomialDistr::mean(20, 0._P), 0.);
	EXPECT_FLOAT_EQ(TBinomialDistr::mean(1000, 0.2_P), 200);
	EXPECT_FLOAT_EQ(TBinomialDistr::mean(10, 0.9_P), 9);
	EXPECT_FLOAT_EQ(TBinomialDistr::mean(0, 1._P), 0.);

	TBinomialDistr test(20, 0._P);
	EXPECT_FLOAT_EQ(test.mean(), 0._P);
	TBinomialDistr test1(1000, 0.2_P);
	EXPECT_FLOAT_EQ(test1.mean(), 200);
	TBinomialDistr test2(10, 0.9_P);
	EXPECT_FLOAT_EQ(test2.mean(), 9);
	TBinomialDistr test3(0, 1._P);
	EXPECT_FLOAT_EQ(test3.mean(), 0.);
}

TEST(TBinomialDistrTests, binomBools) {
	EXPECT_TRUE(TBinomialDistr::isDiscrete());
	EXPECT_FALSE(TBinomialDistr::isMultiVariate());
}

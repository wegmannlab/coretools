#include "gtest/gtest.h"

#include "coretools/Distributions/TMultivariateNormalDistr.h"

using namespace coretools;
using namespace probdist;

// multivariate normal distribution

auto getMuSigmaMVN_1D() {
	size_t K               = 1;
	std::vector<double> mu = {0.278860370628536};
	arma::mat Sigma(K, K);
	Sigma(0, 0) = 2.32;
	return std::make_pair(mu, Sigma);
}

auto getMuSigmaMVN_3D() {
	size_t K                       = 3;
	std::vector<double> mu         = {0.740568845532835, 0.6502931562718, 0.112098386511207};
	std::vector<double> sigma_vals = {0.538685301270347,   0.133225979835782,   -0.0567380277903317,
	                                  0.133225979835782,   0.417507249360392,   -0.0469386472422036,
	                                  -0.0567380277903317, -0.0469386472422036, 0.192504279399791};
	arma::mat Sigma(K, K);
	for (size_t j = 0; j < K; j++) {
		for (size_t k = 0; k < K; k++) { Sigma(j, k) = sigma_vals[j * K + k]; }
	}
	return std::make_pair(mu, Sigma);
}

auto getMuSigmaMVN_Singular_3D() {
	size_t K                       = 3;
	std::vector<double> mu         = {0.740568845532835, 0.6502931562718, 0.112098386511207};
	std::vector<double> sigma_vals = {0.538685301270347, 0.133225979835782, 0.133225979835782,
	                                  0.133225979835782, 0.417507249360392, 0.417507249360392,
	                                  0.133225979835782, 0.417507249360392, 0.417507249360392};
	arma::mat Sigma(K, K);
	for (size_t j = 0; j < K; j++) {
		for (size_t k = 0; k < K; k++) { Sigma(j, k) = sigma_vals[j * K + k]; }
	}
	return std::make_pair(mu, Sigma);
}

TEST(TMultivariateNormalDistrTests, MVN_density_1D) {
	auto [mu, sigma] = getMuSigmaMVN_1D();

	std::vector<double> x1 = {0.905713662272319};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x1)>::density(x1, mu, sigma), 0.2406508);
	std::vector<double> x2 = {-0.000130202852505906};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x2)>::density(x2, mu, sigma), 0.2575614);
	std::vector<double> x3 = {-1.40930869212447};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x3)>::density(x3, mu, sigma), 0.1417163);

	TMultivariateNormalDistr<decltype(x1)> test(mu, sigma);
	EXPECT_FLOAT_EQ(test.density(x1), 0.2406508);
	EXPECT_FLOAT_EQ(test.density(x2), 0.2575614);
	EXPECT_FLOAT_EQ(test.density(x3), 0.1417163);
}

TEST(TMultivariateNormalDistrTests, MVN_density_3D) {
	auto [mu, sigma] = getMuSigmaMVN_3D();

	std::vector<double> x1 = {0.905713662272319, 0.0509292869828641, 0.371122959069908};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x1)>::density(x1, mu, sigma), 0.1629365);
	std::vector<double> x2 = {-0.000130202852505906, -0.000142203918037065, -0.00011858405551011};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x2)>::density(x2, mu, sigma), 0.1274769);
	std::vector<double> x3 = {0.918755614361442, -1.40930869212447, -1.29314488833492};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x3)>::density(x3, mu, sigma), 1.259946e-06);

	TMultivariateNormalDistr<decltype(x1)> test(mu, sigma);
	EXPECT_FLOAT_EQ(test.density(x1), 0.1629365);
	EXPECT_FLOAT_EQ(test.density(x2), 0.1274769);
	EXPECT_FLOAT_EQ(test.density(x3), 1.259946e-06);
}

TEST(TMultivariateNormalDistrTests, MVN_density_singular_3D) {
	auto [mu, sigma] = getMuSigmaMVN_Singular_3D();

	std::vector<double> x1 = {0.905713662272319, 0.0509292869828641, 0.371122959069908};
	EXPECT_FLOAT_EQ((TMultivariateNormalDistr<decltype(x1), true>::density(x1, mu, sigma)), 0.09076746198);
	std::vector<double> x2 = {-0.000130202852505906, -0.000142203918037065, -0.00011858405551011};
	EXPECT_FLOAT_EQ((TMultivariateNormalDistr<decltype(x2), true>::density(x2, mu, sigma)), 0.056330071);
	std::vector<double> x3 = {0.918755614361442, -1.40930869212447, -1.29314488833492};
	EXPECT_FLOAT_EQ((TMultivariateNormalDistr<decltype(x3), true>::density(x3, mu, sigma)), 0.001582064408);

	TMultivariateNormalDistr<decltype(x1), true> test(mu, sigma);
	EXPECT_FLOAT_EQ(test.density(x1), 0.09076746198);
	EXPECT_FLOAT_EQ(test.density(x2), 0.056330071);
	EXPECT_FLOAT_EQ(test.density(x3), 0.001582064408);
}

TEST(TMultivariateNormalDistrTests, MVN_logDensity_1D) {
	auto [mu, sigma] = getMuSigmaMVN_1D();

	std::vector<double> x1 = {0.905713662272319};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x1)>::logDensity(x1, mu, sigma), log(0.2406508));
	std::vector<double> x2 = {-0.000130202852505906};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x2)>::logDensity(x2, mu, sigma), log(0.2575614));
	std::vector<double> x3 = {-1.40930869212447};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x3)>::logDensity(x3, mu, sigma), log(0.1417163));

	TMultivariateNormalDistr<decltype(x1)> test(mu, sigma);
	EXPECT_FLOAT_EQ(test.logDensity(x1), log(0.2406508));
	EXPECT_FLOAT_EQ(test.logDensity(x2), log(0.2575614));
	EXPECT_FLOAT_EQ(test.logDensity(x3), log(0.1417163));
}

TEST(TMultivariateNormalDistrTests, MVN_logDensity_3D) {
	auto [mu, sigma] = getMuSigmaMVN_3D();

	std::vector<double> x1 = {0.905713662272319, 0.0509292869828641, 0.371122959069908};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x1)>::logDensity(x1, mu, sigma), log(0.1629365));
	std::vector<double> x2 = {-0.000130202852505906, -0.000142203918037065, -0.00011858405551011};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x2)>::logDensity(x2, mu, sigma), log(0.1274769));
	std::vector<double> x3 = {0.918755614361442, -1.40930869212447, -1.29314488833492};
	EXPECT_FLOAT_EQ(TMultivariateNormalDistr<decltype(x3)>::logDensity(x3, mu, sigma), log(1.259946e-06));

	TMultivariateNormalDistr<decltype(x1)> test(mu, sigma);
	EXPECT_FLOAT_EQ(test.logDensity(x1), log(0.1629365));
	EXPECT_FLOAT_EQ(test.logDensity(x2), log(0.1274769));
	EXPECT_FLOAT_EQ(test.logDensity(x3), log(1.259946e-06));
}

TEST(TMultivariateNormalDistrTests, MVN_logDensity_singular_3D) {
	auto [mu, sigma] = getMuSigmaMVN_Singular_3D();

	std::vector<double> x1 = {0.905713662272319, 0.0509292869828641, 0.371122959069908};
	EXPECT_FLOAT_EQ((TMultivariateNormalDistr<decltype(x1), true>::logDensity(x1, mu, sigma)), log(0.09076746198));
	std::vector<double> x2 = {-0.000130202852505906, -0.000142203918037065, -0.00011858405551011};
	EXPECT_FLOAT_EQ((TMultivariateNormalDistr<decltype(x2), true>::logDensity(x2, mu, sigma)), log(0.056330071));
	std::vector<double> x3 = {0.918755614361442, -1.40930869212447, -1.29314488833492};
	EXPECT_FLOAT_EQ((TMultivariateNormalDistr<decltype(x3), true>::logDensity(x3, mu, sigma)), log(0.001582064408));

	TMultivariateNormalDistr<decltype(x1), true> test(mu, sigma);
	EXPECT_FLOAT_EQ(test.logDensity(x1), log(0.09076746198));
	EXPECT_FLOAT_EQ(test.logDensity(x2), log(0.056330071));
	EXPECT_FLOAT_EQ(test.logDensity(x3), log(0.001582064408));
}

#include "gtest/gtest.h"

#include "coretools/Distributions/TBetaDistr.h"

using namespace coretools;
using namespace probdist;

// beta distribution

TEST(TBetaDistrTests, dbeta) {
	EXPECT_FLOAT_EQ(TBetaDistr::density(0.1_P, 0.7, 0.7), 1.08441);
	EXPECT_FLOAT_EQ(TBetaDistr::density(0.00000001_P, 0.1, 2.6), 1800021);
	EXPECT_FLOAT_EQ(TBetaDistr::density(0.1_P, 28, 99), 0.007578113);

	TBetaDistr test(0.7, 0.7);
	EXPECT_FLOAT_EQ(test.density(0.1_P), 1.08441);
	TBetaDistr test1(0.1, 2.6);
	EXPECT_FLOAT_EQ(test1.density(0.00000001_P), 1800021);
	TBetaDistr test2(28, 99);
	EXPECT_FLOAT_EQ(test2.density(0.1_P), 0.007578113);
}

TEST(TBetaDistrTests, dbetaLog) {
	EXPECT_FLOAT_EQ(TBetaDistr::logDensity(0.1_P, 0.7, 0.7), 0.08103628);
	EXPECT_FLOAT_EQ(TBetaDistr::logDensity(0.00000001_P, 0.1, 2.6), 14.40331);
	EXPECT_FLOAT_EQ(TBetaDistr::logDensity(0.1_P, 28, 99), -4.882491);

	TBetaDistr test(0.7, 0.7);
	EXPECT_FLOAT_EQ(test.logDensity(0.1_P), 0.08103628);
	TBetaDistr test1(0.1, 2.6);
	EXPECT_FLOAT_EQ(test1.logDensity(0.00000001_P), 14.40331);
	TBetaDistr test2(28, 99);
	EXPECT_FLOAT_EQ(test2.logDensity(0.1_P), -4.882491);
}

TEST(TBetaDistrTests, pbeta) {
	EXPECT_FLOAT_EQ(TBetaDistr::cumulativeDensity(0.1_P, 0.7, 0.7), 0.1520303);
	EXPECT_FLOAT_EQ(TBetaDistr::cumulativeDensity(0.00000001_P, 0.1, 2.6), 0.1800021);
	EXPECT_FLOAT_EQ(TBetaDistr::cumulativeDensity(0.1_P, 28, 99), 0.00004273548);

	TBetaDistr test(0.7, 0.7);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.1_P), 0.1520303);
	TBetaDistr test1(0.1, 2.6);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(0.00000001_P), 0.1800021);
	TBetaDistr test2(28, 99);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(0.1_P), 0.00004273548);
}

TEST(TBetaDistrTests, betaCalculateAlphaBetaForGivenMeanVar) {
	auto mean    = P(0.1);
	double var         = 0.01;
	auto [alpha, beta] = TBetaDistr::calculateAlphaBetaForGivenMeanVar(mean, var);
	EXPECT_FLOAT_EQ(TBetaDistr::mean(alpha, beta), mean);
	EXPECT_FLOAT_EQ(TBetaDistr::var(alpha, beta), var);

	mean                  = P(0.99);
	var                   = 0.001;
	std::tie(alpha, beta) = TBetaDistr::calculateAlphaBetaForGivenMeanVar(mean, var);
	EXPECT_FLOAT_EQ(TBetaDistr::mean(alpha, beta), mean);
	EXPECT_FLOAT_EQ(TBetaDistr::var(alpha, beta), var);

	mean                  = P(0.5);
	var                   = 0.2;
	std::tie(alpha, beta) = TBetaDistr::calculateAlphaBetaForGivenMeanVar(mean, var);
	EXPECT_FLOAT_EQ(TBetaDistr::mean(alpha, beta), mean);
	EXPECT_FLOAT_EQ(TBetaDistr::var(alpha, beta), var);
}

TEST(TBetaDistrTests, betaMean) {
	EXPECT_FLOAT_EQ(TBetaDistr::mean(1., 0.7), 0.5882352941176471);
	EXPECT_FLOAT_EQ(TBetaDistr::mean(0.1, 30.), 0.0033222591362126);
	EXPECT_FLOAT_EQ(TBetaDistr::mean(13000., 28.), 0.9978507829290758);

	TBetaDistr test(1., 0.7);
	EXPECT_FLOAT_EQ(test.mean(), 0.5882352941176471);
	TBetaDistr test1(0.1, 30.);
	EXPECT_FLOAT_EQ(test1.mean(), 0.0033222591362126);
	TBetaDistr test2(13000., 28.);
	EXPECT_FLOAT_EQ(test2.mean(), 0.9978507829290758);
}

TEST(TBetaDistrTests, betaBools) {
	EXPECT_FALSE(TBetaDistr::isDiscrete());
	EXPECT_FALSE(TBetaDistr::isMultiVariate());
}

#include "gtest/gtest.h"

#include "coretools/Distributions/TParetoDistr.h"

using namespace coretools;
using namespace probdist;

TEST(TParetoDistrTests, dgpdThrow) {
	// x < mu
	EXPECT_ANY_THROW(TParetoDistr::density(1., 2., 3., 10.));
	// shapeXi < 0.0 && x > locationMu - scaleSigma / shapeXi
	EXPECT_ANY_THROW(TParetoDistr::density(100., 2., 3., -1.));
}

TEST(TParetoDistrTests, dgpd) {
	EXPECT_FLOAT_EQ(TParetoDistr::density(10., 1., 10., 20.), 0.004542644);
	EXPECT_FLOAT_EQ(TParetoDistr::density(10001., 10000., 1., 2.), 0.1924501);
	EXPECT_FLOAT_EQ(TParetoDistr::density(90000., 89999., 10., -1.), 0.1);

	TParetoDistr test(1., 10., 20.);
	EXPECT_FLOAT_EQ(test.density(10.), 0.004542644);
	TParetoDistr test1(10000., 1., 2.);
	EXPECT_FLOAT_EQ(test1.density(10001.), 0.1924501);
	TParetoDistr test2(89999., 10., -1.);
	EXPECT_FLOAT_EQ(test2.density(90000.), 0.1);
}

TEST(TParetoDistrTests, dgpdLog) {
	EXPECT_FLOAT_EQ(TParetoDistr::logDensity(10., 1., 10., 20.), log(0.004542644));
	EXPECT_FLOAT_EQ(TParetoDistr::logDensity(10001., 10000., 1., 2.), log(0.1924501));
	EXPECT_FLOAT_EQ(TParetoDistr::logDensity(90000., 89999., 10., -1.), log(0.1));

	TParetoDistr test(1., 10., 20.);
	EXPECT_FLOAT_EQ(test.logDensity(10.), log(0.004542644));
	TParetoDistr test1(10000., 1., 2.);
	EXPECT_FLOAT_EQ(test1.logDensity(10001.), log(0.1924501));
	TParetoDistr test2(89999., 10., -1.);
	EXPECT_FLOAT_EQ(test2.logDensity(90000.), log(0.1));
}

TEST(TParetoDistrTests, pgpd) {
	EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDensity(10., 1., 10., 20.), 0.1368976);
	EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDensity(10001., 10000., 1., 2.), 0.4226497);
	EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDensity(90000., 89999., 10., -1.), 0.1);

	TParetoDistr test(1., 10., 20.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(10.), 0.1368976);
	TParetoDistr test1(10000., 1., 2.);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(10001.), 0.4226497);
	TParetoDistr test2(89999., 10., -1.);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(90000.), 0.1);
}

TEST(TParetoDistrTests, dgpdMeanThrow) {
	// shapeXi == 1
	EXPECT_ANY_THROW(TParetoDistr::mean(2., 3., 1.));
	// shapeXi > 1
	EXPECT_ANY_THROW(TParetoDistr::mean(2., 3., 200));

	TParetoDistr test(2., 3., 1.);
	EXPECT_ANY_THROW((void)test.mean());
	TParetoDistr test1(2., 3., 200.);
	EXPECT_ANY_THROW((void)test.mean());
}

TEST(TParetoDistrTests, dgpdMean) {
	EXPECT_FLOAT_EQ(TParetoDistr::mean(1., 10., 0.2), 13.5);
	EXPECT_FLOAT_EQ(TParetoDistr::mean(10000., 1., 0.02), 10001.02);
	EXPECT_FLOAT_EQ(TParetoDistr::mean(89999., 10., -1.), 90004);

	TParetoDistr test(1., 10., 0.2);
	EXPECT_FLOAT_EQ(test.mean(), 13.5);
	TParetoDistr test1(10000., 1., 0.02);
	EXPECT_FLOAT_EQ(test1.mean(), 10001.02);
	TParetoDistr test2(89999., 10., -1.);
	EXPECT_FLOAT_EQ(test2.mean(), 90004);
}


#include "gtest/gtest.h"

#include "coretools/Distributions/TNormalDistr.h"

using namespace coretools;
using namespace probdist;
// normal distribution

TEST(TNormalDistrTests, dnorm) {
	EXPECT_FLOAT_EQ(TNormalDistr::density(0., 0., 1.), 0.3989423);
	EXPECT_FLOAT_EQ(TNormalDistr::density(-10, 9.3, sqrt(50.41)), 0.001396708);
	EXPECT_FLOAT_EQ(TNormalDistr::density(92.91, 88.2, sqrt(9.61)), 0.04057674);

	TNormalDistr test(0., 1.);
	EXPECT_FLOAT_EQ(test.density(0.), 0.3989423);
	TNormalDistr test1(9.3, sqrt(50.41));
	EXPECT_FLOAT_EQ(test1.density(-10), 0.001396708);
	TNormalDistr test2(88.2, sqrt(9.61));
	EXPECT_FLOAT_EQ(test2.density(92.91), 0.04057674);
}

TEST(TNormalDistrTests, dnormLog) {
	EXPECT_FLOAT_EQ(TNormalDistr::logDensity(0., 0., 1.), -0.9189385);
	EXPECT_FLOAT_EQ(TNormalDistr::logDensity(-10, 9.3, sqrt(50.41)), -6.573638);
	EXPECT_FLOAT_EQ(TNormalDistr::logDensity(92.91, 88.2, sqrt(9.61)), -3.20456);

	TNormalDistr test(0., 1.);
	EXPECT_FLOAT_EQ(test.logDensity(0.), -0.9189385);
	TNormalDistr test1(9.3, sqrt(50.41));
	EXPECT_FLOAT_EQ(test1.logDensity(-10), -6.573638);
	TNormalDistr test2(88.2, sqrt(9.61));
	EXPECT_FLOAT_EQ(test2.logDensity(92.91), -3.20456);
}

TEST(TNormalDistrTests, pnorm) {
	EXPECT_FLOAT_EQ(TNormalDistr::cumulativeDensity(0., 0., 1.), 0.5);
	EXPECT_FLOAT_EQ(TNormalDistr::cumulativeDensity(-10, 9.3, sqrt(50.41)), 0.003280818);
	EXPECT_FLOAT_EQ(TNormalDistr::cumulativeDensity(92.91, 88.2, sqrt(9.61)), 0.9356634);

	TNormalDistr test(0., 1.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.), 0.5);
	TNormalDistr test1(9.3, sqrt(50.41));
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(-10), 0.003280818);
	TNormalDistr test2(88.2, sqrt(9.61));
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(92.91), 0.9356634);
}

TEST(TNormalDistrTests, qnorm) {
	EXPECT_FLOAT_EQ(TNormalDistr::invCumulativeDensity(0.1, 0., 1.), -1.281552);
	EXPECT_FLOAT_EQ(TNormalDistr::invCumulativeDensity(0.3, 9.3, sqrt(50.41)), 5.576756);
	EXPECT_FLOAT_EQ(TNormalDistr::invCumulativeDensity(0.63, 88.2, sqrt(9.61)), 89.22875);

	TNormalDistr test(0., 1.);
	EXPECT_FLOAT_EQ(test.invCumulativeDensity(0.1), -1.281552);
	TNormalDistr test1(9.3, sqrt(50.41));
	EXPECT_FLOAT_EQ(test1.invCumulativeDensity(0.3), 5.576756);
	TNormalDistr test2(88.2, sqrt(9.61));
	EXPECT_FLOAT_EQ(test2.invCumulativeDensity(0.63), 89.22875);
}

TEST(TNormalDistrTests, normMean) {
	EXPECT_FLOAT_EQ(TNormalDistr::mean(0.1), 0.1);
	EXPECT_FLOAT_EQ(TNormalDistr::mean(100), 100);
	EXPECT_FLOAT_EQ(TNormalDistr::mean(1000000), 1000000);

	TNormalDistr test(0.1, 0.9);
	EXPECT_FLOAT_EQ(test.mean(), 0.1);
	TNormalDistr test1(100, 50);
	EXPECT_FLOAT_EQ(test1.mean(), 100);
	TNormalDistr test2(1000000, 0.1);
	EXPECT_FLOAT_EQ(test2.mean(), 1000000);
}

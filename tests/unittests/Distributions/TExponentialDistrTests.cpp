#include "gtest/gtest.h"
#include "coretools/Distributions/TExponentialDistr.h"

using namespace coretools;
using namespace probdist;

TEST(TExponentialDistrTests, dexp) {
	EXPECT_FLOAT_EQ(TExponentialDistr::density(0., 1.), 1.);
	EXPECT_FLOAT_EQ(TExponentialDistr::density(0.92, 10.), 0.001010394);
	EXPECT_FLOAT_EQ(TExponentialDistr::density(0.0001, 73.2), 72.66613);

	TExponentialDistr test(1.);
	EXPECT_FLOAT_EQ(test.density(0.), 1.);
	TExponentialDistr test1(10.);
	EXPECT_FLOAT_EQ(test1.density(0.92), 0.001010394);
	TExponentialDistr test2(73.2);
	EXPECT_FLOAT_EQ(test2.density(0.0001), 72.66613);
}

TEST(TExponentialDistrTests, pexp) {
	EXPECT_FLOAT_EQ(TExponentialDistr::cumulativeDensity(0., 1.), 0.);
	EXPECT_FLOAT_EQ(TExponentialDistr::cumulativeDensity(0.92, 10.), 0.999899);
	EXPECT_FLOAT_EQ(TExponentialDistr::cumulativeDensity(0.0001, 73.2), 0.007293274);

	TExponentialDistr test(1.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.), 0.);
	TExponentialDistr test1(10.);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(0.92), 0.999899);
	TExponentialDistr test2(73.2);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(0.0001), 0.007293274);
}

TEST(TExponentialDistrTests, dexpLog) {
	EXPECT_FLOAT_EQ(TExponentialDistr::logDensity(0., 1.), 0.);
	EXPECT_FLOAT_EQ(TExponentialDistr::logDensity(0.92, 10.), -6.897415);
	EXPECT_FLOAT_EQ(TExponentialDistr::logDensity(0.0001, 73.2), 4.285875);

	TExponentialDistr test(1.);
	EXPECT_FLOAT_EQ(test.logDensity(0.), 0.);
	TExponentialDistr test1(10.);
	EXPECT_FLOAT_EQ(test1.logDensity(0.92), -6.897415);
	TExponentialDistr test2(73.2);
	EXPECT_FLOAT_EQ(test2.logDensity(0.0001), 4.285875);
}

TEST(TExponentialDistrTests, expMean) {
	EXPECT_FLOAT_EQ(TExponentialDistr::mean(0.1), 10.);
	EXPECT_FLOAT_EQ(TExponentialDistr::mean(1.), 1);
	EXPECT_FLOAT_EQ(TExponentialDistr::mean(0.4), 2.5);
	EXPECT_FLOAT_EQ(TExponentialDistr::mean(1000), 0.001);

	TExponentialDistr test(0.1);
	EXPECT_FLOAT_EQ(test.mean(), 10.);
	TExponentialDistr test1(1.);
	EXPECT_FLOAT_EQ(test1.mean(), 1);
	TExponentialDistr test2(0.4);
	EXPECT_FLOAT_EQ(test2.mean(), 2.5);
	TExponentialDistr test3(1000);
	EXPECT_FLOAT_EQ(test3.mean(1000), 0.001);
}

TEST(TExponentialDistrTests, expBools) {
	EXPECT_FALSE(TExponentialDistr::isDiscrete());
	EXPECT_FALSE(TExponentialDistr::isMultiVariate());
}


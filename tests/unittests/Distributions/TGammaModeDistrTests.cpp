/*
 * TProbabilityDistUnitTests.cpp
 *
 *  Created on: Jul 7, 2022
 *      Author: raphael
 */
#include "gtest/gtest.h"

#include "coretools/Distributions/TGammaModeDistr.h"

using namespace coretools;
using namespace probdist;


// gamma mode distribution

TEST(TGammaModeDistrTests, dgammaMode) {
	EXPECT_FLOAT_EQ(TGammaModeDistr::density(0.1, 9, 10), 2.493489e-15);
	EXPECT_FLOAT_EQ(TGammaModeDistr::density(5, 9, 10), 0.03626558);
	EXPECT_FLOAT_EQ(TGammaModeDistr::density(0.5, 90, 1000), 5.119792e-19);
	EXPECT_FLOAT_EQ(TGammaModeDistr::density(0.5, 0.09, 0.001), 1.038109e-10);
	EXPECT_FLOAT_EQ(TGammaModeDistr::density(0.0000001, 0.0015, 2.5e-06), 0.0007521776);

	TGammaModeDistr test(9, 10);
	EXPECT_FLOAT_EQ(test.density(0.1), 2.493489e-15);
	EXPECT_FLOAT_EQ(test.density(5), 0.03626558);
	TGammaModeDistr test1(90, 1000);
	EXPECT_FLOAT_EQ(test1.density(0.5), 5.119792e-19);
	TGammaModeDistr test2(0.09, 0.001);
	EXPECT_FLOAT_EQ(test2.density(0.5), 1.038109e-10);
	TGammaModeDistr test3(0.0015, 2.5e-06);
	EXPECT_FLOAT_EQ(test3.density(0.0000001), 0.0007521776);
}

TEST(TGammaModeDistrTests, dgammaModeLog) {
	EXPECT_FLOAT_EQ(TGammaModeDistr::logDensity(0.1, 9, 10), log(2.493489e-15));
	EXPECT_FLOAT_EQ(TGammaModeDistr::logDensity(5, 9, 10), log(0.03626558));
	EXPECT_FLOAT_EQ(TGammaModeDistr::logDensity(0.5, 90, 1000), log(5.119792e-19));
	EXPECT_FLOAT_EQ(TGammaModeDistr::logDensity(0.5, 0.09, 0.001), log(1.038109e-10));
	EXPECT_FLOAT_EQ(TGammaModeDistr::logDensity(0.0000001, 0.0015, 2.5e-06), log(0.0007521776));

	TGammaModeDistr test(9, 10);
	EXPECT_FLOAT_EQ(test.logDensity(0.1), log(2.493489e-15));
	EXPECT_FLOAT_EQ(test.logDensity(5), log(0.03626558));
	TGammaModeDistr test1(90, 1000);
	EXPECT_FLOAT_EQ(test1.logDensity(0.5), log(5.119792e-19));
	TGammaModeDistr test2(0.09, 0.001);
	EXPECT_FLOAT_EQ(test2.logDensity(0.5), log(1.038109e-10));
	TGammaModeDistr test3(0.0015, 2.5e-06);
	EXPECT_FLOAT_EQ(test3.logDensity(0.0000001), log(0.0007521776));
}

TEST(TGammaModeDistrTests, pgammaMode) {
	EXPECT_FLOAT_EQ(TGammaModeDistr::cumulativeDensity(0.1, 9, 10), 2.516348e-17);
	EXPECT_FLOAT_EQ(TGammaModeDistr::cumulativeDensity(5, 9, 10), 0.03182806);
	EXPECT_FLOAT_EQ(TGammaModeDistr::cumulativeDensity(0.5, 90, 1000), 2.57158e-20);
	EXPECT_FLOAT_EQ(TGammaModeDistr::cumulativeDensity(0.5, 0.09, 0.01), 0.9927348);
	EXPECT_FLOAT_EQ(TGammaModeDistr::cumulativeDensity(0.0000001, 0.0015, 2.5e-06), 3.008796e-11);

	TGammaModeDistr test(9, 10);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.1), 2.516348e-17);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(5), 0.03182806);
	TGammaModeDistr test1(90, 1000);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(0.5), 2.57158e-20);
	TGammaModeDistr test2(0.09, 0.01);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(0.5), 0.9927348);
	TGammaModeDistr test3(0.0015, 2.5e-06);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(0.0000001), 3.008796e-11);
}

TEST(TGammaModeDistrTests, gammaModeMean) {
	EXPECT_FLOAT_EQ(TGammaModeDistr::mean(9, 10), 10);
	EXPECT_FLOAT_EQ(TGammaModeDistr::mean(90, 1000), 100);
	EXPECT_FLOAT_EQ(TGammaModeDistr::mean(0.09, 0.001), 0.1);
	EXPECT_FLOAT_EQ(TGammaModeDistr::mean(0.0015, 2.5e-06), 0.0025);

	TGammaModeDistr test(9, 10);
	EXPECT_FLOAT_EQ(test.mean(), 10);
	TGammaModeDistr test1(90, 1000);
	EXPECT_FLOAT_EQ(test1.mean(), 100);
	TGammaModeDistr test2(0.09, 0.001);
	EXPECT_FLOAT_EQ(test2.mean(), 0.1);
	TGammaModeDistr test3(0.0015, 2.5e-06);
	EXPECT_FLOAT_EQ(test3.mean(), 0.0025);
}

#include "gtest/gtest.h"
#include "coretools/Distributions/TUniformDistr.h"

using namespace coretools;
using namespace probdist;

TEST(TUniformDistrTests, dunif) {
	EXPECT_FLOAT_EQ(TUniformDistr::density(0., 0., 1.), 1.);
	EXPECT_FLOAT_EQ(TUniformDistr::density(0.5, 0., 1.), 1.);
	EXPECT_FLOAT_EQ(TUniformDistr::density(1., 0., 1.), 1.);
	EXPECT_FLOAT_EQ(TUniformDistr::density(-10., -1000., 5002.), 0.0001666111);
	EXPECT_FLOAT_EQ(
	    TUniformDistr::density(0.0, std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max()),
	    std::numeric_limits<double>::min());
	EXPECT_FLOAT_EQ(TUniformDistr::density(0.0, -10., std::numeric_limits<double>::max()),
	                std::numeric_limits<double>::min());
	EXPECT_FLOAT_EQ(TUniformDistr::density(0.0, std::numeric_limits<double>::lowest(), 10.),
	                std::numeric_limits<double>::min());
	EXPECT_FLOAT_EQ(TUniformDistr::density(0.0, std::numeric_limits<double>::lowest() + 100000.,
	                                       std::numeric_limits<double>::max() - 100000.),
	                std::numeric_limits<double>::min());

	TUniformDistr test(0., 1);
	EXPECT_FLOAT_EQ(test.density(0.), 1.);
	EXPECT_FLOAT_EQ(test.density(0.5), 1.);
	EXPECT_FLOAT_EQ(test.density(1.), 1.);
	test.set(-1000., 5002.);
	EXPECT_FLOAT_EQ(test.density(-10), 0.0001666111);
	test.set(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());
	EXPECT_FLOAT_EQ(test.density(0.0), std::numeric_limits<double>::min());
	test.set(-10., std::numeric_limits<double>::max());
	EXPECT_FLOAT_EQ(test.density(0.0), std::numeric_limits<double>::min());
	test.set(std::numeric_limits<double>::lowest(), 10.);
	EXPECT_FLOAT_EQ(test.density(0.0), std::numeric_limits<double>::min());
	test.set(std::numeric_limits<double>::lowest() + 100000., std::numeric_limits<double>::max() - 100000.);
	EXPECT_FLOAT_EQ(test.density(0.0), std::numeric_limits<double>::min());
}

TEST(TUniformDistrTests, punif) {
	EXPECT_FLOAT_EQ(TUniformDistr::cumulativeDensity(0., 0., 1.), 0.);
	EXPECT_FLOAT_EQ(TUniformDistr::cumulativeDensity(0.5, 0., 1.), 0.5);
	EXPECT_FLOAT_EQ(TUniformDistr::cumulativeDensity(1., 0., 1.), 1.);
	EXPECT_FLOAT_EQ(TUniformDistr::cumulativeDensity(-10., -1000., 5002.), 0.164945);
	EXPECT_FLOAT_EQ(TUniformDistr::cumulativeDensity(0.0, std::numeric_limits<double>::lowest(),
	                                                 std::numeric_limits<double>::max()),
	                0.5);

	TUniformDistr test(0., 1);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.), 0.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.5), 0.5);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(1.), 1.);
	test.set(-1000., 5002.);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(-10), 0.164945);
	test.set(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.0), 0.5);
}

TEST(TUniformDistrTests, dunifLog) {
	EXPECT_FLOAT_EQ(TUniformDistr::logDensity(0., 0., 1.), 0.);
	EXPECT_FLOAT_EQ(TUniformDistr::logDensity(0.5, 0., 1.), 0.);
	EXPECT_FLOAT_EQ(TUniformDistr::logDensity(1., 0., 1.), 0.);
	EXPECT_FLOAT_EQ(TUniformDistr::logDensity(-10., -1000., 5002.), log(0.0001666111));
	EXPECT_FLOAT_EQ(
	    TUniformDistr::logDensity(0.0, std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max()),
	    log(std::numeric_limits<double>::min()));
	EXPECT_FLOAT_EQ(TUniformDistr::logDensity(0.0, -10., std::numeric_limits<double>::max()),
	                log(std::numeric_limits<double>::min()));
	EXPECT_FLOAT_EQ(TUniformDistr::logDensity(0.0, std::numeric_limits<double>::lowest() + 100000.,
	                                          std::numeric_limits<double>::max() - 100000.),
	                log(std::numeric_limits<double>::min()));

	TUniformDistr test(0., 1);
	EXPECT_FLOAT_EQ(test.logDensity(0.), 0.);
	EXPECT_FLOAT_EQ(test.logDensity(0.5), 0.);
	EXPECT_FLOAT_EQ(test.logDensity(1.), 0.);
	test.set(-1000., 5002.);
	EXPECT_FLOAT_EQ(test.logDensity(-10), log(0.0001666111));
	test.set(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());
	EXPECT_FLOAT_EQ(test.logDensity(0.0), log(std::numeric_limits<double>::min()));
	test.set(-10., std::numeric_limits<double>::max());
	EXPECT_FLOAT_EQ(test.logDensity(0.0), log(std::numeric_limits<double>::min()));
	test.set(std::numeric_limits<double>::lowest() + 100000., std::numeric_limits<double>::max() - 100000.);
	EXPECT_FLOAT_EQ(test.logDensity(0.0), log(std::numeric_limits<double>::min()));
}

TEST(TUniformDistrTests, unifBools) {
	EXPECT_FALSE(TUniformDistr::isDiscrete());
	EXPECT_FALSE(TUniformDistr::isMultiVariate());
}

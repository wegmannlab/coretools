#include "gtest/gtest.h"
#include "coretools/Distributions/TNegativeBinomialDistr.h"
#include "coretools/Distributions/TNegativeBinomialDistrVariableN.h"

using namespace coretools;
using namespace probdist;

// negative binomial distribution variable N

TEST(TNegativeBinomialDistrTests, dnbinomVariableN) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(0, 0, 0.2_P), 1.0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(0, 10, 0.2_P), 0.0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(3, 0, 0.2_P), 0.008);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(3, 1, 0.2_P), 0.0192);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(3, 2, 0.2_P), 0.03072);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(3, 3, 0.2_P), 0.04096);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(3, 4, 0.2_P), 0.049152);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::density(3, 4, 0.9_P), 0.0010935);

	TNegativeBinomialDistrVariableN test(0.2_P);
	EXPECT_FLOAT_EQ(test.density(0, 0), 1.0);
	EXPECT_FLOAT_EQ(test.density(0, 10), 0.0);
	EXPECT_FLOAT_EQ(test.density(3, 0), 0.008);
	EXPECT_FLOAT_EQ(test.density(3, 1), 0.0192);
	EXPECT_FLOAT_EQ(test.density(3, 2), 0.03072);
	EXPECT_FLOAT_EQ(test.density(3, 3), 0.04096);
	EXPECT_FLOAT_EQ(test.density(3, 4), 0.049152);
	TNegativeBinomialDistrVariableN test1(0.9_P);
	EXPECT_FLOAT_EQ(test1.density(3, 4), 0.0010935);
}

TEST(TNegativeBinomialDistrTests, dnbinomVariableNLog) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::logDensity(0, 0, 0.2_P), 0.0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::logDensity(3, 0, 0.2_P), log(0.008));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::logDensity(3, 1, 0.2_P), log(0.0192));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::logDensity(3, 2, 0.2_P), log(0.03072));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::logDensity(3, 3, 0.2_P), log(0.04096));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::logDensity(3, 4, 0.2_P), log(0.049152));

	TNegativeBinomialDistrVariableN test(0.2_P);
	EXPECT_FLOAT_EQ(test.logDensity(0, 0), 0.0);
	EXPECT_FLOAT_EQ(test.logDensity(3, 0), log(0.008));
	EXPECT_FLOAT_EQ(test.logDensity(3, 1), log(0.0192));
	EXPECT_FLOAT_EQ(test.logDensity(3, 2), log(0.03072));
	EXPECT_FLOAT_EQ(test.logDensity(3, 3), log(0.04096));
	EXPECT_FLOAT_EQ(test.logDensity(3, 4), log(0.049152));
}

TEST(TNegativeBinomialDistrTests, pnbinomVariableNThrow) {
	// n = 0
	EXPECT_ANY_THROW(TNegativeBinomialDistrVariableN::cumulativeDensity(0, 0, 0.2_P));
	TNegativeBinomialDistrVariableN test(0.2_P);
	EXPECT_ANY_THROW((void)test.cumulativeDensity(0, 0));

	// n < 0
	EXPECT_ANY_THROW(TNegativeBinomialDistrVariableN::cumulativeDensity(-1, 0, 0.2_P));
	TNegativeBinomialDistrVariableN test1(0.2_P);
	EXPECT_ANY_THROW((void)test1.cumulativeDensity(-1, 0));
}

TEST(TNegativeBinomialDistrTests, pnbinomVariableN) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::cumulativeDensity(3, 0, 0.2_P), 0.008);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::cumulativeDensity(3, 1, 0.2_P), 0.0272);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::cumulativeDensity(3, 2, 0.2_P), 0.05792);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::cumulativeDensity(100, 100, 0.5_P), 0.5281742);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::cumulativeDensity(100, 50, 0.4_P), 3.875816e-11);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::cumulativeDensity(3, 4, 0.9_P), 0.9998235);

	TNegativeBinomialDistrVariableN test(0.2_P);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(3, 0), 0.008);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(3, 1), 0.0272);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(3, 2), 0.05792);
	TNegativeBinomialDistrVariableN test1(0.5_P);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(100, 100), 0.5281742);
	TNegativeBinomialDistrVariableN test2(0.4_P);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(100, 50), 3.875816e-11);
	TNegativeBinomialDistrVariableN test3(0.9_P);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(3, 4), 0.9998235);
}

TEST(TNegativeBinomialDistrTests, nbinomMeanVariableN) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::mean(10, 0.5_P), 10);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::mean(1, 0.9_P), 9);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::mean(1000, 0._P), 0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistrVariableN::mean(1000, 0.2_P), 250);

	TNegativeBinomialDistrVariableN test(0.5_P);
	EXPECT_FLOAT_EQ(test.mean(10), 10);
	TNegativeBinomialDistrVariableN test1(0.9_P);
	EXPECT_FLOAT_EQ(test1.mean(1), 9);
	TNegativeBinomialDistrVariableN test2(0._P);
	EXPECT_FLOAT_EQ(test2.mean(1000), 0);
	TNegativeBinomialDistrVariableN test3(0.2_P);
	EXPECT_FLOAT_EQ(test3.mean(1000), 250);
}

TEST(TNegativeBinomialDistrTests, nbinomVariableNBools) {
	EXPECT_TRUE(TNegativeBinomialDistrVariableN::isDiscrete());
	EXPECT_FALSE(TNegativeBinomialDistrVariableN::isMultiVariate());
}

// negative binomial distribution fixed N

TEST(TNegativeBinomialDistrTests, dnbinom) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(0, 0, 0.2_P), 1.0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(10, 0, 0.2_P), 1.024e-07);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(3, 0, 0.2_P), 0.008);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(3, 1, 0.2_P), 0.0192);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(3, 2, 0.2_P), 0.03072);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(3, 3, 0.2_P), 0.04096);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(3, 4, 0.2_P), 0.049152);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::density(3, 4, 0.9_P), 0.0010935);

	TNegativeBinomialDistr test(0, 0.2_P);
	EXPECT_FLOAT_EQ(test.density(0), 1.0_P);

	TNegativeBinomialDistr test1(10, 0.2_P);
	EXPECT_FLOAT_EQ(test1.density(0), 1.024e-07);

	TNegativeBinomialDistr test2(3, 0.2_P);
	EXPECT_FLOAT_EQ(test2.density(0), 0.008);
	EXPECT_FLOAT_EQ(test2.density(1), 0.0192);
	EXPECT_FLOAT_EQ(test2.density(2), 0.03072);
	EXPECT_FLOAT_EQ(test2.density(3), 0.04096);
	EXPECT_FLOAT_EQ(test2.density(4), 0.049152);

	TNegativeBinomialDistr test3(3, 0.9_P);
	EXPECT_FLOAT_EQ(test3.density(4), 0.0010935);
}

TEST(TNegativeBinomialDistrTests, dnbinomLog) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::logDensity(0, 0, 0.2_P), 0.0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::logDensity(3, 0, 0.2_P), log(0.008));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::logDensity(3, 1, 0.2_P), log(0.0192));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::logDensity(3, 2, 0.2_P), log(0.03072));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::logDensity(3, 3, 0.2_P), log(0.04096));
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::logDensity(3, 4, 0.2_P), log(0.049152));

	TNegativeBinomialDistr test(0, 0.2_P);
	EXPECT_FLOAT_EQ(test.logDensity(0), 0.0);
	TNegativeBinomialDistr test1(3, 0.2_P);
	EXPECT_FLOAT_EQ(test1.logDensity(0), log(0.008));
	EXPECT_FLOAT_EQ(test1.logDensity(1), log(0.0192));
	EXPECT_FLOAT_EQ(test1.logDensity(2), log(0.03072));
	EXPECT_FLOAT_EQ(test1.logDensity(3), log(0.04096));
	EXPECT_FLOAT_EQ(test1.logDensity(4), log(0.049152));
}

TEST(TNegativeBinomialDistrTests, pnbinomThrow) {
	// n == 0
	EXPECT_ANY_THROW(TNegativeBinomialDistr::cumulativeDensity(0, 0, 0.2_P));
	TNegativeBinomialDistr test(0, 0.2_P);
	EXPECT_ANY_THROW((void)test.cumulativeDensity(0));

	// n < 0
	EXPECT_ANY_THROW(TNegativeBinomialDistr::cumulativeDensity(-1, 0, 0.2_P));
}

TEST(TNegativeBinomialDistrTests, pnbinom) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::cumulativeDensity(3, 0, 0.2_P), 0.008);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::cumulativeDensity(3, 1, 0.2_P), 0.0272);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::cumulativeDensity(3, 2, 0.2_P), 0.05792);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::cumulativeDensity(100, 100, 0.5_P), 0.5281742);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::cumulativeDensity(100, 50, 0.4_P), 3.875816e-11);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::cumulativeDensity(3, 4, 0.9_P), 0.9998235);

	TNegativeBinomialDistr test(3, 0.2_P);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0), 0.008);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(1), 0.0272);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(2), 0.05792);
	TNegativeBinomialDistr test1(100, 0.5_P);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(100), 0.5281742);
	TNegativeBinomialDistr test2(100, 0.4_P);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(50), 3.875816e-11);
	TNegativeBinomialDistr test3(3, 0.9_P);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(4), 0.9998235);
}

TEST(TNegativeBinomialDistrTests, nbinomMean) {
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::mean(10, 0.5_P), 10);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::mean(1, 0.9_P), 9);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::mean(1000, 0._P), 0);
	EXPECT_FLOAT_EQ(TNegativeBinomialDistr::mean(1000, 0.2_P), 250);

	TNegativeBinomialDistr test(10, 0.5_P);
	EXPECT_FLOAT_EQ(test.mean(), 10);
	TNegativeBinomialDistr test1(1, 0.9_P);
	EXPECT_FLOAT_EQ(test1.mean(), 9);
	TNegativeBinomialDistr test2(1000, 0._P);
	EXPECT_FLOAT_EQ(test2.mean(), 0);
	TNegativeBinomialDistr test3(1000, 0.2_P);
	EXPECT_FLOAT_EQ(test3.mean(), 250);
}

TEST(TNegativeBinomialDistrTests, nbinomBools) {
	EXPECT_TRUE(TNegativeBinomialDistr::isDiscrete());
	EXPECT_FALSE(TNegativeBinomialDistr::isMultiVariate());
}

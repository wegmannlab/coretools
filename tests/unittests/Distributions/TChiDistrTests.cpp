#include "gtest/gtest.h"
#include "coretools/Distributions/TChiDistr.h"

using namespace coretools;
using namespace probdist;

// chi distribution

TEST(TChiDistrTests, chiThrow) {
	// invalid k == 0
	EXPECT_ANY_THROW(TChiDistr::density(1., 0));
	EXPECT_ANY_THROW(TChiDistr::logDensity(1., 0));

	TChiDistr test1(0);
	EXPECT_ANY_THROW((void)test1.density(1.));
	EXPECT_ANY_THROW((void)test1.logDensity(1.));
}

TEST(TChiDistrTests, dchi) {
	EXPECT_FLOAT_EQ(TChiDistr::density(0., 5), 0.);
	EXPECT_FLOAT_EQ(TChiDistr::density(1.5, 1), 0.2590352);
	EXPECT_FLOAT_EQ(TChiDistr::density(0.92, 10), 0.000805315);
	EXPECT_FLOAT_EQ(TChiDistr::density(10, 4), 9.643749e-20);

	TChiDistr test(5);
	EXPECT_FLOAT_EQ(test.density(0.), 0.);
	TChiDistr test1(1);
	EXPECT_FLOAT_EQ(test1.density(1.5), 0.2590352);
	TChiDistr test2(10);
	EXPECT_FLOAT_EQ(test2.density(0.92), 0.000805315);
	TChiDistr test3(4);
	EXPECT_FLOAT_EQ(test3.density(10), 9.643749e-20);
}

TEST(TChiDistrTests, dchiLog) {
	EXPECT_FLOAT_EQ(TChiDistr::logDensity(1., 5), -1.824404);
	EXPECT_FLOAT_EQ(TChiDistr::logDensity(1.5, 1), -1.350791);
	EXPECT_FLOAT_EQ(TChiDistr::logDensity(0.92, 10), -7.124277);
	EXPECT_FLOAT_EQ(TChiDistr::logDensity(10, 4), -43.78539);

	TChiDistr test(5);
	EXPECT_FLOAT_EQ(test.logDensity(1.), -1.824404);
	TChiDistr test1(1);
	EXPECT_FLOAT_EQ(test1.logDensity(1.5), -1.350791);
	TChiDistr test2(10);
	EXPECT_FLOAT_EQ(test2.logDensity(0.92), -7.124277);
	TChiDistr test3(4);
	EXPECT_FLOAT_EQ(test3.logDensity(10), -43.78539);
}

TEST(TChiDistrTests, pchi) {
	EXPECT_FLOAT_EQ(TChiDistr::cumulativeDensity(1., 5), 0.03743423);
	EXPECT_FLOAT_EQ(TChiDistr::cumulativeDensity(1.5, 1), 0.8663856);
	EXPECT_FLOAT_EQ(TChiDistr::cumulativeDensity(0.92, 10), 0.00007964819);
	EXPECT_FLOAT_EQ(TChiDistr::cumulativeDensity(10, 100), 0.5188083);

	TChiDistr test(5);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(1.), 0.03743423);
	TChiDistr test1(1);
	EXPECT_FLOAT_EQ(test1.cumulativeDensity(1.5), 0.8663856);
	TChiDistr test2(10);
	EXPECT_FLOAT_EQ(test2.cumulativeDensity(0.92), 0.00007964819);
	TChiDistr test3(100);
	EXPECT_FLOAT_EQ(test3.cumulativeDensity(10), 0.5188083);
}

TEST(TChiDistrTests, chiMean) {
	EXPECT_FLOAT_EQ(TChiDistr::mean(1), 0.7978846);
	EXPECT_FLOAT_EQ(TChiDistr::mean(5.), 2.127692);
	EXPECT_FLOAT_EQ(TChiDistr::mean(10), 3.084328);
	EXPECT_FLOAT_EQ(TChiDistr::mean(100), 9.975032);
	EXPECT_FLOAT_EQ(TChiDistr::mean(342), 18.47973);

	TChiDistr test(1);
	EXPECT_FLOAT_EQ(test.mean(), 0.7978846);
	TChiDistr test1(5);
	EXPECT_FLOAT_EQ(test1.mean(), 2.127692);
	TChiDistr test2(10);
	EXPECT_FLOAT_EQ(test2.mean(), 3.084328);
	TChiDistr test3(100);
	EXPECT_FLOAT_EQ(test3.mean(), 9.975032);
	TChiDistr test4(342);
	EXPECT_FLOAT_EQ(test4.mean(), 18.47973);
}

TEST(TChiDistrTests, chiBools) {
	EXPECT_FALSE(TChiDistr::isDiscrete());
	EXPECT_FALSE(TChiDistr::isMultiVariate());
}

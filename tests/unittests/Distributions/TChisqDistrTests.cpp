#include "gtest/gtest.h"
#include "coretools/Distributions/TChisqDistr.h"

using namespace coretools;
using namespace probdist;

// chisq distribution

TEST(TChisqDistrTests, dchisqThrow) {
	// k == 1 && x == 0
	EXPECT_ANY_THROW(TChisqDistr::density(0., 1));
	TChisqDistr test(1);
	EXPECT_ANY_THROW((void)test.density(0.));
	// k == 0
	EXPECT_ANY_THROW(TChisqDistr::density(1., 0));
	TChisqDistr test1(0);
	EXPECT_ANY_THROW((void)test1.density(1.));
}

TEST(TChisqDistrTests, dchisq) {
	EXPECT_FLOAT_EQ(TChisqDistr::density(0., 5), 0.);
	EXPECT_FLOAT_EQ(TChisqDistr::density(1.5, 1), 0.1538663);
	EXPECT_FLOAT_EQ(TChisqDistr::density(0.92, 10), 0.0005888635);

	TChisqDistr test(5);
	EXPECT_FLOAT_EQ(test.density(0.), 0.);
	test.set(1);
	EXPECT_FLOAT_EQ(test.density(1.5), 0.1538663);
	test.set(10);
	EXPECT_FLOAT_EQ(test.density(0.92), 0.0005888635);
}

TEST(TChisqDistrTests, dchisqLog) {
	EXPECT_FLOAT_EQ(TChisqDistr::logDensity(1., 5), -2.517551);
	EXPECT_FLOAT_EQ(TChisqDistr::logDensity(1.5, 1), -1.871671);
	EXPECT_FLOAT_EQ(TChisqDistr::logDensity(0.92, 10), -7.437316);

	TChisqDistr test(5);
	EXPECT_FLOAT_EQ(test.logDensity(1.), -2.517551);
	test.set(1);
	EXPECT_FLOAT_EQ(test.logDensity(1.5), -1.871671);
	test.set(10);
	EXPECT_FLOAT_EQ(test.logDensity(0.92), -7.437316);
}

TEST(TChisqDistrTests, pchisq) {
	EXPECT_FLOAT_EQ(TChisqDistr::cumulativeDensity(1., 5), 0.03743423);
	EXPECT_FLOAT_EQ(TChisqDistr::cumulativeDensity(1.5, 1), 0.7793286);
	EXPECT_FLOAT_EQ(TChisqDistr::cumulativeDensity(0.92, 10), 0.00011723673);

	TChisqDistr test(5);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(1.), 0.03743423);
	test.set(1);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(1.5), 0.7793286);
	test.set(10);
	EXPECT_FLOAT_EQ(test.cumulativeDensity(0.92), 0.00011723673);
}

TEST(TChisqDistrTests, chisqMean) {
	EXPECT_FLOAT_EQ(TChisqDistr::mean(1), 1);
	EXPECT_FLOAT_EQ(TChisqDistr::mean(10), 10);
	EXPECT_FLOAT_EQ(TChisqDistr::mean(10000), 10000);

	TChisqDistr test(1);
	EXPECT_FLOAT_EQ(test.mean(), 1);
	test.set(10);
	EXPECT_FLOAT_EQ(test.mean(), 10);
	test.set(10000);
	EXPECT_FLOAT_EQ(test.mean(), 10000);
}

TEST(TChisqDistrTests, chisqBools) {
	EXPECT_FALSE(TChisqDistr::isDiscrete());
	EXPECT_FALSE(TChisqDistr::isMultiVariate());
}

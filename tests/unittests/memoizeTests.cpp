//
// Created by Andreas on 23.12.2021
//

#include "gtest/gtest.h"
#include "coretools/memoize.h"

using namespace coretools;

int basic0() { return 0; }

int basic1(int x) { return x; }

auto basic2(int x, double y) { return x * y; }

auto basic3(int x, double y, int z) { return x * y * z; }

TEST(memoizTest, basic) {
	auto fm0 = memoize(basic0);
	auto fm1 = memoize(basic1);
	auto fm2 = memoize(basic2);
	auto fm3 = memoize(basic3);

	EXPECT_EQ(fm0(), 0);
	EXPECT_EQ(fm1(1), 1);
	EXPECT_FLOAT_EQ(fm2(2, 3.5), 7.);
	EXPECT_EQ(fm3(3, 2., 15), 90.);

	fm1.clear();
	EXPECT_EQ(fm1.size(), 0);
	fm1(1);
	EXPECT_EQ(fm1.size(), 1);
	fm1(1);
	EXPECT_EQ(fm1.size(), 1);
	fm1(2);
	EXPECT_EQ(fm1.size(), 2);
	fm1(2);
	EXPECT_EQ(fm1.size(), 2);

	auto lm = memoize([](int x, int y, int z) { return x + y + z; });
	EXPECT_EQ(lm(1, 2, 3), 6);

	std::function sf = []() { return 42; };
	auto sfm         = memoize(sf);
	EXPECT_EQ(sfm(), 42);
}

// Recursion is slightly tricky
int factorial(int);

auto factorial_m = memoize(factorial);

int factorial(int n) { return n == 0 ? 1 : n * factorial_m(n - 1); }

int fibo(int);
auto fibo_m = memoize(fibo);

int fibo(int n) { return n < 2 ? n : fibo_m(n - 1) + fibo_m(n - 2); }

TEST(memoizTest, recursion) {
	EXPECT_EQ(factorial(5), 5 * 4 * 3 * 2);
	EXPECT_EQ(factorial_m.size(), 5);
	EXPECT_EQ(factorial(3), 3 * 2);
	EXPECT_EQ(factorial_m.size(), 5);
	EXPECT_EQ(factorial(10), 3628800);
	EXPECT_EQ(factorial_m.size(), 10);

	EXPECT_EQ(fibo(10), 55);
	EXPECT_EQ(fibo_m.size(), 10);
	EXPECT_EQ(fibo(0), 0);
	EXPECT_EQ(fibo(1), 1);
	EXPECT_EQ(fibo_m.size(), 10);
	EXPECT_EQ(fibo(12), 144);
	EXPECT_EQ(fibo_m.size(), 12);
}

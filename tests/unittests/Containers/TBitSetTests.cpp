#include <bitset>
#include "gtest/gtest.h"

#include "coretools/Containers/TBitSet.h"
#include "coretools/Main/TRandomGenerator.h"

using namespace coretools;

static_assert(sizeof(TBitSet<8>) == 1);
static_assert(sizeof(TBitSet<16>) == 2);
static_assert(sizeof(TBitSet<32>) == 4);
static_assert(sizeof(TBitSet<64>) == 8);

template<size_t N> void recurse() {
	const size_t r = instances::randomGenerator().getRand(0, 1 << (N - 1));
	TBitSet<N> tbs(r);
	std::bitset<N> bs(r);

	static_assert(sizeof(tbs) <= sizeof(bs));
#ifdef _DEBUG
	EXPECT_DEATH(tbs[N]);
#endif

	for (size_t i = 0; i < N; ++i) { EXPECT_EQ(tbs[i], bs[i]); }
	recurse<N - 1>();
}
template<> void recurse<0>() {}

TEST(TBitSetTests, square_brackets_read) { recurse<32>(); }

TEST(TBitSetTests, square_brackets_write) {
	constexpr auto N = 13;
	const size_t r   = instances::randomGenerator().getRand(0, 1 << (N - 1));

	TBitSet<N> tbs(r);
	std::bitset<N> bs(r);

	for (size_t i = 0; i < N; ++i) {
		auto hwsnbt = tbs[i]; // He who shall not be typed
		EXPECT_EQ(hwsnbt, bs[i]);
		hwsnbt = (hwsnbt ? false : true);
		EXPECT_EQ(tbs[i], hwsnbt);
		EXPECT_NE(tbs[i], bs[i]);
		tbs[i] = ~tbs[i];
		EXPECT_EQ(hwsnbt, bs[i]);
		EXPECT_EQ(tbs[i], bs[i]);
	}
}

template<size_t N> void recursive_test() {
	constexpr auto i = N - 1;
	const size_t r   = instances::randomGenerator().getRand(0, 1 << (N - 1));
	TBitSet<N> tbs(r);
	const auto old = tbs.template get<i>();

	EXPECT_EQ(tbs[i], tbs.template get<i>());
	tbs.template set<i>(!tbs[i]);
	EXPECT_NE(tbs.template get<i>(), old);
	tbs.template set<i>(!tbs[i]);
	EXPECT_EQ(tbs.template get<i>(), old);

	recursive_test<N - 1>();
}
template<> void recursive_test<0>() {}

TEST(TBitSetTests, compileTime_access) {
	constexpr size_t N = 32;
	recursive_test<N>();
}

TEST(TBitSetTests, strong) {
	enum class AIndex : size_t {min, A, B, C, max};
	TStrongBitSet<AIndex> a;
	static_assert(sizeof(a) == 1);
	EXPECT_EQ(a[AIndex::A], false);
	EXPECT_EQ(a[AIndex::B], false);
	EXPECT_EQ(a[AIndex::C], false);
	a[AIndex::A] = true;
	EXPECT_EQ(a[AIndex::A], true);
	a.set<AIndex::B>();
	EXPECT_EQ(a.get<AIndex::B>(), true);
	a.set<AIndex::C>();
	EXPECT_EQ(a[AIndex::C], true);
}

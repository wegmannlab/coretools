#include "gtest/gtest.h"

#include "coretools/Containers/TMassFunction.h"
#include "coretools/Types/probability.h"

using namespace coretools;

enum class AlphaIndex : size_t { a, b, c, d, max };

TEST(MassFunctionTest, access) {
	constexpr TMassFunction<double, 11> mf{};
	static_assert(mf.front() == 1. / 11);
	static_assert(mf[0] == 1. / 11);
	static_assert(mf.at(0) == 1. / 11);
	static_assert(*mf.begin() == 1. / 11);

	static_assert(mf.back() == 1. / 11);
	static_assert(*(mf.end() - 1) == 1. / 11);

	EXPECT_ANY_THROW(mf.at(11));

	constexpr auto smf = TStrongMassFunction<Probability, AlphaIndex>::normalize(std::array<double, 4>{0., 1., 2., 3.});
	static_assert(std::is_same_v<std::remove_cv_t<std::remove_reference_t<decltype(smf.front())>>, Probability>);
	EXPECT_EQ(smf.front(), 0.);
	EXPECT_EQ(*smf.begin(), 0.);
	EXPECT_EQ(*smf.cbegin(), 0.);
	EXPECT_EQ(*(smf.rend() - 1), 0.);
	EXPECT_EQ(*(smf.crend() - 1), 0.);
	EXPECT_EQ(smf[AlphaIndex::a], 0.);

	EXPECT_FLOAT_EQ(smf[AlphaIndex::b], 1. / 6);
	EXPECT_FLOAT_EQ(smf[AlphaIndex::c], 2. / 6);
	EXPECT_FLOAT_EQ(smf[AlphaIndex::d], 3. / 6);
	EXPECT_EQ(smf.back(), 3. / 6);
	EXPECT_EQ(*smf.rbegin(), 3. / 6);
	EXPECT_EQ(*smf.crbegin(), 3. / 6);
	EXPECT_EQ(*(smf.end() - 1), 3. / 6);
	EXPECT_EQ(*(smf.cend() - 1), 3. / 6);
}

TEST(MassFunctionTest, for_each) {
	auto mf = TMassFunction<double, 4>::normalize({0., 1., 2., 3.});
	EXPECT_EQ(mf.front(), 0.);
	EXPECT_FLOAT_EQ(mf[1], 1. / 6);
	EXPECT_FLOAT_EQ(mf[2], 1. / 3);
	EXPECT_FLOAT_EQ(mf[3], 1. / 2);

	mf.for_each([](double x) { return x * x; });
	const auto s = 1. / (6 * 6) + 1. / (3 * 3) + 1. / (2 * 2);
	EXPECT_FLOAT_EQ(mf[0], 0.);
	EXPECT_FLOAT_EQ(mf[1], 1. / (6 * 6) / s);
	EXPECT_FLOAT_EQ(mf[2], 1. / (3 * 3) / s);
	EXPECT_FLOAT_EQ(mf[3], 1. / (2 * 2) / s);
}

TEST(MassFunctionTest, for_each_index) {
	auto mf = TMassFunction<double, 4>::normalize({0., 1., 2., 3.});
	EXPECT_EQ(mf.front(), 0.);
	EXPECT_FLOAT_EQ(mf[1], 1. / 6);
	EXPECT_FLOAT_EQ(mf[2], 1. / 3);
	EXPECT_FLOAT_EQ(mf[3], 1. / 2);

	std::array<size_t, 4> a;
	std::iota(a.rbegin(), a.rend(), 0);

	mf.for_each_index([&a](double, size_t i) { return (double)a[i]; });

	EXPECT_EQ(mf.back(), 0.);
	EXPECT_FLOAT_EQ(mf[2], 1. / 6);
	EXPECT_FLOAT_EQ(mf[1], 1. / 3);
	EXPECT_FLOAT_EQ(mf[0], 1. / 2);
}

TEST(MassFunctionTest, binary) {
	constexpr TStrongArray<double, AlphaIndex> sa1({1., 2., 3., 4.});
	constexpr TStrongArray<double, AlphaIndex> sa2({2., 2., 2., 2.});
	const auto mf_mul = TStrongMassFunction<double, AlphaIndex>::normalize(sa1, sa2, std::multiplies<double>{});
	EXPECT_FLOAT_EQ(mf_mul.front(), 0.1);
	EXPECT_FLOAT_EQ(mf_mul[AlphaIndex::b], 0.2);
	EXPECT_FLOAT_EQ(mf_mul[AlphaIndex::c], 0.3);
	EXPECT_FLOAT_EQ(mf_mul[AlphaIndex::d], 0.4);

	const auto mf_plus = TStrongMassFunction<double, AlphaIndex>::normalize(sa1, sa2, std::plus<double>{});
	EXPECT_FLOAT_EQ(mf_plus.front(), 3. / (3 + 4 + 5 + 6));
	EXPECT_FLOAT_EQ(mf_plus[AlphaIndex::b], 4. / (3 + 4 + 5 + 6));
	EXPECT_FLOAT_EQ(mf_plus[AlphaIndex::c], 5. / (3 + 4 + 5 + 6));
	EXPECT_FLOAT_EQ(mf_plus[AlphaIndex::d], 6. / (3 + 4 + 5 + 6));
}

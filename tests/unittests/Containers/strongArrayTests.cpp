#include <numeric>

#include "gtest/gtest.h"

#include "coretools/Containers/TStrongArray.h"

using namespace coretools;

enum class AlphaIndex : size_t { a, b, c, d, e, f, g, h, i, j, max };
enum class IndexNoMax : size_t { A, B, C, D };

TEST(StrongArrayTest, access) {
	constexpr TStrongArray<int, AlphaIndex> a({3, 0, 0});

	static_assert(a[AlphaIndex::a] == 3);
	static_assert(a[AlphaIndex::b] == 0);
	static_assert(a[AlphaIndex::j] == 0);

	static_assert(a.at(AlphaIndex::a) == 3);
	static_assert(a.at(AlphaIndex::b) == 0);
	EXPECT_ANY_THROW(a.at(AlphaIndex::max));

	static_assert(a.front() == 3);
	static_assert(a.back() == 0);

	static_assert(a.data()[0] == 3);
	static_assert(a.data()[1] == 0);

	static_assert(!a.empty());
	static_assert(TStrongArray<int, AlphaIndex, 0>{}.empty());

	static_assert(a.size() == index(AlphaIndex::max));
	static_assert(a.max_size() == index(AlphaIndex::max));

	constexpr TStrongArray<char, IndexNoMax, 4> b({'A', 'B', 'C', 'D'});

	static_assert(b[IndexNoMax::A] == 'A');
	static_assert(b[IndexNoMax::B] == 'B');
	static_assert(b[IndexNoMax::C] == 'C');
	static_assert(b[IndexNoMax::D] == 'D');

	EXPECT_ANY_THROW(b.at(IndexNoMax{4}));

	static_assert(b.front() == 'A');
	static_assert(b.back() == 'D');

	static_assert(b.data()[0] == 'A');
	static_assert(b.data()[3] == 'D');

	static_assert(b.size() == 4);
	static_assert(b.max_size() == 4);

	constexpr TStrongArray<int, AlphaIndex> c{3};
	static_assert(c.front() == c.back());
	static_assert(c[AlphaIndex::b] == 3);
}

TEST(StrongArrayTest, iterators) {
	TStrongArray<int, AlphaIndex> a;
	a.fill(15);
	EXPECT_EQ(std::accumulate(a.cbegin(), a.cend(), 0), index(AlphaIndex::max) * 15);

	std::fill(a.begin(), a.end(), 3);
	EXPECT_EQ(a[AlphaIndex::j], 3);

	std::iota(a.rbegin(), a.rend(), 0);
	EXPECT_EQ(a.back(), 0);
	EXPECT_EQ(a.front(), index(AlphaIndex::max) - 1);
	;
}

TEST(StrongArrayTest, arithmetic) {
	TStrongArray<int, AlphaIndex> a;
	TStrongArray<int, AlphaIndex> b;
	std::iota(a.begin(), a.end(), 1);
	std::iota(b.rbegin(), b.rend(), 1);

	EXPECT_EQ(a-a, b-b);
	EXPECT_EQ(a+b, b+a);
	EXPECT_EQ(a*b, b*a);
	EXPECT_EQ(a/a, b/b);

	auto c = a + b;
	EXPECT_EQ(c.front(), c.back());

	c -= b;
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c <= a);
	EXPECT_TRUE(c >= a);
	EXPECT_FALSE(c != a);
	EXPECT_FALSE(c < a);
	EXPECT_FALSE(c > a);

	c += b;
	EXPECT_EQ(c.front(), c.back());
	c -= b;

	c *= b;
	EXPECT_TRUE(c >= a);
	EXPECT_TRUE(c > a);
	EXPECT_EQ(c.front(), a.front() * b.front());
	EXPECT_EQ(c.back(), a.back() * b.back());

	c /= b;
	EXPECT_TRUE(c == a);

	c += 2;
	EXPECT_EQ(c.front(), a.front() + 2);
	EXPECT_EQ(c.back(), a.back() + 2);
	c -= 2;
	EXPECT_EQ(c, a);

	c *= 3;
	EXPECT_EQ(c.front(), a.front() * 3);
	EXPECT_EQ(c.back(), a.back() * 3);
	c /= 3;
	EXPECT_EQ(c, a);

	auto d = -c;
	EXPECT_EQ(d.front(), -c.front());
	EXPECT_EQ(d.back(), -c.back());

}

#include <numeric>

#include "gtest/gtest.h"

#include "coretools/Containers/TDualArray.h"

using namespace coretools;

TEST(DualArrayTest, type) {
	constexpr std::array<int, 3> a3{0, 1, 2};
	constexpr std::array<int, 5> a5{0, 1, 2, 3, 4};
	constexpr TDualArray<int, 3, 5> da3(a3);
	constexpr TDualArray<int, 3, 5> da5(a5);
	static_assert(da3.isType<ABType::A>());
	static_assert(da3.isType(ABType::A));
	static_assert(da3.type == ABType::A);
	static_assert(da5.isType<ABType::B>());
	static_assert(da5.isType(ABType::B));
	static_assert(da5.type == ABType::B);

	TDualArray<int, 3, 5> da(a3);
	EXPECT_TRUE(da.isType<ABType::A>());
	da.type = ABType::B;
	EXPECT_TRUE(da.isType<ABType::B>());

	enum class YZ : uint8_t { min = 0, Y = min, Z = 1, max = 2 };
	constexpr TDualArray<int, 3, 5, YZ> ya(a3);
	static_assert(ya.type == YZ::Y);
	constexpr TDualArray<int, 3, 5, YZ> yb{};
	static_assert(yb.type == YZ::Y);
	static_assert(yb.front() == 0);
}

TEST(DualArrayTest, access) {
	constexpr std::array<int, 3> a3{0, 1, 2};
	constexpr TDualArray<int, 3, 5> da3(a3);
	static_assert(da3.size() == 3);

	static_assert(da3.front() == 0);
	static_assert(*da3.begin() == 0);
	static_assert(*(da3.rend() - 1) == 0);

	static_assert(da3[1] == 1);
	static_assert(da3.at(1) == 1);

	static_assert(da3.back() == 2);
	static_assert(*da3.rbegin() == 2);
	static_assert(*(da3.end() - 1) == da3.back());

	EXPECT_ANY_THROW(da3.at(3));
	EXPECT_ANY_THROW(da3.at(4));

	constexpr std::array<int, 5> a5{3, 4, 5, 6, 7};
	constexpr TDualArray<int, 3, 5> da5(a5);

	static_assert(da5.size() == 5);

	static_assert(da5.front() == 3);
	static_assert(*da5.begin() == 3);
	static_assert(*(da5.rend() - 1) == 3);

	static_assert(da5[3] == 6);
	static_assert(da5.at(2) == 5);

	static_assert(da5.back() == 7);
	static_assert(*(da5.end() - 1) == da5.back());
	static_assert(*da5.rbegin() == 7);

	EXPECT_ANY_THROW(da3.at(5));
}

TEST(DualArrayTest, iterate) {
	constexpr std::array<int, 3> a3{7, 8, 9};
	constexpr TDualArray<int, 3, 5> da3(a3);

	for (size_t i = 0; i < da3.size(); ++i) { EXPECT_EQ(a3[i], da3[i]); }

	size_t sum = 0;
	for (const auto dai : da3) sum += dai;
	EXPECT_EQ(sum, std::accumulate(da3.cbegin(), da3.cend(), 0));

	constexpr std::array<int, 5> a5{10, 20, 30, 40, 50};
	TDualArray<int, 5, 3> da5(a5);

	const auto s5 = std::accumulate(da5.begin(), da5.end(), 0);
	da5.type      = ABType::B;
	const auto s3 = std::accumulate(da5.begin(), da5.end(), 0);
	EXPECT_NE(s5, s3);
}

#include <numeric>

#include "coretools/Containers/TDualStrongArray.h"
#include "gtest/gtest.h"

using namespace coretools;

enum class SmallAlphaIndex : size_t { a, b, c, d, max };
enum class BigAlphaIndex : size_t { A, B, max };

TEST(DualStrongArrayTest, type) {
	constexpr TStrongArray<char, SmallAlphaIndex> sa({'a', 'b', 'c', 'd'});
	constexpr TStrongArray<char, BigAlphaIndex> SA({'A', 'B'});
	static_assert(sa.size() == index(SmallAlphaIndex::max));
	static_assert(SA.size() == index(BigAlphaIndex::max));

	constexpr TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> dsa(sa);
	static_assert(dsa.type == ABType::A);
	static_assert(dsa.isType<ABType::A>());
	static_assert(dsa.isType(ABType::A));

	constexpr TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> DSA(SA);
	static_assert(DSA.type == ABType::B);
	static_assert(DSA.isType<ABType::B>());
	static_assert(DSA.isType(ABType::B));

	TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> das1{};
	EXPECT_TRUE(das1.isType<ABType::A>());
	das1.type = ABType::B;
	EXPECT_TRUE(das1.isType<ABType::B>());
}

TEST(DualStrongArrayTest, access) {
	constexpr TStrongArray<char, SmallAlphaIndex> sa({'a', 'b', 'c', 'd'});
	constexpr TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> dsa(sa);
	static_assert(dsa.size() == sa.size());
	static_assert(dsa.front() == 'a');
	static_assert(dsa[SmallAlphaIndex::a] == 'a');
	static_assert(*dsa.begin() == 'a');
	static_assert(*(dsa.rend() - 1) == 'a');

	static_assert(dsa[SmallAlphaIndex::b] == 'b');
	static_assert(dsa.at(SmallAlphaIndex::b) == 'b');

	static_assert(dsa.back() == 'd');
	static_assert(*dsa.rbegin() == 'd');
	static_assert(*(dsa.end() - 1) == 'd');

	EXPECT_ANY_THROW(dsa.at(BigAlphaIndex::A));

	constexpr TStrongArray<char, BigAlphaIndex> SA({'A', 'B'});
	constexpr TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> DSA(SA);
	static_assert(DSA.size() == SA.size());
	static_assert(DSA.front() == 'A');
	static_assert(DSA[BigAlphaIndex::A] == 'A');
	static_assert(*DSA.begin() == 'A');
	static_assert(*(DSA.rend() - 1) == 'A');

	static_assert(DSA[BigAlphaIndex::B] == 'B');
	static_assert(DSA.at(BigAlphaIndex::B) == 'B');

	static_assert(DSA.back() == 'B');
	static_assert(*DSA.rbegin() == 'B');
	static_assert(*(DSA.end() - 1) == 'B');

	EXPECT_ANY_THROW(DSA.at(SmallAlphaIndex::a));
	EXPECT_ANY_THROW(DSA.at(SmallAlphaIndex::c));
}

TEST(DualStrongArrayTest, iterate) {
	constexpr TStrongArray<char, SmallAlphaIndex> sa({'a', 'b', 'c', 'd'});
	constexpr TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> dsa(sa);
	EXPECT_TRUE(std::equal(dsa.begin(), dsa.end(), sa.begin()));

	std::string s;
	for (const auto dsai : dsa) s += dsai;
	EXPECT_EQ(s, "abcd");

	TDualStrongArray<char, SmallAlphaIndex, BigAlphaIndex> dsa1(sa);
	dsa1.type            = ABType::B;
	const std::string s1 = std::accumulate(dsa1.rbegin(), dsa1.rend(), std::string{});
	EXPECT_EQ(s1, "ba");
}

#include "coretools/Containers/TView.h"
#include "gtest/gtest.h"

using namespace coretools;

static_assert(TView<int>::npos == size_t(-1));

TEST(TViewTest, access) {
	std::array<int, 3> a{0, 1, 2};
	TView<int> viewA(a);
	EXPECT_EQ(viewA.front(), a.front());
	EXPECT_EQ(viewA.back(), a.back());
	EXPECT_EQ(viewA[0], a[0]);
	EXPECT_EQ(viewA[1], a[1]);
	EXPECT_EQ(viewA[2], a[2]);
	EXPECT_EQ(viewA.size(), a.size());
	EXPECT_FALSE(viewA.empty());

	std::vector<int> v{2, 1, 0};
	TView<int> viewV(v);
	EXPECT_EQ(viewV.front(), v.front());
	EXPECT_EQ(viewV.back(), v.back());
	EXPECT_EQ(viewV[0], v[0]);
	EXPECT_EQ(viewV[1], v[1]);
	EXPECT_EQ(viewV[2], v[2]);
	EXPECT_EQ(viewV.size(), v.size());
	EXPECT_FALSE(viewV.empty());

	EXPECT_TRUE(std::equal(viewA.begin(), viewA.end(), a.begin()));
	EXPECT_TRUE(std::equal(viewA.rbegin(), viewA.rend(), v.begin()));
	EXPECT_TRUE(std::equal(viewV.cbegin(), viewV.cend(), v.begin()));
	EXPECT_TRUE(std::equal(viewV.crbegin(), viewV.crend(), a.begin()));

	auto svT = viewA.subview();
	EXPECT_TRUE(std::equal(svT.begin(), svT.end(), a.begin()));

	auto svA = viewA.subview(1);
	EXPECT_EQ(svA.front(), a[1]);
	EXPECT_EQ(svA.back(), a.back());

	auto svV = viewV.subview(0, 2);
	EXPECT_EQ(svV.front(), v.front());
	EXPECT_EQ(svV.back(), v[1]);

	std::vector<int> empty{};
	EXPECT_TRUE(TView<int>(empty).empty());

	svT.remove_prefix(1);
	EXPECT_EQ(svT.front(), viewA[1]);
	svT.remove_suffix(1);
	EXPECT_EQ(svT.back(), viewA[1]);
	svT.remove_suffix(1);
	EXPECT_TRUE(svT.empty());

	double gs[10];
	TView<double> vgs(gs);
	EXPECT_EQ(vgs.size(), 10);
}

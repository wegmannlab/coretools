#include <numeric>

#include "gtest/gtest.h"

#include "coretools/Containers/TShortVector.h"
#include "coretools/Containers/TView.h"

using namespace coretools;

TEST(ShortVectorTest, access) {
	constexpr size_t N = 10;
	TShortVector<size_t, N> sv;
	for (size_t i = 0; i < N; ++i) {
		sv.push_back(i);
		EXPECT_EQ(sv.size(), i + 1);
		EXPECT_EQ(sv[i], i);
		EXPECT_EQ(sv.at(i), i);
	}

	EXPECT_EQ(sv.front(), 0);
	EXPECT_EQ(sv.back(), N - 1);
	
	sv.clear();
	EXPECT_EQ(sv.size(), 0);

	sv.resize(N, 9);
	EXPECT_EQ(std::count(sv.begin(), sv.end(),9), N);
	TShortVector<double, 100> ds(100, 3.14);
	EXPECT_FLOAT_EQ(std::accumulate(ds.cbegin(), ds.cend(), 0.), 100*3.14);

	TView<double> dview(ds);
	TShortVector<double, 200> ds2(dview.begin(), dview.end());
	EXPECT_EQ(ds2.size(), 100);
	ds2.push_back(33);
	EXPECT_EQ(ds2.size(), 101);
	EXPECT_FLOAT_EQ(std::accumulate(ds2.begin(), ds2.end(), 0.), 100*3.14 + 33);

	TShortVector<double, 101> ds3(ds2);
	EXPECT_TRUE(std::equal(ds3.begin(), ds3.end(), ds2.begin()));

	ds3 = ds;
	EXPECT_TRUE(std::equal(ds3.begin(), ds3.end(), ds2.begin()));

	TShortVector<double, 10000> ds4{3.,1.,3.,};
	EXPECT_EQ(ds4.size(), 3);

	ds4.assign(10, 555.666);
	EXPECT_EQ(ds4.size(), 10);
	EXPECT_EQ(std::count(ds4.begin(), ds4.end(), 555.666), 10);
}

TEST(ShortVectorTest, iterator) {
	constexpr size_t N = 10;
	TShortVector<size_t, N> sv;
	sv.resize(10);
	std::iota(sv.begin(), sv.end(), 0);

	for (size_t i = 0; i < N; ++i) {
		EXPECT_EQ(sv[i], i);
	}

	size_t i = 0;
	for (const auto v : sv) {
		EXPECT_EQ(v, i);
		++i;
	}

	i = sv.back();
	for (auto it = sv.rbegin(); it < sv.rend(); ++it) {
		EXPECT_EQ(*it, i);
		--i;
	}

	TView<size_t> view(sv);
	EXPECT_TRUE(std::equal(sv.begin(), sv.end(), view.begin()));

	std::vector<size_t> vec;
	std::copy(sv.begin(), sv.end(), std::back_inserter(vec));
	EXPECT_TRUE(std::equal(vec.begin(), vec.end(), sv.begin()));

	sv.clear();
	std::copy(vec.rbegin(), vec.rend(), std::back_inserter(sv));
	EXPECT_TRUE(std::equal(vec.cbegin(), vec.cend(), sv.crbegin()));
}

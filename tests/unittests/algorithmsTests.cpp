//
// Created by madleina on 19.04.21.
//

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/algorithms.h"
#include <algorithm>

using namespace testing;
using namespace coretools;

//------------------------------------------------
// Algorithms
//------------------------------------------------

TEST(algorithmsAndVectorsTests, reset) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	reset(a);
	EXPECT_FLOAT_EQ(std::accumulate(a.begin(), a.end(), 0.), 0.);
}

TEST(algorithmsAndVectorsTests, mean) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	EXPECT_FLOAT_EQ(mean(a), 7.);
}

TEST(algorithmsAndVectorsTests, mean_neg) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), -2.);
	EXPECT_FLOAT_EQ(mean(a), 0.0);
}

TEST(algorithmsAndVectorsTests, meanVar) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	const auto [mean, var] = meanVar(a);
	EXPECT_FLOAT_EQ(mean, 7.);
	EXPECT_FLOAT_EQ(var, 2.);
}

TEST(algorithmsAndVectorsTests, meanVar_neg) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), -2.);
	const auto [mean, var] = meanVar(a);
	EXPECT_FLOAT_EQ(mean, 0.);
	EXPECT_FLOAT_EQ(var, 2.);
}

TEST(algorithmsAndVectorsTests, var) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	EXPECT_FLOAT_EQ(var(a), 2.);
	EXPECT_FLOAT_EQ(sd(a), sqrt(2.));
}

TEST(algorithmsAndVectorsTests, var_neg) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), -2.);
	EXPECT_FLOAT_EQ(var(a), 2.);
	EXPECT_FLOAT_EQ(sd(a), sqrt(2.));
}

TEST(algorithmsAndVectorsTests, ranks) {
	std::array<double, 10> a = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19};
	auto r = ranks(a);
	EXPECT_EQ(r[0], 1);
	EXPECT_EQ(r[1], 2);
	EXPECT_EQ(r[5], 6);
	EXPECT_EQ(r[9], 10);

	std::reverse(a.begin(), a.end());
	r = ranks(a);
	EXPECT_EQ(r[0], 10);
	EXPECT_EQ(r[1], 9);
	EXPECT_EQ(r[5], 5);
	EXPECT_EQ(r[9], 1);

	a = {1, 10, 2, 9, 3, 8, 4, 7, 5, 6};
	r = ranks(a);
	EXPECT_EQ(r[0], 1);
	EXPECT_EQ(r[1], 10);
	EXPECT_EQ(r[5], 8);
	EXPECT_EQ(r[9], 6);

	a = {1, 1, 2, 2, 3, 3, 4, 4, 5, 5};
	r = ranks(a);
	EXPECT_EQ(r[0], 1.5);
	EXPECT_EQ(r[2], 3.5);
	EXPECT_EQ(r[4], 5.5);
	EXPECT_EQ(r[9], 9.5);

	a = {3, 3, 3, 1, 1, 1, 5, 5, 5, 0};
	r = ranks(a);
	EXPECT_EQ(r[0], 6);
	EXPECT_EQ(r[2], 6);
	EXPECT_EQ(r[3], 3);
	EXPECT_EQ(r[5], 3);
	EXPECT_EQ(r[6], 9);
	EXPECT_EQ(r[8], 9);
	EXPECT_EQ(r[9], 1);
}

TEST(algorithmsAndVectorsTests, sumPairwiseProduct) {
	std::array<double, 5> a,b;

	double res = 0.0;
	for(size_t i = 0; i < 5; ++i){
		a[i] = i;
		b[i] = 2*i;
		res += a[i] * b[i];
	}

	EXPECT_FLOAT_EQ(sumPairwiseProduct(a,b), res);
	
	res = 0.0;
	for(size_t i = 0; i < 5; ++i){
		a[i] = i + 0.5;
		b[i] = i - 0.5;
		res += a[i] * b[i];
	}	
	EXPECT_FLOAT_EQ(sumPairwiseProduct(a,b), res);
}

TEST(algorithmsAndVectorsTests, pearsonCorrelation) {
	std::array<double, 10> a,b;
	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		b[i] = 2.5 * a[i];
	}
	EXPECT_FLOAT_EQ(pearsonCorrelation(a,b), 1.0);

	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		b[i] = -3.2 * a[i];
	}
	EXPECT_FLOAT_EQ(pearsonCorrelation(a,b), -1.0);

	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		b[i] = 9.0 - i;
	}
	EXPECT_FLOAT_EQ(pearsonCorrelation(a,b), -1.0);

	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		if(i % 2 == 0){
			b[i] = a[i];
		} else {
			b[i] = 0.0;
		}
	}
	EXPECT_FLOAT_EQ(pearsonCorrelation(a,b), 0.3692745);
}

TEST(algorithmsAndVectorsTests, spearmanCorrelation) {
	std::array<double, 10> a,b;
	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		b[i] = i*i;
	}
	EXPECT_FLOAT_EQ(spearmanCorrelation(a,b), 1.0);

	for(size_t i = 0; i < 10; ++i){
		a[i] = i * i;
		b[i] = 9 - i;
	}
	EXPECT_FLOAT_EQ(spearmanCorrelation(a,b), -1.0);

	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		b[i] = 9.0 - i;
	}
	EXPECT_FLOAT_EQ(spearmanCorrelation(a,b), -1.0);

	for(size_t i = 0; i < 10; ++i){
		a[i] = i;
		if(i % 2 == 0){
			b[i] = a[i];
		} else {
			b[i] = 0.0;
		}
	}
	EXPECT_FLOAT_EQ(spearmanCorrelation(a,b), 0.2731155);
}

TEST(algorithmsAndVectorsTests, min) {
	EXPECT_EQ(coretools::min(1.1), 1.1);                // 1 arg
	EXPECT_EQ(coretools::min(1.1, -0.5), -0.5);         // 2 args
	EXPECT_EQ(coretools::min(-0.1, 0.3, 0.5), -0.1);    // 3 args
	EXPECT_EQ(coretools::min(1.1, 2, -0.2, 100), -0.2); // 4 args
	EXPECT_EQ(coretools::min(1.1, 2, 100, -0.2), -0.2); // 4 args
	EXPECT_EQ(coretools::min(-0.2, 2, 100, -10), -10); // 4 args
}

TEST(algorithmsAndVectorsTests, max) {
	EXPECT_EQ(coretools::max(1.1), 1.1);               // 1 arg
	EXPECT_EQ(coretools::max(1.1, -0.5), 1.1);         // 2 args
	EXPECT_EQ(coretools::max(-0.1, 0.3, 0.5), 0.5);    // 3 args
	EXPECT_EQ(coretools::max(1.1, 2, -0.2, 100), 100); // 4 args
	EXPECT_EQ(coretools::max(1.1, 2, 100, -0.2), 100); // 4 args
	EXPECT_EQ(coretools::max(100, 2, -0.2, -10), 100); // 4 args
}

TEST(algorithmsAndVectorsTests, normalize) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	normalize(a);
	EXPECT_FLOAT_EQ(std::accumulate(a.begin(), a.end(), 0.), 1.);
}

TEST(algorithmsAndVectorsTests, normalize_tot) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	normalize(a, 35.0);
	EXPECT_FLOAT_EQ(std::accumulate(a.begin(), a.end(), 0.), 1.);
}

TEST(algorithmsAndVectorsTests, normalize_iterators) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	normalize(a.begin(), a.end());
	EXPECT_FLOAT_EQ(std::accumulate(a.begin(), a.end(), 0.), 1.);
}

TEST(algorithmsAndVectorsTests, normalize_tot_iterators) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	normalize(a.begin(), a.end(), 35.0);
	EXPECT_FLOAT_EQ(std::accumulate(a.begin(), a.end(), 0.), 1.);
}

TEST(algorithmsAndVectorsTests, normalizeLogSumExp) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	normalizeLogSumExp(a);
	EXPECT_FLOAT_EQ(std::accumulate(a.begin(), a.end(), 0.), 1.);

	// huge numbers
	std::array<double, 5> b = {-10000, -101000, -102000, -103000, -104000};
	normalizeLogSumExp(b);
	EXPECT_FLOAT_EQ(std::accumulate(b.begin(), b.end(), 0.), 1.);
}

TEST(algorithmsAndVectorsTests, standardizeZeroMean) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	standardizeZeroMean(a);
	EXPECT_THAT(a, ElementsAre(-2., -1., 0., 1., 2.));
}

TEST(algorithmsAndVectorsTests, standardizeZeroMeanUnitVar) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 5.);
	standardizeZeroMeanUnitVar(a);
	std::vector<double> expected = {-1.4142136, -0.7071068, 0.0000000, 0.7071068, 1.4142136};
	for (size_t i = 0; i < a.size(); i++) { EXPECT_FLOAT_EQ(a[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, nonZero) {
	std::array<double, 5> a{};
	a[2] = 1.;
	a[4] = -1.;
	EXPECT_EQ(numNonZero(a), 2);
}

TEST(algorithmsAndVectorsTests, weightedSum) {
	std::array<double, 5> a;
	std::array<double, 5> b;
	std::iota(a.begin(), a.end(), 1.);
	std::iota(b.rbegin(), b.rend(), 1.);

	double s = 0.;
	for (size_t i = 0; i < a.size(); ++i) s += a[i] * b[i];

	EXPECT_EQ(weightedSum(a, b), s);
}

TEST(algorithmsAndVectorsTests, sumOfSquares) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 1.);

	double s = 0.;
	for (double i : a) s += i * i;

	EXPECT_EQ(sumOfSquares(a), s);
}

TEST(algorithmsAndVectorsTests, sumOfSquares_iterators) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 1.);

	double s = 0.;
	for (double i : a) s += i * i;

	EXPECT_EQ(sumOfSquares(a.begin(), a.end()), s);
}

TEST(algorithmsAndVectorsTests, pairwiseSum) {
	std::array<double, 5> a;
	std::array<double, 5> b;
	std::iota(a.begin(), a.end(), 1.);
	std::iota(b.begin(), b.end(), 2.);

	pairwiseSum(a, b);

	EXPECT_THAT(a, ElementsAre(3, 5, 7, 9, 11));
}

TEST(algorithmsAndVectorsTests, vectorNorm) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 1.);

	double s = 0.;
	for (double i : a) s += i * i;

	EXPECT_EQ(vectorNorm(a), sqrt(s));
}

TEST(algorithmsAndVectorsTests, vectorNorm_iterators) {
	std::array<double, 5> a;
	std::iota(a.begin(), a.end(), 1.);

	double s = 0.;
	for (double i : a) s += i * i;

	EXPECT_EQ(vectorNorm(a.begin(), a.end()), sqrt(s));
}

TEST(algorithmsAndVectorsTests, fillCumulative) {
	std::array<double, 4> probs = {0.1, 0.25, 0.25, 0.4};
	std::vector<double> cumu;
	fillCumulative(probs, cumu);
	EXPECT_EQ(cumu.size(), probs.size());
	EXPECT_EQ(cumu.front(), probs.front());
	EXPECT_EQ(cumu.back(), 1.);

	std::vector<double> probs2;
	std::copy(cumu.begin(), cumu.end(), std::back_insert_iterator(probs2));
	probs2.pop_back();
	fillCumulative(probs2, cumu);
	EXPECT_EQ(cumu.back(), 1.);

	std::array<double, 4> probs3 = {1.0, 2.0, 3.0, 4.0};
	fillCumulative(probs3, cumu);
	EXPECT_EQ(cumu.size(), probs3.size());
	EXPECT_EQ(cumu.front(), 0.1);
	EXPECT_EQ(cumu.back(), 1.);
	EXPECT_FLOAT_EQ(cumu[2], 0.6);

	std::vector<Probability> cumuProb;
	fillCumulative(probs3, cumuProb);
	EXPECT_EQ(cumuProb.size(), probs3.size());
	EXPECT_EQ(cumuProb.front(), 0.1);
	EXPECT_EQ(cumuProb.back(), 1.);
	EXPECT_FLOAT_EQ(cumuProb[2], 0.6);
}

TEST(algorithmsAndVectorsTests, rankSort) {
	std::vector<double> input   = {5, 2, 1, 4, 3};
	std::vector<uint64_t> ranks = rankSort(input);

	std::vector<uint64_t> expected = {2, 1, 4, 3, 0};

	EXPECT_EQ(ranks.size(), 5);
	for (uint64_t i = 0; i < ranks.size(); i++) { EXPECT_EQ(ranks[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, rankSort_increasing) {
	std::vector<double> input   = {5, 2, 1, 4, 3};
	std::vector<uint64_t> ranks = rankSort(input, true);

	std::vector<uint64_t> expected = {0, 3, 4, 1, 2};

	EXPECT_EQ(ranks.size(), 5);
	for (uint64_t i = 0; i < ranks.size(); i++) { EXPECT_EQ(ranks[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, medianOfUnsorted) {
	// odd number of elements
	std::vector<double> input = {5, 2, 1, 4, 3};
	EXPECT_EQ(medianOfUnsorted(input), 3);

	// even number of elements
	input = {5, 2, 6, 1, 4, 3};
	EXPECT_EQ(medianOfUnsorted(input), 3.5);
}

TEST(algorithmsAndVectorsTests, percentileOfUnsorted) {
	// odd number of elements
	std::vector<double> input = {20, 15, 35, 50, 40};
	EXPECT_EQ(percentileOfUnsorted(input, 0.05_P), 15);
	EXPECT_EQ(percentileOfUnsorted(input, 0.3_P), 20);
	EXPECT_EQ(percentileOfUnsorted(input, 0.4_P), 20);
	EXPECT_EQ(percentileOfUnsorted(input, 0.5_P), 35);
	EXPECT_EQ(percentileOfUnsorted(input, 1.0_P), 50);

	// even number of elements
	input = {7, 8, 6, 10, 16, 8, 20, 13, 3, 15};
	EXPECT_EQ(percentileOfUnsorted(input, 0.25_P), 7);
	EXPECT_EQ(percentileOfUnsorted(input, 0.5_P), 8);
	EXPECT_EQ(percentileOfUnsorted(input, 0.75_P), 15);
	EXPECT_EQ(percentileOfUnsorted(input, 1.0_P), 20);
}

TEST(algorithmsAndVectorsTests, distance) {
	std::array<double, 16> a;
	std::iota(a.begin(), a.end(), 0.);
	std::vector<double> v(16);
	std::iota(v.begin(), v.end(), 3.);
	EXPECT_FLOAT_EQ(euclideanDistance(a, v), 4 * 3.);
}

TEST(algorithmsAndVectorsTests, findDuplicates) {
	// no duplicates
	std::vector<std::string> Vec1 = {"a", "b", "c", "d", "e"};

	EXPECT_EQ(findDuplicate(Vec1).first, false);
	EXPECT_EQ(findDuplicate(Vec1).second, 0);

	// with duplicates
	std::vector<std::string> Vec2 = {"a", "b", "c", "a", "e"};

	EXPECT_EQ(findDuplicate(Vec2).first, true);
	EXPECT_EQ(findDuplicate(Vec2).second, 3);
}

TEST(algorithmsAndVectorsTests, binarySearch_getIndex) {
	std::vector<std::string> vec = {"a", "a", "b", "c", "e"};

	// duplicates: return first
	EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "a"), 0);

	// easy cases
	EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "b"), 2);
	EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "c"), 3);
	EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "e"), 4);

	// throw if not found
	EXPECT_ANY_THROW(binarySearch_getIndex(vec.begin(), vec.end(), "f"));

	// other iterators
	EXPECT_EQ(binarySearch_getIndex(vec.begin() + 2, vec.end(), "b"), 0);
	EXPECT_EQ(binarySearch_getIndex(vec.begin() + 2, vec.end(), "c"), 1);
	EXPECT_EQ(binarySearch_getIndex(vec.begin() + 2, vec.end(), "e"), 2);
	EXPECT_ANY_THROW(binarySearch_getIndex(vec.begin() + 2, vec.end(), "a"));
}

TEST(algorithmsAndVectorsTests, calculateMovingAverage) {
	std::vector<double> vec      = {0, 0, 1, 2, 2, 2, 0, 2, 0, 0, 1, 0, 0};
	auto res                     = calculateMovingAverage(vec, 3);
	std::vector<double> expected = {0.75,
	                                1,
	                                1.16666666666667,
	                                1,
	                                1.28571428571429,
	                                1.28571428571429,
	                                1.14285714285714,
	                                1,
	                                0.714285714285714,
	                                0.428571428571429,
	                                0.5,
	                                0.2,
	                                0.25};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingAverage_round) {
	std::vector<double> vec      = {0, 0, 1, 2, 2, 2, 0, 2, 0, 0, 1, 0, 0};
	auto res                     = calculateMovingAverage<true>(vec, 3);
	std::vector<double> expected = {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingAverage_shorterBandwidth) {
	std::vector<double> vec      = {2, 0, 1};
	auto res                     = calculateMovingAverage(vec, 5); // bandwidth > vec.size()
	std::vector<double> expected = {1.0, 1.0, 1.0};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingAverage_bandwidth0) {
	std::vector<double> vec      = {2, 0, 1};
	auto res                     = calculateMovingAverage(vec, 0);
	std::vector<double> expected = {2, 0, 1};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingAverage_emptyVector) {
	std::vector<double> vec      = {};
	auto res                     = calculateMovingAverage(vec, 10);
	std::vector<double> expected = {};
	EXPECT_EQ(res.size(), expected.size());
}

TEST(algorithmsAndVectorsTests, calculateMovingMedian) {
	std::vector<double> vec      = {0, 0, 1, 2, 2, 2, 0, 2, 0, 0, 1, 0, 0};
	auto res                     = calculateMovingMedian(vec, 3);
	std::vector<double> expected = {0.5, 1, 1.5, 1, 2, 2, 2, 1, 0, 0, 0, 0, 0};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingMedian_round) {
	std::vector<double> vec      = {0, 0, 1, 2, 2, 2, 0, 2, 0, 0, 1, 0, 0};
	auto res                     = calculateMovingMedian<true>(vec, 3);
	std::vector<double> expected = {1, 1, 2, 1, 2, 2, 2, 1, 0, 0, 0, 0, 0};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingMedian_shorterBandwidth) {
	std::vector<double> vec      = {2, 0, 1};
	auto res                     = calculateMovingMedian(vec, 5); // bandwidth > vec.size()
	std::vector<double> expected = {1.0, 1.0, 1.0};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingMedian_bandwidth0) {
	std::vector<double> vec      = {2, 0, 1};
	auto res                     = calculateMovingMedian(vec, 0);
	std::vector<double> expected = {2, 0, 1};
	EXPECT_EQ(res.size(), expected.size());
	for (size_t i = 0; i < res.size(); ++i) { EXPECT_FLOAT_EQ(res[i], expected[i]); }
}

TEST(algorithmsAndVectorsTests, calculateMovingMedian_emptyVector) {
	std::vector<double> vec      = {};
	auto res                     = calculateMovingMedian(vec, 10);
	std::vector<double> expected = {};
	EXPECT_EQ(res.size(), expected.size());
}

//------------------------------------------------

TEST(algorithmsAndVectorsTests, getLinearIndex_array) {
	// single element
	EXPECT_EQ((getLinearIndex<size_t, 1>({0}, {1})), 0);
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndex<size_t, 1>({}, {1}), "");
	EXPECT_DEATH(getLinearIndex<size_t, 1>({1}, {1}), "");
#endif

	// 1D, 5 elements
	for (size_t i = 0; i < 5; i++) { EXPECT_EQ((getLinearIndex<size_t, 1>({i}, {5})), i); }
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<size_t, 1>({5}, {5})), "");
#endif

	// 2D, 1x5 elements
	for (size_t i = 0; i < 1; i++) {
		for (size_t j = 0; j < 5; j++) { EXPECT_EQ((getLinearIndex<size_t, 2>({i, j}, {1, 5})), i * 5 + j); }
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<size_t, 2>({0, 5}, {1, 5})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 2>({1, 0}, {1, 5})), "");
#endif

	// 2D, 5x1 elements
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 1; j++) { EXPECT_EQ((getLinearIndex<size_t, 2>({i, j}, {5, 1})), j + i); }
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<size_t, 2>({0, 1}, {5, 1})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 2>({5, 0}, {5, 1})), "");
#endif

	// 2D, 5x3 elements
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 3; j++) { EXPECT_EQ((getLinearIndex<size_t, 2>({i, j}, {5, 3})), i * 3 + j); }
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<size_t, 2>({0}, {5, 3})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 2>({0, 0, 0}, {5, 3})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 2>({0, 3}, {5, 3})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 2>({5, 0}, {5, 3})), "");
#endif

	// 3D
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				EXPECT_EQ((getLinearIndex<size_t, 3>({i, j, k}, {2, 3, 4})), k + 4 * (j + 3 * i));
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<size_t, 3>({0, 0, 4}, {2, 3, 4})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 3>({0, 3, 0}, {2, 3, 4})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 3>({2, 0, 0}, {2, 3, 4})), "");
#endif

	// 4D
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				for (size_t l = 0; l < 5; l++) {
					EXPECT_EQ((getLinearIndex<size_t, 4>({i, j, k, l}, {2, 3, 4, 5})), l + 5 * (k + 4 * (j + 3 * i)));
				}
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<size_t, 4>({0, 0, 0, 5}, {2, 3, 4, 5})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 4>({0, 0, 4, 0}, {2, 3, 4, 5})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 4>({0, 3, 0, 0}, {2, 3, 4, 5})), "");
	EXPECT_DEATH((getLinearIndex<size_t, 4>({2, 0, 0, 0}, {2, 3, 4, 5})), "");
#endif
}

TEST(algorithmsAndVectorsTests, getLinearIndex_vec) {
	// single element
	using Type = std::vector<size_t>;
	EXPECT_EQ((getLinearIndex<Type, Type>({0}, {1})), 0);
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndex<Type, Type>({}, {1}), "");
	EXPECT_DEATH(getLinearIndex<Type, Type>({1}, {1}), "");
#endif

	// 1D, 5 elements
	for (size_t i = 0; i < 5; i++) { EXPECT_EQ((getLinearIndex<Type, Type>({i}, {5})), i); }
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<Type, Type>({5}, {5})), "");
#endif

	// 2D, 1x5 elements
	for (size_t i = 0; i < 1; i++) {
		for (size_t j = 0; j < 5; j++) { EXPECT_EQ((getLinearIndex<Type, Type>({i, j}, {1, 5})), i * 5 + j); }
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 5}, {1, 5})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({1, 0}, {1, 5})), "");
#endif

	// 2D, 5x1 elements
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 1; j++) { EXPECT_EQ((getLinearIndex<Type, Type>({i, j}, {5, 1})), j + i); }
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 1}, {5, 1})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({5, 0}, {5, 1})), "");
#endif

	// 2D, 5x3 elements
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 3; j++) { EXPECT_EQ((getLinearIndex<Type, Type>({i, j}, {5, 3})), i * 3 + j); }
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<Type, Type>({0}, {5, 3})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 0, 0}, {5, 3})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 3}, {5, 3})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({5, 0}, {5, 3})), "");
#endif

	// 3D
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				EXPECT_EQ((getLinearIndex<Type, Type>({i, j, k}, {2, 3, 4})), k + 4 * (j + 3 * i));
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 0, 4}, {2, 3, 4})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 3, 0}, {2, 3, 4})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({2, 0, 0}, {2, 3, 4})), "");
#endif

	// 4D
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				for (size_t l = 0; l < 5; l++) {
					EXPECT_EQ((getLinearIndex<Type, Type>({i, j, k, l}, {2, 3, 4, 5})), l + 5 * (k + 4 * (j + 3 * i)));
				}
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 0, 0, 5}, {2, 3, 4, 5})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 0, 4, 0}, {2, 3, 4, 5})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({0, 3, 0, 0}, {2, 3, 4, 5})), "");
	EXPECT_DEATH((getLinearIndex<Type, Type>({2, 0, 0, 0}, {2, 3, 4, 5})), "");
#endif
}

TEST(algorithmsAndVectorsTests, getLinearIndexColMajor) {
	// single element
	EXPECT_EQ(getLinearIndexColMajor<size_t>({0}, {1}), 0);
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({}, {1}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0}, {1}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({1}, {1}), "");
#endif

	// 1D, 5 elements
	for (size_t i = 0; i < 5; i++) { EXPECT_EQ(getLinearIndexColMajor<size_t>({i}, {5}), i); }
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({5}, {5}), "");
#endif

	// 2D, 1x5 elements
	for (size_t i = 0; i < 1; i++) {
		for (size_t j = 0; j < 5; j++) { EXPECT_EQ(getLinearIndexColMajor<size_t>({i, j}, {1, 5}), i * 5 + j); }
	}
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 5}, {1, 5}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({1, 0}, {1, 5}), "");
#endif

	// 2D, 5x1 elements
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 1; j++) { EXPECT_EQ(getLinearIndexColMajor<size_t>({i, j}, {5, 1}), j + i); }
	}
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 1}, {5, 1}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({5, 0}, {5, 1}), "");
#endif

	// 2D, 5x3 elements
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 3; j++) { EXPECT_EQ(getLinearIndexColMajor<size_t>({i, j}, {5, 3}), i + j * 5); }
	}
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0}, {5, 3}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 0}, {5, 3}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 3}, {5, 3}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({5, 0}, {5, 3}), "");
#endif

	// 3D
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				EXPECT_EQ(getLinearIndexColMajor<size_t>({i, j, k}, {2, 3, 4}), i + 2 * (j + 3 * k));
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0}, {2, 3, 4}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 0, 0}, {2, 3, 4}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 4}, {2, 3, 4}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 3, 0}, {2, 3, 4}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({2, 0, 0}, {2, 3, 4}), "");
#endif

	// 4D
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				for (size_t l = 0; l < 5; l++) {
					EXPECT_EQ(getLinearIndexColMajor<size_t>({i, j, k, l}, {2, 3, 4, 5}),
					          i + 2 * (j + 3 * (k + 4 * l)));
				}
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 0}, {2, 3, 4, 5}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 0, 0, 0}, {2, 3, 4, 5}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 0, 5}, {2, 3, 4, 5}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 0, 4, 0}, {2, 3, 4, 5}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({0, 3, 0, 0}, {2, 3, 4, 5}), "");
	EXPECT_DEATH(getLinearIndexColMajor<size_t>({2, 0, 0, 0}, {2, 3, 4, 5}), "");
#endif
}

TEST(algorithmsAndVectorsTests, getSubscripts) {
	// single element
	std::vector<size_t> dimensions = {1};
	size_t linearIndex             = 0;
	std::vector<size_t> coords     = getSubscripts<size_t>(linearIndex, dimensions);
	EXPECT_EQ(linearIndex, getLinearIndex(coords, dimensions));
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(1, dimensions), "");
#endif

	// 1D, 5 elements
	dimensions = {5};
	for (size_t i = 0; i < 5; i++) {
		coords = getSubscripts<size_t>(i, dimensions);
		EXPECT_EQ(getLinearIndex(coords, dimensions), i);
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(5, dimensions), "");
#endif

	// 2D, 1x5 elements
	dimensions = {1, 5};
	for (size_t i = 0; i < 1; i++) {
		for (size_t j = 0; j < 5; j++) {
			coords = getSubscripts<size_t>(j, dimensions);
			EXPECT_EQ(getLinearIndex(coords, dimensions), j);
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(5, dimensions), "");
#endif

	// 2D, 5x1 elements
	dimensions = {5, 1};
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 1; j++) {
			coords = getSubscripts<size_t>(i, dimensions);
			EXPECT_EQ(getLinearIndex(coords, dimensions), i);
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(5, dimensions), "");
#endif

	// 2D, 5x3 elements
	dimensions  = {5, 3};
	linearIndex = 0;
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 3; j++, linearIndex++) {
			coords = getSubscripts<size_t>(linearIndex, dimensions);
			EXPECT_EQ(getLinearIndex(coords, dimensions), linearIndex);
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(15, dimensions), "");
#endif

	// 3D
	dimensions  = {2, 3, 4};
	linearIndex = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++, linearIndex++) {
				coords = getSubscripts<size_t>(linearIndex, dimensions);
				EXPECT_EQ(getLinearIndex(coords, dimensions), linearIndex);
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(24, dimensions), "");
#endif

	// 4D
	dimensions  = {2, 3, 4, 5};
	linearIndex = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				for (size_t l = 0; l < 5; l++, linearIndex++) {
					coords = getSubscripts<size_t>(linearIndex, dimensions);
					EXPECT_EQ(getLinearIndex(coords, dimensions), linearIndex);
				}
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscripts<size_t>(120, dimensions), "");
#endif
}

TEST(algorithmsAndVectorsTests, getSubscriptsColMajor) {
	// single element
	std::vector<size_t> dimensions = {1};
	size_t linearIndex             = 0;
	std::vector<size_t> coords     = getSubscriptsColMajor<size_t>(linearIndex, dimensions);
	EXPECT_EQ(linearIndex, getLinearIndexColMajor<size_t>(coords, {1}));
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(1, dimensions), "");
#endif

	// 1D, 5 elements
	dimensions = {5};
	for (size_t i = 0; i < 5; i++) {
		coords = getSubscriptsColMajor<size_t>(i, dimensions);
		EXPECT_EQ(getLinearIndexColMajor<size_t>(coords, dimensions), i);
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(5, dimensions), "");
#endif

	// 2D, 1x5 elements
	dimensions = {1, 5};
	for (size_t i = 0; i < 1; i++) {
		for (size_t j = 0; j < 5; j++) {
			coords = getSubscriptsColMajor<size_t>(j, dimensions);
			EXPECT_EQ(getLinearIndexColMajor<size_t>(coords, dimensions), j);
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(5, dimensions), "");
#endif

	// 2D, 5x1 elements
	dimensions = {5, 1};
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 1; j++) {
			coords = getSubscriptsColMajor<size_t>(i, dimensions);
			EXPECT_EQ(getLinearIndexColMajor<size_t>(coords, dimensions), i);
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(5, dimensions), "");
#endif

	// 2D, 5x3 elements
	dimensions  = {5, 3};
	linearIndex = 0;
	for (size_t i = 0; i < 5; i++) {
		for (size_t j = 0; j < 3; j++, linearIndex++) {
			coords = getSubscriptsColMajor<size_t>(linearIndex, dimensions);
			EXPECT_EQ(getLinearIndexColMajor<size_t>(coords, dimensions), linearIndex);
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(15, dimensions), "");
#endif

	// 3D
	dimensions  = {2, 3, 4};
	linearIndex = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++, linearIndex++) {
				coords = getSubscriptsColMajor<size_t>(linearIndex, dimensions);
				EXPECT_EQ(getLinearIndexColMajor<size_t>(coords, dimensions), linearIndex);
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(24, dimensions), "");
#endif

	// 4D
	dimensions  = {2, 3, 4, 5};
	linearIndex = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < 4; k++) {
				for (size_t l = 0; l < 5; l++, linearIndex++) {
					coords = getSubscriptsColMajor<size_t>(linearIndex, dimensions);
					EXPECT_EQ(getLinearIndexColMajor<size_t>(coords, dimensions), linearIndex);
				}
			}
		}
	}
#ifdef _DEBUG
	EXPECT_DEATH(getSubscriptsColMajor<size_t>(120, dimensions), "");
#endif
}

TEST(algorithmsAndVectorsTests, getIncrements) {
	// 1D
	EXPECT_EQ(1, 1);
	auto incr = getIncrements<size_t>({33});
	EXPECT_EQ(incr.size(), 1);
	EXPECT_EQ(incr[0], 1);

	// pseudo-1D
	incr = getIncrements<size_t>({33, 1});
	EXPECT_EQ(incr.size(), 2);
	EXPECT_EQ(incr[0], 1);
	EXPECT_EQ(incr[1], 1);

	// 2D
	incr = getIncrements<size_t>({29, 33});
	EXPECT_EQ(incr.size(), 2);
	EXPECT_EQ(incr[0], 33);
	EXPECT_EQ(incr[1], 1);

	// 3D
	incr = getIncrements<size_t>({29, 33, 47});
	EXPECT_EQ(incr.size(), 3);
	EXPECT_EQ(incr[0], 33 * 47);
	EXPECT_EQ(incr[1], 47);
	EXPECT_EQ(incr[2], 1);

	// pseudo-4D
	incr = getIncrements<size_t>({29, 33, 47, 1});
	EXPECT_EQ(incr.size(), 4);
	EXPECT_EQ(incr[0], 33 * 47);
	EXPECT_EQ(incr[1], 47);
	EXPECT_EQ(incr[2], 1);
	EXPECT_EQ(incr[3], 1);

	// 4D
	incr = getIncrements<size_t>({29, 33, 47, 11});
	EXPECT_EQ(incr.size(), 4);
	EXPECT_EQ(incr[0], 33 * 47 * 11);
	EXPECT_EQ(incr[1], 47 * 11);
	EXPECT_EQ(incr[2], 11);
	EXPECT_EQ(incr[3], 1);
}

TEST(algorithmsAndVectorsTests, getIncrementsColMajor) {
	// 1D
	EXPECT_EQ(1, 1);
	auto incr = getIncrementsColMajor<size_t>({33});
	EXPECT_EQ(incr.size(), 1);
	EXPECT_EQ(incr[0], 1);

	// pseudo-1D
	incr = getIncrementsColMajor<size_t>({1, 33});
	EXPECT_EQ(incr.size(), 2);
	EXPECT_EQ(incr[0], 1);
	EXPECT_EQ(incr[1], 1);

	// 2D
	incr = getIncrementsColMajor<size_t>({29, 33});
	EXPECT_EQ(incr.size(), 2);
	EXPECT_EQ(incr[0], 1);
	EXPECT_EQ(incr[1], 29);

	// 3D
	incr = getIncrementsColMajor<size_t>({29, 33, 47});
	EXPECT_EQ(incr.size(), 3);
	EXPECT_EQ(incr[0], 1);
	EXPECT_EQ(incr[1], 29);
	EXPECT_EQ(incr[2], 29 * 33);

	// pseudo-4D
	incr = getIncrementsColMajor<size_t>({1, 29, 33, 47});
	EXPECT_EQ(incr.size(), 4);
	EXPECT_EQ(incr[0], 1);
	EXPECT_EQ(incr[1], 1);
	EXPECT_EQ(incr[2], 29);
	EXPECT_EQ(incr[3], 29 * 33);

	// 4D
	incr = getIncrementsColMajor<size_t>({29, 33, 47, 11});
	EXPECT_EQ(incr.size(), 4);
	EXPECT_EQ(incr[0], 1);
	EXPECT_EQ(incr[1], 29);
	EXPECT_EQ(incr[2], 29 * 33);
	EXPECT_EQ(incr[3], 29 * 33 * 47);
}

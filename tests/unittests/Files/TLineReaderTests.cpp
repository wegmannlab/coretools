#include "gtest/gtest.h"

#include "coretools/Files/TLineReader.h"
#include "coretools/Files/TStdWriter.h"
#include "coretools/Files/TWriter.h"

using namespace coretools;

void emptyFile(std::string_view fileName) {
	{
		std::unique_ptr<TWriter> writer(makeWriter(fileName, "w"));
		// use scope to force flush
	}
	size_t i = 0;
	for (TLineReader lreader(fileName);!lreader.empty(); lreader.popFront(), ++i) {
	}
	EXPECT_EQ(i, 0);
	remove(fileName.data());
}

void lineByLine(std::string_view fileName) {
	constexpr size_t Nlines = 10;
	{
		std::unique_ptr<TWriter> writer(makeWriter(fileName, "w"));
		for (size_t i = 0; i < Nlines; ++i) {
			writer->write(str::toString("line", i + 1, "\n"));
		}
		// use scope to force flush
	}
	size_t i = 0;
	for (TLineReader lreader(fileName);!lreader.empty(); lreader.popFront(), ++i) {
		EXPECT_EQ(lreader.front(), str::toString("line", i + 1));
	}
	EXPECT_EQ(i, Nlines);
	remove(fileName.data());
}

void hugeLine(std::string_view fileName) {
	constexpr size_t Nlines = 3;
	constexpr size_t Nchars = 100000;
	{
		std::unique_ptr<TWriter> writer(makeWriter(fileName, "w"));

		for (size_t i = 0; i < Nlines; ++i) {
			writer->write(std::string(Nchars + i, 'a' + i));
			writer->write('\n');
		}
	}
	
	size_t i = 0;
	for (TLineReader lreader(fileName);!lreader.empty(); lreader.popFront(), ++i) {
		EXPECT_EQ(lreader.front().size(), Nchars + i);
		EXPECT_EQ(lreader.front().back(), 'a' + i);
	}
	EXPECT_EQ(i, Nlines);
	remove(fileName.data());
}

void manyLines(std::string_view fileName) {
	constexpr size_t Nlines = 100000;
	{
		std::unique_ptr<TWriter> writer(makeWriter(fileName, "w"));

		for (size_t i = 0; i < Nlines; ++i) { writer->write(str::toString("line", i + 1, "\n")); }
	}
	// read lines

	size_t i = 0;
	for (TLineReader lreader(fileName); !lreader.empty(); lreader.popFront(), ++i) {
		EXPECT_EQ(lreader.front(), str::toString("line", i + 1));
	}
	EXPECT_EQ(i, Nlines);
	// get/set position
	constexpr auto line = 37;

	TLineReader lreader(fileName);
	for (size_t i = 0; i < line; ++i) lreader.popFront();
	EXPECT_EQ(lreader.front(), str::toString("line", line + 1));
	const auto pos = lreader.getPosition();

	// don't re-read buffer if not necessary
	lreader.setPosition(pos);
	EXPECT_EQ(lreader.front(), str::toString("line", line + 1));
	lreader.popFront();
	lreader.setPosition(pos);
	EXPECT_EQ(lreader.front(), str::toString("line", line + 1));

	// go to end
	while (!lreader.empty()) { lreader.popFront(); }
	// reset buffer
	lreader.setPosition(pos);
	EXPECT_EQ(lreader.front(), str::toString("line", line + 1));
	remove(fileName.data());
}

TEST(LineReaderTests, emptyFile) {
	emptyFile("test.txt");
	emptyFile("test.txt.gz");
}

TEST(LineReaderTests, lineByLine) {
	lineByLine("test.txt");
	lineByLine("test.txt.gz");
}

TEST(LineReaderTests, hugeLine) {
	lineByLine("test.txt");
	lineByLine("test.gz");
}

TEST(LineReaderTests, manyLines) {
	manyLines("test.txt");
	manyLines("test.gz");
}


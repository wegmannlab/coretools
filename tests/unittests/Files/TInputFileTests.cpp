#include "coretools/Files/TInputFile.h"
#include "coretools/Files/TOutputFile.h"

#include "gtest/gtest.h"
using namespace coretools;

template<typename... Args> void closeRemove(TInputFile &file, Args &...files) {
	const auto name = file.name();
	file.close();
	remove(name.c_str());
	if constexpr (sizeof...(Args) > 0) { closeRemove(files...); }
}

std::array<std::string_view, 5> numbers{"one", "two", "three", "four", "five"};
std::array<std::string_view, 5> defHeader{"0", "1", "2", "3", "4"};

void writeHeader(std::string_view name) {
	TOutputFile oFile(name, numbers);
	oFile.writeln(1,2,3,4,5);
	oFile.writeln(1,4,9,16,25);
}
void writeNoHeader(std::string_view name) {
	TOutputFile oFile(name);
	oFile.writeln(1,2,3,4,5);
	oFile.writeln(1,4,9,16,25);
}

TEST(TInputFileTests, ConstructorDefault) {
	TInputFile file;
	EXPECT_FALSE(file.isOpen());
	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.name(), "");
	EXPECT_ANY_THROW(file.numCols());
	EXPECT_EQ(file.delim(), str::whitespaces);
}

void constructorName(TInputFile& file, TConstView<std::string_view> Header) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.numCols(), 5);
	EXPECT_EQ(file.header()[0], Header[0]);
	EXPECT_EQ(file.header()[1], Header[1]);
	EXPECT_EQ(file.header()[2], Header[2]);
	EXPECT_EQ(file.header()[3], Header[3]);
	EXPECT_EQ(file.header()[4], Header[4]);
	EXPECT_EQ(file.delim(), str::whitespaces);

	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.frontRaw(), "1\t2\t3\t4\t5");
	for (size_t i = 0; i < file.numCols(); ++i) {
		EXPECT_EQ(file.get(i), str::toString(i+1));
		EXPECT_EQ(file.get(i), file.front()[i]);
		EXPECT_EQ(file.get<size_t>(i), i+1);
		EXPECT_EQ(file.get(Header[i]), str::toString(i+1));
		EXPECT_EQ(file.get<size_t>(Header[i]), i+1);
	}
	EXPECT_ANY_THROW(file.get(5));
	EXPECT_ANY_THROW(file.get("six"));

	file.popFront();
	EXPECT_EQ(file.curLine(), 1);
	for (size_t i = 0; i < 5; ++i) {
		const auto ii = (i+1)*(i+1);
		EXPECT_EQ(file.get(i), str::toString(ii));
		EXPECT_EQ(file.get<size_t>(i), ii);
	}
	file.popFront();
	EXPECT_TRUE(file.empty());
	EXPECT_TRUE(file.isOpen());
}
TEST(TInputFileTests, ConstructorName) {
	writeHeader("file1.txt");
	TInputFile file1a("file1.txt", FileType::Header);
	EXPECT_EQ(file1a.name(), "file1.txt");
	constructorName(file1a, numbers);

	TInputFile file1b;
	EXPECT_EQ(file1b.name(), "");
	file1b.open("file1.txt", FileType::Header);
	EXPECT_EQ(file1b.name(), "file1.txt");
	constructorName(file1b, numbers);

	writeNoHeader("file2.txt");
	TInputFile file2a("file2.txt", FileType::NoHeader);
	EXPECT_EQ(file2a.name(), "file2.txt");
	constructorName(file2a, defHeader);

	TInputFile file2b;
	EXPECT_EQ(file2b.name(), "");
	file2b.open("file2.txt", FileType::NoHeader);
	EXPECT_EQ(file2b.name(), "file2.txt");
	constructorName(file2b, defHeader);

	closeRemove(file1b, file2b);
}

std::array<std::string_view, 2> columns{"four", "two"};
std::array<int, 2> vals{4, 2};

TEST(TInputFileTests, indexes) {
	writeHeader("file1.txt");
	TInputFile file1("file1.txt", FileType::Header);

	EXPECT_TRUE(file1.hasIndex("three"));
	EXPECT_TRUE(file1.hasIndex("five"));
	EXPECT_FALSE(file1.hasIndex("six"));
	EXPECT_TRUE(file1.hasOneOfTheseIndeces({"zeo", "three", "six"}));
	EXPECT_FALSE(file1.hasOneOfTheseIndeces({"zeo", "minusone", "six"}));

	const auto is = file1.indices({"four", "two"});
	EXPECT_EQ(is.size(), 2);
	EXPECT_EQ(is.front(), 3);
	EXPECT_EQ(is.back(), 1);
	const auto as = file1.indices<std::array<size_t, 5>>(numbers);
	EXPECT_EQ(as.size(), 5);
	EXPECT_EQ(as.front(), 0);
	EXPECT_EQ(as.back(), 4);

	std::vector<std::string> cc{"one", "five"};
	auto is2 = file1.indices(cc);
	std::array<size_t, 2> as2;
	EXPECT_ANY_THROW(file1.fillIndices(defHeader, as2));
	
	EXPECT_EQ(file1.indexOfFirstMatch({"THREE", "Three", "three", "number3"}), 2);	
}

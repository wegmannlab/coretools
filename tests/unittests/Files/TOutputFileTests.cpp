#include "gtest/gtest.h"

#include "coretools/Files/TOutputFile.h"

using namespace coretools;

template<typename... Args> void closeRemove(TOutputFile &file, Args &...files) {
	const auto name = file.name();
	file.close();
	remove(name.c_str());
	if constexpr (sizeof...(Args) > 0) { closeRemove(files...); }
}

TEST(TOutputFileTests, ConstructorDefault) {
	TOutputFile file;
	EXPECT_FALSE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.name(), "");
	EXPECT_FALSE(file.isTable());
	EXPECT_EQ(file.numCols(), 0);
	EXPECT_EQ(file.delim(), "\t");
}

void constructorName(TOutputFile &file, std::string_view name) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.name(), name);
	EXPECT_FALSE(file.isTable());
	EXPECT_EQ(file.numCols(), 0);
	EXPECT_EQ(file.delim(), "\t");
	EXPECT_EQ(file.delim(), "\t");
	EXPECT_ANY_THROW(file.open(name));
}
TEST(TOutputFileTests, ConstructorName) {
	TOutputFile file1("file1.txt");
	constructorName(file1, "file1.txt");
	file1.close();
	file1.open("file1.txt");
	constructorName(file1, "file1.txt");

	TOutputFile file2;
	file2.open("file2.txt");
	constructorName(file2, "file2.txt");

	TOutputFile file1g("file1.txt.gz");
	constructorName(file1g, "file1.txt.gz");
	file1g.close();
	file1g.open("file1.txt.gz");
	constructorName(file1g, "file1.txt.gz");

	TOutputFile file1b("file1.txt.bgz");
	constructorName(file1b, "file1.txt.bgz");
	file1b.close();
	file1b.open("file1.txt.bgz");
	constructorName(file1b, "file1.txt.bgz");

	closeRemove(file1, file1g, file1b, file2);
}

void constructorNCols(TOutputFile &file) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.name(), "file1.txt");
	EXPECT_TRUE(file.isTable());
	EXPECT_EQ(file.numCols(), 3);
	EXPECT_EQ(file.delim(), "\t");
	EXPECT_ANY_THROW(file.open("file1.txt", 3));
}
TEST(TOutputFileTests, ConstructorNCols) {
	TOutputFile file1("file1.txt", 3);
	constructorNCols(file1);

	file1.close();
	file1.open("file1.txt", 3);
	constructorNCols(file1);

	TOutputFile file2;
	file2.open("file1.txt", 3);
	constructorNCols(file2);

	closeRemove(file1, file2);
}

void constructorDelim(TOutputFile &file) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.name(), "file1.txt");
	EXPECT_FALSE(file.isTable());
	EXPECT_EQ(file.numCols(), 0);
	EXPECT_EQ(file.delim(), " ");
	EXPECT_ANY_THROW(file.open("file1.txt", " "));
}
TEST(TOutputFileTests, ConstructorDelim) {
	TOutputFile file1("file1.txt", " ");
	constructorDelim(file1);

	file1.close();
	file1.open("file1.txt", " ");
	constructorDelim(file1);

	TOutputFile file2;
	file2.open("file1.txt", " ");
	constructorDelim(file2);

	closeRemove(file1, file2);
}

void constructorDelimNCols(TOutputFile &file) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 0);
	EXPECT_EQ(file.name(), "file1.txt");
	EXPECT_TRUE(file.isTable());
	EXPECT_EQ(file.numCols(), 3);
	EXPECT_EQ(file.delim(), " ");
	EXPECT_ANY_THROW(file.open("file1.txt", 3, " "));
}
TEST(TOutputFileTests, ConstructorDelimNCols) {
	TOutputFile file1("file1.txt", 3, " ");
	constructorDelimNCols(file1);

	file1.close();
	file1.open("file1.txt", 3, " ");
	constructorDelimNCols(file1);

	TOutputFile file2;
	file2.open("file1.txt", 3, " ");
	constructorDelimNCols(file2);

	closeRemove(file1, file2);
}

void constructorHeader(TOutputFile &file) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 1);
	EXPECT_EQ(file.name(), "file1.txt");
	EXPECT_TRUE(file.isTable());
	EXPECT_EQ(file.delim(), "\t");
	EXPECT_EQ(file.numCols(), 3);
	EXPECT_ANY_THROW(file.open("file1.txt", {"one", "two", "three"}));
}
TEST(TOutputFileTests, ConstructorHeader) {
	std::vector<std::string> header = {"one", "two", "three"};
	TOutputFile file1("file1.txt", header);
	constructorHeader(file1);

	file1.close();
	file1.open("file1.txt", header);
	constructorHeader(file1);

	TOutputFile file2;
	file2.open("file1.txt", header);
	constructorHeader(file2);

	closeRemove(file1, file2);
}

void constructorHeaderDelim(TOutputFile &file) {
	EXPECT_TRUE(file.isOpen());
	EXPECT_EQ(file.curCol(), 0);
	EXPECT_EQ(file.curLine(), 1);
	EXPECT_EQ(file.name(), "file1.txt");
	EXPECT_TRUE(file.isTable());
	EXPECT_EQ(file.delim(), " ");
	EXPECT_EQ(file.numCols(), 3);
	EXPECT_ANY_THROW(file.open("file1.txt", {"one", "two", "three"}, " "));
}
TEST(TOutputFileTests, ConstructorHeaderDelim) {
	std::vector<std::string> header = {"one", "two", "three"};
	TOutputFile file1("file1.txt", header, " ");
	constructorHeaderDelim(file1);

	file1.close();
	file1.open("file1.txt", header, " ");
	constructorHeaderDelim(file1);

	TOutputFile file2;
	file2.open("file1.txt", header, " ");
	constructorHeaderDelim(file2);

	closeRemove(file1, file2);
}

TEST(TOutputFileTests, append) {
	std::vector<std::string> header = {"one", "two", "three"};
	TOutputFile file("file1.txt", header, " ");
	file.close();
	EXPECT_FALSE(file.isOpen());

	// now open again -> will jump to the end without overwriting
	TOutputFile file2;
	file2.append("file1.txt", 3, " ");
	EXPECT_TRUE(file2.isOpen());
	EXPECT_EQ(file2.curCol(), 0);
	EXPECT_EQ(file2.curLine(), 0);
	EXPECT_EQ(file2.name(), "file1.txt");
	EXPECT_TRUE(file2.isTable());
	EXPECT_EQ(file2.numCols(), 3);
	EXPECT_EQ(file2.delim(), " ");
	closeRemove(file2);
}

TEST(TOutputFileTests, header) {
	TOutputFile file1;
	file1.writeHeader({"one", "two", "three"}); // only throws when actually writing
	EXPECT_ANY_THROW(file1.close());

	TOutputFile file2("file1.txt");
	EXPECT_EQ(file2.numCols(), 0);
	file2.numCols(3);
	EXPECT_EQ(file2.numCols(), 3);
	file2.numCols(4);
	EXPECT_EQ(file2.numCols(), 4);

	TOutputFile file3("file1.txt");
	file3.writeHeader({"one", "two", "three"});
	EXPECT_EQ(file3.numCols(), 3);
	EXPECT_ANY_THROW(file3.writeHeader({"four", "five", "six"}));
	EXPECT_ANY_THROW(file3.numCols(4));

	closeRemove(file2, file3);
}

//
// Created by madleina on 20.04.21.
//
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/Storage/TDataFile.h"
#include "coretools/Types/commonWeakTypes.h"

using namespace testing;
using namespace coretools;

template<typename Type, size_t NumDim> class BridgeDataReader : public TDataReaderBase<Type, NumDim> {
public:
	void _checkDelimiters() { TDataReaderBase<Type, NumDim>::_checkDelimiters(); }
	void _initialize(std::string_view Filename, TMultiDimensionalStorage<Type, NumDim> *Storage,
	                 const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters,
	                 const std::vector<bool> &NameIsWritten, std::vector<size_t> &Dimensions,
	                 std::vector<bool> &DimensionsFilled, size_t GuessLengthUnknownDimension,
	                 const colNameTypes &ColNameType) {
		TDataReaderBase<Type, NumDim>::_initialize(Filename, Storage, Names, Delimiters, NameIsWritten, Dimensions,
		                                           DimensionsFilled, GuessLengthUnknownDimension, ColNameType);
	}
	void _checkIfBlockSizeCanBeInferred(const std::vector<bool> &DimensionsFilled) {
		TDataReaderBase<Type, NumDim>::_checkIfBlockSizeCanBeInferred(DimensionsFilled);
	}
	void _countNumberDelims_oneLine(std::string_view Line, std::map<char, size_t> &DelimCounter, bool FirstLine) {
		TDataReaderBase<Type, NumDim>::_countNumberDelims_oneLine(Line, DelimCounter, FirstLine);
	}
	size_t _getProductOverAllDim_WithSameDelim(size_t Dim, const std::vector<size_t> &Dimensions,
	                                           const std::vector<bool> &DimensionsFilled) {
		return TDataReaderBase<Type, NumDim>::_getProductOverAllDim_WithSameDelim(Dim, Dimensions, DimensionsFilled);
	}
	size_t _removeAllUpperColumns_FromDelimCounter(size_t TotalCountsDelim, size_t Dim,
	                                               const std::vector<size_t> &Dimensions,
	                                               const std::vector<bool> &DimensionsFilled) {
		return TDataReaderBase<Type, NumDim>::_removeAllUpperColumns_FromDelimCounter(TotalCountsDelim, Dim, Dimensions,
		                                                                              DimensionsFilled);
	}
	void _inferDimensions_oneLine(std::map<char, size_t> &DelimCounter, std::vector<size_t> &Dimensions,
	                              std::vector<bool> &DimensionsFilled) {
		TDataReaderBase<Type, NumDim>::_inferDimensions_oneLine(DelimCounter, Dimensions, DimensionsFilled);
	}
	void _translateBlockCoordinates_toIndexInStorage(const std::vector<size_t> &BlockDimensions,
	                                                 const std::vector<std::vector<size_t>> &CoordinatesOneBlock) {
		TDataReaderBase<Type, NumDim>::_translateBlockCoordinates_toIndexInStorage(BlockDimensions,
		                                                                           CoordinatesOneBlock);
	}
	std::vector<size_t> _getAllExceptFirst(const std::vector<size_t> &Vec) {
		return TDataReaderBase<Type, NumDim>::_getAllExceptFirst(Vec);
	}
	void cheat_setColNamesSizes(size_t Dim, size_t Length, std::vector<size_t> &Dimensions,
	                            std::vector<bool> &DimensionsFilled) {
		this->_colNames->setLengthUnnamedDimension(Length, Dim, Dimensions, DimensionsFilled);
	}
	void cheat_setRowNamesSizes(size_t Dim, size_t Length, std::vector<size_t> &Dimensions,
	                            std::vector<bool> &DimensionsFilled) {
		this->_rowNames->setLengthUnnamedDimension(Length, Dim, Dimensions, DimensionsFilled);
	}
	bool storeCell(size_t Index) { return this->_storeCell[Index]; }
	size_t indexCell_inOneBlockStorage(size_t Index) { return this->_indexCell_inOneBlockStorage[Index]; }
	size_t dimensionsOfStorage(size_t Dim) { return this->_dimensionsOfStorage[Dim]; }
};

template<size_t NumDim> class TDataReaderTestBase {
public:
	BridgeDataReader<Unbounded, NumDim> dataReader;

	std::string fileName;
	TMultiDimensionalStorage<Unbounded, NumDim> storage;
	std::vector<std::shared_ptr<TNamesEmpty>> names;
	std::vector<char> delimiters;
	std::vector<bool> nameIsWritten;
	std::vector<size_t> dimensions;
	std::vector<bool> knownDimensions;
	size_t guessLengthUnknownDimension;
	colNameTypes colNameType;

	void initializeToDefault() {
		fileName = "test.txt";

		guessLengthUnknownDimension = 10;
		colNameType                 = colNames_multiline;

		// set names
		names.clear();
		for (size_t dim = 0; dim < NumDim; dim++) {
			auto name = std::make_shared<TNamesIndices>();
			names.push_back(name);
		}

		// set delimiters
		delimiters.clear();
		delimiters.push_back('\n');
		if (NumDim > 1) {
			delimiters.push_back('\t');
			for (size_t dim = 2; dim < NumDim; dim++) { delimiters.push_back('/'); }
		}

		nameIsWritten.clear();
		dimensions.clear();
		knownDimensions.clear();
		for (size_t dim = 0; dim < NumDim; dim++) {
			nameIsWritten.push_back(true);
			dimensions.push_back(0);
			knownDimensions.push_back(false);
		}
	}

	void initialize() {
		dataReader._initialize(fileName, &storage, names, delimiters, nameIsWritten, dimensions, knownDimensions,
		                       guessLengthUnknownDimension, colNameType);
	}
};

class TDataReaderTest_5D : public Test, public TDataReaderTestBase<5> {
public:
	void SetUp() override { initializeToDefault(); }
};

class TDataReaderTest_4D : public Test, public TDataReaderTestBase<4> {
public:
	void SetUp() override { initializeToDefault(); }
};

TEST_F(TDataReaderTest_5D, _checkDelimiters) {
	// all delimiters must either be the same as the delimiter of the previous dimension;
	//  or then be a character that has not occurred before
	delimiters = {'\n', '\t', '/', '_', '.'}; // ok
	initialize();
	EXPECT_NO_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\n', '/', '/', '/'}; // ok
	initialize();
	EXPECT_NO_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\t', '/', '_', '\n'}; // not ok
	initialize();
	EXPECT_ANY_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\t', '/', '_', '/'}; // not ok
	initialize();
	EXPECT_ANY_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\t', '/', '_', '\t'}; // not ok
	initialize();
	EXPECT_ANY_THROW(dataReader._checkDelimiters());
}

TEST_F(TDataReaderTest_5D, _checkDelimiters_newlines) {
	// only first delimiter(s) are allowed to be \n. As soon a non-\n delimiter is used, we can't use \n anymore
	delimiters = {'\n', '\t', '/', '_', '.'}; // ok
	initialize();
	EXPECT_NO_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\n', '/', '_', '.'}; // ok
	initialize();
	EXPECT_NO_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\n', '\n', '\n', '\n'}; // ok
	initialize();
	EXPECT_NO_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\t', '\n', '_', '/'}; // not ok
	initialize();
	EXPECT_ANY_THROW(dataReader._checkDelimiters());

	delimiters = {'\n', '\t', '/', '_', '\n'}; // not ok
	initialize();
	EXPECT_ANY_THROW(dataReader._checkDelimiters());
}

TEST_F(TDataReaderTest_5D, _checkIfBlockSizeCanBeInferred) {
	// for all dimension names with the same delimiter:
	// max. 1 can have unknown dimensions, all others must be known, otherwise we can't infer dimensions
	// dimensions are known if a) set by developer or b) written in header
	nameIsWritten = {false, true, true, true, true};
	delimiters    = {'\n', '/', '/', '/', '/'}; // ok, 0 unknown
	initialize();
	EXPECT_NO_THROW(dataReader._checkIfBlockSizeCanBeInferred(knownDimensions));

	nameIsWritten = {false, true, true, true, false};
	delimiters    = {'\n', '/', '/', '/', '/'}; // ok, 1 unknown
	initialize();
	EXPECT_NO_THROW(dataReader._checkIfBlockSizeCanBeInferred(knownDimensions));

	nameIsWritten = {false, false, true, true, false};
	delimiters    = {'\n', '/', '/', '/', '/'}; // not ok, 2 unknown
	initialize();
	EXPECT_ANY_THROW(dataReader._checkIfBlockSizeCanBeInferred(knownDimensions));

	nameIsWritten   = {false, false, true, true, false};
	delimiters      = {'\n', '/', '/', '/', '/'};
	knownDimensions = {false, false, false, false, true}; // ok, 1 unknown
	initialize();
	EXPECT_NO_THROW(dataReader._checkIfBlockSizeCanBeInferred(knownDimensions));
}

TEST_F(TDataReaderTest_4D, _countNumberDelims_oneLine) {
	delimiters = {'\n', '/', '\t', '_'}; // all different
	initialize();

	// first line
	std::string line = "1/2/3_4/5/6_7/8/9_10/11/12\t13/14/15_16/17/18_19/20/21_22/23/24\t25/26/27_28/29/30_31/32/33_34/"
	                   "35/36\t37/38/39_40/41/42_43/44/45_46/47/48\t49/50/51_52/53/54_55/56/57_58/59/60";

	std::map<char, size_t> delimCounter;
	dataReader._countNumberDelims_oneLine(line, delimCounter, true);

	EXPECT_EQ(delimCounter.size(), 4);
	EXPECT_EQ(delimCounter['\n'], 0);
	EXPECT_EQ(delimCounter['/'], 40);
	EXPECT_EQ(delimCounter['\t'], 4);
	EXPECT_EQ(delimCounter['_'], 15);

	// another line -> ok, same!
	line = "1/2/3_4/5/6_7/8/9_10/11/12\t13/14/15_16/17/18_19/20/21_22/23/24\t25/26/27_28/29/30_31/32/33_34/35/36\t37/"
	       "38/39_40/41/42_43/44/45_46/47/48\t49/50/51_52/53/54_55/56/57_58/59/60";

	dataReader._countNumberDelims_oneLine(line, delimCounter, false);

	EXPECT_EQ(delimCounter.size(), 4);
	EXPECT_EQ(delimCounter['\n'], 0);
	EXPECT_EQ(delimCounter['/'], 40);
	EXPECT_EQ(delimCounter['\t'], 4);
	EXPECT_EQ(delimCounter['_'], 15);

	// another line, a bit different -> should throw!
	line = "1/2/3_4/5/6_7/8/9_10/11/12\t13/14/15_16/17/18_19/20/21_22/23_24\t25/26/27_28/29/30_31/32/33_34/35/36\t37/"
	       "38/39_40/41/42_43/44/45_46/47/48\t49/50/51_52/53/54_55/56/57_58/59/60";
	//                                                                  | changed / to _
	EXPECT_ANY_THROW(dataReader._countNumberDelims_oneLine(line, delimCounter, false));
}

TEST_F(TDataReaderTest_4D, _getProductOverAllDim_WithSameDelim) {
	// all different
	delimiters = {'\n', '/', '\t', '_'};
	initialize();

	std::vector<size_t> dimensions     = {0, 5, 4, 3};
	std::vector<bool> dimensionsFilled = {false, true, true, true};

	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(0, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(1, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(2, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(3, dimensions, dimensionsFilled), 1);

	// two are the same
	delimiters = {'\n', '\t', '/', '/'};
	initialize();

	dimensions       = {0, 5, 4, 3};
	dimensionsFilled = {false, true, true, true};

	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(0, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(1, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(2, dimensions, dimensionsFilled), 3);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(3, dimensions, dimensionsFilled), 4);

	// all the same
	delimiters = {'\n', '/', '/', '/'};
	initialize();

	dimensions       = {0, 5, 4, 3};
	dimensionsFilled = {false, true, true, true};

	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(0, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(1, dimensions, dimensionsFilled), 12);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(2, dimensions, dimensionsFilled), 15);
	EXPECT_EQ(dataReader._getProductOverAllDim_WithSameDelim(3, dimensions, dimensionsFilled), 20);
}

TEST_F(TDataReaderTest_4D, _removeAllUpperColumns_FromDelimCounter) {
	delimiters = {'\n', '\t', '_', '_'};
	initialize();

	std::vector<size_t> dimensions     = {0, 5, 0, 3};
	std::vector<bool> dimensionsFilled = {false, true, false, true};

	EXPECT_EQ(dataReader._removeAllUpperColumns_FromDelimCounter(1, 0, dimensions, dimensionsFilled), 1);
	EXPECT_EQ(dataReader._removeAllUpperColumns_FromDelimCounter(4, 1, dimensions, dimensionsFilled), 4);
	EXPECT_EQ(dataReader._removeAllUpperColumns_FromDelimCounter(55, 2, dimensions, dimensionsFilled), 11);
	EXPECT_EQ(dataReader._removeAllUpperColumns_FromDelimCounter(55, 3, dimensions, dimensionsFilled), 11);
}

TEST_F(TDataReaderTest_4D, _inferDimensions_oneLine_allDifferent) {
	// all different
	delimiters = {'\n', '\t', '_', '/'}; // all different
	initialize();

	std::map<char, size_t> delimCounter;
	delimCounter['\n'] = 0;
	delimCounter['/']  = 40;
	delimCounter['\t'] = 4;
	delimCounter['_']  = 15;

	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false};

	dataReader._inferDimensions_oneLine(delimCounter, dimensions, dimensionsFilled);

	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, true, true, true));
}

TEST_F(TDataReaderTest_4D, _inferDimensions_oneLine_someAreDifferent) {
	// all different
	delimiters = {'\n', '\t', '_', '_'};
	initialize();

	std::map<char, size_t> delimCounter;
	delimCounter['\n'] = 0;
	delimCounter['\t'] = 4;
	delimCounter['_']  = 55;

	std::vector<size_t> dimensions     = {0, 0, 0, 3}; // must set one of the two dimensions
	std::vector<bool> dimensionsFilled = {false, false, false, true};

	dataReader._inferDimensions_oneLine(delimCounter, dimensions, dimensionsFilled);

	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, true, true, true));
}

TEST_F(TDataReaderTest_4D, _inferDimensions_oneLine_allAreSame) {
	// all same
	delimiters = {'\n', '_', '_', '_'};
	initialize();

	std::map<char, size_t> delimCounter;
	delimCounter['\n'] = 0;
	delimCounter['_']  = 59;

	std::vector<size_t> dimensions     = {0, 5, 0, 3}; // must set two of the three dimensions
	std::vector<bool> dimensionsFilled = {false, true, false, true};

	dataReader._inferDimensions_oneLine(delimCounter, dimensions, dimensionsFilled);

	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, true, true, true));
}

TEST_F(TDataReaderTest_4D, _translateBlockCoordinates_toIndexInStorage_noShuffle_keepAll) {
	// all different
	delimiters = {'\n', '\n', '_', '_'}; // 2 rownames, 2 colnames
	names[1]->resize(5);
	names[2]->resize(4);
	names[3]->resize(3);
	initialize();

	std::vector<size_t> dimensions     = {0, 5, 4, 3};
	std::vector<bool> dimensionsFilled = {false, true, true, true};

	// cheat: set row- and col name sizes
	dataReader.cheat_setRowNamesSizes(1, 5, dimensions, dimensionsFilled);
	dataReader.cheat_setColNamesSizes(2, 4, dimensions, dimensionsFilled);
	dataReader.cheat_setColNamesSizes(3, 3, dimensions, dimensionsFilled);

	// construct delimiters and coordinates for one block (= all except most outer dimension)
	std::vector<size_t> dimensionsOneBlock               = dataReader._getAllExceptFirst(dimensions);
	std::vector<std::vector<size_t>> coordinatesOneBlock = TDataBlock::getCoordinatesOneBlock(dimensionsOneBlock);

	dataReader._translateBlockCoordinates_toIndexInStorage(dimensionsOneBlock, coordinatesOneBlock);

	// check
	for (size_t i = 0; i < 5 * 4 * 3; i++) {
		EXPECT_TRUE(dataReader.storeCell(i));
		EXPECT_EQ(dataReader.indexCell_inOneBlockStorage(i), i);
	}

	EXPECT_EQ(dataReader.dimensionsOfStorage(1), 5);
	EXPECT_EQ(dataReader.dimensionsOfStorage(2), 4);
	EXPECT_EQ(dataReader.dimensionsOfStorage(3), 3);
}

//--------------------------------
// End-to-End tests
//--------------------------------

class TDataWriterAndReaderTest : public Test {
protected:
	template<typename Type, size_t NumDim>
	void _fillStorage_Writing(TMultiDimensionalStorage<Type, NumDim> *Storage, size_t FirstDim,
	                          const std::array<size_t, NumDim - 1> &AllOtherDimensions) {
		Storage->prepareFillData(FirstDim, AllOtherDimensions);

		size_t totalSize = FirstDim;
		for (auto &dim : AllOtherDimensions) { totalSize *= dim; }

		for (size_t i = 0; i < totalSize; i++) { Storage->emplace_back(i + 0.5); }

		Storage->finalizeFillData();
	}

	template<typename Type, size_t NumDim>
	void _fillStorage_Writing_SkipShuffle(std::string_view FileName, const std::vector<char> &Delimiters,
	                                      const std::vector<bool> &NameIsWritten, const colNameTypes &ColnamesType,
	                                      TMultiDimensionalStorage<Type, NumDim> *StorageOther,
	                                      const std::vector<size_t> &NewDimensions,
	                                      const std::vector<std::shared_ptr<TNamesEmpty>> &NamesSkippedShuffled) {
		// get block size of new storage
		std::array<size_t, NumDim - 1> oneBlock;
		for (size_t d = 0; d < NumDim - 1; d++) { oneBlock[d] = NewDimensions[d + 1]; }

		// get total size of new storage
		size_t totalSize = NewDimensions[0];
		for (auto &dim : oneBlock) { totalSize *= dim; }

		// adjust length of names
		std::vector<size_t> dimsThatWereResized;
		for (size_t dim = 0; dim < NewDimensions.size(); dim++) {
			if (NamesSkippedShuffled[dim]->size() == 0) {
				NamesSkippedShuffled[dim]->resize(NewDimensions[dim]);
				dimsThatWereResized.push_back(dim);
			}
		}

		// now get values -> how to fill storage
		double curVal = 0.5;
		std::vector<double> values(totalSize);
		for (size_t i = 0; i < StorageOther->size(); i++) { // iterate over other storage (linear)
			std::array<size_t, NumDim> coordinates = StorageOther->getSubscripts(i);
			// check if one value should be kept
			bool keep                              = true;
			for (size_t dim = 0; dim < coordinates.size(); dim++) {
				std::string nameOther = (*StorageOther->getDimensionName(dim))[coordinates[dim]];
				if (!NamesSkippedShuffled[dim]->exists(nameOther)) { keep = false; }
			}

			// value should be kept -> get coordinates where to store it in new storage
			if (keep) {
				std::vector<size_t> coordinatesInNewStorage(coordinates.size());
				for (size_t dim = 0; dim < coordinates.size(); dim++) {
					std::string nameOther        = (*StorageOther->getDimensionName(dim))[coordinates[dim]];
					size_t index                 = NamesSkippedShuffled[dim]->getIndex(nameOther);
					coordinatesInNewStorage[dim] = index;
				}
				size_t linearIndex  = getLinearIndex(coordinatesInNewStorage, NewDimensions);
				values[linearIndex] = curVal;
			}
			// increase curVal, no matter if we stored it or not
			curVal++;
		}

		// write file with these shuffled/skipped names to compare with!
		TDataWriterBase<Type, NumDim> writerTruth;
		TMultiDimensionalStorage<Type, NumDim> storageNew;

		// finally fill new storage!
		storageNew.prepareFillData(NewDimensions[0], oneBlock);
		for (size_t i = 0; i < totalSize; i++) { storageNew.emplace_back(values[i]); }

		storageNew.finalizeFillData();
		writerTruth.write(NamesSkippedShuffled, FileName, &storageNew, Delimiters, NameIsWritten, ColnamesType);

		// now change names back
		for (auto &dim : dimsThatWereResized) { NamesSkippedShuffled[dim]->resize(0); }
	}

public:
	// write
	TMultiDimensionalStorage<Unbounded, 1> storage_1D_write;
	TMultiDimensionalStorage<Unbounded, 2> storage_2D_write;
	TMultiDimensionalStorage<Unbounded, 3> storage_3D_write;
	TMultiDimensionalStorage<Unbounded, 4> storage_4D_write;

	// read
	TMultiDimensionalStorage<Unbounded, 1> storage_1D_read;
	TMultiDimensionalStorage<Unbounded, 2> storage_2D_read;
	TMultiDimensionalStorage<Unbounded, 3> storage_3D_read;
	TMultiDimensionalStorage<Unbounded, 4> storage_4D_read;

	std::shared_ptr<TNamesEmpty> names_dim0_write;
	std::shared_ptr<TNamesEmpty> names_dim1_write;
	std::shared_ptr<TNamesEmpty> names_dim2_write;
	std::shared_ptr<TNamesEmpty> names_dim3_write;

	std::shared_ptr<TNamesEmpty> names_dim0_read;
	std::shared_ptr<TNamesEmpty> names_dim1_read;
	std::shared_ptr<TNamesEmpty> names_dim2_read;
	std::shared_ptr<TNamesEmpty> names_dim3_read;

	size_t guessLengthFirstDimension = 10;

	void prepareWriting() {
		// build storages
		_fillStorage_Writing(&storage_1D_write, 10, {});
		_fillStorage_Writing(&storage_2D_write, 10, {5});
		_fillStorage_Writing(&storage_3D_write, 10, {5, 4});
		_fillStorage_Writing(&storage_4D_write, 10, {5, 4, 3});

		// create names
		names_dim0_write             = std::make_shared<TNamesIndicesAlphabetUpperCase>(10);
		std::vector<std::string> tmp = {"one", "two", "three", "four", "five"};
		names_dim1_write             = std::make_shared<TNamesStrings>(tmp);
		names_dim2_write             = std::make_shared<TNamesIndicesAlphabetLowerCase>(4);
		names_dim3_write             = std::make_shared<TNamesIndices>(3);
		storage_1D_write.setDimensionName(names_dim0_write, 0);
		storage_2D_write.setDimensionName(names_dim0_write, 0);
		storage_2D_write.setDimensionName(names_dim1_write, 1);
		storage_3D_write.setDimensionName(names_dim0_write, 0);
		storage_3D_write.setDimensionName(names_dim1_write, 1);
		storage_3D_write.setDimensionName(names_dim2_write, 2);
		storage_4D_write.setDimensionName(names_dim0_write, 0);
		storage_4D_write.setDimensionName(names_dim1_write, 1);
		storage_4D_write.setDimensionName(names_dim2_write, 2);
		storage_4D_write.setDimensionName(names_dim3_write, 3);
	}

	void prepareReading() {
		// create names
		names_dim0_read = std::make_shared<TNamesIndicesAlphabetUpperCase>();
		names_dim1_read = std::make_shared<TNamesStrings>();
		names_dim2_read = std::make_shared<TNamesIndicesAlphabetLowerCase>();
		names_dim3_read = std::make_shared<TNamesIndices>();
	}

	void SetUp() override {
		prepareWriting();
		prepareReading();
	}

	// check if two files are equal
	bool range_equal(std::istreambuf_iterator<char> first1, std::istreambuf_iterator<char> last1,
	                 std::istreambuf_iterator<char> first2, std::istreambuf_iterator<char> last2) {
		while (first1 != last1 && first2 != last2) {
			if (*first1 != *first2) return false;
			++first1;
			++first2;
		}
		return (first1 == last1) && (first2 == last2);
	}

	bool compare_files(std::string_view filename1, std::string_view filename2) {
		std::ifstream file1(std::string{filename1});
		std::ifstream file2(std::string{filename2});

		std::istreambuf_iterator<char> begin1(file1);
		std::istreambuf_iterator<char> begin2(file2);

		std::istreambuf_iterator<char> end;

		return range_equal(begin1, end, begin2, end);
	}
};

TEST_F(TDataWriterAndReaderTest, write_1D) {
	std::string filename         = "file1D_col.txt";
	std::vector<char> delimiters = {'\n'};
	std::vector<bool> isWritten  = {true};

	TDataWriterBase<Unbounded, 1> writer;
	writer.write(filename, &storage_1D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0};
	std::vector<bool> dimensionIsKnown = {false};

	TDataReaderBase<Unbounded, 1> reader;
	reader.read(filename, &storage_1D_read, {names_dim0_read}, delimiters, isWritten, dimensions, dimensionIsKnown,
	            guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 1> writerCheck;
	std::string fileNameCheck = "file1D_col_check.txt";
	writerCheck.write({names_dim0_read}, fileNameCheck, &storage_1D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_1D_row) {
	std::string filename         = "file1D_row.txt";
	std::vector<char> delimiters = {'\t'};
	std::vector<bool> isWritten  = {true};

	TDataWriterBase<Unbounded, 1> writer;
	writer.write(filename, &storage_1D_write, delimiters, isWritten, colNames_multiline);

	// read!
	// Note: this should throw -> we need to read it into 2D-storage!
	std::vector<size_t> dimensions     = {0};
	std::vector<bool> dimensionIsKnown = {false};

	TDataReaderBase<Unbounded, 1> reader;
	EXPECT_ANY_THROW(reader.read(filename, &storage_1D_read, {names_dim0_read}, delimiters, isWritten, dimensions,
	                             dimensionIsKnown, guessLengthFirstDimension, colNames_multiline));

	// read it into 2D storage instead
	dimensions       = {1, 0};
	dimensionIsKnown = {true, false};
	delimiters       = {'\n', '\t'};
	isWritten        = {false, true};
	auto names       = {std::make_shared<TNamesEmpty>(), names_dim0_read};

	TDataReaderBase<Unbounded, 2> reader2;
	reader2.read(filename, &storage_2D_read, names, delimiters, isWritten, dimensions, dimensionIsKnown,
	             guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 2> writerCheck;
	std::string fileNameCheck = "file1D_row_check.txt";
	writerCheck.write(names, fileNameCheck, &storage_2D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_2D) {
	std::string filename         = "file2D.txt";
	std::vector<char> delimiters = {'\n', '\t'};
	std::vector<bool> isWritten  = {true, true};

	TDataWriterBase<Unbounded, 2> writer;
	writer.write("file2D.txt", &storage_2D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0, 0};
	std::vector<bool> dimensionIsKnown = {false, false};

	TDataReaderBase<Unbounded, 2> reader;
	reader.read(filename, &storage_2D_read, {names_dim0_read, names_dim1_read}, delimiters, isWritten, dimensions,
	            dimensionIsKnown, guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 2> writerCheck;
	std::string fileNameCheck = "file2D_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read}, fileNameCheck, &storage_2D_read, delimiters, isWritten,
	                  colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_2D_full) {
	std::string filename         = "file2D_full.txt";
	std::vector<char> delimiters = {'\n', '\t'};
	std::vector<bool> isWritten  = {true, true};

	TDataWriterBase<Unbounded, 2> writer;
	writer.write(filename, &storage_2D_write, delimiters, isWritten, colNames_full);

	// read!
	std::vector<size_t> dimensions     = {0, 0};
	std::vector<bool> dimensionIsKnown = {false, false};

	TDataReaderBase<Unbounded, 2> reader;
	reader.read(filename, &storage_2D_read, {names_dim0_read, names_dim1_read}, delimiters, isWritten, dimensions,
	            dimensionIsKnown, guessLengthFirstDimension, colNames_full);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 2> writerCheck;
	std::string fileNameCheck = "file2D_full_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read}, fileNameCheck, &storage_2D_read, delimiters, isWritten,
	                  colNames_full);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_2D_concatenated) {
	std::string filename         = "file2D_concatenated.txt";
	std::vector<char> delimiters = {'\n', '\t'};
	std::vector<bool> isWritten  = {true, true};

	TDataWriterBase<Unbounded, 2> writer;
	writer.write(filename, &storage_2D_write, delimiters, isWritten, colNames_concatenated);

	// read!
	std::vector<size_t> dimensions     = {0, 0};
	std::vector<bool> dimensionIsKnown = {false, false};

	TDataReaderBase<Unbounded, 2> reader;
	reader.read(filename, &storage_2D_read, {names_dim0_read, names_dim1_read}, delimiters, isWritten, dimensions,
	            dimensionIsKnown, guessLengthFirstDimension, colNames_concatenated);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 2> writerCheck;
	std::string fileNameCheck = "file2D_concatenated_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read}, fileNameCheck, &storage_2D_read, delimiters, isWritten,
	                  colNames_concatenated);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_3D) {
	std::string filename         = "file3D.txt";
	std::vector<char> delimiters = {'\n', '\t', '/'};
	std::vector<bool> isWritten  = {true, true, true};

	TDataWriterBase<Unbounded, 3> writer;
	writer.write(filename, &storage_3D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false};

	TDataReaderBase<Unbounded, 3> reader;
	reader.read(filename, &storage_3D_read, {names_dim0_read, names_dim1_read, names_dim2_read}, delimiters, isWritten,
	            dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 3> writerCheck;
	std::string fileNameCheck = "file3D_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read}, fileNameCheck, &storage_3D_read, delimiters,
	                  isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_3D_full) {
	std::string filename         = "file3D_full.txt";
	std::vector<char> delimiters = {'\n', '\t', '/'};
	std::vector<bool> isWritten  = {true, true, true};

	TDataWriterBase<Unbounded, 3> writer;
	writer.write(filename, &storage_3D_write, delimiters, isWritten, colNames_full);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false};

	TDataReaderBase<Unbounded, 3> reader;
	reader.read(filename, &storage_3D_read, {names_dim0_read, names_dim1_read, names_dim2_read}, delimiters, isWritten,
	            dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_full);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 3> writerCheck;
	std::string fileNameCheck = "file3D_full_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read}, fileNameCheck, &storage_3D_read, delimiters,
	                  isWritten, colNames_full);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_3D_concatenated) {
	std::string filename         = "file3D_concatenated.txt";
	std::vector<char> delimiters = {'\n', '\t', '/'};
	std::vector<bool> isWritten  = {true, true, true};

	TDataWriterBase<Unbounded, 3> writer;
	writer.write(filename, &storage_3D_write, delimiters, isWritten, colNames_concatenated);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false};

	TDataReaderBase<Unbounded, 3> reader;
	reader.read(filename, &storage_3D_read, {names_dim0_read, names_dim1_read, names_dim2_read}, delimiters, isWritten,
	            dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_concatenated);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 3> writerCheck;
	std::string fileNameCheck = "file3D_concatenated_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read}, fileNameCheck, &storage_3D_read, delimiters,
	                  isWritten, colNames_concatenated);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D) {
	std::string filename         = "file4D.txt";
	std::vector<char> delimiters = {'\n', '\t', '_', '_'};
	std::vector<bool> isWritten  = {true, true, true, true};

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read},
	            delimiters, isWritten, dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read}, fileNameCheck,
	                  &storage_4D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_full) {
	std::string filename         = "file4D_full.txt";
	std::vector<char> delimiters = {'\n', '\t', ':', '/'};
	std::vector<bool> isWritten  = {true, true, false, true};

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, colNames_full);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read},
	            delimiters, isWritten, dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_full);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_full_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read}, fileNameCheck,
	                  &storage_4D_read, delimiters, isWritten, colNames_full);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_concatenated) {
	std::string filename         = "file4D_concatenated.txt";
	std::vector<char> delimiters = {'\n', '\t', '\t', '\t'};
	std::vector<bool> isWritten  = {true, true, true, true};

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, colNames_concatenated);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read},
	            delimiters, isWritten, dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_concatenated);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_concatenated_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read}, fileNameCheck,
	                  &storage_4D_read, delimiters, isWritten, colNames_concatenated);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_doubletabDelim) {
	std::string filename         = "file4D_tabs.txt";
	std::vector<char> delimiters = {'\n', '\t', '\t', '\t'};
	std::vector<bool> isWritten  = {true, true, true, true};

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read},
	            delimiters, isWritten, dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_tabs_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read}, fileNameCheck,
	                  &storage_4D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_doubleNewlineDelim) {
	std::string filename         = "file4D_newlines.txt";
	std::vector<char> delimiters = {'\n', '\n', '\n', '\t'};
	std::vector<bool> isWritten  = {true, true, true, true};

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read},
	            delimiters, isWritten, dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_newlines_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read}, fileNameCheck,
	                  &storage_4D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_omitHeader) {
	std::string filename         = "file4D_omitHeader.txt";
	std::vector<char> delimiters = {'\n', '\n', '\t', '\t'};
	std::vector<bool> isWritten  = {true, true, false, true};

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, colNames_multiline);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read},
	            delimiters, isWritten, dimensions, dimensionIsKnown, guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_omitHeader_check.txt";
	writerCheck.write({names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read}, fileNameCheck,
	                  &storage_4D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(filename, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameCheck.c_str());
}

//--------------------------------
// More complex cases!
// Shuffle and skip names
//--------------------------------
TEST_F(TDataWriterAndReaderTest, write_1D_1Col_shuffleSkip) {
	std::string filename         = "file1D_1col_shuffleSkip.txt";
	std::vector<char> delimiters = {'\n'};
	std::vector<bool> isWritten  = {true};
	colNameTypes type            = colNames_concatenated;

	TDataWriterBase<Unbounded, 1> writer;
	writer.write(filename, &storage_1D_write, delimiters, isWritten, type);

	// read!
	std::vector<size_t> dimensions     = {0};
	std::vector<bool> dimensionIsKnown = {false};

	// fill names
	// skip some rownames
	names_dim0_read = std::make_shared<TNamesStrings>();
	names_dim0_read->addName({"B"});
	names_dim0_read->addName({"C"});
	names_dim0_read->addName({"D"});
	names_dim0_read->addName({"F"});
	names_dim0_read->addName({"H"});
	names_dim0_read->addName({"J"});

	auto vecNames                     = {names_dim0_read};
	std::vector<size_t> newDimensions = {6};

	std::string fileNameTruth = "file1D_1col_shuffleSkip_check_truth.txt";
	_fillStorage_Writing_SkipShuffle(fileNameTruth, delimiters, isWritten, type, &storage_1D_write, newDimensions,
	                                 vecNames);

	// now read
	TDataReaderBase<Unbounded, 1> reader;
	reader.read(filename, &storage_1D_read, vecNames, delimiters, isWritten, dimensions, dimensionIsKnown,
	            guessLengthFirstDimension, type);

	// write what we read
	TDataWriterBase<Unbounded, 1> writerCheck;
	std::string fileNameCheck = "file1D_1col_shuffleSkip_check.txt";
	writerCheck.write(vecNames, fileNameCheck, &storage_1D_read, delimiters, isWritten, type);

	EXPECT_TRUE(compare_files(fileNameTruth, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameTruth.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_1D_1Row_shuffleSkip) {
	std::string filename         = "file1D_1row_shuffleSkip.txt";
	std::vector<char> delimiters = {'\t'};
	std::vector<bool> isWritten  = {true};
	colNameTypes type            = colNames_concatenated;

	TDataWriterBase<Unbounded, 1> writer;
	writer.write(filename, &storage_1D_write, delimiters, isWritten, type);

	// read!
	std::vector<size_t> dimensions     = {0};
	std::vector<bool> dimensionIsKnown = {false};

	// fill names
	// skip some rownames
	names_dim0_read = std::make_shared<TNamesStrings>();
	names_dim0_read->addName({"B"});
	names_dim0_read->addName({"C"});
	names_dim0_read->addName({"D"});
	names_dim0_read->addName({"F"});
	names_dim0_read->addName({"H"});
	names_dim0_read->addName({"J"});

	auto vecNames                     = {names_dim0_read};
	std::vector<size_t> newDimensions = {6};

	std::string fileNameTruth = "file1D_1row_shuffleSkip_check_truth.txt";
	_fillStorage_Writing_SkipShuffle(fileNameTruth, delimiters, isWritten, type, &storage_1D_write, newDimensions,
	                                 vecNames);

	// now read
	// Note: this should throw -> we need to read it into 2D-storage!
	TDataReaderBase<Unbounded, 1> reader;
	EXPECT_ANY_THROW(reader.read(filename, &storage_1D_read, vecNames, delimiters, isWritten, dimensions,
	                             dimensionIsKnown, guessLengthFirstDimension, type));

	// read it into 2D storage instead
	dimensions       = {1, 0};
	dimensionIsKnown = {true, false};
	delimiters       = {'\n', '\t'};
	isWritten        = {false, true};
	auto names       = {std::make_shared<TNamesEmpty>(), names_dim0_read};

	TDataReaderBase<Unbounded, 2> reader2;
	reader2.read(filename, &storage_2D_read, names, delimiters, isWritten, dimensions, dimensionIsKnown,
	             guessLengthFirstDimension, colNames_multiline);

	// write again -> can we reproduce?
	TDataWriterBase<Unbounded, 2> writerCheck;
	std::string fileNameCheck = "file1D_1row_shuffleSkip_check.txt";
	writerCheck.write(names, fileNameCheck, &storage_2D_read, delimiters, isWritten, colNames_multiline);

	EXPECT_TRUE(compare_files(fileNameTruth, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameTruth.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_2D_shuffleSkip) {
	std::string filename         = "file2D_shuffleSkip.txt";
	std::vector<char> delimiters = {'\n', '\t'};
	std::vector<bool> isWritten  = {true, true};
	colNameTypes type            = colNames_concatenated;

	TDataWriterBase<Unbounded, 2> writer;
	writer.write(filename, &storage_2D_write, delimiters, isWritten, type);

	// read!
	std::vector<size_t> dimensions     = {0, 0};
	std::vector<bool> dimensionIsKnown = {false, false};

	// fill names
	// skip some rownames
	names_dim0_read = std::make_shared<TNamesStrings>();
	names_dim0_read->addName({"B"});
	names_dim0_read->addName({"C"});
	names_dim0_read->addName({"D"});
	names_dim0_read->addName({"F"});
	names_dim0_read->addName({"H"});
	names_dim0_read->addName({"J"});

	// shuffle and skip names of dim1
	names_dim1_read->addName({"four"});
	names_dim1_read->addName({"one"});
	names_dim1_read->addName({"three"});

	auto vecNames                     = {names_dim0_read, names_dim1_read};
	std::vector<size_t> newDimensions = {6, 3};

	std::string fileNameTruth = "file2D_shuffleSkip_check_truth.txt";
	_fillStorage_Writing_SkipShuffle(fileNameTruth, delimiters, isWritten, type, &storage_2D_write, newDimensions,
	                                 vecNames);

	// now read
	TDataReaderBase<Unbounded, 2> reader;
	reader.read(filename, &storage_2D_read, vecNames, delimiters, isWritten, dimensions, dimensionIsKnown,
	            guessLengthFirstDimension, type);

	// write what we read
	TDataWriterBase<Unbounded, 2> writerCheck;
	std::string fileNameCheck = "file2D_shuffleSkip_check.txt";
	writerCheck.write(vecNames, fileNameCheck, &storage_2D_read, delimiters, isWritten, type);

	EXPECT_TRUE(compare_files(fileNameTruth, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameTruth.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_shuffleSkip) {
	std::string filename         = "file4D_shuffleSkip.txt";
	std::vector<char> delimiters = {'\n', '\t', '\t', '\t'};
	std::vector<bool> isWritten  = {true, true, false, true};
	colNameTypes type            = colNames_concatenated;

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, type);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	// fill names
	// skip some rownames
	names_dim0_read = std::make_shared<TNamesStrings>();
	names_dim0_read->addName({"B"});
	names_dim0_read->addName({"C"});
	names_dim0_read->addName({"D"});
	names_dim0_read->addName({"F"});
	names_dim0_read->addName({"H"});
	names_dim0_read->addName({"J"});

	// shuffle and skip names of dim1
	names_dim1_read->addName({"four"});
	names_dim1_read->addName({"one"});
	names_dim1_read->addName({"three"});

	// skip names of dim3
	names_dim3_read = std::make_shared<TNamesStrings>();
	names_dim3_read->addName({"3"});
	names_dim3_read->addName({"2"});
	names_dim3_read->addName({"1"});
	auto vecNames                     = {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read};
	std::vector<size_t> newDimensions = {6, 3, 4, 3};

	std::string fileNameTruth = "file4D_shuffleSkip_check_truth.txt";
	_fillStorage_Writing_SkipShuffle(fileNameTruth, delimiters, isWritten, type, &storage_4D_write, newDimensions,
	                                 vecNames);

	// now read
	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, vecNames, delimiters, isWritten, dimensions, dimensionIsKnown,
	            guessLengthFirstDimension, type);

	// write what we read
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_shuffleSkip_check.txt";
	writerCheck.write(vecNames, fileNameCheck, &storage_4D_read, delimiters, isWritten, type);

	EXPECT_TRUE(compare_files(fileNameTruth, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameTruth.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_shuffleSkip_multipleRowNames) {
	std::string filename         = "file4D_shuffleSkip_multipleRowNames.txt";
	std::vector<char> delimiters = {'\n', '\n', '\n', '\t'};
	std::vector<bool> isWritten  = {true, true, false, true};
	colNameTypes type            = colNames_multiline;

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, type);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	// fill names
	// skip some rownames
	names_dim0_read = std::make_shared<TNamesStrings>();
	names_dim0_read->addName({"B"});
	names_dim0_read->addName({"C"});
	names_dim0_read->addName({"D"});
	names_dim0_read->addName({"F"});
	names_dim0_read->addName({"H"});
	names_dim0_read->addName({"J"});

	// shuffle and skip names of dim1
	names_dim1_read->addName({"four"});
	names_dim1_read->addName({"one"});
	names_dim1_read->addName({"three"});

	// skip names of dim3
	names_dim3_read = std::make_shared<TNamesStrings>();
	names_dim3_read->addName({"3"});
	names_dim3_read->addName({"2"});
	names_dim3_read->addName({"1"});
	auto vecNames                     = {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read};
	std::vector<size_t> newDimensions = {6, 3, 4, 3};

	std::string fileNameTruth = "file4D_shuffleSkip_multipleRowNames_check_truth.txt";
	_fillStorage_Writing_SkipShuffle(fileNameTruth, delimiters, isWritten, type, &storage_4D_write, newDimensions,
	                                 vecNames);

	// now read
	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, vecNames, delimiters, isWritten, dimensions, dimensionIsKnown,
	            guessLengthFirstDimension, type);

	// write what we read
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_shuffleSkip_multipleRowNames_check.txt";
	writerCheck.write(vecNames, fileNameCheck, &storage_4D_read, delimiters, isWritten, type);

	EXPECT_TRUE(compare_files(fileNameTruth, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameTruth.c_str());
	remove(fileNameCheck.c_str());
}

TEST_F(TDataWriterAndReaderTest, write_4D_shuffleSkip_omitStuff) {
	std::string filename         = "file4D_shuffleSkip_omit.txt";
	std::vector<char> delimiters = {'\n', '\n', '\t', '\t'};
	std::vector<bool> isWritten  = {false, true, false, true};
	colNameTypes type            = colNames_multiline;

	TDataWriterBase<Unbounded, 4> writer;
	writer.write(filename, &storage_4D_write, delimiters, isWritten, type);

	// read!
	std::vector<size_t> dimensions     = {0, 0, 0, 0};
	std::vector<bool> dimensionIsKnown = {false, false, false, false};

	// fill names
	// shuffle and skip names of dim1
	names_dim1_read->addName({"four"});
	names_dim1_read->addName({"one"});
	names_dim1_read->addName({"three"});

	// skip names of dim3
	names_dim3_read = std::make_shared<TNamesStrings>();
	names_dim3_read->addName({"3"});
	names_dim3_read->addName({"2"});
	names_dim3_read->addName({"1"});
	auto vecNames                     = {names_dim0_read, names_dim1_read, names_dim2_read, names_dim3_read};
	std::vector<size_t> newDimensions = {10, 3, 4, 3};

	std::string fileNameTruth = "file4D_shuffleSkip_omit_check_truth.txt";
	_fillStorage_Writing_SkipShuffle(fileNameTruth, delimiters, isWritten, type, &storage_4D_write, newDimensions,
	                                 vecNames);

	// now read
	TDataReaderBase<Unbounded, 4> reader;
	reader.read(filename, &storage_4D_read, vecNames, delimiters, isWritten, dimensions, dimensionIsKnown,
	            guessLengthFirstDimension, type);

	// write what we read
	TDataWriterBase<Unbounded, 4> writerCheck;
	std::string fileNameCheck = "file4D_shuffleSkip_omit_check.txt";
	writerCheck.write(vecNames, fileNameCheck, &storage_4D_read, delimiters, isWritten, type);

	EXPECT_TRUE(compare_files(fileNameTruth, fileNameCheck));

	// remove files
	remove(filename.c_str());
	remove(fileNameTruth.c_str());
	remove(fileNameCheck.c_str());
}

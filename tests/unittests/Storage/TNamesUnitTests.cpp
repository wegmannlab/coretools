//
// Created by madleina on 16.04.21.
//
#include <fstream>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/Storage/TNames.h"

using namespace testing;
using namespace coretools;

class BridgeTNamesEmpty : public TNamesEmpty {
public:
	std::string _extractFromStringAndReturnString(std::string &String, char DelimiterLast, bool ThrowIfEmpty) const {
		return TNamesEmpty::_extractFromStringAndReturnString(String, DelimiterLast, ThrowIfEmpty);
	}
	std::vector<std::string> _extractFromStringAndReturnVec(std::string &String, char DelimiterLast,
	                                                        bool ThrowIfEmpty) const {
		return TNamesEmpty::_extractFromStringAndReturnVec(String, DelimiterLast, ThrowIfEmpty);
	}
	bool _extractFromStreamAndFillVec(std::vector<std::string> &Vec, std::istream *FilePointer, char DelimiterLast,
	                                  std::string_view DelimiterComment, bool ThrowIfEmpty) const {
		return TNamesEmpty::_extractFromStreamAndFillVec(Vec, FilePointer, DelimiterLast, DelimiterComment,
		                                                 ThrowIfEmpty);
	}
	void setComplexity(size_t Complexity) {
		// tweak complexity
		_complexity = Complexity;
	}
};

TEST(TNamesUnitTests, extractFromStringAndReturnString_complexity0_differentDelim) {
	// complexity = 0 -> should not remove anything
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';

	// 0 occurrences of delimNames, directly start with lastDelim
	std::string line = "\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "\tnext\tanother");

	// 0 occurrences of delimNames
	line = "one\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "one\tnext\tanother");

	// exactly 1 occurrence of delimNames
	line = "one_two\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "one_two\tnext\tanother");

	// >1 ocurrences of delimNames
	line = "one_two_three_four_five\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "one_two_three_four_five\tnext\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnString_complexity0_sameDelim) {
	// complexity = 0 -> should not remove anything
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';

	// 0 occurrences of delimNames, directly start with lastDelim
	std::string line = "\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "\tnext\tanother");

	// 0 occurrences of delimNames
	line = "one\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "one\tnext\tanother");

	// exactly 1 occurrence of delimNames
	line = "one\ttwo\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "one\ttwo\tnext\tanother");

	// >1 ocurrences of delimNames
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "");
	EXPECT_EQ(line, "one\ttwo\tthree\tfour\tfive\tnext\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnString_complexity1_differentDelim) {
	// complexity = 1
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(1);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one");
	EXPECT_EQ(line, "next\tanother");

	// exactly 1 occurrence of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one_two\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one_two");
	EXPECT_EQ(line, "next\tanother");

	// >1 ocurrences of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one_two_three_four_five\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one_two_three_four_five");
	EXPECT_EQ(line, "next\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnString_complexity1_sameDelim) {
	// complexity = 1
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(1);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one");
	EXPECT_EQ(line, "next\tanother");

	// exactly 1 occurrence of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one\ttwo\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one");
	EXPECT_EQ(line, "two\tnext\tanother");

	// >1 ocurrences of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one");
	EXPECT_EQ(line, "two\tthree\tfour\tfive\tnext\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnString_complexity2_differentDelim) {
	// complexity = 2
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(2);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true));

	// 0 occurrences of delimNames -> throw, not enough occurrences of delim!
	line = "one\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true));

	// exactly 1 occurrence of delimNames -> ok
	line = "one_two\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one_two");
	EXPECT_EQ(line, "next\tanother");

	// >1 ocurrences of delimNames -> but we only need complexity = 2 -> just read until \t
	line = "one_two_three_four_five\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one_two_three_four_five");
	EXPECT_EQ(line, "next\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnString_complexity2_sameDelim) {
	// complexity = 2
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(2);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one\tnext");
	EXPECT_EQ(line, "another");

	// exactly 1 occurrence of delimNames
	line = "one\ttwo\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one\ttwo");
	EXPECT_EQ(line, "next\tanother");

	// >1 ocurrences of delimNames
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	EXPECT_EQ(namesEmpty._extractFromStringAndReturnString(line, delimiterLast, true), "one\ttwo");
	EXPECT_EQ(line, "three\tfour\tfive\tnext\tanother");
}

//---------------------------------------------
// _extractFromStringAndReturnVec
//---------------------------------------------

TEST(TNamesUnitTests, extractFromStringAndReturnVec_complexity0_differentDelim) {
	// complexity = 0 -> should not remove anything
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';

	// 0 occurrences of delimNames, directly start with lastDelim
	std::string line = "\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "\tnext\tanother");

	// 0 occurrences of delimNames
	line = "one\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "one\tnext\tanother");

	// exactly 1 occurrence of delimNames
	line = "one_two\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "one_two\tnext\tanother");

	// >1 ocurrences of delimNames
	line = "one_two_three_four_five\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "one_two_three_four_five\tnext\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnVec_complexity0_sameDelim) {
	// complexity = 0 -> should not remove anything
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';

	// 0 occurrences of delimNames, directly start with lastDelim
	std::string line = "\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "\tnext\tanother");

	// 0 occurrences of delimNames
	line = "one\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "one\tnext\tanother");

	// exactly 1 occurrence of delimNames
	line = "one\ttwo\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "one\ttwo\tnext\tanother");

	// >1 ocurrences of delimNames
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre());
	EXPECT_EQ(line, "one\ttwo\tthree\tfour\tfive\tnext\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnVec_complexity1_differentDelim) {
	// complexity = 1
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(1);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one"));
	EXPECT_EQ(line, "next\tanother");

	// exactly 1 occurrence of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one_two\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one_two"));
	EXPECT_EQ(line, "next\tanother");

	// >1 ocurrences of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one_two_three_four_five\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true),
	            ElementsAre("one_two_three_four_five"));
	EXPECT_EQ(line, "next\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnVec_complexity1_sameDelim) {
	// complexity = 1
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(1);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one"));
	EXPECT_EQ(line, "next\tanother");

	// exactly 1 occurrence of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one\ttwo\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one"));
	EXPECT_EQ(line, "two\tnext\tanother");

	// >1 ocurrences of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one"));
	EXPECT_EQ(line, "two\tthree\tfour\tfive\tnext\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnVec_complexity2_differentDelim) {
	// complexity = 2
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(2);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true));

	// 0 occurrences of delimNames -> throw, not enough occurrences of delim!
	line = "one\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true));

	// exactly 1 occurrence of delimNames -> ok
	line = "one_two\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one", "two"));
	EXPECT_EQ(line, "next\tanother");

	// >1 ocurrences of delimNames -> but we only need complexity = 2 -> just read until \t
	line = "one_two_three_four_five\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true),
	            ElementsAre("one", "two_three_four_five"));
	EXPECT_EQ(line, "next\tanother");
}

TEST(TNamesUnitTests, extractFromStringAndReturnVec_complexity2_sameDelim) {
	// complexity = 2
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(2);

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	EXPECT_ANY_THROW(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one", "next"));
	EXPECT_EQ(line, "another");

	// exactly 1 occurrence of delimNames
	line = "one\ttwo\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one", "two"));
	EXPECT_EQ(line, "next\tanother");

	// >1 ocurrences of delimNames
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	EXPECT_THAT(namesEmpty._extractFromStringAndReturnVec(line, delimiterLast, true), ElementsAre("one", "two"));
	EXPECT_EQ(line, "three\tfour\tfive\tnext\tanother");
}

//---------------------------------------------
// _extractFromStreamAndFillVec
//---------------------------------------------

void writeToFile(std::string_view String) {
	std::string filename = "test.txt";
	std::ofstream out(filename.c_str());
	out << String;
	out.close();
}

bool readFromFile(std::vector<std::string> &Vec, const BridgeTNamesEmpty &Names, char DelimiterLast) {
	std::string filename = "test.txt";
	std::ifstream in(filename.c_str());
	bool what = Names._extractFromStreamAndFillVec(Vec, &in, DelimiterLast, "//", true);
	in.close();

	remove(filename.c_str());

	return what;
}

TEST(TNamesUnitTests, extractFromStreamAndFillVec_complexity0_differentDelim) {
	// complexity = 0 -> should not remove anything
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	std::vector<std::string> vec;

	// 0 occurrences of delimNames, directly start with lastDelim
	std::string line = "\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());

	// 0 occurrences of delimNames
	line = "one\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());

	// exactly 1 occurrence of delimNames
	line = "one_two\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());

	// >1 ocurrences of delimNames
	line = "one_two_three_four_five\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());
}

TEST(TNamesUnitTests, extractFromStreamAndFillVec_complexity0_sameDelim) {
	// complexity = 0 -> should not remove anything
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	std::vector<std::string> vec;

	// 0 occurrences of delimNames, directly start with lastDelim
	std::string line = "\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());

	// 0 occurrences of delimNames
	line = "one\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());

	// exactly 1 occurrence of delimNames
	line = "one\ttwo\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());

	// >1 ocurrences of delimNames
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre());
}

TEST(TNamesUnitTests, extractFromStreamAndFillVec_complexity1_differentDelim) {
	// complexity = 1
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(1);
	std::vector<std::string> vec;

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	writeToFile(line);
	EXPECT_ANY_THROW(readFromFile(vec, namesEmpty, delimiterLast));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one"));

	// exactly 1 occurrence of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one_two\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one_two"));

	// >1 ocurrences of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one_two_three_four_five\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one_two_three_four_five"));
}

TEST(TNamesUnitTests, extractFromStreamAndFillVec_complexity1_sameDelim) {
	// complexity = 1
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(1);
	std::vector<std::string> vec;

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	writeToFile(line);
	EXPECT_ANY_THROW(readFromFile(vec, namesEmpty, delimiterLast););

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one"));

	// exactly 1 occurrence of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one\ttwo\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one"));

	// >1 ocurrences of delimNames -> but we only need complexity = 1 -> just read until \t
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one"));
}

TEST(TNamesUnitTests, extractFromStreamAndFillVec_complexity2_differentDelim) {
	// complexity = 2
	// delimiter for names (_) and for columns (\t) is different
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('_');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(2);
	std::vector<std::string> vec;

	// 0 occurrences of delimNames, directly start with lastDelim -> return false - just reads until end and doesn't
	// find delimiter
	std::string line = "\tnext\tanother";
	writeToFile(line);
	EXPECT_FALSE(readFromFile(vec, namesEmpty, delimiterLast));

	// 0 occurrences of delimNames -> return false - just reads until end and doesn't find delimiter
	line = "one\tnext\tanother";
	writeToFile(line);
	EXPECT_FALSE(readFromFile(vec, namesEmpty, delimiterLast));

	// exactly 1 occurrence of delimNames -> ok
	line = "one_two\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one", "two"));

	// >1 ocurrences of delimNames -> but we only need complexity = 2 -> just read until \t
	line = "one_two_three_four_five\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one", "two_three_four_five"));
}

TEST(TNamesUnitTests, extractFromStreamAndFillVec_complexity2_sameDelim) {
	// complexity = 2
	// delimiter for names (\t) and for columns (\t) is the same
	BridgeTNamesEmpty namesEmpty;
	namesEmpty.setDelimName('\t');
	char delimiterLast = '\t';
	namesEmpty.setComplexity(2);
	std::vector<std::string> vec;

	// 0 occurrences of delimNames, directly start with lastDelim -> throw, name is empty!
	std::string line = "\tnext\tanother";
	writeToFile(line);
	EXPECT_ANY_THROW(readFromFile(vec, namesEmpty, delimiterLast));

	// 0 occurrences of delimNames -> ok!
	line = "one\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one", "next"));

	// exactly 1 occurrence of delimNames
	line = "one\ttwo\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one", "two"));

	// >1 ocurrences of delimNames
	line = "one\ttwo\tthree\tfour\tfive\tnext\tanother";
	writeToFile(line);
	EXPECT_TRUE(readFromFile(vec, namesEmpty, delimiterLast));
	EXPECT_THAT(vec, ElementsAre("one", "two"));
}

TEST(TNamesUnitTests, addNameAndCheckIfItShouldBeKept_allInSameOrder) {
	std::shared_ptr<TNamesEmpty> names = std::make_shared<TNamesStrings>();
	names->addName({"one"});
	names->addName({"two"});
	names->addName({"three"});
	names->addName({"four"});
	names->addName({"five"});
	names->addName({"six"});

	EXPECT_TRUE(names->checkIfNameShouldBeKept({"one"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"two"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"three"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"four"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"five"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"six"}, "file.txt"));
}

TEST(TNamesUnitTests, addNameAndCheckIfItShouldBeKept_skipSome) {
	std::shared_ptr<TNamesEmpty> names = std::make_shared<TNamesStrings>();
	names->addName({"one"});
	names->addName({"two"});
	names->addName({"four"});
	names->addName({"six"}); // three and five are missing

	EXPECT_TRUE(names->checkIfNameShouldBeKept({"one"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"two"}, "file.txt"));
	EXPECT_FALSE(names->checkIfNameShouldBeKept({"three"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"four"}, "file.txt"));
	EXPECT_FALSE(names->checkIfNameShouldBeKept({"five"}, "file.txt"));
	EXPECT_TRUE(names->checkIfNameShouldBeKept({"six"}, "file.txt"));
}

TEST(TNamesUnitTests, addNameAndCheckIfItShouldBeKept_scrambled) {
	TNamesStrings names;
	names.addName({"one"});
	names.addName({"two"});
	names.addName({"three"});
	names.addName({"four"});
	names.addName({"five"});
	names.addName({"six"});

	// convert to base class ptr
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesStrings>(names);

	EXPECT_TRUE(base->checkIfNameShouldBeKept({"one"}, "file.txt"));
	EXPECT_TRUE(base->checkIfNameShouldBeKept({"two"}, "file.txt"));
	// skip three
	EXPECT_ANY_THROW(base->checkIfNameShouldBeKept({"four"}, "file.txt"));
}

TEST(TNamesUnitTests, addNameAndCheckIfItShouldBeKept_moreThanNames) {
	TNamesStrings names;
	names.addName({"one"});
	names.addName({"two"});
	names.addName({"three"});
	names.addName({"four"});

	// convert to base class ptr
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesStrings>(names);

	EXPECT_TRUE(base->checkIfNameShouldBeKept({"one"}, "file.txt"));
	EXPECT_TRUE(base->checkIfNameShouldBeKept({"two"}, "file.txt"));
	EXPECT_TRUE(base->checkIfNameShouldBeKept({"three"}, "file.txt"));
	EXPECT_TRUE(base->checkIfNameShouldBeKept({"four"}, "file.txt"));
	EXPECT_ANY_THROW(base->checkIfNameShouldBeKept({"five"}, "file.txt")); // more than expected
}

TEST(TNamesUnitTests, TNamesEmpty) {
	TNamesEmpty names;

	names.resize(3);
	names.addName({}, 0);
	names.addName({}, 1);
	names.addName({}, 2);
	names.addName({});

	// convert to base class ptr
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesEmpty>(names);

	// doesn't store anything
	EXPECT_EQ((*base)[0], "");
	EXPECT_EQ((*base)[1], "");
	EXPECT_EQ((*base)[2], "");
	EXPECT_EQ((*base)[3], "");
	EXPECT_EQ(base->getTitle(), "");

	// ... but knows its size
	EXPECT_EQ(base->size(), 4);
	EXPECT_TRUE(base->isFilled());

	EXPECT_FALSE(base->exists("one"));
	EXPECT_FALSE(base->exists("two"));
	EXPECT_FALSE(base->exists("three"));
	EXPECT_FALSE(base->exists("four"));

	EXPECT_ANY_THROW(base->getIndex("one"));
	EXPECT_ANY_THROW(base->getIndex("two"));
	EXPECT_ANY_THROW(base->getIndex("three"));
	EXPECT_ANY_THROW(base->getIndex("four"));

	// operator ==
	std::shared_ptr<TNamesEmpty> other = std::make_shared<TNamesEmpty>();
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	other->resize(4);
	EXPECT_TRUE((*base) == (*other));
	EXPECT_FALSE((*base) != (*other));
}

TEST(TNamesUnitTests, TNamesStrings) {
	TNamesStrings names;

	names.resize(3);
	names.addName({"one"}, 0);
	names.addName({"two"}, 1);
	names.addName({"three"}, 2);
	names.addName({"four"});

	// convert to base class ptr
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesStrings>(names);

	EXPECT_EQ(base->size(), 4);
	EXPECT_TRUE(base->isFilled());

	// []
	EXPECT_EQ((*base)[0], "one");
	EXPECT_EQ((*base)[1], "two");
	EXPECT_EQ((*base)[2], "three");
	EXPECT_EQ((*base)[3], "four");

	// exists
	EXPECT_TRUE(base->exists("one"));
	EXPECT_TRUE(base->exists("two"));
	EXPECT_TRUE(base->exists("three"));
	EXPECT_TRUE(base->exists("four"));
	EXPECT_FALSE(base->exists("five"));

	// get index
	EXPECT_EQ(base->getIndex("one"), 0);
	EXPECT_EQ(base->getIndex("two"), 1);
	EXPECT_EQ(base->getIndex("three"), 2);
	EXPECT_EQ(base->getIndex("four"), 3);
	EXPECT_ANY_THROW(base->getIndex("five"));

	// title
	EXPECT_EQ(base->getTitle(), "-");

	// operator ==
	std::shared_ptr<TNamesEmpty> other = std::make_shared<TNamesStrings>();
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	std::shared_ptr<TNamesEmpty> other2 = std::make_shared<TNamesStrings>();
	other2->addName({"one"});
	other2->addName({"four"});
	EXPECT_FALSE((*base) == (*other2));
	EXPECT_TRUE((*base) != (*other2));

	std::shared_ptr<TNamesEmpty> other3 = std::make_shared<TNamesStrings>(names);
	EXPECT_TRUE((*base) == (*other3));
	EXPECT_FALSE((*base) != (*other3));
}

TEST(TNamesUnitTests, TNamesIndices_offset1) {
	// base class ptr, offset = 1
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesIndices>();

	EXPECT_EQ(base->size(), 0);
	EXPECT_FALSE(base->isFilled());

	// []
	EXPECT_EQ((*base)[0], "1");
	EXPECT_EQ((*base)[1], "2");
	EXPECT_EQ((*base)[2], "3");
	EXPECT_EQ((*base)[3], "4");

	// exists
	EXPECT_FALSE(base->exists("1"));
	EXPECT_FALSE(base->exists("2"));
	EXPECT_FALSE(base->exists("3"));
	EXPECT_FALSE(base->exists("4"));
	EXPECT_FALSE(base->exists("5"));

	// get index
	EXPECT_ANY_THROW(base->getIndex("1"));
	EXPECT_ANY_THROW(base->getIndex("2"));
	EXPECT_ANY_THROW(base->getIndex("3"));
	EXPECT_ANY_THROW(base->getIndex("4"));
	EXPECT_ANY_THROW(base->getIndex("5"));

	// resize
	base->resize(4);
	EXPECT_EQ(base->size(), 4);
	EXPECT_TRUE(base->isFilled());

	// [] -> still the same
	EXPECT_EQ((*base)[0], "1");
	EXPECT_EQ((*base)[1], "2");
	EXPECT_EQ((*base)[2], "3");
	EXPECT_EQ((*base)[3], "4");

	// exists -> now true if < size
	EXPECT_TRUE(base->exists("1"));
	EXPECT_TRUE(base->exists("2"));
	EXPECT_TRUE(base->exists("3"));
	EXPECT_TRUE(base->exists("4"));
	EXPECT_FALSE(base->exists("5"));

	// get index
	EXPECT_EQ(base->getIndex("1"), 0);
	EXPECT_EQ(base->getIndex("2"), 1);
	EXPECT_EQ(base->getIndex("3"), 2);
	EXPECT_EQ(base->getIndex("4"), 3);
	EXPECT_ANY_THROW(base->getIndex("5"));

	// title
	EXPECT_EQ(base->getTitle(), "-");

	// operator ==
	std::shared_ptr<TNamesEmpty> other = std::make_shared<TNamesIndices>();
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	std::shared_ptr<TNamesEmpty> other2 = std::make_shared<TNamesIndices>(5);
	EXPECT_FALSE((*base) == (*other2));
	EXPECT_TRUE((*base) != (*other2));

	TNamesIndices other3tmp(4);
	other3tmp.setOffset(0);
	std::shared_ptr<TNamesEmpty> other3 = std::make_shared<TNamesIndices>(other3tmp);
	EXPECT_FALSE((*base) == (*other3));
	EXPECT_TRUE((*base) != (*other3));

	std::shared_ptr<TNamesEmpty> other4 = std::make_shared<TNamesIndices>(4);
	EXPECT_TRUE((*base) == (*other4));
	EXPECT_FALSE((*base) != (*other4));
}

TEST(TNamesUnitTests, TNamesIndices_offset0) {
	// base class ptr, offset = 0
	TNamesIndices offset0;
	offset0.setOffset(0);
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesIndices>(offset0);

	EXPECT_EQ(base->size(), 0);
	EXPECT_FALSE(base->isFilled());

	// []
	EXPECT_EQ((*base)[0], "0");
	EXPECT_EQ((*base)[1], "1");
	EXPECT_EQ((*base)[2], "2");
	EXPECT_EQ((*base)[3], "3");

	// exists
	EXPECT_FALSE(base->exists("0"));
	EXPECT_FALSE(base->exists("1"));
	EXPECT_FALSE(base->exists("2"));
	EXPECT_FALSE(base->exists("3"));
	EXPECT_FALSE(base->exists("4"));

	// get index
	EXPECT_ANY_THROW(base->getIndex("0"));
	EXPECT_ANY_THROW(base->getIndex("1"));
	EXPECT_ANY_THROW(base->getIndex("2"));
	EXPECT_ANY_THROW(base->getIndex("3"));
	EXPECT_ANY_THROW(base->getIndex("4"));

	// resize
	base->resize(4);
	EXPECT_EQ(base->size(), 4);
	EXPECT_TRUE(base->isFilled());

	// [] -> still the same
	EXPECT_EQ((*base)[0], "0");
	EXPECT_EQ((*base)[1], "1");
	EXPECT_EQ((*base)[2], "2");
	EXPECT_EQ((*base)[3], "3");

	// exists -> now true if < size
	EXPECT_TRUE(base->exists("0"));
	EXPECT_TRUE(base->exists("1"));
	EXPECT_TRUE(base->exists("2"));
	EXPECT_TRUE(base->exists("3"));
	EXPECT_FALSE(base->exists("4"));

	// get index
	EXPECT_EQ(base->getIndex("0"), 0);
	EXPECT_EQ(base->getIndex("1"), 1);
	EXPECT_EQ(base->getIndex("2"), 2);
	EXPECT_EQ(base->getIndex("3"), 3);
	EXPECT_ANY_THROW(base->getIndex("4"));

	// title
	EXPECT_EQ(base->getTitle(), "-");

	// operator ==
	TNamesIndices othertmp;
	othertmp.setOffset(0);
	std::shared_ptr<TNamesEmpty> other = std::make_shared<TNamesIndices>(othertmp);
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	TNamesIndices other2tmp(5);
	other2tmp.setOffset(0);
	std::shared_ptr<TNamesEmpty> other2 = std::make_shared<TNamesIndices>(other2tmp);
	EXPECT_FALSE((*base) == (*other2));
	EXPECT_TRUE((*base) != (*other2));

	std::shared_ptr<TNamesEmpty> other3 = std::make_shared<TNamesIndices>(4); // wrong offset
	EXPECT_FALSE((*base) == (*other3));
	EXPECT_TRUE((*base) != (*other3));

	TNamesIndices other4tmp(4);
	other4tmp.setOffset(0);
	std::shared_ptr<TNamesEmpty> other4 = std::make_shared<TNamesIndices>(other4tmp);
	EXPECT_TRUE((*base) == (*other4));
	EXPECT_FALSE((*base) != (*other4));
}

TEST(TNamesUnitTests, TNamesIndicesAlphabetUpperCase) {
	// base class ptr
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesIndicesAlphabetUpperCase>();

	EXPECT_EQ(base->size(), 0);
	EXPECT_FALSE(base->isFilled());

	EXPECT_EQ((*base)[0], "A");
	EXPECT_EQ((*base)[1], "B");
	EXPECT_EQ((*base)[2], "C");
	EXPECT_EQ((*base)[26], "AA");
	EXPECT_EQ((*base)[52], "BA");

	// exists
	EXPECT_FALSE(base->exists("A"));
	EXPECT_FALSE(base->exists("B"));
	EXPECT_FALSE(base->exists("C"));
	EXPECT_FALSE(base->exists("AA"));
	EXPECT_FALSE(base->exists("BA"));

	// get index
	EXPECT_ANY_THROW(base->getIndex("A"));
	EXPECT_ANY_THROW(base->getIndex("B"));
	EXPECT_ANY_THROW(base->getIndex("C"));
	EXPECT_ANY_THROW(base->getIndex("AA"));
	EXPECT_ANY_THROW(base->getIndex("BA"));

	// resize
	base->resize(53);
	EXPECT_EQ(base->size(), 53);
	EXPECT_TRUE(base->isFilled());

	// [] -> still the same
	EXPECT_EQ((*base)[0], "A");
	EXPECT_EQ((*base)[1], "B");
	EXPECT_EQ((*base)[2], "C");
	EXPECT_EQ((*base)[26], "AA");
	EXPECT_EQ((*base)[52], "BA");

	// exists -> now true if < size
	EXPECT_TRUE(base->exists("A"));
	EXPECT_TRUE(base->exists("B"));
	EXPECT_TRUE(base->exists("C"));
	EXPECT_TRUE(base->exists("AA"));
	EXPECT_TRUE(base->exists("BA"));
	EXPECT_FALSE(base->exists("BB"));

	// get index
	EXPECT_EQ(base->getIndex("A"), 0);
	EXPECT_EQ(base->getIndex("B"), 1);
	EXPECT_EQ(base->getIndex("C"), 2);
	EXPECT_EQ(base->getIndex("AA"), 26);
	EXPECT_EQ(base->getIndex("BA"), 52);
	EXPECT_ANY_THROW(base->getIndex("BB"));

	// title
	EXPECT_EQ(base->getTitle(), "-");

	// operator ==
	std::shared_ptr<TNamesEmpty> other = std::make_shared<TNamesIndicesAlphabetUpperCase>();
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	std::shared_ptr<TNamesEmpty> other2 = std::make_shared<TNamesIndicesAlphabetUpperCase>(10);
	EXPECT_FALSE((*base) == (*other2));
	EXPECT_TRUE((*base) != (*other2));

	std::shared_ptr<TNamesEmpty> other4 = std::make_shared<TNamesIndicesAlphabetUpperCase>(53);
	EXPECT_TRUE((*base) == (*other4));
	EXPECT_FALSE((*base) != (*other4));
}

TEST(TNamesUnitTests, TNamesIndicesAlphabetLowerCase) {
	// base class ptr
	std::shared_ptr<TNamesEmpty> base = std::make_shared<TNamesIndicesAlphabetLowerCase>();

	EXPECT_EQ(base->size(), 0);
	EXPECT_FALSE(base->isFilled());

	EXPECT_EQ((*base)[0], "a");
	EXPECT_EQ((*base)[1], "b");
	EXPECT_EQ((*base)[2], "c");
	EXPECT_EQ((*base)[26], "aa");
	EXPECT_EQ((*base)[52], "ba");

	// exists
	EXPECT_FALSE(base->exists("a"));
	EXPECT_FALSE(base->exists("b"));
	EXPECT_FALSE(base->exists("c"));
	EXPECT_FALSE(base->exists("aa"));
	EXPECT_FALSE(base->exists("ba"));

	// get index
	EXPECT_ANY_THROW(base->getIndex("a"));
	EXPECT_ANY_THROW(base->getIndex("b"));
	EXPECT_ANY_THROW(base->getIndex("c"));
	EXPECT_ANY_THROW(base->getIndex("aa"));
	EXPECT_ANY_THROW(base->getIndex("ba"));

	// resize
	base->resize(53);
	EXPECT_EQ(base->size(), 53);
	EXPECT_TRUE(base->isFilled());

	// [] -> still the same
	EXPECT_EQ((*base)[0], "a");
	EXPECT_EQ((*base)[1], "b");
	EXPECT_EQ((*base)[2], "c");
	EXPECT_EQ((*base)[26], "aa");
	EXPECT_EQ((*base)[52], "ba");

	// exists -> now true if < size
	EXPECT_TRUE(base->exists("a"));
	EXPECT_TRUE(base->exists("b"));
	EXPECT_TRUE(base->exists("c"));
	EXPECT_TRUE(base->exists("aa"));
	EXPECT_TRUE(base->exists("ba"));
	EXPECT_FALSE(base->exists("bb"));

	// get index
	EXPECT_EQ(base->getIndex("a"), 0);
	EXPECT_EQ(base->getIndex("b"), 1);
	EXPECT_EQ(base->getIndex("c"), 2);
	EXPECT_EQ(base->getIndex("aa"), 26);
	EXPECT_EQ(base->getIndex("ba"), 52);
	EXPECT_ANY_THROW(base->getIndex("bb"));

	// title
	EXPECT_EQ(base->getTitle(), "-");

	// operator ==
	std::shared_ptr<TNamesEmpty> other = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	std::shared_ptr<TNamesEmpty> other2 = std::make_shared<TNamesIndicesAlphabetLowerCase>(10);
	EXPECT_FALSE((*base) == (*other2));
	EXPECT_TRUE((*base) != (*other2));

	std::shared_ptr<TNamesEmpty> other4 = std::make_shared<TNamesIndicesAlphabetLowerCase>(53);
	EXPECT_TRUE((*base) == (*other4));
	EXPECT_FALSE((*base) != (*other4));
}

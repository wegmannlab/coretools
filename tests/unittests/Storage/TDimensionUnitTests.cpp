//
// Created by madleina on 20.10.21.
//

#include "gtest/gtest.h"

#include "coretools/Storage/TDimension.h"

using namespace coretools;

//-------------------------------------------
// TRange
//-------------------------------------------

TEST(TRange, single) {
	TRange range(0);
	size_t c = 0;
	std::array<size_t, 1> expected = {0};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 1);
}

TEST(TRange, single2) {
	TRange range(5);
	size_t c = 0;
	std::array<size_t, 1> expected = {5};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 1);
}

TEST(TRange, pair) {
	TRange range(0, 10);
	size_t c                       = 0;
	std::array<size_t, 2> expected = {0, 10};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 2);
}

TEST(TRange, pair2) {
	TRange range(5, 10);
	size_t c = 0;
	std::array<size_t, 2> expected = {5, 10};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 2);
}

TEST(TRange, multi) {
	TRange range(0, 10, 1);
	size_t c = 0;
	std::array<size_t, 10> expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 10);
}

TEST(TRange, multi2) {
	TRange range(10, 20, 1);
	size_t c = 0;
	std::array<size_t, 10> expected = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 10);
}

TEST(TRange, inc) {
	TRange range(0, 10, 2);
	size_t c = 0;
	std::array<size_t, 5> expected = {0, 2, 4, 6, 8};
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(r, expected[c]);
		++c;
	}
	EXPECT_EQ(c, 5);
}

//-------------------------------------------
// TDimension
//-------------------------------------------

class TDimensionTestSingle : public testing::Test {
protected:
	std::array<size_t, 1> dimensions = {1};
	TDimension<1> dim;

	double *linearArray = nullptr;

	void SetUp() override {
		dim.init(dimensions);

		// create some 1D array and fill it
		linearArray    = new double[1];
		linearArray[0] = 0.1;
	}
	void TearDown() override { delete[] linearArray; }
};

TEST_F(TDimensionTestSingle, getElement) {
	// go over all possible coordinates and check if value in multi-array is same
	// as in linear array
	std::array<size_t, 1> coord = {0};
	size_t i                    = dim.getIndex(coord);
	EXPECT_EQ(linearArray[0], linearArray[i]);
}

TEST_F(TDimensionTestSingle, getRange) {
	// take last coordinate as end-coordinate
	std::array<size_t, 1> endCoord = {0};
	std::array<size_t, 1> coord    = {0};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if correct
	size_t indexInLinear           = range.begin;
	int c                          = 0;
	for (size_t i = 0; i < dimensions.at(0); i++, c++) {
		EXPECT_EQ(linearArray[c], linearArray[indexInLinear]);
		indexInLinear += range.increment;
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTestSingle, getFull) {
	TRange range         = dim.getFull();
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear = range.begin;
	for (size_t i = 0; i < dimensions.at(0); i++) {
		EXPECT_EQ(linearArray[i], linearArray[indexInLinear]);
		indexInLinear += range.increment;
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTestSingle, get1DSlice_invalidDimension) {
	std::array<size_t, 1> startCoord = {0};
	// this is ok
	EXPECT_NO_THROW(dim.get1DSlice(0, startCoord));
	// this is too much
#ifdef _DEBUG
	EXPECT_DEATH(dim.get1DSlice(1, startCoord), "");
#endif
}

TEST_F(TDimensionTestSingle, get1DSlice_startToEndOfDim0) {
	std::array<size_t, 1> startCoord = {0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 2D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(linearArray[0], linearArray[indexInLinear]);
	indexInLinear += range.increment;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTestSingle, beginSlice) {
	for (TRangeIterator r = dim.beginSlice(0, 0); !r.end(); ++r) { EXPECT_EQ(linearArray[0], linearArray[r.cur()]); }
}

TEST_F(TDimensionTestSingle, getSubscripts) {
	std::array<size_t, 1> coords = dim.getSubscripts(0);
	EXPECT_EQ(coords.size(), 1);
	EXPECT_EQ(coords[0], 0);

	// throw for all other cases
	EXPECT_ANY_THROW(dim.getSubscripts(1));
	EXPECT_ANY_THROW(dim.getSubscripts(2));
}

class TDimensionTest1D : public testing::Test {
protected:
	std::array<size_t, 1> dimensions = {3};
	TDimension<1> dim;

	double *linearArray = nullptr;

	void SetUp() override {
		dim.init(dimensions);

		// create some 1D array and fill it
		double val  = 0.1;
		linearArray = new double[3];
		for (size_t i = 0; i < 3; i++) {
			linearArray[i] = val;
			val += 0.1;
		}
	}
	void TearDown() override { delete[] linearArray; }
};

TEST_F(TDimensionTest1D, getElement) {
	// go over all possible coordinates and check if value in multi-array is same
	// as in linear array
	for (size_t i = 0; i < dimensions.at(0); i++) {
		// get index in linear array
		std::array<size_t, 1> coord = {i};
		size_t z                    = dim.getIndex(coord);
		EXPECT_EQ(linearArray[i], linearArray[z]);
	}
}

TEST_F(TDimensionTest1D, getRange_startToEnd) {
	// take last coordinate as end-coordinate
	std::array<size_t, 1> endCoord = {2};
	// get index in linear array
	std::array<size_t, 1> coord    = {0};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if correct
	size_t indexInLinear           = range.begin;
	int c                          = 0;
	for (size_t i = 0; i < dimensions.at(0); i++, c++) {
		EXPECT_EQ(linearArray[c], linearArray[indexInLinear]);
		indexInLinear += range.increment;
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest1D, getRange_endToEnd) {
	// take last coordinate as end-coordinate
	std::array<size_t, 1> endCoord = {2};
	// get index in linear array
	std::array<size_t, 1> coord    = {2};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	for (size_t r = range.begin; r < range.end; r += range.increment) { EXPECT_EQ(linearArray[2], linearArray[r]); }
}

TEST_F(TDimensionTest1D, getRange_somewhereToSomewhere) {
	std::array<size_t, 1> startCoord = {1};
	std::array<size_t, 1> endCoord   = {2};
	// get index in linear array
	TRange range                     = dim.getRange(startCoord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(linearArray[1], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(linearArray[2], linearArray[indexInLinear]);
	indexInLinear++;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest1D, getRange_startIsLargerThanEnd) {
	std::array<size_t, 1> startCoord = {1};
	std::array<size_t, 1> endCoord   = {0};
	// get index in linear array
	EXPECT_ANY_THROW(dim.getRange(startCoord, endCoord));
}

TEST_F(TDimensionTest1D, getFull) {
	TRange range         = dim.getFull();
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear = range.begin;
	for (size_t i = 0; i < dimensions.at(0); i++) {
		EXPECT_EQ(linearArray[i], linearArray[indexInLinear]);
		indexInLinear += range.increment;
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest1D, get1DSlice_invalidDimension) {
	std::array<size_t, 1> startCoord = {0};
	// this is ok
	EXPECT_NO_THROW(dim.get1DSlice(0, startCoord));
	// this is too much
#ifdef _DEBUG
	EXPECT_DEATH(dim.get1DSlice(1, startCoord), "");
#endif
}

TEST_F(TDimensionTest1D, get1DSlice_startToEndOfDim0) {
	std::array<size_t, 1> startCoord = {0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 2D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(linearArray[0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(linearArray[1], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(linearArray[2], linearArray[indexInLinear]);
	indexInLinear += range.increment;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest1D, get1DSlice_endToEndOfDim0) {
	std::array<size_t, 1> startCoord = {2};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array

	for (size_t r = range.begin; r < range.end; r += range.increment) { EXPECT_EQ(linearArray[2], linearArray[r]); }

	size_t indexInLinear = range.begin;
	EXPECT_EQ(linearArray[2], linearArray[indexInLinear]);
	indexInLinear += range.increment;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest1D, beginSlice) {
	// Note: in a linear 1D array, beginSlice will simply return one element!
	// (slice of first dimension at nth element -> just one value)

	// indexInDim = 0
	size_t linear  = 0;
	size_t counter = 0;
	for (TRangeIterator r = dim.beginSlice(0, linear); !r.end(); ++r) {
		EXPECT_EQ(linearArray[linear], linearArray[r.cur()]);
		++counter;
	}

	EXPECT_EQ(counter, 1);

	// indexInDim = 1
	linear  = 1;
	counter = 0;
	for (TRangeIterator r = dim.beginSlice(0, linear); !r.end(); ++r) {
		EXPECT_EQ(linearArray[linear], linearArray[r.cur()]);
		++counter;
	}

	EXPECT_EQ(counter, 1);

	// indexInDim = 2
	linear  = 2;
	counter = 0;
	for (TRangeIterator r = dim.beginSlice(0, linear); !r.end(); ++r) {
		EXPECT_EQ(linearArray[linear], linearArray[r.cur()]);
		++counter;
	}

	EXPECT_EQ(counter, 1);
}

TEST_F(TDimensionTest1D, getSubscripts) {
	// first element
	std::array<size_t, 1> coords = dim.getSubscripts(0);
	EXPECT_EQ(coords.size(), 1);
	EXPECT_EQ(coords[0], 0);

	// second element
	coords = dim.getSubscripts(1);
	EXPECT_EQ(coords.size(), 1);
	EXPECT_EQ(coords[0], 1);

	// third element
	coords = dim.getSubscripts(2);
	EXPECT_EQ(coords.size(), 1);
	EXPECT_EQ(coords[0], 2);

	// throw for all other cases
	EXPECT_ANY_THROW(dim.getSubscripts(3));
	EXPECT_ANY_THROW(dim.getSubscripts(4));
}

class TDimensionTest2D : public testing::Test {
protected:
	std::array<size_t, 2> dimensions = {2, 3};
	TDimension<2> dim;

	double **multiDimArray = nullptr;
	double *linearArray    = nullptr;

	void SetUp() override {
		dim.init(dimensions);

		// create some 3D array and fill it
		// create a 1D array and fill it with the same values as the 3D array
		double val    = 0.1;
		int c         = 0;
		multiDimArray = new double *[2];
		linearArray   = new double[2 * 3];
		for (size_t i = 0; i < 2; i++) {
			multiDimArray[i] = new double[3];
			for (size_t j = 0; j < 3; j++) {
				multiDimArray[i][j] = val;
				linearArray[c]      = val;
				val += 0.1;
				c++;
			}
		}
	}
	void TearDown() override {
		for (size_t i = 0; i < 2; i++) { delete[] multiDimArray[i]; }
		delete[] multiDimArray;

		delete[] linearArray;
	}
};

TEST_F(TDimensionTest2D, getElement) {
	// go over all possible coordinates and check if value in multi-array is same
	// as in linear array
	for (size_t i = 0; i < dimensions.at(0); i++) {
		for (size_t j = 0; j < dimensions.at(1); j++) {
			// get index in linear array
			std::array<size_t, 2> coord = {i, j};
			size_t k                    = dim.getIndex(coord);
			EXPECT_EQ(multiDimArray[i][j], linearArray[k]);
		}
	}
}

TEST_F(TDimensionTest2D, getRange_startToEnd) {
	// take last coordinate as end-coordinate
	std::array<size_t, 2> endCoord = {1, 2};
	// get index in linear array
	std::array<size_t, 2> coord    = {0, 0};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear           = range.begin;
	for (size_t i = 0; i < dimensions.at(0); i++) {
		for (size_t j = 0; j < dimensions.at(1); j++) {
			EXPECT_EQ(multiDimArray[i][j], linearArray[indexInLinear]);
			indexInLinear += range.increment;
		}
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, getRange_endToEnd) {
	// take last coordinate as end-coordinate
	std::array<size_t, 2> endCoord = {1, 2};
	// get index in linear array
	std::array<size_t, 2> coord    = {1, 2};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[1][2], linearArray[r]);
	}
}

TEST_F(TDimensionTest2D, getRange_somewhereToSomewhere) {
	std::array<size_t, 2> startCoord = {0, 1};
	std::array<size_t, 2> endCoord   = {1, 1};
	// get index in linear array
	TRange range                     = dim.getRange(startCoord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][1], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[0][2], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[1][0], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[1][1], linearArray[indexInLinear]);
	indexInLinear++;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, getRange_startIsLargerThanEnd) {
	std::array<size_t, 2> startCoord = {1, 0};
	std::array<size_t, 2> endCoord   = {0, 2};
	// get index in linear array
	EXPECT_ANY_THROW(dim.getRange(startCoord, endCoord));
}

TEST_F(TDimensionTest2D, getFull) {
	TRange range         = dim.getFull();
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear = range.begin;
	for (size_t i = 0; i < dimensions.at(0); i++) {
		for (size_t j = 0; j < dimensions.at(1); j++) {
			EXPECT_EQ(multiDimArray[i][j], linearArray[indexInLinear]);
			indexInLinear += range.increment;
		}
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, get1DSlice_invalidDimension) {
	std::array<size_t, 2> startCoord = {0, 0};
	// this is ok
	EXPECT_NO_THROW(dim.get1DSlice(0, startCoord));
	EXPECT_NO_THROW(dim.get1DSlice(1, startCoord));
	// this is too much
#ifdef _DEBUG
	EXPECT_DEATH(dim.get1DSlice(2, startCoord), "");
#endif
}

TEST_F(TDimensionTest2D, get1DSlice_startToEndOfDim0) {
	std::array<size_t, 2> startCoord = {0, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 2D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[1][0], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, get1DSlice_startToEndOfDim1) {
	std::array<size_t, 2> startCoord = {0, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(1, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][1], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][2], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, get1DSlice_endToEndOfDim0) {
	std::array<size_t, 2> startCoord = {1, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array

	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[1][0], linearArray[r]);
	}

	size_t indexInLinear = range.begin;
	EXPECT_EQ(multiDimArray[1][0], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, get1DSlice_endToEndOfDim1) {
	std::array<size_t, 2> startCoord = {0, 2};
	// get index in linear array
	TRange range                     = dim.get1DSlice(1, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array

	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[0][2], linearArray[r]);
	}

	size_t indexInLinear = range.begin;
	EXPECT_EQ(multiDimArray[0][2], linearArray[indexInLinear]);
	indexInLinear += range.increment;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest2D, beginSlice_row) {
	// indexInDim = 0
	size_t indexInDim = 0;
	size_t counter    = 0;
	for (TRangeIterator r = dim.beginSlice(0, indexInDim); !r.end(); ++r) {
		EXPECT_EQ(multiDimArray[indexInDim][counter], linearArray[r.cur()]);
		++counter;
	}
	EXPECT_EQ(counter, 3);

	// indexInDim = 1
	indexInDim = 1;
	counter    = 0;
	for (TRangeIterator r = dim.beginSlice(0, indexInDim); !r.end(); ++r) {
		EXPECT_EQ(multiDimArray[indexInDim][counter], linearArray[r.cur()]);
		++counter;
	}
	EXPECT_EQ(counter, 3);
}

TEST_F(TDimensionTest2D, beginSlice_column) {
	// indexInDim = 0
	size_t indexInDim = 0;
	size_t counter    = 0;
	for (TRangeIterator r = dim.beginSlice(1, indexInDim); !r.end(); ++r) {
		EXPECT_EQ(multiDimArray[counter][indexInDim], linearArray[r.cur()]);
		++counter;
	}
	EXPECT_EQ(counter, 2);

	// indexInDim = 1
	indexInDim = 1;
	counter    = 0;
	for (TRangeIterator r = dim.beginSlice(1, indexInDim); !r.end(); ++r) {
		EXPECT_EQ(multiDimArray[counter][indexInDim], linearArray[r.cur()]);
		++counter;
	}
	EXPECT_EQ(counter, 2);

	// indexInDim = 2
	indexInDim = 2;
	counter    = 0;
	for (TRangeIterator r = dim.beginSlice(1, indexInDim); !r.end(); ++r) {
		EXPECT_EQ(multiDimArray[counter][indexInDim], linearArray[r.cur()]);
		++counter;
	}
	EXPECT_EQ(counter, 2);
}

TEST_F(TDimensionTest2D, getSubscripts) {
	// compute forth and back
	for (size_t linearIndex = 0; linearIndex < dim.size(); linearIndex++) {
		std::array<size_t, 2> coords = dim.getSubscripts(linearIndex);
		EXPECT_EQ(linearIndex, dim.getIndex(coords));
	}

	// throw for all other cases
	EXPECT_ANY_THROW(dim.getSubscripts(6));
	EXPECT_ANY_THROW(dim.getSubscripts(7));
}

class TDimensionTest3D : public testing::Test {
protected:
	std::array<size_t, 3> dimensions = {2, 3, 4};
	TDimension<3> dim;

	double ***multiDimArray = nullptr;
	double *linearArray     = nullptr;

	void SetUp() override {
		dim.init(dimensions);

		// create some 3D array and fill it
		// create a 1D array and fill it with the same values as the 3D array
		double val    = 0.1;
		int c         = 0;
		multiDimArray = new double **[2];
		linearArray   = new double[2 * 3 * 4];
		for (size_t i = 0; i < 2; i++) {
			multiDimArray[i] = new double *[3];
			for (size_t j = 0; j < 3; j++) {
				multiDimArray[i][j] = new double[4];
				for (size_t k = 0; k < 4; k++) {
					multiDimArray[i][j][k] = val;
					linearArray[c]         = val;
					val += 0.1;
					c++;
				}
			}
		}
	}

	std::vector<double> getExpectation_3D_dim0(size_t i) {
		std::vector<double> expected;
		for (size_t j = 0; j < dimensions[1]; j++) {
			for (size_t k = 0; k < dimensions[2]; k++) { expected.push_back(multiDimArray[i][j][k]); }
		}
		return expected;
	}

	std::vector<double> getExpectation_3D_dim1(size_t j) {
		std::vector<double> expected;
		for (size_t i = 0; i < dimensions[0]; i++) {
			for (size_t k = 0; k < dimensions[2]; k++) { expected.push_back(multiDimArray[i][j][k]); }
		}
		return expected;
	}

	std::vector<double> getExpectation_3D_dim2(size_t k) {
		std::vector<double> expected;
		for (size_t i = 0; i < dimensions[0]; i++) {
			for (size_t j = 0; j < dimensions[1]; j++) { expected.push_back(multiDimArray[i][j][k]); }
		}
		return expected;
	}

	void TearDown() override {
		for (size_t i = 0; i < 2; i++) {
			for (size_t j = 0; j < 3; j++) delete[] multiDimArray[i][j];
			delete[] multiDimArray[i];
		}
		delete[] multiDimArray;

		delete[] linearArray;
	}
};

TEST_F(TDimensionTest3D, getElement) {
	// go over all possible coordinates and check if value in multi-array is same
	// as in linear array
	for (size_t i = 0; i < dimensions.at(0); i++) {
		for (size_t j = 0; j < dimensions.at(1); j++) {
			for (size_t k = 0; k < dimensions.at(2); k++) {
				// get index in linear array
				std::array<size_t, 3> coord = {i, j, k};
				size_t z                    = dim.getIndex(coord);
				EXPECT_EQ(multiDimArray[i][j][k], linearArray[z]);
			}
		}
	}
}

TEST_F(TDimensionTest3D, getRange_startToEnd) {
	// take last coordinate as end-coordinate
	std::array<size_t, 3> endCoord = {1, 2, 3};
	// get index in linear array
	std::array<size_t, 3> coord    = {0, 0, 0};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear           = range.begin;
	for (size_t i = 0; i < dimensions.at(0); i++) {
		for (size_t j = 0; j < dimensions.at(1); j++) {
			for (size_t k = 0; k < dimensions.at(2); k++) {
				EXPECT_EQ(multiDimArray[i][j][k], linearArray[indexInLinear]);
				indexInLinear += range.increment;
			}
		}
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, getRange_endToEnd) {
	// take last coordinate as end-coordinate
	std::array<size_t, 3> endCoord = {1, 2, 3};
	// get index in linear array
	std::array<size_t, 3> coord    = {1, 2, 3};
	TRange range                   = dim.getRange(coord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[1][2][3], linearArray[r]);
	}
}

TEST_F(TDimensionTest3D, getRange_somewhereToSomewhere) {
	std::array<size_t, 3> startCoord = {0, 0, 3};
	std::array<size_t, 3> endCoord   = {0, 2, 0};
	// get index in linear array
	TRange range                     = dim.getRange(startCoord, endCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][0][3], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[0][1][0], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[0][1][1], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[0][1][2], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[0][1][3], linearArray[indexInLinear]);
	indexInLinear++;
	EXPECT_EQ(multiDimArray[0][2][0], linearArray[indexInLinear]);
	indexInLinear++;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, getRange_startIsLargerThanEnd) {
	std::array<size_t, 3> startCoord = {0, 2, 0};
	std::array<size_t, 3> endCoord   = {0, 0, 1};
	// get index in linear array
	EXPECT_ANY_THROW(dim.getRange(startCoord, endCoord));
}

TEST_F(TDimensionTest3D, getFull) {
	TRange range         = dim.getFull();
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear = range.begin;
	for (size_t i = 0; i < dimensions.at(0); i++) {
		for (size_t j = 0; j < dimensions.at(1); j++) {
			for (size_t k = 0; k < dimensions.at(2); k++) {
				EXPECT_EQ(multiDimArray[i][j][k], linearArray[indexInLinear]);
				indexInLinear += range.increment;
			}
		}
	}
	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, get1DSlice_invalidDimension) {
	std::array<size_t, 3> startCoord = {0, 0, 3};
	// this is ok
	EXPECT_NO_THROW(dim.get1DSlice(0, startCoord));
	EXPECT_NO_THROW(dim.get1DSlice(2, startCoord));
	// this is too much
#ifdef _DEBUG
	EXPECT_DEATH(dim.get1DSlice(3, startCoord), "");
#endif
}

TEST_F(TDimensionTest3D, get1DSlice_startToEndOfDim0) {
	std::array<size_t, 3> startCoord = {0, 0, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][0][0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[1][0][0], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, get1DSlice_startToEndOfDim1) {
	std::array<size_t, 3> startCoord = {0, 0, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(1, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][0][0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][1][0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][2][0], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, get1DSlice_startToEndOfDim2) {
	std::array<size_t, 3> startCoord = {0, 0, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(2, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array
	size_t indexInLinear             = range.begin;

	EXPECT_EQ(multiDimArray[0][0][0], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][0][1], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][0][2], linearArray[indexInLinear]);
	indexInLinear += range.increment;
	EXPECT_EQ(multiDimArray[0][0][3], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, get1DSlice_endToEndOfDim0) {
	std::array<size_t, 3> startCoord = {1, 0, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(0, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array

	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[1][0][0], linearArray[r]);
	}

	size_t indexInLinear = range.begin;
	EXPECT_EQ(multiDimArray[1][0][0], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, get1DSlice_endToEndOfDim1) {
	std::array<size_t, 3> startCoord = {0, 2, 0};
	// get index in linear array
	TRange range                     = dim.get1DSlice(1, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array

	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[0][2][0], linearArray[r]);
	}

	size_t indexInLinear = range.begin;
	EXPECT_EQ(multiDimArray[0][2][0], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, get1DSlice_endToEndOfDim2) {
	std::array<size_t, 3> startCoord = {0, 0, 3};
	// get index in linear array
	TRange range                     = dim.get1DSlice(2, startCoord);
	// now check if these values from 1D array corrspond to values in 3D array

	for (size_t r = range.begin; r < range.end; r += range.increment) {
		EXPECT_EQ(multiDimArray[0][0][3], linearArray[r]);
	}

	size_t indexInLinear = range.begin;
	EXPECT_EQ(multiDimArray[0][0][3], linearArray[indexInLinear]);
	indexInLinear += 1;

	EXPECT_EQ(indexInLinear, range.end);
}

TEST_F(TDimensionTest3D, beginSlice_dim0) {
	size_t d = 0;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_3D_dim0(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

TEST_F(TDimensionTest3D, beginSlice_dim1) {
	size_t d = 1;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_3D_dim1(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

TEST_F(TDimensionTest3D, beginSlice_dim2) {
	size_t d = 2;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_3D_dim2(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

TEST_F(TDimensionTest3D, getSubscripts) {
	// compute forth and back
	for (size_t linearIndex = 0; linearIndex < dim.size(); linearIndex++) {
		std::array<size_t, 3> coords = dim.getSubscripts(linearIndex);
		EXPECT_EQ(linearIndex, dim.getIndex(coords));
	}

	// throw for all other cases
	EXPECT_ANY_THROW(dim.getSubscripts(25));
	EXPECT_ANY_THROW(dim.getSubscripts(26));
}

class TDimensionTest4D : public testing::Test {
protected:
	std::array<size_t, 4> dimensions = {5, 7, 11, 13};
	TDimension<4> dim;

	double ****multiDimArray = nullptr;
	double *linearArray      = nullptr;

	void SetUp() override {
		dim.init(dimensions);

		// create some 4D array and fill it
		// create a 1D array and fill it with the same values as the 4D array
		double val    = 0.1;
		int c         = 0;
		multiDimArray = new double ***[dimensions[0]];
		linearArray   = new double[coretools::containerProduct(dimensions)];
		for (size_t i = 0; i < dimensions[0]; i++) {
			multiDimArray[i] = new double **[dimensions[1]];
			for (size_t j = 0; j < dimensions[1]; j++) {
				multiDimArray[i][j] = new double *[dimensions[2]];
				for (size_t k = 0; k < dimensions[2]; k++) {
					multiDimArray[i][j][k] = new double[dimensions[3]];
					for (size_t l = 0; l < dimensions[3]; l++) {
						multiDimArray[i][j][k][l] = val;
						linearArray[c]            = val;
						val += 0.1;
						c++;
					}
				}
			}
		}
	}

	std::vector<double> getExpectation_4D_dim0(size_t i) {
		std::vector<double> expected;
		for (size_t j = 0; j < dimensions[1]; j++) {
			for (size_t k = 0; k < dimensions[2]; k++) {
				for (size_t l = 0; l < dimensions[3]; l++) { expected.push_back(multiDimArray[i][j][k][l]); }
			}
		}
		return expected;
	}

	std::vector<double> getExpectation_4D_dim1(size_t j) {
		std::vector<double> expected;
		for (size_t i = 0; i < dimensions[0]; i++) {
			for (size_t k = 0; k < dimensions[2]; k++) {
				for (size_t l = 0; l < dimensions[3]; l++) { expected.push_back(multiDimArray[i][j][k][l]); }
			}
		}
		return expected;
	}

	std::vector<double> getExpectation_4D_dim2(size_t k) {
		std::vector<double> expected;
		for (size_t i = 0; i < dimensions[0]; i++) {
			for (size_t j = 0; j < dimensions[1]; j++) {
				for (size_t l = 0; l < dimensions[3]; l++) { expected.push_back(multiDimArray[i][j][k][l]); }
			}
		}
		return expected;
	}

	std::vector<double> getExpectation_4D_dim3(size_t l) {
		std::vector<double> expected;
		for (size_t i = 0; i < dimensions[0]; i++) {
			for (size_t j = 0; j < dimensions[1]; j++) {
				for (size_t k = 0; k < dimensions[2]; k++) { expected.push_back(multiDimArray[i][j][k][l]); }
			}
		}
		return expected;
	}
	void TearDown() override {
		for (size_t i = 0; i < dimensions[0]; i++) {
			for (size_t j = 0; j < dimensions[1]; j++) {
				for (size_t k = 0; k < dimensions[2]; k++) { delete[] multiDimArray[i][j][k]; }
				delete[] multiDimArray[i][j];
			}
			delete[] multiDimArray[i];
		}
		delete[] multiDimArray;

		delete[] linearArray;
	}
};

TEST_F(TDimensionTest4D, beginSlice_dim0) {
	size_t d = 0;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_4D_dim0(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

TEST_F(TDimensionTest4D, beginSlice_dim1) {
	size_t d = 1;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_4D_dim1(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

TEST_F(TDimensionTest4D, beginSlice_dim2) {
	size_t d = 2;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_4D_dim2(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

TEST_F(TDimensionTest4D, beginSlice_dim3) {
	size_t d = 3;
	for (size_t indexInDim = 0; indexInDim < dimensions[d]; indexInDim++) {
		std::vector<double> expected = getExpectation_4D_dim3(indexInDim);
		size_t counter               = 0;
		for (TSliceIterator r = dim.beginSlice(d, indexInDim); !r.end(); ++r) {
			EXPECT_EQ(expected[counter], linearArray[r.cur()]);
			++counter;
		}
		EXPECT_EQ(counter, expected.size());
	}
}

//-------------------------------------------
// TMultiIndex
//-------------------------------------------

TEST(TMultiIndexTest, test){
	std::vector<size_t> dim = {4, 5};
	TMultiIndex ix(dim);

	EXPECT_EQ(ix.size(), 20);

	for (size_t i = 0; i < ix.size(); ++i){
		const auto coord = ix.get(i);
		EXPECT_EQ(coord.size(), 2);
		EXPECT_EQ(coord[0], coretools::getSubscripts(i, dim)[0]);
		EXPECT_EQ(coord[1], coretools::getSubscripts(i, dim)[1]);
	}
}
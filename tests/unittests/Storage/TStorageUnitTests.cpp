//
// Created by madleina on 28.07.20.
//
#include "gtest/gtest.h"

#include "coretools/Storage/TStorage.h"
#include "coretools/Types/commonWeakTypes.h"

using namespace coretools;

//-------------------------------------------
// TMultiDimensionalStorage
//-------------------------------------------
class TMultiDimensionalStorageTest : public testing::Test {
public:
	TMultiDimensionalStorage<Unbounded, 3> storage;
	TMultiDimensionalStorage<Unbounded, 3> storage_1Dim;

	void SetUp() override {
		// multidimensional
		storage.resize(std::array<size_t, 3>{10, 5, 2});
		// multidimensional, but size 1 for each dimension
		storage_1Dim.resize(std::array<size_t, 3>{1, 1, 1});

		// create name pointers and add them to storage
		std::shared_ptr<TNamesEmpty> namesDim0 = std::make_shared<TNamesIndices>();
		std::shared_ptr<TNamesEmpty> namesDim1 = std::make_shared<TNamesIndicesAlphabetLowerCase>();
		std::shared_ptr<TNamesEmpty> namesDim2 = std::make_shared<TNamesIndicesAlphabetUpperCase>();
		storage.setDimensionName(namesDim0, 0);
		storage.setDimensionName(namesDim1, 1);
		storage.setDimensionName(namesDim2, 2);
		storage_1Dim.setDimensionNames({namesDim0, namesDim1, namesDim2});
	}
};

TEST_F(TMultiDimensionalStorageTest, getDimensionName) {
	EXPECT_EQ((*storage.getDimensionName(0))[0], "1");
	EXPECT_EQ((*storage.getDimensionName(1))[0], "a");
	EXPECT_EQ((*storage.getDimensionName(2))[0], "A");
}

TEST_F(TMultiDimensionalStorageTest, getDimensionName_1D) {
	EXPECT_EQ((*storage_1Dim.getDimensionName(0))[0], "1");
	EXPECT_EQ((*storage_1Dim.getDimensionName(1))[0], "a");
	EXPECT_EQ((*storage_1Dim.getDimensionName(2))[0], "A");
}

TEST_F(TMultiDimensionalStorageTest, fillFullDimensionName) {
	// linear index 0
	auto fullName = storage.getFullDimensionName(storage.getSubscripts(0));
	EXPECT_EQ(fullName.size(), 3);
	EXPECT_EQ(fullName[0], "1");
	EXPECT_EQ(fullName[1], "a");
	EXPECT_EQ(fullName[2], "A");

	// linear index 1
	fullName = storage.getFullDimensionName(storage.getSubscripts(1));
	EXPECT_EQ(fullName.size(), 3);
	EXPECT_EQ(fullName[0], "1");
	EXPECT_EQ(fullName[1], "a");
	EXPECT_EQ(fullName[2], "B");

	// linear index 2
	fullName = storage.getFullDimensionName(storage.getSubscripts(2));
	EXPECT_EQ(fullName.size(), 3);
	EXPECT_EQ(fullName[0], "1");
	EXPECT_EQ(fullName[1], "b");
	EXPECT_EQ(fullName[2], "A");

	// linear index 99 (last)
	fullName = storage.getFullDimensionName(storage.getSubscripts(99));
	EXPECT_EQ(fullName.size(), 3);
	EXPECT_EQ(fullName[0], "10");
	EXPECT_EQ(fullName[1], "e");
	EXPECT_EQ(fullName[2], "B");
}

TEST_F(TMultiDimensionalStorageTest, fillFullDimensionName_1D) {
	// linear index 0
	auto fullName = storage_1Dim.getFullDimensionName(storage.getSubscripts(0));
	EXPECT_EQ(fullName.size(), 3);
	EXPECT_EQ(fullName[0], "1");
	EXPECT_EQ(fullName[1], "a");
	EXPECT_EQ(fullName[2], "A");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNames) {
	std::vector<std::string> fullName;
	storage.appendToVectorOfAllFullDimensionNames(fullName);

	EXPECT_EQ(fullName.size(), 100);
	EXPECT_EQ(fullName[0], "1_a_A");
	EXPECT_EQ(fullName[1], "1_a_B");
	EXPECT_EQ(fullName[2], "1_b_A");
	EXPECT_EQ(fullName[99], "10_e_B");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNames_1D) {
	std::vector<std::string> fullName;
	storage_1Dim.appendToVectorOfAllFullDimensionNames(fullName);
	EXPECT_EQ(fullName.size(), 1);
	EXPECT_EQ(fullName[0], "1_a_A");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNamesWithPrefix) {
	std::vector<std::string> fullName;
	storage.appendToVectorOfAllFullDimensionNamesWithPrefix(fullName, "param");

	EXPECT_EQ(fullName.size(), 100);
	EXPECT_EQ(fullName[0], "param_1_a_A");
	EXPECT_EQ(fullName[1], "param_1_a_B");
	EXPECT_EQ(fullName[2], "param_1_b_A");
	EXPECT_EQ(fullName[99], "param_10_e_B");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNamesWithPrefix_1D) {
	std::vector<std::string> fullName;
	storage_1Dim.appendToVectorOfAllFullDimensionNamesWithPrefix(fullName, "param");

	EXPECT_EQ(fullName.size(), 1);
	EXPECT_EQ(fullName[0], "param_1_a_A");
}

TEST_F(TMultiDimensionalStorageTest, filling_1D) {
	TMultiDimensionalStorage<Unbounded, 1> storage;

	storage.prepareFillData(1000, {});

	// no data has been added yet
	EXPECT_EQ(storage.size(), 0);
	EXPECT_EQ(storage.capacity(), 1000);

	// add some data
	for (size_t i = 0; i < 10; i++) { storage.emplace_back((double)i); }
	for (size_t i = 10; i < 20; i++) { storage.push_back((double)i); }

	// check if data has been filled correctly
	EXPECT_EQ(storage.size(), 20);
	EXPECT_EQ(storage.capacity(), 1000);
	for (size_t i = 0; i < 20; i++) { EXPECT_EQ(storage[i], (double)i); }

	// add entire block
	auto ptr = storage.addMemoryBlock(80);
	EXPECT_EQ(storage.size(), 100);
	EXPECT_EQ(storage.capacity(), 1000);
	for (size_t i = 0; i < 100; i++) {
		if (i < 20) {
			EXPECT_EQ(storage[i], (double)i);
		} else {
			EXPECT_EQ(storage[i], std::numeric_limits<double>::lowest());
		}
	}
	EXPECT_EQ(*(ptr - 1), 19.0); // last value filled above
	EXPECT_EQ(*ptr, std::numeric_limits<double>::lowest());
	EXPECT_EQ(*(ptr + 1), std::numeric_limits<double>::lowest());

	// pop_back last 10 element
	storage.pop_back(10);
	EXPECT_EQ(storage.size(), 90);
	EXPECT_EQ(storage.capacity(), 1000);
	for (size_t i = 0; i < 90; i++) {
		if (i < 20) {
			EXPECT_EQ(storage[i], (double)i);
		} else {
			EXPECT_EQ(storage[i], std::numeric_limits<double>::lowest());
		}
	}

	// finalize filling
	storage.finalizeFillData();
	EXPECT_EQ(storage.size(), 90);
	EXPECT_EQ(storage.capacity(), 90);
	EXPECT_EQ(storage.dimensions()[0], 90);
	for (size_t i = 0; i < 90; i++) {
		if (i < 20) {
			EXPECT_EQ(storage[i], (double)i);
		} else {
			EXPECT_EQ(storage[i], std::numeric_limits<double>::lowest());
		}
	}
}

TEST_F(TMultiDimensionalStorageTest, filling_2D) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(1000, {10});

	// no data has been added yet
	EXPECT_EQ(storage.size(), 0);
	EXPECT_EQ(storage.capacity(), 10000);

	// add some data
	for (size_t i = 0; i < 10; i++) { storage.emplace_back((double)i); }
	for (size_t i = 10; i < 20; i++) { storage.push_back((double)i); }

	// check if data has been filled correctly
	EXPECT_EQ(storage.size(), 20);
	EXPECT_EQ(storage.capacity(), 10000);
	for (size_t i = 0; i < 20; i++) { EXPECT_EQ(storage[i], (double)i); }

	// add entire block
	auto ptr = storage.addMemoryBlock(80);
	EXPECT_EQ(storage.size(), 100);
	EXPECT_EQ(storage.capacity(), 10000);
	for (size_t i = 0; i < 100; i++) {
		if (i < 20) {
			EXPECT_EQ(storage[i], (double)i);
		} else {
			EXPECT_EQ(storage[i], std::numeric_limits<double>::lowest());
		}
	}
	EXPECT_EQ(*(ptr - 1), 19.0); // last value filled above
	EXPECT_EQ(*ptr, std::numeric_limits<double>::lowest());
	EXPECT_EQ(*(ptr + 1), std::numeric_limits<double>::lowest());

	// pop_back last 10 element
	storage.pop_back(10);
	EXPECT_EQ(storage.size(), 90);
	EXPECT_EQ(storage.capacity(), 10000);
	for (size_t i = 0; i < 90; i++) {
		if (i < 20) {
			EXPECT_EQ(storage[i], (double)i);
		} else {
			EXPECT_EQ(storage[i], std::numeric_limits<double>::lowest());
		}
	}

	// finalize filling
	storage.finalizeFillData();
	EXPECT_EQ(storage.size(), 90);
	EXPECT_EQ(storage.capacity(), 90);
	EXPECT_EQ(storage.dimensions()[0], 9);
	EXPECT_EQ(storage.dimensions()[1], 10);
	for (size_t i = 0; i < 90; i++) {
		if (i < 20) {
			EXPECT_EQ(storage[i], (double)i);
		} else {
			EXPECT_EQ(storage[i], std::numeric_limits<double>::lowest());
		}
	}
}

class Dummy {
private:
	size_t _value{};
	size_t _otherValue{};

public:
	Dummy(size_t Value, size_t OtherValue) {
		_value      = Value;
		_otherValue = OtherValue;
	};
	using value_type = size_t;

	size_t value() const { return _value; }
	size_t otherValue() const { return _otherValue; }
};

TEST_F(TMultiDimensionalStorageTest, emplaceBack) {
	TMultiDimensionalStorage<Dummy, 1> storage;

	// emplace_back
	for (size_t i = 0; i < 20; i++) { storage.emplace_back(i, i + 1); }

	// push_back with move
	for (size_t i = 0; i < 20; i++) { storage.push_back(Dummy(i + 5, i + 10)); }

	// push back with const reference
	Dummy dummy(0, 1);
	for (size_t i = 0; i < 20; i++) { storage.push_back(dummy); }

	// check if all are correctly filled
	for (size_t i = 0; i < 20; i++) {
		EXPECT_EQ(storage[i].value(), i);
		EXPECT_EQ(storage[i].otherValue(), i + 1);
	}

	for (size_t i = 20; i < 40; i++) {
		EXPECT_EQ(storage[i].value(), i - 20 + 5);
		EXPECT_EQ(storage[i].otherValue(), i - 20 + 10);
	}

	for (size_t i = 40; i < 60; i++) {
		EXPECT_EQ(storage[i].value(), 0);
		EXPECT_EQ(storage[i].otherValue(), 1);
	}
}

TEST_F(TMultiDimensionalStorageTest, addMemoryBlock) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	size_t M = 3;
	size_t N = 2;
	storage.prepareFillData(M, {N});

	auto ptr = storage.addMemoryBlock(N);
	for (size_t i = 0; i < M + 1; i++) { // one more than initially thought
		for (size_t j = 0; j < N; j++) { ptr[j] = j; }
		ptr = storage.addMemoryBlock(N);
	}

	// remove last, extra element
	storage.pop_back(N);

	storage.finalizeFillData();

	// check
	EXPECT_EQ(storage.size(), (M + 1) * N);
	EXPECT_EQ(storage.capacity(), (M + 1) * N);
	EXPECT_EQ(storage.dimensions()[0], M + 1);
	EXPECT_EQ(storage.dimensions()[1], N);

	size_t c = 0;
	for (size_t i = 0; i < M + 1; i++) {
		for (size_t j = 0; j < N; j++, c++) { EXPECT_EQ(storage[c], (double)j); }
	}
}

TEST_F(TMultiDimensionalStorageTest, operators) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});

	size_t c = 0;
	for (size_t i = 0; i < 10; i++) {
		for (size_t j = 0; j < 5; j++, c++) { storage.emplace_back(c); }
	}
	storage.finalizeFillData();

	// check
	c = 0;
	for (size_t i = 0; i < 10; i++) {
		for (size_t j = 0; j < 5; j++, c++) {
			EXPECT_EQ((storage[{i, j}]), (double)c);
			EXPECT_EQ(storage(i, j), (double)c);
			EXPECT_EQ(storage[storage.getIndex({i, j})], (double)c);
		}
	}
}

TEST_F(TMultiDimensionalStorageTest, numNonZero) {
	TMultiDimensionalStorage<Boolean, 2> storage;

	storage.prepareFillData(10, {5});

	for (size_t i = 0; i < 50; i++) {
		if (i % 2 == 0) {
			storage.push_back(true);
		} else {
			storage.push_back(false);
		}
	}

	storage.finalizeFillData();

	// check
	EXPECT_EQ(storage.numNonZero(), 25);
}

TEST_F(TMultiDimensionalStorageTest, sum) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back((double)i + 1.0); }
	storage.finalizeFillData();

	// check
	EXPECT_EQ(storage.sum(), 1275);
}

TEST_F(TMultiDimensionalStorageTest, prod) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(3, {4});
	for (size_t i = 0; i < 12; i++) { storage.push_back(((double)i + 1.0) / 10.); }
	storage.finalizeFillData();

	// check
	EXPECT_FLOAT_EQ(storage.prod(), 0.0004790016);
}

TEST_F(TMultiDimensionalStorageTest, sumOfLogs) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back((double)i + 1.0); }
	storage.finalizeFillData();

	// check
	EXPECT_FLOAT_EQ(storage.sumOfLogs(), 148.4778);
}

TEST_F(TMultiDimensionalStorageTest, sumOfLogsComplement) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back(((double)i + 1.0) / 100.); }
	storage.finalizeFillData();

	// check
	EXPECT_FLOAT_EQ(storage.sumOfLogsComplement(), -15.69005);
}

TEST_F(TMultiDimensionalStorageTest, mean_var_sd) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back(((double)i + 1.0) / 100.); }
	storage.finalizeFillData();

	// check
	auto m = storage.mean();
	EXPECT_FLOAT_EQ(m, 0.255);
	EXPECT_FLOAT_EQ(storage.var(), 0.02125);
	EXPECT_FLOAT_EQ(storage.var(m), 0.02125);
	EXPECT_FLOAT_EQ(storage.sd(), 0.1457738);
	EXPECT_FLOAT_EQ(storage.sd(m), 0.1457738);
}

TEST_F(TMultiDimensionalStorageTest, customSum) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back(((double)i + 1.0) / 100.); }
	storage.finalizeFillData();

	// check
	double sum = storage.customSum([](auto v) { return log(-(v - 1.) * v); });
	EXPECT_FLOAT_EQ(sum, -97.47079022);
}

TEST_F(TMultiDimensionalStorageTest, customLogSum) {
	TMultiDimensionalStorage<Unbounded, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back(((double)i + 1.0) / 100.); }
	storage.finalizeFillData();

	// check
	double sum = storage.customLogSum([](auto v) { return -(v - 1.) * v; });
	EXPECT_FLOAT_EQ(sum, -97.47079022);
}

TEST_F(TMultiDimensionalStorageTest, countLevels) {
	TMultiDimensionalStorage<UnsignedInt64, 2> storage;

	storage.prepareFillData(10, {5});
	for (size_t i = 0; i < 50; i++) { storage.push_back(i % 5); }
	storage.finalizeFillData();

	// check
	auto counts = storage.countLevels(5);
	EXPECT_EQ(counts.size(), 5);
	for (auto &it : counts) { EXPECT_EQ(it, 10); }
}

//
// Created by madleina on 04.05.21.
//

#include <fstream>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/Storage/TDataFileNames.h"
#include "coretools/Storage/TNames.h"
#include "coretools/Strings/stringManipulations.h"

using namespace testing;
using namespace coretools;

namespace impl {
template<typename StreamType>
bool readUntilDelimiter(StreamType *FilePointer, std::string &String, char Delimiter,
                        std::string_view DelimiterComment = "//") {
	String.clear();
	if (FilePointer->good() && !FilePointer->eof()) {
		std::getline(*FilePointer, String, Delimiter);

		// skip comments
		String = str::extractBefore(String, DelimiterComment, false);
	}

	if (!FilePointer->good() || FilePointer->eof()) { return false; }
	return true;
}
}

//--------------------
// TDataBlock
//--------------------

TEST(TDataBlockTests, getBlockSize) {
	EXPECT_EQ(TDataBlock::getBlockSize({1}), 1);
	EXPECT_EQ(TDataBlock::getBlockSize({2}), 2);
	EXPECT_EQ(TDataBlock::getBlockSize({1, 2}), 2);
	EXPECT_EQ(TDataBlock::getBlockSize({10, 1}), 10);
	EXPECT_EQ(TDataBlock::getBlockSize({5, 6, 1, 2, 4}), 240);
}

TEST(TDataBlockTests, getDelimiterLookupOneBlock) {
	// all dimensions are >1
	std::vector<char> delimiters   = {'\t', '_', '/'};
	std::vector<size_t> dimensions = {2, 2, 3};
	char delimiterLastDim          = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim),
	            ElementsAre('/', '/', '_', '/', '/', '\t', '/', '/', '_', '/', '/', '\n'));

	// first dimension is only 1
	delimiters       = {'\t', '_', '/'};
	dimensions       = {1, 2, 3};
	delimiterLastDim = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim),
	            ElementsAre('/', '/', '_', '/', '/', '\n'));

	// intermediate dimension is only 1
	delimiters       = {'\t', '_', '/'};
	dimensions       = {2, 1, 3};
	delimiterLastDim = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim),
	            ElementsAre('/', '/', '\t', '/', '/', '\n'));

	// last dimension is only 1
	delimiters       = {'\t', '_', '/'};
	dimensions       = {2, 3, 1};
	delimiterLastDim = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim),
	            ElementsAre('_', '_', '\t', '_', '_', '\n'));

	// only 1 dimension
	delimiters       = {'\t'};
	dimensions       = {3};
	delimiterLastDim = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim),
	            ElementsAre('\t', '\t', '\n'));

	// only 1 element
	delimiters       = {'\t'};
	dimensions       = {1};
	delimiterLastDim = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim), ElementsAre('\n'));

	// empty
	delimiters       = {};
	dimensions       = {};
	delimiterLastDim = '\n';
	EXPECT_THAT(TDataBlock::getDelimiterLookupOneBlock(delimiters, dimensions, delimiterLastDim), ElementsAre('\n'));
}

TEST(TDataBlockTests, getCoordinatesOneBlock) {
	// all dimensions are >1
	std::vector<size_t> dimensions               = {2, 2, 3};
	std::vector<std::vector<size_t>> coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 12);
	EXPECT_THAT(coordinates[0], ElementsAre(0, 0, 0));
	EXPECT_THAT(coordinates[1], ElementsAre(0, 0, 1));
	EXPECT_THAT(coordinates[2], ElementsAre(0, 0, 2));
	EXPECT_THAT(coordinates[3], ElementsAre(0, 1, 0));
	EXPECT_THAT(coordinates[4], ElementsAre(0, 1, 1));
	EXPECT_THAT(coordinates[5], ElementsAre(0, 1, 2));
	EXPECT_THAT(coordinates[6], ElementsAre(1, 0, 0));
	EXPECT_THAT(coordinates[7], ElementsAre(1, 0, 1));
	EXPECT_THAT(coordinates[8], ElementsAre(1, 0, 2));
	EXPECT_THAT(coordinates[9], ElementsAre(1, 1, 0));
	EXPECT_THAT(coordinates[10], ElementsAre(1, 1, 1));
	EXPECT_THAT(coordinates[11], ElementsAre(1, 1, 2));

	// first dimension is only 1
	dimensions  = {1, 2, 3};
	coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 6);
	EXPECT_THAT(coordinates[0], ElementsAre(0, 0, 0));
	EXPECT_THAT(coordinates[1], ElementsAre(0, 0, 1));
	EXPECT_THAT(coordinates[2], ElementsAre(0, 0, 2));
	EXPECT_THAT(coordinates[3], ElementsAre(0, 1, 0));
	EXPECT_THAT(coordinates[4], ElementsAre(0, 1, 1));
	EXPECT_THAT(coordinates[5], ElementsAre(0, 1, 2));

	// intermediate dimension is only 1
	dimensions  = {2, 1, 3};
	coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 6);
	EXPECT_THAT(coordinates[0], ElementsAre(0, 0, 0));
	EXPECT_THAT(coordinates[1], ElementsAre(0, 0, 1));
	EXPECT_THAT(coordinates[2], ElementsAre(0, 0, 2));
	EXPECT_THAT(coordinates[3], ElementsAre(1, 0, 0));
	EXPECT_THAT(coordinates[4], ElementsAre(1, 0, 1));
	EXPECT_THAT(coordinates[5], ElementsAre(1, 0, 2));

	// last dimension is only 1
	dimensions  = {2, 3, 1};
	coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 6);
	EXPECT_THAT(coordinates[0], ElementsAre(0, 0, 0));
	EXPECT_THAT(coordinates[1], ElementsAre(0, 1, 0));
	EXPECT_THAT(coordinates[2], ElementsAre(0, 2, 0));
	EXPECT_THAT(coordinates[3], ElementsAre(1, 0, 0));
	EXPECT_THAT(coordinates[4], ElementsAre(1, 1, 0));
	EXPECT_THAT(coordinates[5], ElementsAre(1, 2, 0));

	// only 1 dimension
	dimensions  = {3};
	coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 3);
	EXPECT_THAT(coordinates[0], ElementsAre(0));
	EXPECT_THAT(coordinates[1], ElementsAre(1));
	EXPECT_THAT(coordinates[2], ElementsAre(2));

	// only 1 element
	dimensions  = {1};
	coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 1);
	EXPECT_THAT(coordinates[0], ElementsAre(0));

	// empty
	dimensions  = {};
	coordinates = TDataBlock::getCoordinatesOneBlock(dimensions);
	EXPECT_EQ(coordinates.size(), 1);
	EXPECT_THAT(coordinates[0], ElementsAre());
}

//--------------------
// TFileNames
//--------------------

class BridgeFileNames : public TFileNames {
public:
	BridgeFileNames(size_t NumDim, const std::vector<bool> &NameIsWritten,
	                const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters,
	                std::string_view FileName)
	    : TFileNames(NumDim, NameIsWritten, Names, Delimiters, FileName){};

	void _fillNameIsRowName(const std::vector<char> &Delimiters) { TFileNames::_fillNameIsRowName(Delimiters); }
	void _assignIndices_Sorted(const std::vector<std::string> &UniqueNames, size_t Dim) {
		TFileNames::_assignIndices_Sorted(UniqueNames, Dim);
	}
	void _assignIndices_Unsorted(size_t Size, size_t Dim) { TFileNames::_assignIndices_Unsorted(Size, Dim); }
	void static _removeDuplicatesFromVec(std::vector<std::string> &Vec) { TFileNames::_removeDuplicatesFromVec(Vec); }
};

class BridgeRowNames : public TRowNames {
public:
	BridgeRowNames(size_t NumDim, const std::vector<bool> &NameIsWritten,
	               const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters,
	               std::string_view FileName)
	    : TRowNames(NumDim, NameIsWritten, Names, Delimiters, FileName){};

	void _setLengthSubsequentDimensions_RowName(size_t Dim, size_t ProductOverAllNext, std::vector<size_t> &Dimensions,
	                                            std::vector<bool> &DimensionsFilled) {
		TRowNames::_setLengthSubsequentDimensions_RowName(Dim, ProductOverAllNext, Dimensions, DimensionsFilled);
	}
	bool _extractMostOuterRowName(std::istream *FilePointer, std::string_view DelimiterComments, bool &Keep,
	                              bool FirstBlock) {
		return TRowNames::_extractMostOuterRowName(FilePointer, DelimiterComments, Keep, FirstBlock);
	}
};

class BridgeTColNameMultiLine : public TColNameMultiLine {
public:
	BridgeTColNameMultiLine(size_t NumDim, const std::vector<bool> &NameIsWritten,
	                        const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters,
	                        std::string_view FileName)
	    : TColNameBase(NumDim, NameIsWritten, Names, Delimiters, FileName),
	      TColNameMultiLine(NumDim, NameIsWritten, Names, Delimiters, FileName){};
	bool _hasHeader() { return TColNameMultiLine::_hasHeader(); }
};

class BridgeTColNameConcatenated : public TColNameConcatenated {
public:
	BridgeTColNameConcatenated(size_t NumDim, const std::vector<bool> &NameIsWritten,
	                           const std::vector<std::shared_ptr<TNamesEmpty>> &Names,
	                           const std::vector<char> &Delimiters, std::string_view FileName)
	    : TColNameBase(NumDim, NameIsWritten, Names, Delimiters, FileName),
	      TColNameConcatenated(NumDim, NameIsWritten, Names, Delimiters, FileName){};
	char _getDelimMostOuterColumn(const std::vector<char> &Delimiters) {
		return TColNameConcatenated::_getDelimMostOuterColumn(Delimiters);
	}
	bool _hasHeader() { return TColNameConcatenated::_hasHeader(); }
};

class BridgeTColNameFull : public TColNameFull {
public:
	BridgeTColNameFull(size_t NumDim, const std::vector<bool> &NameIsWritten,
	                   const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters,
	                   std::string_view FileName)
	    : TColNameBase(NumDim, NameIsWritten, Names, Delimiters, FileName),
	      TColNameFull(NumDim, NameIsWritten, Names, Delimiters, FileName){};
	char _getDelimMostOuterColumn(const std::vector<char> &Delimiters) {
		return TColNameFull::_getDelimMostOuterColumn(Delimiters);
	}
	bool _hasHeader() { return TColNameFull::_hasHeader(); }
};

class TFileNamesTest : public Test {
protected:
public:
	std::unique_ptr<BridgeFileNames> fileNamesObj;
	std::unique_ptr<BridgeRowNames> rowNamesObj;
	std::unique_ptr<BridgeTColNameMultiLine> colNamesMultiLineObj;
	std::unique_ptr<BridgeTColNameConcatenated> colNamesConcatenatedObj;
	std::unique_ptr<BridgeTColNameFull> colNamesFullObj;

	std::string fileName;
	size_t numDim;
	std::vector<std::shared_ptr<TNamesEmpty>> names;
	std::vector<char> delimiters;
	std::vector<bool> nameIsWritten;
	std::vector<size_t> dimensions;
	std::vector<bool> knownDimensions;
	size_t guessLengthUnknownDimension;

	void SetUp() override {
		fileName = "test.txt";

		numDim                      = 3;
		guessLengthUnknownDimension = 10;
		initializeToDefault(numDim);
	}

	void initializeToDefault(size_t NumDim) {
		numDim = NumDim;

		// set names
		names.clear();
		for (size_t dim = 0; dim < NumDim; dim++) {
			auto name = std::make_shared<TNamesIndices>();
			names.push_back(name);
		}

		// set delimiters
		delimiters.clear();
		delimiters.push_back('\n');
		if (NumDim > 1) {
			delimiters.push_back('\t');
			for (size_t dim = 2; dim < NumDim; dim++) { delimiters.push_back('/'); }
		}

		nameIsWritten.clear();
		dimensions.clear();
		knownDimensions.clear();
		for (size_t dim = 0; dim < NumDim; dim++) {
			nameIsWritten.push_back(true);
			dimensions.push_back(0);
			knownDimensions.push_back(false);
		}
	}

	void initialize() {
		fileNamesObj = std::make_unique<BridgeFileNames>(numDim, nameIsWritten, names, delimiters, fileName);
		rowNamesObj  = std::make_unique<BridgeRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

		colNamesMultiLineObj =
		    std::make_unique<BridgeTColNameMultiLine>(numDim, nameIsWritten, names, delimiters, fileName);
		colNamesMultiLineObj->initialize(delimiters);
		colNamesConcatenatedObj =
		    std::make_unique<BridgeTColNameConcatenated>(numDim, nameIsWritten, names, delimiters, fileName);
		colNamesConcatenatedObj->initialize(delimiters);
		colNamesFullObj = std::make_unique<BridgeTColNameFull>(numDim, nameIsWritten, names, delimiters, fileName);
		colNamesFullObj->initialize(delimiters);
	}
};

void writeFile(std::string_view String) {
	std::string filename = "test.txt";
	std::ofstream out(filename.c_str());
	out << String;
	out.close();
}

//--------------------
// TFileNames
//--------------------

TEST_F(TFileNamesTest, fillNameIsRowName) {
	// only rowNames
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\n'};
	initialize();
	EXPECT_TRUE(fileNamesObj->nameIsRowName(0));
	EXPECT_FALSE(fileNamesObj->nameIsColName(0));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(1));
	EXPECT_FALSE(fileNamesObj->nameIsColName(1));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(2));
	EXPECT_FALSE(fileNamesObj->nameIsColName(2));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(3));
	EXPECT_FALSE(fileNamesObj->nameIsColName(3));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(4));
	EXPECT_FALSE(fileNamesObj->nameIsColName(4));

	// last is not rowName
	delimiters = {'\n', '\n', '\n', '\n', '\t'};
	initialize();
	EXPECT_TRUE(fileNamesObj->nameIsRowName(0));
	EXPECT_FALSE(fileNamesObj->nameIsColName(0));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(1));
	EXPECT_FALSE(fileNamesObj->nameIsColName(1));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(2));
	EXPECT_FALSE(fileNamesObj->nameIsColName(2));
	EXPECT_TRUE(fileNamesObj->nameIsRowName(3));
	EXPECT_FALSE(fileNamesObj->nameIsColName(3));
	EXPECT_FALSE(fileNamesObj->nameIsRowName(4));
	EXPECT_TRUE(fileNamesObj->nameIsColName(4));
}

TEST_F(TFileNamesTest, removeDuplicateFromVec) {
	std::vector<std::string> vec = {"one", "two", "three", "one", "two", "three",
	                                "one", "two", "three", "one", "two", "three"};
	BridgeFileNames::_removeDuplicatesFromVec(vec);
	EXPECT_THAT(vec, ElementsAre("one", "two", "three"));

	vec = {"one", "one", "one", "two", "two", "two", "three", "three", "three"};
	BridgeFileNames::_removeDuplicatesFromVec(vec);
	EXPECT_THAT(vec, ElementsAre("one", "two", "three"));
}

TEST_F(TFileNamesTest, _assignIndices_Unsorted) {
	initializeToDefault(5);

	// set name at dim 0: empty
	auto name = std::make_shared<TNamesEmpty>();
	names[0]  = name;
	initialize();

	fileNamesObj->_assignIndices_Unsorted(5, 0);

	// check:
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 0));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 1));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 2));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 3));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 4));

	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 0), 0);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 1), 1);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 2), 2);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 3), 3);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 4), 4);
}

TEST_F(TFileNamesTest, assignIndices_Sorted_KeepAll) {
	initializeToDefault(5);

	// set name at dim 0: strings, other order
	auto name = std::make_shared<TNamesStrings>();
	name->addName({"five"});
	name->addName({"three"});
	name->addName({"one"});
	name->addName({"four"});
	name->addName({"two"});
	names[0] = name;
	initialize();

	std::vector<std::string> uniqueNames = {"one", "two", "three", "four", "five"};
	fileNamesObj->_assignIndices_Sorted(uniqueNames, 0);

	// check:
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 0));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 1));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 2));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 3));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 4));

	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 0), 2);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 1), 4);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 2), 1);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 3), 3);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 4), 0);
}

TEST_F(TFileNamesTest, assignIndices_Sorted_KeepSome) {
	initializeToDefault(5);

	// set name at dim 0: strings, other order, four and one are missing
	auto name = std::make_shared<TNamesStrings>();
	name->addName({"five"});
	name->addName({"three"});
	name->addName({"two"});
	names[0] = name;
	initialize();

	std::vector<std::string> uniqueNames = {"one", "two", "three", "four", "five"};
	fileNamesObj->_assignIndices_Sorted(uniqueNames, 0);

	// check:
	EXPECT_FALSE(fileNamesObj->elementIsStored(0, 0));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 1));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 2));
	EXPECT_FALSE(fileNamesObj->elementIsStored(0, 3));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 4));

	EXPECT_ANY_THROW(fileNamesObj->indexToStoreElement(0, 0));
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 1), 2);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 2), 1);
	EXPECT_ANY_THROW(fileNamesObj->indexToStoreElement(0, 3));
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 4), 0);
}

TEST_F(TFileNamesTest, assignIndices_Sorted_ExtraNames) {
	initializeToDefault(5);

	// set name at dim 0: strings, other order
	auto name = std::make_shared<TNamesStrings>();
	name->addName({"five"});
	name->addName({"three"});
	name->addName({"six"});
	name->addName({"one"});
	name->addName({"four"});
	name->addName({"two"});
	names[0] = name;
	initialize();

	std::vector<std::string> uniqueNames = {"one", "two", "three", "four", "five"}; // six is missing!
	EXPECT_ANY_THROW(fileNamesObj->_assignIndices_Sorted(uniqueNames, 0));
}

TEST_F(TFileNamesTest, assignIndices_Sorted_KeepSome_Indices) {
	initializeToDefault(5);

	// set name at dim 0: strings, other order, four and one are missing
	auto name = std::make_shared<TNamesIndices>();
	name->addName({"2"});
	name->addName({"3"});
	name->addName({"1"});
	names[0] = name;
	initialize();

	std::vector<std::string> uniqueNames = {"1", "2", "3", "4", "5"};
	fileNamesObj->_assignIndices_Sorted(uniqueNames, 0);

	// check:
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 0));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 1));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 2));
	EXPECT_FALSE(fileNamesObj->elementIsStored(0, 3));
	EXPECT_FALSE(fileNamesObj->elementIsStored(0, 4));

	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 0), 0);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 1), 1);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 2), 2);
	EXPECT_ANY_THROW(fileNamesObj->indexToStoreElement(0, 3));
	EXPECT_ANY_THROW(fileNamesObj->indexToStoreElement(0, 4));
}

TEST_F(TFileNamesTest, assignIndices_Sorted_KeepSome_ReOrder_Indices) {
	initializeToDefault(5);

	// set name at dim 0: strings, other order, four and one are missing
	auto name = std::make_shared<TNamesIndices>();
	name->addName({"1"});
	name->addName({"2"});
	name->addName({"3"});
	names[0] = name;
	initialize();

	std::vector<std::string> uniqueNames = {"5", "1", "3", "4", "2"};
	fileNamesObj->_assignIndices_Sorted(uniqueNames, 0);

	// check:
	EXPECT_FALSE(fileNamesObj->elementIsStored(0, 0));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 1));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 2));
	EXPECT_FALSE(fileNamesObj->elementIsStored(0, 3));
	EXPECT_TRUE(fileNamesObj->elementIsStored(0, 4));

	EXPECT_ANY_THROW(fileNamesObj->indexToStoreElement(0, 0));
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 1), 0);
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 2), 2);
	EXPECT_ANY_THROW(fileNamesObj->indexToStoreElement(0, 3));
	EXPECT_EQ(fileNamesObj->indexToStoreElement(0, 4), 1);
}

TEST_F(TFileNamesTest, assignIndices_Sorted_Indices_ExtraNames) {
	initializeToDefault(5);

	// set name at dim 0: strings, other order
	auto name = std::make_shared<TNamesIndices>();
	name->addName({"1"});
	name->addName({"2"});
	name->addName({"3"});
	name->addName({"4"});
	name->addName({"5"});
	name->addName({"6"});
	names[0] = name;
	initialize();

	std::vector<std::string> uniqueNames = {"5", "1", "3", "4", "2"}; // 6 is missing!
	EXPECT_ANY_THROW(fileNamesObj->_assignIndices_Sorted(uniqueNames, 0));
}

//--------------------
// TRowNames
//--------------------

TEST_F(TFileNamesTest, correctWrittenNamesBasedOnFormat) {
	// only row names, all are written
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\n'};
	initialize();

	EXPECT_TRUE(rowNamesObj->nameIsWritten(0));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(1));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(2));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(3));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(4));

	// some are not written
	delimiters    = {'\n', '\n', '\n', '\n', '\n'};
	nameIsWritten = {true, false, true, false, true};
	initialize();

	EXPECT_TRUE(rowNamesObj->nameIsWritten(0));
	EXPECT_FALSE(rowNamesObj->nameIsWritten(1));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(2));
	EXPECT_FALSE(rowNamesObj->nameIsWritten(3));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(4));

	// some are not written, some are not rownames
	delimiters    = {'\n', '\n', '\n', '\t', '/'};
	nameIsWritten = {true, false, true, false, true};
	initialize();

	EXPECT_TRUE(rowNamesObj->nameIsWritten(0));
	EXPECT_FALSE(rowNamesObj->nameIsWritten(1));
	EXPECT_TRUE(rowNamesObj->nameIsWritten(2));
	EXPECT_FALSE(rowNamesObj->nameIsWritten(3));
	EXPECT_FALSE(rowNamesObj->nameIsWritten(4));
}

TEST_F(TFileNamesTest, allRowNameDimensionsAreKnown) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\n'};
	initialize();

	std::vector<bool> dimensionsFilled = {true, false, false, true, true};
	EXPECT_FALSE(rowNamesObj->allRowNameDimensionsAreKnown(dimensionsFilled));

	dimensionsFilled = {false, true, true, true, true};
	EXPECT_TRUE(rowNamesObj->allRowNameDimensionsAreKnown(dimensionsFilled));
}

TEST_F(TFileNamesTest, extractTitleRowNames_notFilled) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\t', '\t', '\t'};
	// set names
	names[0]   = std::make_shared<TNamesStrings>();
	names[1]   = std::make_shared<TNamesStrings>();
	initialize();

	std::string line = "greatTitle\tfantasticTitle\tbla1\tbla2\tbla3";

	rowNamesObj->extractTitleRowNames(line, true);

	// should set title
	EXPECT_THAT(names[0]->getTitleVec(), ElementsAre("greatTitle"));
	EXPECT_THAT(names[1]->getTitleVec(), ElementsAre("fantasticTitle"));
}

TEST_F(TFileNamesTest, extractTitleRowNames_preFilled) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\t', '\t', '\t'};
	// set names, fill them
	names[0]   = std::make_shared<TNamesStrings>();
	names[0]->addName({"one"});
	names[0]->setTitle({"predefinedTitle1"});
	names[1] = std::make_shared<TNamesStrings>();
	names[1]->addName({"two"});
	names[1]->setTitle({"predefinedTitle2"});
	initialize();

	std::string line = "greatTitle\tfantasticTitle\tbla1\tbla2\tbla3";

	rowNamesObj->extractTitleRowNames(line, false);

	// should not overwrite title, but also not throw
	EXPECT_THAT(names[0]->getTitleVec(), ElementsAre("predefinedTitle1"));
	EXPECT_THAT(names[1]->getTitleVec(), ElementsAre("predefinedTitle2"));
}

TEST_F(TFileNamesTest, extractTitleRowNames_preFilled_thrw) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\t', '\t', '\t'};
	// set names, fill them
	names[0]   = std::make_shared<TNamesStrings>();
	names[0]->addName({"one"});
	names[0]->setTitle({"predefinedTitle1"});
	names[1] = std::make_shared<TNamesStrings>();
	names[1]->addName({"two"});
	names[1]->setTitle({"predefinedTitle2"});
	initialize();

	std::string line = "greatTitle\tfantasticTitle\tbla1\tbla2\tbla3";

	EXPECT_ANY_THROW(rowNamesObj->extractTitleRowNames(line, true));
}

TEST_F(TFileNamesTest, setLengthSubsequentDimensions_RowName_tooManyUnknowns) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\t', '\t'};
	initialize();

	std::vector<size_t> dimensions    = {0, 0, 0, 2, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, false, true, true};
	rowNamesObj->_setLengthSubsequentDimensions_RowName(0, 20, dimensions, dimensionsKnown);

	EXPECT_THAT(dimensions, ElementsAre(0, 0, 0, 2, 10));                       // same as before
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, false, false, true, true)); // same as before
}

TEST_F(TFileNamesTest, setLengthSubsequentDimensions_RowName_1Unknown_example1) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\t', '\t'};
	initialize();

	std::vector<size_t> dimensions    = {0, 0, 4, 2, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, true, true, true};
	rowNamesObj->_setLengthSubsequentDimensions_RowName(0, 20, dimensions, dimensionsKnown);

	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 2, 10));                     // could infer dim1
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, true, true, true, true)); // could infer dim1
}

TEST_F(TFileNamesTest, setLengthSubsequentDimensions_RowName_1Unknown_example2) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\t', '\t'};
	initialize();

	std::vector<size_t> dimensions    = {0, 5, 0, 2, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, true, false, true, true};
	rowNamesObj->_setLengthSubsequentDimensions_RowName(0, 20, dimensions, dimensionsKnown);

	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 2, 10));                     // could infer dim2
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, true, true, true, true)); // could infer dim2
}

TEST_F(TFileNamesTest, setLengthSubsequentDimensions_RowName_1Unknown_example3) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\t', '\t'};
	initialize();

	std::vector<size_t> dimensions    = {0, 0, 0, 2, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, false, true, true};
	rowNamesObj->_setLengthSubsequentDimensions_RowName(1, 4, dimensions, dimensionsKnown);

	EXPECT_THAT(dimensions, ElementsAre(0, 0, 4, 2, 10));                      // could infer dim2
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, false, true, true, true)); // could infer dim2
}

TEST_F(TFileNamesTest, setLengthSubsequentDimensions_RowName_0Unknowns_ok) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\t', '\t'};
	initialize();

	std::vector<size_t> dimensions    = {0, 5, 4, 2, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, true, true, true, true};
	rowNamesObj->_setLengthSubsequentDimensions_RowName(0, 20, dimensions, dimensionsKnown);

	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 2, 10));                     // same as before, matched!
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, true, true, true, true)); // same as before, matched!
}

TEST_F(TFileNamesTest, setLengthSubsequentDimensions_RowName_0Unknowns_throw) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\t', '\t'};
	initialize();

	std::vector<size_t> dimensions    = {0, 5, 4, 2, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, true, true, true, true};
	EXPECT_ANY_THROW(rowNamesObj->_setLengthSubsequentDimensions_RowName(0, 19, dimensions, dimensionsKnown));
}

TEST_F(TFileNamesTest, parseRowNames_InferDimensions_allWritten) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\t'};
	names[0]   = std::make_shared<TNamesIndicesAlphabetUpperCase>();
	names[1]   = std::make_shared<TNamesStrings>();
	names[2]   = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[3]   = std::make_shared<TNamesIndices>();
	initialize();

	// 4 rownames, all written
	std::vector<std::string> lines = {
	    "A\tone\ta\t1",   "A\tone\ta\t2",   "A\tone\ta\t3",   "A\tone\tb\t1",   "A\tone\tb\t2",   "A\tone\tb\t3",
	    "A\tone\tc\t1",   "A\tone\tc\t2",   "A\tone\tc\t3",   "A\tone\td\t1",   "A\tone\td\t2",   "A\tone\td\t3",
	    "A\ttwo\ta\t1",   "A\ttwo\ta\t2",   "A\ttwo\ta\t3",   "A\ttwo\tb\t1",   "A\ttwo\tb\t2",   "A\ttwo\tb\t3",
	    "A\ttwo\tc\t1",   "A\ttwo\tc\t2",   "A\ttwo\tc\t3",   "A\ttwo\td\t1",   "A\ttwo\td\t2",   "A\ttwo\td\t3",
	    "A\tthree\ta\t1", "A\tthree\ta\t2", "A\tthree\ta\t3", "A\tthree\tb\t1", "A\tthree\tb\t2", "A\tthree\tb\t3",
	    "A\tthree\tc\t1", "A\tthree\tc\t2", "A\tthree\tc\t3", "A\tthree\td\t1", "A\tthree\td\t2", "A\tthree\td\t3",
	    "A\tfour\ta\t1",  "A\tfour\ta\t2",  "A\tfour\ta\t3",  "A\tfour\tb\t1",  "A\tfour\tb\t2",  "A\tfour\tb\t3",
	    "A\tfour\tc\t1",  "A\tfour\tc\t2",  "A\tfour\tc\t3",  "A\tfour\td\t1",  "A\tfour\td\t2",  "A\tfour\td\t3",
	    "A\tfive\ta\t1",  "A\tfive\ta\t2",  "A\tfive\ta\t3",  "A\tfive\tb\t1",  "A\tfive\tb\t2",  "A\tfive\tb\t3",
	    "A\tfive\tc\t1",  "A\tfive\tc\t2",  "A\tfive\tc\t3",  "A\tfive\td\t1",  "A\tfive\td\t2",  "A\tfive\td\t3",
	    "B\tone\ta\t1"};

	std::vector<size_t> dimensions    = {0, 0, 0, 0, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, false, false, true};
	bool firstLine                    = true;
	for (auto &line : lines) {
		rowNamesObj->parseRowNames_InferDimensions(line, dimensions, dimensionsKnown, firstLine);
		firstLine = false;
	}

	// check dimensions
	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 3, 10));
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, true, true, true, true));

	// check rownames
	EXPECT_EQ(names[1]->size(), 5);
	EXPECT_EQ((*names[1])[0], "one");
	EXPECT_EQ((*names[1])[1], "two");
	EXPECT_EQ((*names[1])[2], "three");
	EXPECT_EQ((*names[1])[3], "four");
	EXPECT_EQ((*names[1])[4], "five");
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 0));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 1));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 2));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 3));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 4));
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 0), 0);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 1), 1);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 2), 2);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 3), 3);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 4), 4);

	EXPECT_EQ(names[2]->size(), 4);
	EXPECT_EQ((*names[2])[0], "a");
	EXPECT_EQ((*names[2])[1], "b");
	EXPECT_EQ((*names[2])[2], "c");
	EXPECT_EQ((*names[2])[3], "d");
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 0));
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 1));
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 2));
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 3));
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 3), 3);

	EXPECT_EQ(names[3]->size(), 3);
	EXPECT_EQ((*names[3])[0], "1");
	EXPECT_EQ((*names[3])[1], "2");
	EXPECT_EQ((*names[3])[2], "3");
	EXPECT_TRUE(rowNamesObj->elementIsStored(3, 0));
	EXPECT_TRUE(rowNamesObj->elementIsStored(3, 1));
	EXPECT_TRUE(rowNamesObj->elementIsStored(3, 2));
	EXPECT_EQ(rowNamesObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(3, 2), 2);
}

TEST_F(TFileNamesTest, parseRowNames_InferDimensions_NotAllAreWritten) {
	initializeToDefault(5);
	delimiters    = {'\n', '\n', '\n', '\n', '\t'};
	names[0]      = std::make_shared<TNamesIndicesAlphabetUpperCase>();
	names[1]      = std::make_shared<TNamesIndices>();
	names[2]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[3]      = std::make_shared<TNamesIndices>(3);
	nameIsWritten = {true, false, true, false, true};
	initialize();

	// 4 rownames, 1st and 3rd are written
	std::vector<std::string> lines = {
	    "A\ta", "A\ta", "A\ta", "A\tb", "A\tb", "A\tb", "A\tc", "A\tc", "A\tc", "A\td", "A\td", "A\td", "A\ta",
	    "A\ta", "A\ta", "A\tb", "A\tb", "A\tb", "A\tc", "A\tc", "A\tc", "A\td", "A\td", "A\td", "A\ta", "A\ta",
	    "A\ta", "A\tb", "A\tb", "A\tb", "A\tc", "A\tc", "A\tc", "A\td", "A\td", "A\td", "A\ta", "A\ta", "A\ta",
	    "A\tb", "A\tb", "A\tb", "A\tc", "A\tc", "A\tc", "A\td", "A\td", "A\td", "A\ta", "A\ta", "A\ta", "A\tb",
	    "A\tb", "A\tb", "A\tc", "A\tc", "A\tc", "A\td", "A\td", "A\td", "B\ta"};

	std::vector<size_t> dimensions = {
	    0, 0, 0, 3, 10}; // last rowName dimension is known; header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, false, true, true};
	bool firstLine                    = true;
	for (auto &line : lines) {
		rowNamesObj->parseRowNames_InferDimensions(line, dimensions, dimensionsKnown, firstLine);
		firstLine = false;
	}

	// check dimensions
	EXPECT_THAT(dimensions, ElementsAre(0, 5, 4, 3, 10));
	EXPECT_THAT(dimensionsKnown, ElementsAre(false, true, true, true, true));

	// check rownames
	EXPECT_EQ(names[1]->size(), 5);
	EXPECT_EQ((*names[1])[0], "1");
	EXPECT_EQ((*names[1])[1], "2");
	EXPECT_EQ((*names[1])[2], "3");
	EXPECT_EQ((*names[1])[3], "4");
	EXPECT_EQ((*names[1])[4], "5");
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 0));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 1));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 2));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 3));
	EXPECT_TRUE(rowNamesObj->elementIsStored(1, 4));
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 0), 0);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 1), 1);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 2), 2);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 3), 3);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(1, 4), 4);

	EXPECT_EQ(names[2]->size(), 4);
	EXPECT_EQ((*names[2])[0], "a");
	EXPECT_EQ((*names[2])[1], "b");
	EXPECT_EQ((*names[2])[2], "c");
	EXPECT_EQ((*names[2])[3], "d");
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 0));
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 1));
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 2));
	EXPECT_TRUE(rowNamesObj->elementIsStored(2, 3));
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(2, 3), 3);

	EXPECT_EQ(names[3]->size(), 3);
	EXPECT_EQ((*names[3])[0], "1");
	EXPECT_EQ((*names[3])[1], "2");
	EXPECT_EQ((*names[3])[2], "3");
	EXPECT_TRUE(rowNamesObj->elementIsStored(3, 0));
	EXPECT_TRUE(rowNamesObj->elementIsStored(3, 1));
	EXPECT_TRUE(rowNamesObj->elementIsStored(3, 2));
	EXPECT_EQ(rowNamesObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(rowNamesObj->indexToStoreElement(3, 2), 2);
}

TEST_F(TFileNamesTest, extractMostOuterRowName_NotPrefilled) {
	// write simple file
	writeFile("A\tone\ta\t1\nA\tone\ta\t2");

	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\t'};
	names[0]   = std::make_shared<TNamesIndicesAlphabetUpperCase>();
	names[1]   = std::make_shared<TNamesStrings>();
	names[2]   = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[3]   = std::make_shared<TNamesIndices>();
	initialize();

	std::string filename = "test.txt";
	std::ifstream in(filename.c_str());
	bool keep;
	EXPECT_TRUE(rowNamesObj->_extractMostOuterRowName(&in, "//", keep, true));

	EXPECT_TRUE(keep);
	EXPECT_EQ((*names[0])[0], "A");
	EXPECT_EQ(names[0]->size(), 1);

	// now read until next \n
	std::string waste;
	impl::readUntilDelimiter(&in, waste, '\n');

	// read next A -> should not be added to names, because there is already one
	EXPECT_TRUE(rowNamesObj->_extractMostOuterRowName(&in, "//", keep, true));

	EXPECT_TRUE(keep);
	EXPECT_EQ((*names[0])[0], "A");
	EXPECT_EQ(names[0]->size(), 1);

	remove("test.txt");
}

TEST_F(TFileNamesTest, extractMostOuterRowName_Prefilled) {
	// write simple file
	writeFile("A\tone\ta\t1\nZ\tone\ta\t2");

	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\t'};
	names[0]   = std::make_shared<TNamesIndicesAlphabetUpperCase>(10); // is prefilled
	names[1]   = std::make_shared<TNamesStrings>();
	names[2]   = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[3]   = std::make_shared<TNamesIndices>();
	initialize();

	std::string filename = "test.txt";
	std::ifstream in(filename.c_str());
	bool keep;
	EXPECT_TRUE(rowNamesObj->_extractMostOuterRowName(&in, "//", keep, true));

	EXPECT_TRUE(keep);
	EXPECT_EQ((*names[0])[0], "A");
	EXPECT_EQ(names[0]->size(), 10);

	// now read until next \n
	std::string waste;
	impl::readUntilDelimiter(&in, waste, '\n');

	// read next Z -> should not be kept!
	EXPECT_TRUE(rowNamesObj->_extractMostOuterRowName(&in, "//", keep, true));

	EXPECT_FALSE(keep);
	EXPECT_EQ(names[0]->size(), 10);

	remove("test.txt");
}

TEST_F(TFileNamesTest, _extractBlockRowNames_ok) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\t'};
	names[0]   = std::make_shared<TNamesIndicesAlphabetUpperCase>();
	names[1]   = std::make_shared<TNamesStrings>();
	names[2]   = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[3]   = std::make_shared<TNamesIndices>();
	initialize();

	// 4 rownames, all written
	std::vector<std::string> lines = {
	    "A\tone\ta\t1",   "A\tone\ta\t2",   "A\tone\ta\t3",   "A\tone\tb\t1",   "A\tone\tb\t2",   "A\tone\tb\t3",
	    "A\tone\tc\t1",   "A\tone\tc\t2",   "A\tone\tc\t3",   "A\tone\td\t1",   "A\tone\td\t2",   "A\tone\td\t3",
	    "A\ttwo\ta\t1",   "A\ttwo\ta\t2",   "A\ttwo\ta\t3",   "A\ttwo\tb\t1",   "A\ttwo\tb\t2",   "A\ttwo\tb\t3",
	    "A\ttwo\tc\t1",   "A\ttwo\tc\t2",   "A\ttwo\tc\t3",   "A\ttwo\td\t1",   "A\ttwo\td\t2",   "A\ttwo\td\t3",
	    "A\tthree\ta\t1", "A\tthree\ta\t2", "A\tthree\ta\t3", "A\tthree\tb\t1", "A\tthree\tb\t2", "A\tthree\tb\t3",
	    "A\tthree\tc\t1", "A\tthree\tc\t2", "A\tthree\tc\t3", "A\tthree\td\t1", "A\tthree\td\t2", "A\tthree\td\t3",
	    "A\tfour\ta\t1",  "A\tfour\ta\t2",  "A\tfour\ta\t3",  "A\tfour\tb\t1",  "A\tfour\tb\t2",  "A\tfour\tb\t3",
	    "A\tfour\tc\t1",  "A\tfour\tc\t2",  "A\tfour\tc\t3",  "A\tfour\td\t1",  "A\tfour\td\t2",  "A\tfour\td\t3",
	    "A\tfive\ta\t1",  "A\tfive\ta\t2",  "A\tfive\ta\t3",  "A\tfive\tb\t1",  "A\tfive\tb\t2",  "A\tfive\tb\t3",
	    "A\tfive\tc\t1",  "A\tfive\tc\t2",  "A\tfive\tc\t3",  "A\tfive\td\t1",  "A\tfive\td\t2",  "A\tfive\td\t3",
	    "B\tone\ta\t1"};

	std::vector<size_t> dimensions    = {0, 0, 0, 0, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, false, false, true};
	bool firstLine                    = true;
	for (auto &line : lines) {
		rowNamesObj->parseRowNames_InferDimensions(line, dimensions, dimensionsKnown, firstLine);
		firstLine = false;
	}

	// write file with many rownames
	writeFile(
	    "A\tone\ta\t1\t5.5\nA\tone\ta\t2\t5.5\nA\tone\ta\t3\t5.5\nA\tone\tb\t1\t5.5\nA\tone\tb\t2\t5."
	    "5\nA\tone\tb\t3\t5.5\nA\tone\tc\t1\t5.5\nA\tone\tc\t2\t5.5\nA\tone\tc\t3\t5.5\nA\tone\td\t1\t5."
	    "5\nA\tone\td\t2\t5.5\nA\tone\td\t3\t5.5\nA\ttwo\ta\t1\t5.5\nA\ttwo\ta\t2\t5.5\nA\ttwo\ta\t3\t5."
	    "5\nA\ttwo\tb\t1\t5.5\nA\ttwo\tb\t2\t5.5\nA\ttwo\tb\t3\t5.5\nA\ttwo\tc\t1\t5.5\nA\ttwo\tc\t2\t5."
	    "5\nA\ttwo\tc\t3\t5.5\nA\ttwo\td\t1\t5.5\nA\ttwo\td\t2\t5.5\nA\ttwo\td\t3\t5.5\nA\tthree\ta\t1\t5."
	    "5\nA\tthree\ta\t2\t5.5\nA\tthree\ta\t3\t5.5\nA\tthree\tb\t1\t5.5\nA\tthree\tb\t2\t5.5\nA\tthree\tb\t3\t5."
	    "5\nA\tthree\tc\t1\t5.5\nA\tthree\tc\t2\t5.5\nA\tthree\tc\t3\t5.5\nA\tthree\td\t1\t5.5\nA\tthree\td\t2\t5."
	    "5\nA\tthree\td\t3\t5.5\nA\tfour\ta\t1\t5.5\nA\tfour\ta\t2\t5.5\nA\tfour\ta\t3\t5.5\nA\tfour\tb\t1\t5."
	    "5\nA\tfour\tb\t2\t5.5\nA\tfour\tb\t3\t5.5\nA\tfour\tc\t1\t5.5\nA\tfour\tc\t2\t5.5\nA\tfour\tc\t3\t5."
	    "5\nA\tfour\td\t1\t5.5\nA\tfour\td\t2\t5.5\nA\tfour\td\t3\t5.5\nA\tfive\ta\t1\t5.5\nA\tfive\ta\t2\t5."
	    "5\nA\tfive\ta\t3\t5.5\nA\tfive\tb\t1\t5.5\nA\tfive\tb\t2\t5.5\nA\tfive\tb\t3\t5.5\nA\tfive\tc\t1\t5."
	    "5\nA\tfive\tc\t2\t5.5\nA\tfive\tc\t3\t5.5\nA\tfive\td\t1\t5.5\nA\tfive\td\t2\t5.5\nA\tfive\td\t3\t5."
	    "5\nB\tone\ta\t1\t5.5\nB\tone\ta\t2\t5.5\nB\tone\ta\t3\t5.5\nB\tone\tb\t1\t5.5\nB\tone\tb\t2\t5."
	    "5\nB\tone\tb\t3\t5.5\nB\tone\tc\t1\t5.5\nB\tone\tc\t2\t5.5\nB\tone\tc\t3\t5.5\nB\tone\td\t1\t5."
	    "5\nB\tone\td\t2\t5.5\nB\tone\td\t3\t5.5\nB\ttwo\ta\t1\t5.5\nB\ttwo\ta\t2\t5.5\nB\ttwo\ta\t3\t5."
	    "5\nB\ttwo\tb\t1\t5.5\nB\ttwo\tb\t2\t5.5\nB\ttwo\tb\t3\t5.5\nB\ttwo\tc\t1\t5.5\nB\ttwo\tc\t2\t5."
	    "5\nB\ttwo\tc\t3\t5.5\nB\ttwo\td\t1\t5.5\nB\ttwo\td\t2\t5.5\nB\ttwo\td\t3\t5.5\nB\tthree\ta\t1\t5."
	    "5\nB\tthree\ta\t2\t5.5\nB\tthree\ta\t3\t5.5\nB\tthree\tb\t1\t5.5\nB\tthree\tb\t2\t5.5\nB\tthree\tb\t3\t5."
	    "5\nB\tthree\tc\t1\t5.5\nB\tthree\tc\t2\t5.5\nB\tthree\tc\t3\t5.5\nB\tthree\td\t1\t5.5\nB\tthree\td\t2\t5."
	    "5\nB\tthree\td\t3\t5.5\nB\tfour\ta\t1\t5.5\nB\tfour\ta\t2\t5.5\nB\tfour\ta\t3\t5.5\nB\tfour\tb\t1\t5."
	    "5\nB\tfour\tb\t2\t5.5\nB\tfour\tb\t3\t5.5\nB\tfour\tc\t1\t5.5\nB\tfour\tc\t2\t5.5\nB\tfour\tc\t3\t5."
	    "5\nB\tfour\td\t1\t5.5\nB\tfour\td\t2\t5.5\nB\tfour\td\t3\t5.5\nB\tfive\ta\t1\t5.5\nB\tfive\ta\t2\t5."
	    "5\nB\tfive\ta\t3\t5.5\nB\tfive\tb\t1\t5.5\nB\tfive\tb\t2\t5.5\nB\tfive\tb\t3\t5.5\nB\tfive\tc\t1\t5."
	    "5\nB\tfive\tc\t2\t5.5\nB\tfive\tc\t3\t5.5\nB\tfive\td\t1\t5.5\nB\tfive\td\t2\t5.5\nB\tfive\td\t3\t5.5");

	// get coordinates
	std::vector<std::vector<size_t>> coordinates = TDataBlock::getCoordinatesOneBlock({5, 4, 3, 10});

	// read
	std::string filename = "test.txt";
	std::ifstream in(filename.c_str());

	size_t counter = 0;
	bool keep;
	while (rowNamesObj->extractRowNames(&in, "//", coordinates[counter], keep, true)) {
		// read "data"
		std::string waste;
		impl::readUntilDelimiter(&in, waste, '\n');

		counter += 10;
		EXPECT_TRUE(keep);

		if (counter == coordinates.size()) { counter = 0; }
	}

	remove("test.txt");
}

TEST_F(TFileNamesTest, _extractBlockRowNames_NotOk) {
	initializeToDefault(5);
	delimiters = {'\n', '\n', '\n', '\n', '\t'};
	names[0]   = std::make_shared<TNamesIndicesAlphabetUpperCase>();
	names[1]   = std::make_shared<TNamesStrings>();
	names[2]   = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[3]   = std::make_shared<TNamesIndices>();
	initialize();

	// 4 rownames, all written
	std::vector<std::string> lines = {
	    "A\tone\ta\t1",   "A\tone\ta\t2",   "A\tone\ta\t3",   "A\tone\tb\t1",   "A\tone\tb\t2",   "A\tone\tb\t3",
	    "A\tone\tc\t1",   "A\tone\tc\t2",   "A\tone\tc\t3",   "A\tone\td\t1",   "A\tone\td\t2",   "A\tone\td\t3",
	    "A\ttwo\ta\t1",   "A\ttwo\ta\t2",   "A\ttwo\ta\t3",   "A\ttwo\tb\t1",   "A\ttwo\tb\t2",   "A\ttwo\tb\t3",
	    "A\ttwo\tc\t1",   "A\ttwo\tc\t2",   "A\ttwo\tc\t3",   "A\ttwo\td\t1",   "A\ttwo\td\t2",   "A\ttwo\td\t3",
	    "A\tthree\ta\t1", "A\tthree\ta\t2", "A\tthree\ta\t3", "A\tthree\tb\t1", "A\tthree\tb\t2", "A\tthree\tb\t3",
	    "A\tthree\tc\t1", "A\tthree\tc\t2", "A\tthree\tc\t3", "A\tthree\td\t1", "A\tthree\td\t2", "A\tthree\td\t3",
	    "A\tfour\ta\t1",  "A\tfour\ta\t2",  "A\tfour\ta\t3",  "A\tfour\tb\t1",  "A\tfour\tb\t2",  "A\tfour\tb\t3",
	    "A\tfour\tc\t1",  "A\tfour\tc\t2",  "A\tfour\tc\t3",  "A\tfour\td\t1",  "A\tfour\td\t2",  "A\tfour\td\t3",
	    "A\tfive\ta\t1",  "A\tfive\ta\t2",  "A\tfive\ta\t3",  "A\tfive\tb\t1",  "A\tfive\tb\t2",  "A\tfive\tb\t3",
	    "A\tfive\tc\t1",  "A\tfive\tc\t2",  "A\tfive\tc\t3",  "A\tfive\td\t1",  "A\tfive\td\t2",  "A\tfive\td\t3",
	    "B\tone\ta\t1"};

	std::vector<size_t> dimensions    = {0, 0, 0, 0, 10}; // header dimensions can be known or not, should be ignored
	std::vector<bool> dimensionsKnown = {false, false, false, false, true};
	bool firstLine                    = true;
	for (auto &line : lines) {
		rowNamesObj->parseRowNames_InferDimensions(line, dimensions, dimensionsKnown, firstLine);
		firstLine = false;
	}

	// write file with many rownames
	writeFile(
	    "A\tone\ta\t1\t5.5\nA\tone\ta\t2\t5.5\nA\tone\ta\t3\t5.5\nA\tone\tb\t1\t5.5\nA\tone\tb\t2\t5."
	    "5\nA\tone\tb\t2\t5.5\nA\tone\tc\t1\t5.5\nA\tone\tc\t2\t5.5\nA\tone\tc\t3\t5.5\nA\tone\td\t1\t5."
	    "5\nA\tone\td\t2\t5.5\nA\tone\td\t3\t5.5\nA\ttwo\ta\t1\t5.5\nA\ttwo\ta\t2\t5.5\nA\ttwo\ta\t3\t5."
	    "5\nA\ttwo\tb\t1\t5.5\nA\ttwo\tb\t2\t5.5\nA\ttwo\tb\t3\t5.5\nA\ttwo\tc\t1\t5.5\nA\ttwo\tc\t2\t5."
	    "5\nA\ttwo\tc\t3\t5.5\nA\ttwo\td\t1\t5.5\nA\ttwo\td\t2\t5.5\nA\ttwo\td\t3\t5.5\nA\tthree\ta\t1\t5."
	    "5\nA\tthree\ta\t2\t5.5\nA\tthree\ta\t3\t5.5\nA\tthree\tb\t1\t5.5\nA\tthree\tb\t2\t5.5\nA\tthree\tb\t3\t5."
	    "5\nA\tthree\tc\t1\t5.5\nA\tthree\tc\t2\t5.5\nA\tthree\tc\t3\t5.5\nA\tthree\td\t1\t5.5\nA\tthree\td\t2\t5."
	    "5\nA\tthree\td\t3\t5.5\nA\tfour\ta\t1\t5.5\nA\tfour\ta\t2\t5.5\nA\tfour\ta\t3\t5.5\nA\tfour\tb\t1\t5."
	    "5\nA\tfour\tb\t2\t5.5\nA\tfour\tb\t3\t5.5\nA\tfour\tc\t1\t5.5\nA\tfour\tc\t2\t5.5\nA\tfour\tc\t3\t5."
	    "5\nA\tfour\td\t1\t5.5\nA\tfour\td\t2\t5.5\nA\tfour\td\t3\t5.5\nA\tfive\ta\t1\t5.5\nA\tfive\ta\t2\t5."
	    "5\nA\tfive\ta\t3\t5.5\nA\tfive\tb\t1\t5.5\nA\tfive\tb\t2\t5.5\nA\tfive\tb\t3\t5.5\nA\tfive\tc\t1\t5."
	    "5\nA\tfive\tc\t2\t5.5\nA\tfive\tc\t3\t5.5\nA\tfive\td\t1\t5.5\nA\tfive\td\t2\t5.5\nA\tfive\td\t3\t5."
	    "5\nB\tone\ta\t1\t5.5\nB\tone\ta\t2\t5.5\nB\tone\ta\t3\t5.5\nB\tone\tb\t1\t5.5\nB\tone\tb\t2\t5."
	    "5\nB\tone\tb\t3\t5.5\nB\tone\tc\t1\t5.5\nB\tone\tc\t2\t5.5\nB\tone\tc\t3\t5.5\nB\tone\td\t1\t5."
	    "5\nB\tone\td\t2\t5.5\nB\tone\td\t3\t5.5\nB\ttwo\ta\t1\t5.5\nB\ttwo\ta\t2\t5.5\nB\ttwo\ta\t3\t5."
	    "5\nB\ttwo\tb\t1\t5.5\nB\ttwo\tb\t2\t5.5\nB\ttwo\tb\t3\t5.5\nB\ttwo\tc\t1\t5.5\nB\ttwo\tc\t2\t5."
	    "5\nB\ttwo\tc\t3\t5.5\nB\ttwo\td\t1\t5.5\nB\ttwo\td\t2\t5.5\nB\ttwo\td\t3\t5.5\nB\tthree\ta\t1\t5."
	    "5\nB\tthree\ta\t2\t5.5\nB\tthree\ta\t3\t5.5\nB\tthree\tb\t1\t5.5\nB\tthree\tb\t2\t5.5\nB\tthree\tb\t3\t5."
	    "5\nB\tthree\tc\t1\t5.5\nB\tthree\tc\t2\t5.5\nB\tthree\tc\t3\t5.5\nB\tthree\td\t1\t5.5\nB\tthree\td\t2\t5."
	    "5\nB\tthree\td\t3\t5.5\nB\tfour\ta\t1\t5.5\nB\tfour\ta\t2\t5.5\nB\tfour\ta\t3\t5.5\nB\tfour\tb\t1\t5."
	    "5\nB\tfour\tb\t2\t5.5\nB\tfour\tb\t3\t5.5\nB\tfour\tc\t1\t5.5\nB\tfour\tc\t2\t5.5\nB\tfour\tc\t3\t5."
	    "5\nB\tfour\td\t1\t5.5\nB\tfour\td\t2\t5.5\nB\tfour\td\t3\t5.5\nB\tfive\ta\t1\t5.5\nB\tfive\ta\t2\t5."
	    "5\nB\tfive\ta\t3\t5.5\nB\tfive\tb\t1\t5.5\nB\tfive\tb\t2\t5.5\nB\tfive\tb\t3\t5.5\nB\tfive\tc\t1\t5."
	    "5\nB\tfive\tc\t2\t5.5\nB\tfive\tc\t3\t5.5\nB\tfive\td\t1\t5.5\nB\tfive\td\t2\t5.5\nB\tfive\td\t3\t5.5");
	//                                                                                                                          | made mistake here: replaced 2 with 4

	// get coordinates
	std::vector<std::vector<size_t>> coordinates = TDataBlock::getCoordinatesOneBlock({5, 4, 3, 10});

	// read
	std::string filename = "test.txt";
	std::ifstream in(filename.c_str());

	size_t counter = 0;
	bool keep;
	EXPECT_ANY_THROW(while (rowNamesObj->extractRowNames(&in, "//", coordinates[counter], keep, true)) {
		// read "data"
		std::string waste;
		impl::readUntilDelimiter(&in, waste, '\n');

		counter += 10;
		EXPECT_TRUE(keep);

		if (counter == coordinates.size()) { counter = 0; }
	});

	remove("test.txt");
}

//--------------------
// TColNameMultiLine
//--------------------

TEST_F(TFileNamesTest, multiline_initialize_allAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters = {'\n', '\n', '\t', '\t', '/'};
	initialize();

	EXPECT_TRUE(colNamesMultiLineObj->nameIsWritten(2));
	EXPECT_TRUE(colNamesMultiLineObj->nameIsWritten(3));
	EXPECT_TRUE(colNamesMultiLineObj->nameIsWritten(4));

	EXPECT_TRUE(colNamesMultiLineObj->_hasHeader());

	EXPECT_EQ(colNamesMultiLineObj->numLinesHeader(), 3);
}

TEST_F(TFileNamesTest, multiline_initialize_someAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {false, true, false, true, false};
	initialize();

	EXPECT_FALSE(colNamesMultiLineObj->nameIsWritten(2));
	EXPECT_TRUE(colNamesMultiLineObj->nameIsWritten(3));
	EXPECT_FALSE(colNamesMultiLineObj->nameIsWritten(4));

	EXPECT_TRUE(colNamesMultiLineObj->_hasHeader());

	EXPECT_EQ(colNamesMultiLineObj->numLinesHeader(), 1);
}

TEST_F(TFileNamesTest, multiline_initialize_noneAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {false, true, false, false, false}; // rownames don't matter
	initialize();

	EXPECT_FALSE(colNamesMultiLineObj->nameIsWritten(2));
	EXPECT_FALSE(colNamesMultiLineObj->nameIsWritten(3));
	EXPECT_FALSE(colNamesMultiLineObj->nameIsWritten(4));

	EXPECT_FALSE(colNamesMultiLineObj->_hasHeader());

	EXPECT_EQ(colNamesMultiLineObj->numLinesHeader(), 0);
}

TEST_F(TFileNamesTest, multiline_parseHeader_allTabs_allWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '\t'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-"
	    "\tone\tone\tone\tone\tone\tone\tone\tone\tone\tone\tone\tone\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo"
	    "\ttwo\ttwo\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tfour\tfour\tfo"
	    "ur\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfive\tfive\tfive\tfive\tfive\tfive\tfive\tfive\tfive"
	    "\tfive\tfive\tfive",
	    "-\t-"
	    "\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\t"
	    "a\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td",
	    "-\t-"
	    "\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t"
	    "1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesMultiLineObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {"-\t-"
	              "\tone\tone\tone\tone\tone\tone\tone\tone\tone\tone\tone\tone\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo\ttwo"
	              "\ttwo\ttwo\ttwo\ttwo\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tthree\tt"
	              "hree\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfour\tfive\tfive\tfive\tfive"
	              "\tfive\tfive\tfive\tfive\tfive\tfive\tfive\tfive",
	              "-\t-"
	              "\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc"
	              "\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td",
	              "-\t-"
	              "\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3"
	              "\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3\t1\t2\t3"};
	EXPECT_NO_THROW(
	    colNamesMultiLineObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, multiline_parseHeader_allTabs_someWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '\t'};
	nameIsWritten = {true, true, false, true, false};
	names[2]      = std::make_shared<TNamesIndices>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-"
	    "\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\t"
	    "a\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td"};

	std::vector<size_t> dimensions     = {0, 0, 5, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, true, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesMultiLineObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 0));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, false));

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 3), 3);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {"-\t-"
	              "\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc"
	              "\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td\ta\ta\ta\tb\tb\tb\tc\tc\tc\td\td\td"};
	EXPECT_NO_THROW(
	    colNamesMultiLineObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, multiline_parseHeader_differentDelims_allWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '/', '_'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {"-\t-\tone\ttwo\tthree\tfour\tfive",
	                                       "-\t-\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d",
	                                       "-\t-\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/"
	                                       "1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesMultiLineObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {"-\t-\tone\ttwo\tthree\tfour\tfive", "-\t-\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d",
	              "-\t-\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/"
	              "1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3"};
	EXPECT_NO_THROW(
	    colNamesMultiLineObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, multiline_parseHeader_differentDelims_allWritten_shuffle) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '/', '_'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[2]->addName({"four"});
	names[2]->addName({"two"});
	names[2]->addName({"one"});
	names[2]->addName({"five"}); // shuffle and leave out "three"
	names[3] = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4] = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {"-\t-\tone\ttwo\tthree\tfour\tfive",
	                                       "-\t-\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d",
	                                       "-\t-\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/"
	                                       "1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesMultiLineObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "four");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "one");
	EXPECT_EQ((*names[2])[3], "five");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 1));
	EXPECT_FALSE(colNamesMultiLineObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 0), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 1), 1);
	EXPECT_ANY_THROW(colNamesMultiLineObj->indexToStoreElement(2, 2));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 3), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(2, 4), 3);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesMultiLineObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesMultiLineObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {"-\t-\tone\ttwo\tthree\tfour\tfive", "-\t-\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d\ta/b/c/d",
	              "-\t-\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3\t1_2_3/1_2_3/1_2_3/"
	              "1_2_3\t1_2_3/1_2_3/1_2_3/1_2_3"};
	EXPECT_NO_THROW(
	    colNamesMultiLineObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

//--------------------
// THeaderConcatenated
//--------------------

TEST_F(TFileNamesTest, concat_initialize_allAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters = {'\n', '\n', '\t', '\t', '/'};
	initialize();

	EXPECT_EQ(colNamesConcatenatedObj->_getDelimMostOuterColumn(delimiters), '\t');

	EXPECT_TRUE(colNamesConcatenatedObj->nameIsWritten(2));
	EXPECT_TRUE(colNamesConcatenatedObj->nameIsWritten(3));
	EXPECT_FALSE(colNamesConcatenatedObj->nameIsWritten(4));

	EXPECT_TRUE(colNamesConcatenatedObj->_hasHeader());

	EXPECT_EQ(colNamesConcatenatedObj->numLinesHeader(), 1);
}

TEST_F(TFileNamesTest, concat_initialize_someAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {false, true, false, true, false};
	initialize();

	EXPECT_EQ(colNamesConcatenatedObj->_getDelimMostOuterColumn(delimiters), '\t');

	EXPECT_FALSE(colNamesConcatenatedObj->nameIsWritten(2));
	EXPECT_TRUE(colNamesConcatenatedObj->nameIsWritten(3));
	EXPECT_FALSE(colNamesConcatenatedObj->nameIsWritten(4));

	EXPECT_TRUE(colNamesConcatenatedObj->_hasHeader());

	EXPECT_EQ(colNamesConcatenatedObj->numLinesHeader(), 1);
}

TEST_F(TFileNamesTest, concat_initialize_noneAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {false, true, false, false, true};
	initialize();

	EXPECT_FALSE(colNamesConcatenatedObj->nameIsWritten(2));
	EXPECT_FALSE(colNamesConcatenatedObj->nameIsWritten(3));
	EXPECT_FALSE(colNamesConcatenatedObj->nameIsWritten(4));

	EXPECT_FALSE(colNamesConcatenatedObj->_hasHeader());

	EXPECT_EQ(colNamesConcatenatedObj->numLinesHeader(), 0);
}

TEST_F(TFileNamesTest, concat_parseHeader_allTabs_allWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '\t'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_a_1\tone_a_2\tone_a_3\tone_b_1\tone_b_2\tone_b_3\tone_c_1\tone_c_2\tone_c_3\tone_d_1\tone_d_2\tone_"
	    "d_3\ttwo_a_1\ttwo_a_2\ttwo_a_3\ttwo_b_1\ttwo_b_2\ttwo_b_3\ttwo_c_1\ttwo_c_2\ttwo_c_3\ttwo_d_1\ttwo_d_2\ttwo_d_"
	    "3\tthree_a_1\tthree_a_2\tthree_a_3\tthree_b_1\tthree_b_2\tthree_b_3\tthree_c_1\tthree_c_2\tthree_c_3\tthree_d_"
	    "1\tthree_d_2\tthree_d_3\tfour_a_1\tfour_a_2\tfour_a_3\tfour_b_1\tfour_b_2\tfour_b_3\tfour_c_1\tfour_c_2\tfour_"
	    "c_3\tfour_d_1\tfour_d_2\tfour_d_3\tfive_a_1\tfive_a_2\tfive_a_3\tfive_b_1\tfive_b_2\tfive_b_3\tfive_c_1\tfive_"
	    "c_2\tfive_c_3\tfive_d_1\tfive_d_2\tfive_d_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesConcatenatedObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {
	    "-\t-\tone_a_1\tone_a_2\tone_a_3\tone_b_1\tone_b_2\tone_b_3\tone_c_1\tone_c_2\tone_c_3\tone_d_1\tone_d_2\tone_"
	    "d_3\ttwo_a_1\ttwo_a_2\ttwo_a_3\ttwo_b_1\ttwo_b_2\ttwo_b_3\ttwo_c_1\ttwo_c_2\ttwo_c_3\ttwo_d_1\ttwo_d_2\ttwo_d_"
	    "3\tthree_a_1\tthree_a_2\tthree_a_3\tthree_b_1\tthree_b_2\tthree_b_3\tthree_c_1\tthree_c_2\tthree_c_3\tthree_d_"
	    "1\tthree_d_2\tthree_d_3\tfour_a_1\tfour_a_2\tfour_a_3\tfour_b_1\tfour_b_2\tfour_b_3\tfour_c_1\tfour_c_2\tfour_"
	    "c_3\tfour_d_1\tfour_d_2\tfour_d_3\tfive_a_1\tfive_a_2\tfive_a_3\tfive_b_1\tfive_b_2\tfive_b_3\tfive_c_1\tfive_"
	    "c_2\tfive_c_3\tfive_d_1\tfive_d_2\tfive_d_3"};
	EXPECT_NO_THROW(
	    colNamesConcatenatedObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, concat_parseHeader_allTabs_mostOuterIsNotWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '\t'};
	nameIsWritten = {true, true, false, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_"
	    "3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_"
	    "1\tc_2\tc_3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_3\td_1\td_2\td_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesConcatenatedObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 0, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, false, true, true));

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {
	    "-\t-\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_"
	    "3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_"
	    "1\tc_2\tc_3\td_1\td_2\td_3\ta_1\ta_2\ta_3\tb_1\tb_2\tb_3\tc_1\tc_2\tc_3\td_1\td_2\td_3"};
	EXPECT_NO_THROW(
	    colNamesConcatenatedObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, concat_parseHeader_differentDelims_allWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_a\tone_b\tone_c\tone_d\ttwo_a\ttwo_b\ttwo_c\ttwo_d\tthree_a\tthree_b\tthree_c\tthree_d\tfour_"
	    "a\tfour_b\tfour_c\tfour_d\tfive_a\tfive_b\tfive_c\tfive_d"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesConcatenatedObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 0));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, false));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 3), 3);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {"-\t-\tone_a\tone_b\tone_c\tone_d\ttwo_a\ttwo_b\ttwo_c\ttwo_d\tthree_a\tthree_b\tthree_c\tthree_"
	              "d\tfour_a\tfour_b\tfour_c\tfour_d\tfive_a\tfive_b\tfive_c\tfive_d"};
	EXPECT_NO_THROW(
	    colNamesConcatenatedObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, concat_parseHeader_allTabs_allWritten_shuffle) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '\t'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[2]->addName({"four"});
	names[2]->addName({"two"});
	names[2]->addName({"one"});
	names[2]->addName({"five"}); // shuffle and leave out "three"
	names[3] = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4] = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_a_1\tone_a_2\tone_a_3\tone_b_1\tone_b_2\tone_b_3\tone_c_1\tone_c_2\tone_c_3\tone_d_1\tone_d_2\tone_"
	    "d_3\ttwo_a_1\ttwo_a_2\ttwo_a_3\ttwo_b_1\ttwo_b_2\ttwo_b_3\ttwo_c_1\ttwo_c_2\ttwo_c_3\ttwo_d_1\ttwo_d_2\ttwo_d_"
	    "3\tthree_a_1\tthree_a_2\tthree_a_3\tthree_b_1\tthree_b_2\tthree_b_3\tthree_c_1\tthree_c_2\tthree_c_3\tthree_d_"
	    "1\tthree_d_2\tthree_d_3\tfour_a_1\tfour_a_2\tfour_a_3\tfour_b_1\tfour_b_2\tfour_b_3\tfour_c_1\tfour_c_2\tfour_"
	    "c_3\tfour_d_1\tfour_d_2\tfour_d_3\tfive_a_1\tfive_a_2\tfive_a_3\tfive_b_1\tfive_b_2\tfive_b_3\tfive_c_1\tfive_"
	    "c_2\tfive_c_3\tfive_d_1\tfive_d_2\tfive_d_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesConcatenatedObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "four");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "one");
	EXPECT_EQ((*names[2])[3], "five");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 1));
	EXPECT_FALSE(colNamesConcatenatedObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 0), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 1), 1);
	EXPECT_ANY_THROW(colNamesConcatenatedObj->indexToStoreElement(2, 2));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 3), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(2, 4), 3);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesConcatenatedObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesConcatenatedObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {
	    "-\t-\tone_a_1\tone_a_2\tone_a_3\tone_b_1\tone_b_2\tone_b_3\tone_c_1\tone_c_2\tone_c_3\tone_d_1\tone_d_2\tone_"
	    "d_3\ttwo_a_1\ttwo_a_2\ttwo_a_3\ttwo_b_1\ttwo_b_2\ttwo_b_3\ttwo_c_1\ttwo_c_2\ttwo_c_3\ttwo_d_1\ttwo_d_2\ttwo_d_"
	    "3\tthree_a_1\tthree_a_2\tthree_a_3\tthree_b_1\tthree_b_2\tthree_b_3\tthree_c_1\tthree_c_2\tthree_c_3\tthree_d_"
	    "1\tthree_d_2\tthree_d_3\tfour_a_1\tfour_a_2\tfour_a_3\tfour_b_1\tfour_b_2\tfour_b_3\tfour_c_1\tfour_c_2\tfour_"
	    "c_3\tfour_d_1\tfour_d_2\tfour_d_3\tfive_a_1\tfive_a_2\tfive_a_3\tfive_b_1\tfive_b_2\tfive_b_3\tfive_c_1\tfive_"
	    "c_2\tfive_c_3\tfive_d_1\tfive_d_2\tfive_d_3"};
	EXPECT_NO_THROW(
	    colNamesConcatenatedObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

//--------------------
// TColNameFull
//--------------------

TEST_F(TFileNamesTest, full_initialize_allAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters = {'\n', '\n', '\t', '\t', '/'};
	initialize();

	EXPECT_EQ(colNamesFullObj->_getDelimMostOuterColumn(delimiters), '\t');

	EXPECT_TRUE(colNamesFullObj->nameIsWritten(2));
	EXPECT_TRUE(colNamesFullObj->nameIsWritten(3));
	EXPECT_TRUE(colNamesFullObj->nameIsWritten(4));

	EXPECT_TRUE(colNamesFullObj->_hasHeader());

	EXPECT_EQ(colNamesFullObj->numLinesHeader(), 1);
}

TEST_F(TFileNamesTest, full_initialize_someAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {false, true, false, true, true};
	initialize();

	EXPECT_EQ(colNamesFullObj->_getDelimMostOuterColumn(delimiters), '\t');

	EXPECT_FALSE(colNamesFullObj->nameIsWritten(2));
	EXPECT_TRUE(colNamesFullObj->nameIsWritten(3));
	EXPECT_TRUE(colNamesFullObj->nameIsWritten(4));

	EXPECT_TRUE(colNamesFullObj->_hasHeader());

	EXPECT_EQ(colNamesFullObj->numLinesHeader(), 1);
}

TEST_F(TFileNamesTest, full_initialize_noneAreWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '/'};
	nameIsWritten = {false, true, false, false, false};
	initialize();

	EXPECT_FALSE(colNamesFullObj->nameIsWritten(2));
	EXPECT_FALSE(colNamesFullObj->nameIsWritten(3));
	EXPECT_FALSE(colNamesFullObj->nameIsWritten(4));

	EXPECT_FALSE(colNamesFullObj->_hasHeader());

	EXPECT_EQ(colNamesFullObj->numLinesHeader(), 0);
}

TEST_F(TFileNamesTest, full_parseHeader_allTabs_allWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '\t', '\t'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_a_1\tone_a_2\tone_a_3\tone_b_1\tone_b_2\tone_b_3\tone_c_1\tone_c_2\tone_c_3\tone_d_1\tone_d_2\tone_"
	    "d_3\ttwo_a_1\ttwo_a_2\ttwo_a_3\ttwo_b_1\ttwo_b_2\ttwo_b_3\ttwo_c_1\ttwo_c_2\ttwo_c_3\ttwo_d_1\ttwo_d_2\ttwo_d_"
	    "3\tthree_a_1\tthree_a_2\tthree_a_3\tthree_b_1\tthree_b_2\tthree_b_3\tthree_c_1\tthree_c_2\tthree_c_3\tthree_d_"
	    "1\tthree_d_2\tthree_d_3\tfour_a_1\tfour_a_2\tfour_a_3\tfour_b_1\tfour_b_2\tfour_b_3\tfour_c_1\tfour_c_2\tfour_"
	    "c_3\tfour_d_1\tfour_d_2\tfour_d_3\tfive_a_1\tfive_a_2\tfive_a_3\tfive_b_1\tfive_b_2\tfive_b_3\tfive_c_1\tfive_"
	    "c_2\tfive_c_3\tfive_d_1\tfive_d_2\tfive_d_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesFullObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {
	    "-\t-\tone_a_1\tone_a_2\tone_a_3\tone_b_1\tone_b_2\tone_b_3\tone_c_1\tone_c_2\tone_c_3\tone_d_1\tone_d_2\tone_"
	    "d_3\ttwo_a_1\ttwo_a_2\ttwo_a_3\ttwo_b_1\ttwo_b_2\ttwo_b_3\ttwo_c_1\ttwo_c_2\ttwo_c_3\ttwo_d_1\ttwo_d_2\ttwo_d_"
	    "3\tthree_a_1\tthree_a_2\tthree_a_3\tthree_b_1\tthree_b_2\tthree_b_3\tthree_c_1\tthree_c_2\tthree_c_3\tthree_d_"
	    "1\tthree_d_2\tthree_d_3\tfour_a_1\tfour_a_2\tfour_a_3\tfour_b_1\tfour_b_2\tfour_b_3\tfour_c_1\tfour_c_2\tfour_"
	    "c_3\tfour_d_1\tfour_d_2\tfour_d_3\tfive_a_1\tfive_a_2\tfive_a_3\tfive_b_1\tfive_b_2\tfive_b_3\tfive_c_1\tfive_"
	    "c_2\tfive_c_3\tfive_d_1\tfive_d_2\tfive_d_3"};
	EXPECT_NO_THROW(
	    colNamesFullObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, full_parseHeader_allDifferent_allWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', ':', '/'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_a_1/one_a_2/one_a_3:one_b_1/one_b_2/one_b_3:one_c_1/one_c_2/one_c_3:one_d_1/one_d_2/"
	    "one_d_3\ttwo_a_1/two_a_2/two_a_3:two_b_1/two_b_2/two_b_3:two_c_1/two_c_2/two_c_3:two_d_1/two_d_2/"
	    "two_d_3\tthree_a_1/three_a_2/three_a_3:three_b_1/three_b_2/three_b_3:three_c_1/three_c_2/three_c_3:three_d_1/"
	    "three_d_2/three_d_3\tfour_a_1/four_a_2/four_a_3:four_b_1/four_b_2/four_b_3:four_c_1/four_c_2/"
	    "four_c_3:four_d_1/four_d_2/four_d_3\tfive_a_1/five_a_2/five_a_3:five_b_1/five_b_2/five_b_3:five_c_1/five_c_2/"
	    "five_c_3:five_d_1/five_d_2/five_d_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesFullObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {"-\t-\tone_a_1/one_a_2/one_a_3:one_b_1/one_b_2/one_b_3:one_c_1/one_c_2/one_c_3:one_d_1/one_d_2/"
	              "one_d_3\ttwo_a_1/two_a_2/two_a_3:two_b_1/two_b_2/two_b_3:two_c_1/two_c_2/two_c_3:two_d_1/two_d_2/"
	              "two_d_3\tthree_a_1/three_a_2/three_a_3:three_b_1/three_b_2/three_b_3:three_c_1/three_c_2/"
	              "three_c_3:three_d_1/three_d_2/three_d_3\tfour_a_1/four_a_2/four_a_3:four_b_1/four_b_2/"
	              "four_b_3:four_c_1/four_c_2/four_c_3:four_d_1/four_d_2/four_d_3\tfive_a_1/five_a_2/five_a_3:five_b_1/"
	              "five_b_2/five_b_3:five_c_1/five_c_2/five_c_3:five_d_1/five_d_2/five_d_3"};
	EXPECT_NO_THROW(
	    colNamesFullObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, full_parseHeader_allDifferent_someWritten) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', ':', '/'};
	nameIsWritten = {true, true, true, false, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[3]      = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4]      = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_1/one_2/one_3:one_1/one_2/one_3:one_1/one_2/one_3:one_1/one_2/one_3\ttwo_1/two_2/two_3:two_1/two_2/"
	    "two_3:two_1/two_2/two_3:two_1/two_2/two_3\tthree_1/three_2/three_3:three_1/three_2/three_3:three_1/three_2/"
	    "three_3:three_1/three_2/three_3\tfour_1/four_2/four_3:four_1/four_2/four_3:four_1/four_2/four_3:four_1/four_2/"
	    "four_3\tfive_1/five_2/five_3:five_1/five_2/five_3:five_1/five_2/five_3:five_1/five_2/five_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesFullObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 0, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, false, true));

	EXPECT_EQ((*names[2])[0], "one");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "three");
	EXPECT_EQ((*names[2])[3], "four");
	EXPECT_EQ((*names[2])[4], "five");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 2), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 3), 3);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 4), 4);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {
	    "-\t-\tone_1/one_2/one_3:one_1/one_2/one_3:one_1/one_2/one_3:one_1/one_2/one_3\ttwo_1/two_2/two_3:two_1/two_2/"
	    "two_3:two_1/two_2/two_3:two_1/two_2/two_3\tthree_1/three_2/three_3:three_1/three_2/three_3:three_1/three_2/"
	    "three_3:three_1/three_2/three_3\tfour_1/four_2/four_3:four_1/four_2/four_3:four_1/four_2/four_3:four_1/four_2/"
	    "four_3\tfive_1/five_2/five_3:five_1/five_2/five_3:five_1/five_2/five_3:five_1/five_2/five_3"};
	EXPECT_NO_THROW(
	    colNamesFullObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

TEST_F(TFileNamesTest, full_parseHeader_Different_allWritten_shuffle) {
	initializeToDefault(5);
	// all colnames are written
	delimiters    = {'\n', '\n', '\t', '_', '_'};
	nameIsWritten = {true, true, true, true, true};
	names[2]      = std::make_shared<TNamesStrings>();
	names[2]->addName({"four"});
	names[2]->addName({"two"});
	names[2]->addName({"one"});
	names[2]->addName({"five"}); // shuffle and leave out "three"
	names[3] = std::make_shared<TNamesIndicesAlphabetLowerCase>();
	names[4] = std::make_shared<TNamesIndices>();
	initialize();

	std::vector<std::string> fullHeader = {
	    "-\t-\tone_a_1_one_a_2_one_a_3_one_b_1_one_b_2_one_b_3_one_c_1_one_c_2_one_c_3_one_d_1_one_d_2_one_d_3\ttwo_a_"
	    "1_two_a_2_two_a_3_two_b_1_two_b_2_two_b_3_two_c_1_two_c_2_two_c_3_two_d_1_two_d_2_two_d_3\tthree_a_1_three_a_"
	    "2_three_a_3_three_b_1_three_b_2_three_b_3_three_c_1_three_c_2_three_c_3_three_d_1_three_d_2_three_d_3\tfour_a_"
	    "1_four_a_2_four_a_3_four_b_1_four_b_2_four_b_3_four_c_1_four_c_2_four_c_3_four_d_1_four_d_2_four_d_3\tfive_a_"
	    "1_five_a_2_five_a_3_five_b_1_five_b_2_five_b_3_five_c_1_five_c_2_five_c_3_five_d_1_five_d_2_five_d_3"};

	std::vector<size_t> dimensions     = {0, 0, 0, 0, 0};
	std::vector<bool> dimensionsFilled = {false, false, false, false, false};
	std::unique_ptr<TRowNames> rowNames =
	    std::make_unique<TRowNames>(numDim, nameIsWritten, names, delimiters, fileName);

	colNamesFullObj->parseHeader(fullHeader, dimensions, dimensionsFilled, delimiters, rowNames, true);

	// check if ok
	EXPECT_THAT(dimensions, ElementsAre(0, 0, 5, 4, 3));
	EXPECT_THAT(dimensionsFilled, ElementsAre(false, false, true, true, true));

	EXPECT_EQ((*names[2])[0], "four");
	EXPECT_EQ((*names[2])[1], "two");
	EXPECT_EQ((*names[2])[2], "one");
	EXPECT_EQ((*names[2])[3], "five");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 1));
	EXPECT_FALSE(colNamesFullObj->elementIsStored(2, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 3));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(2, 4));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 0), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 1), 1);
	EXPECT_ANY_THROW(colNamesFullObj->indexToStoreElement(2, 2));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 3), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(2, 4), 3);

	EXPECT_EQ((*names[3])[0], "a");
	EXPECT_EQ((*names[3])[1], "b");
	EXPECT_EQ((*names[3])[2], "c");
	EXPECT_EQ((*names[3])[3], "d");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 2));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(3, 3));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 2), 2);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(3, 3), 3);

	EXPECT_EQ((*names[4])[0], "1");
	EXPECT_EQ((*names[4])[1], "2");
	EXPECT_EQ((*names[4])[2], "3");
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 0));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 1));
	EXPECT_TRUE(colNamesFullObj->elementIsStored(4, 2));
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 0), 0);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 1), 1);
	EXPECT_EQ(colNamesFullObj->indexToStoreElement(4, 2), 2);

	// now pretend we've filled all the dimensions -> check for matching dimensions!
	dimensions = {0, 10, 5, 4, 3};
	fullHeader = {
	    "-\t-\tone_a_1_one_a_2_one_a_3_one_b_1_one_b_2_one_b_3_one_c_1_one_c_2_one_c_3_one_d_1_one_d_2_one_d_3\ttwo_a_"
	    "1_two_a_2_two_a_3_two_b_1_two_b_2_two_b_3_two_c_1_two_c_2_two_c_3_two_d_1_two_d_2_two_d_3\tthree_a_1_three_a_"
	    "2_three_a_3_three_b_1_three_b_2_three_b_3_three_c_1_three_c_2_three_c_3_three_d_1_three_d_2_three_d_3\tfour_a_"
	    "1_four_a_2_four_a_3_four_b_1_four_b_2_four_b_3_four_c_1_four_c_2_four_c_3_four_d_1_four_d_2_four_d_3\tfive_a_"
	    "1_five_a_2_five_a_3_five_b_1_five_b_2_five_b_3_five_c_1_five_c_2_five_c_3_five_d_1_five_d_2_five_d_3"};
	EXPECT_NO_THROW(
	    colNamesFullObj->checkIfHeaderMatchesInferredDimensions(fullHeader, dimensions, delimiters, rowNames));
}

#include "coretools/Types/TPseudoInt.h"

#include "gtest/gtest.h"
#include <cstdint>

using coretools::TPseudoInt;

TEST(TPseudoIntTests, Conversion) {
	EXPECT_EQ(TPseudoInt::min(), TPseudoInt::fromLinear(0));
	EXPECT_EQ(TPseudoInt::min(), TPseudoInt::fromPseudo(0));

	EXPECT_EQ(TPseudoInt::max(), TPseudoInt::fromLinear(9223372036854776063u));
	EXPECT_EQ(TPseudoInt::max(), TPseudoInt::fromPseudo(255));

	for (uint8_t lo = 0; lo <= TPseudoInt::maxLin; ++lo) {
		TPseudoInt li = TPseudoInt::fromLinear(lo);
		EXPECT_EQ(li.pseudo(), lo);
		EXPECT_EQ(li.linear(), lo);
	}

	for (int lo = TPseudoInt::maxLin + 1; lo < 256; ++lo) {
		TPseudoInt fPseudo = TPseudoInt::fromPseudo(lo);
		TPseudoInt fLinear = TPseudoInt::fromLinear(fPseudo.linear());
		TPseudoInt fLinMin = TPseudoInt::fromLinear(fPseudo.linear() - 1);

		EXPECT_EQ(fPseudo, fLinear);
		EXPECT_EQ(fPseudo, fLinMin);
		EXPECT_EQ(fPseudo.pseudo(), fLinear.pseudo());
		EXPECT_EQ(fPseudo.linear(), fLinear.linear());
	}
}

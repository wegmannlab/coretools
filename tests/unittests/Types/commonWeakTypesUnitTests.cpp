//
// Created by madleina on 20.11.23.
//

#include "gtest/gtest.h"

#include "coretools/Types/commonWeakTypes.h"

using namespace coretools;
using coretools::intervals::smallestPositiveRepresentableNumber;

TEST(IntervalTypes, AddableNoCheck) {
	// create type with two boundaries (0,1) and use AddableNoCheck
	using Type = WeakType<double, intervals::ZeroOneOpen, 0, skills::AddableNoCheck>;

	Type a(0.1);
	Type b(0.5);
	Type c(a);

	// operator +, no overflow
	c = a + b;
	EXPECT_EQ(c.get(), 0.6);

	// operator +, with overflow
	double result = 0.0;
	EXPECT_NO_THROW(result = b + Type(0.9)); // > 1, but it should not check -> no error
	EXPECT_EQ(result, 1.4);

	// operator +=, no overflow
	c += a;
	EXPECT_EQ(c.get(), 0.7);

	// operator +=, with overflow
	EXPECT_NO_THROW(c += b); // > 1, but it should not check -> no error
	EXPECT_EQ(c.get(), 1.2);

	// operator +: always ok
	double d = 1.0;
	EXPECT_NO_THROW(d = +b);
	EXPECT_EQ(d, 0.5);
}

TEST(IntervalTypes, AddableCheck) {
	// create type with two boundaries (0,1) and use AddableCheck
	using Type = WeakType<double, intervals::ZeroOneOpen, 0, skills::AddableCheck>;

	Type a(0.1);
	Type b(0.5);
	Type c(a);

	// operator +, no overflow
	c = a + b;
	EXPECT_EQ(c.get(), 0.6);

	// operator +, with overflow
	EXPECT_ANY_THROW(__attribute__((unused)) Type result = b + Type(0.9)); // > 1

	// operator +, with overflow, but does not throw since we assign it to a double
	double result = 0.0;
	EXPECT_NO_THROW(result = b + Type(0.9)); // > 1
	EXPECT_EQ(result, 1.4);

	// operator +=, no overflow
	c += a;
	EXPECT_EQ(c.get(), 0.7);

	// operator +=, with overflow
	EXPECT_ANY_THROW(c += b); // > 1

	// operator +: always ok
	double d = 1.0;
	EXPECT_NO_THROW(d = +b);
	EXPECT_EQ(d, 0.5);
}

TEST(IntervalTypes, SubtractableNoCheck) {
	// create type with two boundaries (0,1) and use SubtractableNoCheck
	using Type = WeakType<double, intervals::ZeroOneOpen, 0, skills::SubtractableNoCheck>;

	Type a(0.1);
	Type b(0.5);
	Type c(a);

	// operator -, no overflow
	c = b - a;
	EXPECT_EQ(c.get(), 0.4);

	// operator -, with overflow
	double result = 0.0;
	EXPECT_NO_THROW(result = b - Type(0.9)); // < 0, but it should not check -> no error
	EXPECT_EQ(result, -0.4);

	// operator -=, no overflow
	c -= a;
	EXPECT_FLOAT_EQ(c.get(), 0.3);

	// operator -=, with overflow
	EXPECT_NO_THROW(a -= b); // < 0, but it should not check -> no error
	EXPECT_EQ(a.get(), -0.4);

	// operator -
	double d;
	EXPECT_NO_THROW(d = -b); // < 0, but it should not check -> no error
	EXPECT_EQ(d, -0.5);
}

TEST(IntervalTypes, SubtractableCheck) {
	// create type with two boundaries (0,1) and use SubtractableCheck
	using Type = WeakType<double, intervals::ZeroOneOpen, 0, skills::SubtractableCheck>;

	Type a(0.1);
	Type b(0.5);
	Type c(a);

	// operator -, no overflow
	c = b - a;
	EXPECT_EQ(c.get(), 0.4);

	// operator -, with overflow
	EXPECT_ANY_THROW(__attribute__((unused)) Type result = b - Type(0.9)); // < 0

	// operator -, with overflow, but does not throw since we assign it to a double
	double result = 0.0;
	EXPECT_NO_THROW(result = b - Type(0.9)); // < 0
	EXPECT_EQ(result, -0.4);

	// operator -=, no overflow
	c -= a;
	EXPECT_FLOAT_EQ(c.get(), 0.3);

	// operator -=, with overflow
	EXPECT_ANY_THROW(a -= b); // < 0

	// operator -: decays to underlying type
	__attribute__((unused)) double d;
	EXPECT_NO_THROW(d = -b); // < 0
	EXPECT_EQ(d, -0.5);
}

TEST(IntervalTypes, MultiplicableNoCheck) {
	// create type with maximum [0, 10] and use MultiplicableNoCheck
	using Type = WeakType<double, intervals::PositiveMaxVariable, 0, skills::MultiplicableNoCheck>;
	Type::setMax(2.0);

	Type a(0.1);
	Type b(1.2);
	Type c(a);

	//  operator *, no overflow
	c = a * b;
	EXPECT_EQ(c.get(), 0.12);

	//  operator *, with overflow
	double result = 0.0;
	EXPECT_NO_THROW(result = b * Type(1.9)); // > 2, but it should not check -> no error
	EXPECT_EQ(result, 2.28);

	//  operator *=, no overflow
	c *= a;
	EXPECT_EQ(c.get(), 0.012);

	//  operator *=, with overflow
	c = 1.9;
	b = 1.9;
	EXPECT_NO_THROW(c *= b); // > 2, but it should not check -> no error
	EXPECT_EQ(c.get(), 3.61);
}

TEST(IntervalTypes, MultiplicableCheck) {
	// create type with maximum [0, 10] and use MultiplicableCheck
	using Type = WeakType<double, intervals::PositiveMaxVariable, 0, skills::MultiplicableCheck>;
	Type::setMax(2.0);

	Type a(0.1);
	Type b(1.2);
	Type c(a);

	//  operator *, no overflow
	c = a * b;
	EXPECT_EQ(c.get(), 0.12);

	// operator *, with overflow
	EXPECT_ANY_THROW(__attribute__((unused)) Type result = b * Type(1.9)); // > 2

	// operator *, with overflow, but does not throw since we assign it to a double
	double result = 0.0;
	EXPECT_NO_THROW(result = b * Type(1.9)); // > 2
	EXPECT_EQ(result, 2.28);

	//  operator *=, no overflow
	c *= a;
	EXPECT_EQ(c.get(), 0.012);

	//  operator *=, with overflow
	c = 1.9;
	b = 1.9;
	EXPECT_ANY_THROW(c *= b); // > 2
}

TEST(IntervalTypes, DivisibleNoCheck) {
	// create type with two boundaries (0,1) and use DivisibleNoCheck
	using Type = WeakType<double, intervals::ZeroOneOpen, 0, skills::DivisibleNoCheck>;

	Type a(0.1);
	Type b(0.5);
	Type c(a);

	//  operator /, no overflow
	c = a / b;
	EXPECT_EQ(c.get(), 0.2);

	//  operator /, with overflow
	double result = 0.0;
	EXPECT_NO_THROW(result = b / Type(0.1)); // > 1, but it should not check -> no error
	EXPECT_EQ(result, 5.0);

	//  operator /=, no overflow
	c /= b;
	EXPECT_FLOAT_EQ(c.get(), 0.4);

	//  operator /=, with overflow
	EXPECT_NO_THROW(b /= a); // > 1, but it should not check -> no error
	EXPECT_EQ(b.get(), 5.0);
}

TEST(IntervalTypes, DivisibleCheck) {
	// create type with two boundaries (0,1) and use DivisibleCheck
	using Type = WeakType<double, intervals::ZeroOneOpen, 0, skills::DivisibleCheck>;

	Type a(0.1);
	Type b(0.5);
	Type c(a);

	//  operator /, no overflow
	c = a / b;
	EXPECT_EQ(c.get(), 0.2);

	// operator /, with overflow
	EXPECT_ANY_THROW(__attribute__((unused)) Type result = b / Type(0.1)); // > 1

	// operator /, with overflow, but does not throw since we assign it to a double
	double result = 0.0;
	EXPECT_NO_THROW(result = b / Type(0.1)); // > 1
	EXPECT_EQ(result, 5.0);

	//  operator /=, no overflow
	c /= b;
	EXPECT_FLOAT_EQ(c.get(), 0.4);

	//  operator /=, with overflow
	EXPECT_ANY_THROW(b /= a); // > 1
}

//------------------------------------------------
// Common interval types
//------------------------------------------------

TEST(smallestPositiveNumberTest, smallestPositiveRepresentableNumber) {
	EXPECT_EQ(coretools::intervals::smallestPositiveRepresentableNumber<int>(), 1);
	EXPECT_EQ(smallestPositiveRepresentableNumber<bool>(), 1);
	EXPECT_EQ(smallestPositiveRepresentableNumber<uint8_t>(), 1);
	EXPECT_EQ(smallestPositiveRepresentableNumber<uint16_t>(), 1);
	EXPECT_EQ(smallestPositiveRepresentableNumber<uint32_t>(), 1);
	EXPECT_EQ(smallestPositiveRepresentableNumber<uint64_t>(), 1);

	EXPECT_EQ(smallestPositiveRepresentableNumber<double>(), std::numeric_limits<double>::min());
	EXPECT_EQ(smallestPositiveRepresentableNumber<float>(), std::numeric_limits<float>::min());
}

TEST(PositiveTest, Positive) {
	// initialize < 0
	EXPECT_ANY_THROW(Positive(-0.1));
	EXPECT_NO_THROW(Positive(0.));
	EXPECT_NO_THROW(Positive(10002.348321));

	// constructors
	Positive a(0.1);
	EXPECT_DOUBLE_EQ(a.get(), 0.1);
	Positive b(0.5);
	EXPECT_DOUBLE_EQ(b.get(), 0.5);

	Positive c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	Positive d("14.31");
	EXPECT_DOUBLE_EQ(d.get(), 14.31);

	// operator =
	Positive e = 0.;
	EXPECT_DOUBLE_EQ(e.get(), 0.);

	// compare among Positive
	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	// calculate with Positive
	c *= a;
	EXPECT_TRUE(c == a * a);
	c = a * a;
	EXPECT_TRUE(c == a * a);

	c /= a;
	EXPECT_FLOAT_EQ(c, a);
	c = a / b;
	EXPECT_EQ(c, 0.2);

	c = a + b;
	EXPECT_EQ(c.get(), 0.6);
	c += b;
	EXPECT_EQ(c.get(), 1.1);

	c = b - a;
	EXPECT_EQ(c.get(), 0.4);
	c -= a;
	EXPECT_DOUBLE_EQ(c.get(), 0.3);
	EXPECT_ANY_THROW(c -= b);

	// compare with underlying type (double)
	EXPECT_FALSE(a == 1.5);
	EXPECT_TRUE(a != 1.5);
	EXPECT_TRUE(a < 1.5);
	EXPECT_FALSE(a > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * a, 0.3 * 0.1);
	EXPECT_EQ(0.3 / a, 0.3 / 0.1);
	EXPECT_EQ(0.3 + a, 0.3 + 0.1);
	EXPECT_EQ(0.3 - a, 0.3 - 0.1);
	EXPECT_EQ(a * 0.3, 0.1 * 0.3);
	EXPECT_EQ(a / 0.3, 0.1 / 0.3);
	EXPECT_EQ(a + 0.3, 0.1 + 0.3);
	EXPECT_EQ(a - 0.3, 0.1 - 0.3);
}

TEST(StrictlyPositiveTest, StrictlyPositive) {
	// initialize < 0
	EXPECT_ANY_THROW(StrictlyPositive(-0.1));
	EXPECT_ANY_THROW(StrictlyPositive(0.));
	EXPECT_NO_THROW(StrictlyPositive(10002.348321));

	// constructors
	StrictlyPositive a(0.1);
	EXPECT_DOUBLE_EQ(a.get(), 0.1);
	StrictlyPositive b(0.5);
	EXPECT_DOUBLE_EQ(b.get(), 0.5);

	StrictlyPositive c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	StrictlyPositive d("14.31");
	EXPECT_DOUBLE_EQ(d.get(), 14.31);

	// operator =
	StrictlyPositive e = 0.0001;
	EXPECT_DOUBLE_EQ(e.get(), 0.0001);
	EXPECT_ANY_THROW(e = 0.);

	// compare among StrictlyPositive
	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	// calculate with StrictlyPositive
	c *= a;
	EXPECT_TRUE(c == a * a);
	c = a * a;
	EXPECT_TRUE(c == a * a);

	c /= a;
	EXPECT_FLOAT_EQ(c, a);
	c = a / b;
	EXPECT_EQ(c, 0.2);

	c = a + b;
	EXPECT_EQ(c.get(), 0.6);
	c += b;
	EXPECT_EQ(c.get(), 1.1);

	c = b - a;
	EXPECT_EQ(c.get(), 0.4);
	EXPECT_ANY_THROW(c = b - b);
	c = 0.4;
	c -= a;
	EXPECT_DOUBLE_EQ(c.get(), 0.3);
	c = StrictlyPositive(0.3);
	EXPECT_ANY_THROW(c -= StrictlyPositive(0.3));
	EXPECT_ANY_THROW(c -= b);

	// compare with underlying type (double)
	EXPECT_FALSE(a == 1.5);
	EXPECT_TRUE(a != 1.5);
	EXPECT_TRUE(a < 1.5);
	EXPECT_FALSE(a > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * a, 0.3 * 0.1);
	EXPECT_EQ(0.3 / a, 0.3 / 0.1);
	EXPECT_EQ(0.3 + a, 0.3 + 0.1);
	EXPECT_EQ(0.3 - a, 0.3 - 0.1);
	EXPECT_EQ(a * 0.3, 0.1 * 0.3);
	EXPECT_EQ(a / 0.3, 0.1 / 0.3);
	EXPECT_EQ(a + 0.3, 0.1 + 0.3);
	EXPECT_EQ(a - 0.3, 0.1 - 0.3);
}

TEST(ZeroOneOpenTest, ZeroOneOpen) {
	// initialize outside (0, 1)
	EXPECT_ANY_THROW(ZeroOneOpen(1.2));
	EXPECT_ANY_THROW(ZeroOneOpen(-0.1));
	EXPECT_ANY_THROW(ZeroOneOpen(0.));
	EXPECT_ANY_THROW(ZeroOneOpen(1.));
	EXPECT_NO_THROW(ZeroOneOpen(0.1));

	// constructors
	ZeroOneOpen a(0.1);
	EXPECT_DOUBLE_EQ(a.get(), 0.1);
	ZeroOneOpen b(0.5);
	EXPECT_DOUBLE_EQ(b.get(), 0.5);

	ZeroOneOpen c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	ZeroOneOpen d("0.99");
	EXPECT_DOUBLE_EQ(d.get(), 0.99);

	// operator =
	ZeroOneOpen e = 0.001;
	EXPECT_DOUBLE_EQ(e.get(), 0.001);
	EXPECT_ANY_THROW(e = 0.);
	EXPECT_ANY_THROW(e = 1.);

	// compare among ZeroOneOpen
	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	// calculate with ZeroOneOpen
	c *= a;
	EXPECT_TRUE(c == a * a);
	c = a * a;
	EXPECT_TRUE(c == a * a);

	c /= a;
	EXPECT_FLOAT_EQ(c, a);
	EXPECT_ANY_THROW(c /= c);
	c = a;
	EXPECT_ANY_THROW(b /= c);
	b = 0.5;
	c = a / b;
	EXPECT_EQ(c, 0.2);
	EXPECT_ANY_THROW(c /= a / a);

	c = a + b;
	EXPECT_EQ(c.get(), 0.6);
	EXPECT_ANY_THROW(c = a + ZeroOneOpen(0.9));
	c = 0.6;
	c += a;
	EXPECT_EQ(c.get(), 0.7);
	EXPECT_ANY_THROW(c += ZeroOneOpen(0.3));
	EXPECT_ANY_THROW(c += b);

	c = b - a;
	EXPECT_EQ(c.get(), 0.4);
	EXPECT_ANY_THROW(c = a - ZeroOneOpen(0.1));
	c = 0.4;
	c -= a;
	EXPECT_DOUBLE_EQ(c.get(), 0.3);
	c = ZeroOneOpen(0.3);
	EXPECT_ANY_THROW(c -= ZeroOneOpen(0.3));
	EXPECT_ANY_THROW(c -= b);

	// compare with underlying type (double)
	EXPECT_FALSE(a == 1.5);
	EXPECT_TRUE(a != 1.5);
	EXPECT_TRUE(a < 1.5);
	EXPECT_FALSE(a > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * a, 0.3 * 0.1);
	EXPECT_EQ(0.3 / a, 0.3 / 0.1);
	EXPECT_EQ(0.3 + a, 0.3 + 0.1);
	EXPECT_EQ(0.3 - a, 0.3 - 0.1);
	EXPECT_EQ(a * 0.3, 0.1 * 0.3);
	EXPECT_EQ(a / 0.3, 0.1 / 0.3);
	EXPECT_EQ(a + 0.3, 0.1 + 0.3);
	EXPECT_EQ(a - 0.3, 0.1 - 0.3);
};

TEST(UnsignedIntTest, UnsignedInt) {
	EXPECT_NO_THROW(UnsignedInt8(0));
	EXPECT_NO_THROW(UnsignedInt8(255));

	// constructors
	UnsignedInt8 a(2);
	EXPECT_DOUBLE_EQ(a.get(), 2);
	UnsignedInt8 b(5);
	EXPECT_DOUBLE_EQ(b.get(), 5);

	UnsignedInt8 c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	UnsignedInt8 d("14");
	EXPECT_DOUBLE_EQ(d.get(), 14);

	// operator =
	UnsignedInt8 e = 0;
	EXPECT_DOUBLE_EQ(e.get(), 0);

	// compare among UnsignedInt8
	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	// calculate with UnsignedInt8
	c *= a;
	EXPECT_TRUE(c == a * a);
	c = a * a;
	EXPECT_TRUE(c == a * a);
	EXPECT_ANY_THROW(c = UnsignedInt8(100) * b);
	EXPECT_ANY_THROW(c *= UnsignedInt8(100));

	c /= a;
	EXPECT_FLOAT_EQ(c, a);
	c = a / b; // note: integer division
	EXPECT_EQ(c, (uint8_t)0);

	c = a + b;
	EXPECT_EQ(c.get(), 7);
	c += b;
	EXPECT_EQ(c.get(), 12);
	c += UnsignedInt8(243);
	EXPECT_EQ(c.get(), 255);
	EXPECT_ANY_THROW(c = UnsignedInt8(100) + UnsignedInt8(156));
	EXPECT_ANY_THROW(c += UnsignedInt8(1));

	c = b - a;
	EXPECT_EQ(c.get(), 3);
	c -= a;
	EXPECT_EQ(c.get(), 1);
	c -= UnsignedInt8(1);
	EXPECT_EQ(c.get(), 0);
	EXPECT_ANY_THROW(c -= UnsignedInt8(1));
	EXPECT_ANY_THROW(c = a - b);

	// compare with underlying type (uint8_t)
	EXPECT_FALSE(a == (uint8_t)10);
	EXPECT_TRUE(a != (uint8_t)10);
	EXPECT_TRUE(a < (uint8_t)10);
	EXPECT_FALSE(a > (uint8_t)10);

	// calculate with underlying type
	EXPECT_EQ((uint8_t)10 * a, 10 * 2);
	EXPECT_EQ((uint8_t)10 / a, 10 / 2);
	EXPECT_EQ((uint8_t)10 + a, 10 + 2);
	EXPECT_EQ((uint8_t)10 - a, 10 - 2);
	EXPECT_EQ(a * (uint8_t)10, 2 * 10);
	EXPECT_EQ(a / (uint8_t)10, 2 / 10);
	EXPECT_EQ(a + (uint8_t)10, 2 + 10);
}

TEST(UnsignedIntWithMaxTest, UnsignedIntWithMax) {
	using UnsignedIntWithMax = WeakType<uint8_t, intervals::PositiveMaxVariable, 0, skills::AddableCheck,
										skills::SubtractableCheck, skills::MultiplicableCheck, skills::DivisibleCheck>;

	EXPECT_NO_THROW(UnsignedIntWithMax(0));
	EXPECT_NO_THROW(UnsignedIntWithMax(255));

	// constructors
	UnsignedIntWithMax a(2);
	UnsignedIntWithMax::setMax(10);
	EXPECT_DOUBLE_EQ(a.get(), 2);
	EXPECT_DOUBLE_EQ(a.min(), 0);
	EXPECT_DOUBLE_EQ(a.max(), 10);

	UnsignedIntWithMax b(5);
	UnsignedIntWithMax::setMax(20);
	EXPECT_DOUBLE_EQ(b.get(), 5);
	EXPECT_DOUBLE_EQ(a.max(), 20);
	EXPECT_DOUBLE_EQ(b.max(), 20);

	UnsignedIntWithMax c(a);
	UnsignedIntWithMax::setMax(10);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);
	EXPECT_DOUBLE_EQ(a.max(), 10);
	EXPECT_DOUBLE_EQ(b.max(), 10);
	EXPECT_DOUBLE_EQ(c.max(), 10);

	UnsignedIntWithMax d("6");
	EXPECT_DOUBLE_EQ(d.get(), 6);

	// operator =
	UnsignedIntWithMax e = 0;
	EXPECT_DOUBLE_EQ(e.get(), 0);

	// compare among UnsignedIntWithMax
	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	// calculate with UnsignedIntWithMax
	c *= a;
	EXPECT_TRUE(c == a * a);
	c = a * a;
	EXPECT_TRUE(c == a * a);
	EXPECT_ANY_THROW(c = UnsignedIntWithMax(6) * UnsignedIntWithMax(2));
	c = a * a;
	EXPECT_ANY_THROW(c *= UnsignedIntWithMax(6));
	c = a * a;

	c /= a;
	EXPECT_FLOAT_EQ(c, a);
	c = a / b; // note: integer division
	EXPECT_EQ(c, (uint8_t)0);

	c = a + b;
	EXPECT_EQ(c.get(), 7);
	c += a;
	EXPECT_EQ(c.get(), 9);
	c += UnsignedIntWithMax(1);
	EXPECT_EQ(c.get(), 10);
	EXPECT_ANY_THROW(c = UnsignedIntWithMax(10) + UnsignedIntWithMax(1));
	EXPECT_ANY_THROW(c += UnsignedIntWithMax(1));

	c = b - a;
	EXPECT_EQ(c.get(), 3);
	c -= a;
	EXPECT_EQ(c.get(), 1);
	c -= UnsignedIntWithMax(1);
	EXPECT_EQ(c.get(), 0);
	EXPECT_ANY_THROW(c -= UnsignedIntWithMax(1));
	EXPECT_ANY_THROW(c = a - b);

	// compare with underlying type (uint8_t)
	EXPECT_FALSE(a == (uint8_t)10);
	EXPECT_TRUE(a != (uint8_t)10);
	EXPECT_TRUE(a < (uint8_t)10);
	EXPECT_FALSE(a > (uint8_t)10);

	// calculate with underlying type
	EXPECT_EQ((uint8_t)10 * a, 10 * 2);
	EXPECT_EQ((uint8_t)10 / a, 10 / 2);
	EXPECT_EQ((uint8_t)10 + a, 10 + 2);
	EXPECT_EQ((uint8_t)10 - a, 10 - 2);
	EXPECT_EQ(a * (uint8_t)10, 2 * 10);
	EXPECT_EQ(a / (uint8_t)10, 2 / 10);
	EXPECT_EQ(a + (uint8_t)10, 2 + 10);
	// EXPECT_EQ(a - (uint8_t) 10,  2 - 10); // -> undefined, but that's users problem
}

TEST(DoubleTest, Double) {
	// initialize anything
	EXPECT_NO_THROW(Unbounded(-0.1));
	EXPECT_NO_THROW(Unbounded(0.));
	EXPECT_NO_THROW(Unbounded(10002.348321));

	// constructors
	Unbounded a(0.1);
	EXPECT_DOUBLE_EQ(a.get(), 0.1);
	Unbounded b(0.5);
	EXPECT_DOUBLE_EQ(b.get(), 0.5);

	Unbounded c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	Unbounded d("14.31");
	EXPECT_DOUBLE_EQ(d.get(), 14.31);

	// operator =
	Unbounded e = 0.;
	EXPECT_DOUBLE_EQ(e.get(), 0.);

	// compare among Unbounded
	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	// calculate with Unbounded
	c *= a;
	EXPECT_TRUE(c == a * a);
	c = a * a;
	EXPECT_TRUE(c == a * a);

	c /= a;
	EXPECT_FLOAT_EQ(c, a);
	c = a / b;
	EXPECT_EQ(c, 0.2);

	c = a + b;
	EXPECT_EQ(c.get(), 0.6);
	c += b;
	EXPECT_EQ(c.get(), 1.1);

	c = b - a;
	EXPECT_EQ(c.get(), 0.4);
	c -= a;
	EXPECT_DOUBLE_EQ(c.get(), 0.3);
	EXPECT_DOUBLE_EQ(c -= b, -0.2);

	// compare with underlying type (double)
	EXPECT_FALSE(a == 1.5);
	EXPECT_TRUE(a != 1.5);
	EXPECT_TRUE(a < 1.5);
	EXPECT_FALSE(a > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * a, 0.3 * 0.1);
	EXPECT_EQ(0.3 / a, 0.3 / 0.1);
	EXPECT_EQ(0.3 + a, 0.3 + 0.1);
	EXPECT_EQ(0.3 - a, 0.3 - 0.1);
	EXPECT_EQ(a * 0.3, 0.1 * 0.3);
	EXPECT_EQ(a / 0.3, 0.1 / 0.3);
	EXPECT_EQ(a + 0.3, 0.1 + 0.3);
	EXPECT_EQ(a - 0.3, 0.1 - 0.3);
}

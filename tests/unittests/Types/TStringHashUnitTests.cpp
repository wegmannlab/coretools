//
// Created by madleina on 21.11.23.
//

#include "gtest/gtest.h"

#include "coretools/Types/TStringHash.h"

using namespace coretools;

TEST(ToHashTest, EmptyString) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash(""), 0);
}

TEST(ToHashTest, SingleCharacterString) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("A"), 0xd3d99e8b);
}

TEST(ToHashTest, AllCharactersA) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("AAAAA"), 0x19f85109);
}

TEST(ToHashTest, DifferentCharacters) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("Hello, World!"), 0xec4ac3d0);
}

TEST(ToHashTest, SpecialCharacters) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("!@#$%^&*()"), 0xaea29b98);
}

TEST(ToHashTest, StringWithSpaces) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("   "), 0x69e69b98);
}

TEST(ToHashTest, MixOfUpperAndLowerCaseCharacters) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("AbCdEfG"), 0x3c751a16);
}

TEST(ToHashTest, StringWithNumbers) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("1234567890"), 0x261daee5);
}

TEST(ToHashTest, LongString) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	EXPECT_EQ(toHash("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"), 0xe341a90c);
}

TEST(ToHashTest, Equal) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	auto s_1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
	auto s_2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

	auto hash_1 = toHash(s_1);
	auto hash_2 = toHash(s_2);
	EXPECT_EQ(hash_1, hash_2);
}

TEST(ToHashTest, NotEqual_Prefix) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	auto s_1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
	auto s_2 = "bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

	auto hash_1 = toHash(s_1);
	auto hash_2 = toHash(s_2);
	EXPECT_NE(hash_1, hash_2);
}

TEST(ToHashTest, NotEqual_Middle) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	auto s_1 = "abcdefghijklmnopqrstuvwxyzABCDEFG.HIJKLMNOPQRSTUVWXYZ0123456789_";
	auto s_2 = "abcdefghijklmnopqrstuvwxyzABCDE:FGHIJKLMNOPQRSTUVWXYZ0123456789_";

	auto hash_1 = toHash(s_1);
	auto hash_2 = toHash(s_2);
	EXPECT_NE(hash_1, hash_2);
}

TEST(ToHashTest, NotEqual_Suffix) {
	// compared to online CRC32 calculator: https://emn178.github.io/online-tools/crc32.html
	auto s_1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
	auto s_2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:";

	auto hash_1 = toHash(s_1);
	auto hash_2 = toHash(s_2);
	EXPECT_NE(hash_1, hash_2);
}

#include "gtest/gtest.h"
#include <cstdint>
#include <type_traits>

#include "coretools/Types/TSomeProbability.h"
#include "coretools/Types/probability.h"

using namespace coretools;

static_assert(sizeof(Probability) == sizeof(double));
static_assert(sizeof(LogProbability) == sizeof(double));
static_assert(sizeof(Log10Probability) == sizeof(double));
static_assert(sizeof(PhredInt) == sizeof(uint8_t));
static_assert(sizeof(HPPhredInt) == sizeof(uint16_t));

TEST(TProbabilityTest, TProbability) {
	// initialize outside [0, 1]
	EXPECT_ANY_THROW(Probability(1.2));
	EXPECT_ANY_THROW(P(-0.1));
	EXPECT_NO_THROW(P(0.1));

	// constructors
	const Probability p01{0.1};
	const Probability p05 = P(0.5);
	Probability p         = p01;
	double d               = p05;

	EXPECT_DOUBLE_EQ(p01.get(), 0.1);
	EXPECT_DOUBLE_EQ(p05.get(), 0.5);
	EXPECT_FLOAT_EQ(p01.complement(), 0.9);
	EXPECT_FLOAT_EQ(p05.complement(), 0.5);
	EXPECT_FLOAT_EQ(p01.oddsRatio(), p01/p01.complement());
	EXPECT_FLOAT_EQ(p05.oddsRatio(), p05/(1. - p05));

	EXPECT_TRUE(p == p01);
	EXPECT_TRUE(p != p05);
	EXPECT_TRUE(p05 == d);
	EXPECT_TRUE(d == p05);
	EXPECT_TRUE(d == p05.get());

	EXPECT_DOUBLE_EQ(Probability("1.").get(), 1.);
	EXPECT_DOUBLE_EQ(P("0.5").get(), 0.5);

	// compare among probabilities
	EXPECT_FALSE(p01 == p05);
	EXPECT_TRUE(p01 != p05);
	EXPECT_TRUE(p01 < p05);
	EXPECT_FALSE(p01 > p05);

	// calculate with probabilities
	p = p05;
	p *= p01;
	EXPECT_TRUE(p == p05 * p01);

	p = p01 * p01;
	EXPECT_TRUE(p == p01 * p01);

	EXPECT_FLOAT_EQ(p / p01, p01.get());
	EXPECT_NO_THROW(std::ignore = p05 / p); // decays to double
	EXPECT_ANY_THROW(Probability(p05 / p));

	p = P(p01 / p05);
	EXPECT_EQ(p, 0.2);

	p = P(p01 + p05);
	EXPECT_EQ(p.get(), 0.6);
	EXPECT_NO_THROW(std::ignore = p + p); // decays to double
	EXPECT_ANY_THROW(Probability(p + p));

	p = P(p05 - p01);
	EXPECT_EQ(p.get(), 0.4);
	EXPECT_NO_THROW(std::ignore = p01 - p05);
	EXPECT_ANY_THROW(Probability(p01 - p05));

	// compare with underlying type (double)
	EXPECT_FALSE(p01 == 1.5);
	EXPECT_TRUE(p01 != 1.5);
	EXPECT_TRUE(p01 < 1.5);
	EXPECT_FALSE(p01 > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * p01, 0.3 * 0.1);
	EXPECT_EQ(0.3 / p01, 0.3 / 0.1);
	EXPECT_EQ(0.3 + p01, 0.3 + 0.1);
	EXPECT_EQ(0.3 - p01, 0.3 - 0.1);
	EXPECT_EQ(p01 * 0.3, 0.1 * 0.3);
	EXPECT_EQ(p01 / 0.3, 0.1 / 0.3);
	EXPECT_EQ(p01 + 0.3, 0.1 + 0.3);
	EXPECT_EQ(p01 - 0.3, 0.1 - 0.3);

	// range
	p = P(0.6);
	EXPECT_DOUBLE_EQ(p01.lowest().get(), 0.);
	EXPECT_DOUBLE_EQ(p01.highest().get(), 1.);
	EXPECT_TRUE(p01 < p05);
	EXPECT_FALSE(p01 > p05);

	// normalize
	p = P(0.6);
	p.scale(0.9);
	EXPECT_DOUBLE_EQ(p.get(), 0.6 / 0.9);
	EXPECT_ANY_THROW(p.scale(p01));

	EXPECT_DOUBLE_EQ(0.5 / 2, P(0.5).scale(2.).get());
	EXPECT_ANY_THROW(P(0.5).scale(0.1));
};

TEST(ProbabilityTest, TLogProbability) {
	// initialize > 0
	EXPECT_ANY_THROW(LogProbability(1.2));
	EXPECT_NO_THROW(logP(-0.1));
	EXPECT_NO_THROW(LogProbability(0.0));

	// constructors
	const LogProbability lp5{-5.0};
	const LogProbability lp4(-4.0);
	LogProbability lp = lp5;
	double d           = lp4;

	EXPECT_DOUBLE_EQ((double)lp5, -5.0);
	EXPECT_DOUBLE_EQ((double)lp4, -4.0);

	EXPECT_TRUE(lp == lp5);
	EXPECT_TRUE(lp != lp4);
	EXPECT_TRUE(d == lp4);
	EXPECT_TRUE(lp4 == d);
	EXPECT_TRUE(d == lp4.get());

	EXPECT_DOUBLE_EQ(LogProbability("0").get(), 0.);

	// compare among probabilities
	EXPECT_FALSE(lp5 == lp4);
	EXPECT_TRUE(lp5 != lp4);
	EXPECT_TRUE(lp5 < lp4);
	EXPECT_FALSE(lp5 > lp4);

	// calculate with probabilities
	lp = logP(-5.);
	lp += lp5;
	EXPECT_EQ(lp.get(), -10.);
	lp = lp5 + lp4;
	EXPECT_EQ(lp.get(), -9.);

	EXPECT_NO_THROW(std::ignore = LogProbability(-5.) - logP(-6.)); // decays to double
	EXPECT_ANY_THROW(LogProbability(LogProbability(-5.) - LogProbability(-6.)));
	lp = logP(lp5 - lp4);
	EXPECT_EQ(lp.get(), -1.);
	EXPECT_ANY_THROW(lp = logP(lp4 - lp5));

	// compare with underlying type (double)
	EXPECT_FALSE(lp5 == 1.5);
	EXPECT_TRUE(lp5 != 1.5);
	EXPECT_TRUE(lp5 < 1.5);
	EXPECT_FALSE(lp5 > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * lp5, 0.3 * -5.);
	EXPECT_EQ(0.3 / lp5, 0.3 / (-5.));
	EXPECT_EQ(0.3 + lp5, 0.3 + -5.);
	EXPECT_EQ(0.3 - lp5, 0.3 + 5.);
	EXPECT_EQ(lp5 * 0.3, -5. * 0.3);
	EXPECT_EQ(lp5 / 0.3, -5. / 0.3);
	EXPECT_EQ(lp5 + 0.3, -5. + 0.3);
	EXPECT_EQ(lp5 - 0.3, -5. - 0.3);

	// range
	lp = logP(-9.);
	EXPECT_DOUBLE_EQ(lp5.lowest().get(), std::numeric_limits<double>::lowest());
	EXPECT_DOUBLE_EQ(lp5.highest().get(), 0.);
	EXPECT_TRUE(lp5 <lp4);
	EXPECT_FALSE(lp5 > lp4);

	// normalize
	lp = logP(-1.);
	EXPECT_ANY_THROW(lp.scale(lp5));
	lp = lp5;
	lp.scale(-4.);
	EXPECT_DOUBLE_EQ(lp.get(), -1.0);
	EXPECT_DOUBLE_EQ(-10 - 5, LogProbability(-10.).scale(5).get());
	EXPECT_ANY_THROW(LogProbability(-10.).scale(-11));
};

TEST(TProbabilityTest, TLog10Probability) {
	// initialize > 0
	EXPECT_ANY_THROW(Log10Probability(1.2));
	EXPECT_NO_THROW(log10P(-0.1));
	EXPECT_NO_THROW(Log10Probability(0.0));

	const Log10Probability l10p5{-5.0};
	const Log10Probability l10p4(-4.0);
	Log10Probability l10p = l10p5;
	double d               = l10p4;

	EXPECT_DOUBLE_EQ((double)l10p5, -5.0);
	EXPECT_DOUBLE_EQ((double)l10p4, -4.0);

	EXPECT_TRUE(l10p == l10p5);
	EXPECT_TRUE(l10p != l10p4);
	EXPECT_TRUE(d == l10p4);
	EXPECT_TRUE(l10p4 == d);
	EXPECT_TRUE(l10p4.get() == d);

	EXPECT_DOUBLE_EQ(Log10Probability("0").get(), 0.);

	// compare among probabilities
	EXPECT_FALSE(l10p5 == l10p4);
	EXPECT_TRUE(l10p5 != l10p4);
	EXPECT_TRUE(l10p5 < l10p4);
	EXPECT_FALSE(l10p5 > l10p4);

	// calculate with probabilities
	l10p = log10P(-5.);
	l10p += l10p5;
	EXPECT_EQ(l10p.get(), -10.);
	l10p = l10p5 + l10p4;
	EXPECT_EQ(l10p.get(), -9.);

	EXPECT_NO_THROW(std::ignore = log10P(-5.) - log10P(-6.)); // decays to double
	EXPECT_ANY_THROW(Log10Probability(Log10Probability(-5.) - Log10Probability(-6.)));
	l10p = log10P(l10p5 - l10p4);
	EXPECT_EQ(l10p.get(), -1.);
	EXPECT_ANY_THROW(l10p = log10P(l10p4 - l10p5));

	// compare with underlying type (double)
	EXPECT_FALSE(l10p5 == 1.5);
	EXPECT_TRUE(l10p5 != 1.5);
	EXPECT_TRUE(l10p5 < 1.5);
	EXPECT_FALSE(l10p5 > 1.5);

	// calculate with underlying type
	EXPECT_EQ(0.3 * l10p5, 0.3 * -5.);
	EXPECT_EQ(0.3 / l10p5, 0.3 / (-5.));
	EXPECT_EQ(0.3 + l10p5, 0.3 + -5.);
	EXPECT_EQ(0.3 - l10p5, 0.3 + 5.);
	EXPECT_EQ(l10p5 * 0.3, -5. * 0.3);
	EXPECT_EQ(l10p5 / 0.3, -5. / 0.3);
	EXPECT_EQ(l10p5 + 0.3, -5. + 0.3);
	EXPECT_EQ(l10p5 - 0.3, -5. - 0.3);

	// range
	l10p = log10P(-9.);
	EXPECT_DOUBLE_EQ(l10p5.lowest().get(), std::numeric_limits<double>::lowest());
	EXPECT_DOUBLE_EQ(l10p5.highest().get(), 0.);
	EXPECT_TRUE(l10p5 < l10p4);
	EXPECT_FALSE(l10p5> l10p4);

	// normalize
	l10p = log10P(-1.);
	EXPECT_ANY_THROW(l10p.scale(l10p5));
	l10p = l10p5;
	l10p.scale(l10p4);
	EXPECT_DOUBLE_EQ(l10p.get(), -1);
	EXPECT_DOUBLE_EQ(-10 - 5, Log10Probability(-10.).scale(5).get());

	EXPECT_DOUBLE_EQ(-10 - 5, LogProbability(-10.).scale(5).get());
	EXPECT_ANY_THROW(Log10Probability(-10.).scale(-11));
};

TEST(TProbabilityTest, Conversions) {
	Probability p;
	LogProbability lp{p};
	Log10Probability l10p{p};
	PhredInt phr{p};
	HPPhredInt hpphr{p};

	EXPECT_FLOAT_EQ(p, 0.);
	EXPECT_FLOAT_EQ(lp, LogProbability::lowest());
	EXPECT_FLOAT_EQ(l10p, Log10Probability::lowest());
	EXPECT_EQ(phr, PhredInt::lowest());
	EXPECT_EQ(hpphr, HPPhredInt::lowest());

	p    = P(0.2);
	lp   = LogProbability(p);
	l10p = (Log10Probability)p;
	phr  = PhredInt(p);
	hpphr= HPPhredInt(p);

	EXPECT_FLOAT_EQ(p, 0.2);
	EXPECT_FLOAT_EQ(lp, std::log(0.2));
	EXPECT_FLOAT_EQ(std::log(p), std::log(0.2));
	EXPECT_FLOAT_EQ(l10p, std::log10(0.2));
	EXPECT_EQ(phr, 7);
	EXPECT_EQ(hpphr, 699);

	p    = P(0.3);
	lp   = logP(p);
	l10p = log10P(p);
	phr  = PhredInt(p);
	hpphr= HPPhredInt(p);

	EXPECT_FLOAT_EQ(p, 0.3);
	EXPECT_FLOAT_EQ(lp, std::log(0.3));
	EXPECT_FLOAT_EQ(l10p, std::log10(0.3));
	EXPECT_EQ(phr, 5);
	EXPECT_EQ(hpphr, 523);

	lp   = logP(-1);
	p    = (Probability)lp;
	l10p = (Log10Probability)lp;
	phr  = (PhredInt)lp;
	hpphr= (HPPhredInt)lp;

	EXPECT_FLOAT_EQ(lp, -1.);
	EXPECT_FLOAT_EQ(p, std::exp(-1));
	EXPECT_FLOAT_EQ(l10p, std::log10(std::exp(-1)));
	EXPECT_EQ(phr, 4);
	EXPECT_EQ(hpphr, 434);

	lp   = logP(-2);
	p    = P(lp);
	l10p = log10P(lp);
	phr  = PhredInt(lp);
	hpphr= HPPhredInt(lp);

	EXPECT_FLOAT_EQ(lp, -2.);
	EXPECT_FLOAT_EQ(p, std::exp(-2));
	EXPECT_FLOAT_EQ(l10p, std::log10(std::exp(-2)));
	EXPECT_EQ(phr, 9);
	EXPECT_EQ(hpphr, 869);

	l10p = log10P(-3);
	p    = (Probability)l10p;
	lp   = (LogProbability)l10p;
	phr  = (PhredInt)l10p;
	hpphr= (HPPhredInt)l10p;

	EXPECT_FLOAT_EQ(l10p, -3.);
	EXPECT_FLOAT_EQ(p, std::pow(10, -3));
	EXPECT_FLOAT_EQ(lp, std::log(std::pow(10, -3)));
	EXPECT_EQ(phr, 30);
	EXPECT_EQ(hpphr, 3000);

	l10p = log10P(-4.123);
	p    = P(l10p);
	lp   = logP(l10p);
	phr  = PhredInt(l10p);
	hpphr= HPPhredInt(l10p);

	EXPECT_FLOAT_EQ(l10p, -4.123);
	EXPECT_FLOAT_EQ(p, std::pow(10, -4.123));
	EXPECT_FLOAT_EQ(lp, std::log(std::pow(10, -4.123)));
	EXPECT_EQ(phr, 41);
	EXPECT_EQ(hpphr, 4123);

	phr  = PhredInt(11);
	p    = P(phr);
	lp   = LogProbability(phr);
	l10p = (Log10Probability)phr;
	hpphr= HPPhredInt(phr);

	EXPECT_EQ(phr, 11);
	EXPECT_FLOAT_EQ(p, std::pow(10, 11/-10.));
	EXPECT_FLOAT_EQ(lp, std::log(std::pow(10, 11/-10.)));
	EXPECT_FLOAT_EQ(l10p, 11/-10.);
	EXPECT_EQ(hpphr, 1100);


	hpphr= HPPhredInt(12345);
	p    = P(hpphr);
	lp   = LogProbability(hpphr);
	l10p = (Log10Probability)hpphr;
	phr  = PhredInt(hpphr);

	EXPECT_EQ(hpphr, 12345);
	EXPECT_FLOAT_EQ(p, std::pow(10, 12345/-1000.));
	EXPECT_FLOAT_EQ(lp, std::log(std::pow(10, 12345/-1000.)));
	EXPECT_FLOAT_EQ(l10p, 12345/-1000.);
	EXPECT_EQ(phr, 123);


	p *= p;
	/*p += lp;
	  p += l10p; must not compile!*/
	lp += lp;
	/*lp += p;
	  lp += l10p; must not compile!*/
	l10p += l10p;
	/*l10p += p;
	  l10p += lp; must not compile!*/
	phr += phr;
	hpphr += hpphr;

	static_assert(std::is_same_v<Probability, decltype(p*p)>);
	static_assert(std::is_same_v<double, decltype(lp*lp)>);
	// static_assert(std::is_same_v<double, decltype(p*lp)>); does not compile
	static_assert(std::is_same_v<double, decltype(l10p*l10p)>);
	static_assert(std::is_same_v<uint8_t, decltype(phr*phr)>);
	static_assert(std::is_same_v<uint16_t, decltype(hpphr*hpphr)>);

	static_assert(std::is_same_v<double, decltype(p+p)>);
	static_assert(std::is_same_v<LogProbability, decltype(lp+lp)>);
	static_assert(std::is_same_v<Log10Probability, decltype(l10p+l10p)>);
	static_assert(std::is_same_v<PhredInt, decltype(phr+phr)>);
	static_assert(std::is_same_v<HPPhredInt, decltype(hpphr+hpphr)>);

	static_assert(std::is_same_v<double, decltype(p-p)>);
	static_assert(std::is_same_v<double, decltype(lp-lp)>);
	static_assert(std::is_same_v<double, decltype(l10p-l10p)>);
	static_assert(std::is_same_v<uint8_t, decltype(phr-phr)>);
	static_assert(std::is_same_v<uint16_t, decltype(hpphr-hpphr)>);

	static_assert(std::is_same_v<double, decltype(p/p)>);
	static_assert(std::is_same_v<double, decltype(lp/lp)>);
	static_assert(std::is_same_v<double, decltype(l10p/l10p)>);
	static_assert(std::is_same_v<uint8_t, decltype(phr/phr)>);
	static_assert(std::is_same_v<uint16_t, decltype(hpphr/hpphr)>);
};

TEST(TProbabilityTest, BaseQuality) {
	static_assert(toChar(PhredInt::highest()) == 33);
	static_assert(toChar(PhredInt::lowest()) == 126);

	constexpr auto min = toChar(PhredInt::highest());
	constexpr auto max = toChar(PhredInt::lowest());

	for (auto baseQuality = min; baseQuality <= max; ++baseQuality) {
		const PhredInt phred = fromChar(baseQuality);
		EXPECT_EQ(baseQuality, toChar(phred));

		const HPPhredInt hpphred(fromChar(baseQuality));
		EXPECT_EQ(baseQuality, toChar(PhredInt(hpphred)));

		const Probability p(fromChar(baseQuality));
		EXPECT_EQ(baseQuality, toChar(PhredInt(p)));

		const LogProbability lp(fromChar(baseQuality));
		EXPECT_EQ(baseQuality, toChar(PhredInt(lp)));

		const Log10Probability l10p(fromChar(baseQuality));
		EXPECT_EQ(baseQuality, toChar(PhredInt(l10p)));
	}
}

TEST(TProbabilityTest, PhredInt) {
	for (auto i = PhredInt::min(); i != PhredInt::max(); ++i) {
		const PhredInt phr(i);
		const HPPhredInt hpphr(phr);

		EXPECT_EQ(phr, PhredInt(hpphr));
		EXPECT_EQ(hpphr, HPPhredInt(phr));

		const Probability p(phr);
		EXPECT_EQ(phr, PhredInt(p));
		EXPECT_EQ(hpphr, HPPhredInt(p));

		const LogProbability lp(phr);
		EXPECT_EQ(phr, PhredInt(lp));
		EXPECT_EQ(hpphr, HPPhredInt(lp));

		const Log10Probability l10p(phr);
		EXPECT_EQ(phr, PhredInt(l10p));
		EXPECT_EQ(hpphr, HPPhredInt(l10p));
	}
}

TEST(TProbabilityTest, average) {
	const Probability p1{0.5};
	EXPECT_FLOAT_EQ(p1, average({p1}));
	EXPECT_FLOAT_EQ(p1, average({p1, p1}));
	EXPECT_FLOAT_EQ(p1, average({p1, p1, p1}));
	EXPECT_FLOAT_EQ(p1, average({p1, p1, p1, p1}));

	const Probability p2{1.};
	const Probability p3{0.};
	EXPECT_FLOAT_EQ(p1, average({p1, p2, p3}));

	const Probability p4{0.25};
	EXPECT_FLOAT_EQ(p4, average({p1, p3}));
}

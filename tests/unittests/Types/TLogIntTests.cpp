#include "coretools/Types/TLogInt.h"

#include "gtest/gtest.h"
#include <cstdint>

using coretools::TLogInt;

TEST(TLogIntTests, Conversion) {
	constexpr uint64_t l1 = 1;
	EXPECT_EQ(TLogInt::min(), TLogInt::fromLinear(1));
	EXPECT_EQ(TLogInt::min(), TLogInt::fromLog(0));
	EXPECT_EQ(TLogInt::max(), TLogInt::fromLinear(l1 << 63));
	EXPECT_EQ(TLogInt::max(), TLogInt::fromLog(63));

	TLogInt li = TLogInt::fromLinear(1);
	EXPECT_EQ(li.log(), 0);
	EXPECT_EQ(li.linear(), 1);

	li = TLogInt::fromLog(1);
	EXPECT_EQ(li.log(), 1);
	EXPECT_EQ(li.linear(), 2);

	li = TLogInt::fromLinear(3);
	EXPECT_EQ(li.log(), 2);
	EXPECT_EQ(li.linear(), 4); // 3 becomes 4!

	for (uint8_t lo = 3; lo < 64; ++lo) {
		const uint64_t lin = l1 << lo;
		const auto fLog    = TLogInt::fromLog(lo);
		const auto fLin    = TLogInt::fromLinear(lin);
		const auto fLinMin = TLogInt::fromLinear(fLog.linear() - 1);

		EXPECT_EQ(fLog.log(), lo);
		EXPECT_EQ(fLog.linear(), l1 << lo);

		EXPECT_EQ(fLin.log(), lo);
		EXPECT_EQ(fLin.linear(), l1 << lo);

		EXPECT_EQ(fLinMin.log(), lo);
		EXPECT_EQ(fLinMin.linear(), l1 << lo);

		EXPECT_EQ(fLin, fLog);
		EXPECT_EQ(fLin, fLinMin);
		EXPECT_EQ(fLog.log(), fLin.log());
		EXPECT_EQ(fLog.linear(), fLinMin.linear());
	}
}

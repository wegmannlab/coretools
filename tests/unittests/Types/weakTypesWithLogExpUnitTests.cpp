//
// Created by madleina on 27.07.21.
//

#include "gtest/gtest.h"

#include "coretools/Types/old/weakTypesWithLogExp.h"
#include "coretools/Types/old/weakTypes.h"

using namespace testing;
using namespace coretools;

TEST(WeakTypeWithLogTest, WeakTypeWithLog){
    // initialize < 0
    EXPECT_ANY_THROW(WeakTypeWithLog<double>(-0.1));
    EXPECT_NO_THROW(WeakTypeWithLog<double>(0.));
    EXPECT_NO_THROW(WeakTypeWithLog<double>(10002.348321));

    // constructors
    WeakTypeWithLog<double> a(0.1);
    EXPECT_DOUBLE_EQ(a.get(), 0.1);
    EXPECT_DOUBLE_EQ(log(a), log(0.1));
    WeakTypeWithLog<double> b(0.5);
    EXPECT_DOUBLE_EQ(b.get(), 0.5);
    EXPECT_DOUBLE_EQ(log(b), log(0.5));

    WeakTypeWithLog<double> c(a);
    EXPECT_TRUE(c == a);
    EXPECT_TRUE(c != b);

    WeakTypeWithLog<double> d("14.31");
    EXPECT_DOUBLE_EQ(d.get(), 14.31);
    EXPECT_DOUBLE_EQ(log(d), log(14.31));

    // operator =
    WeakTypeWithLog<double> e = 0.;
    EXPECT_DOUBLE_EQ(e.get(), 0.);
    EXPECT_DOUBLE_EQ(log(e), log(0.));

    // compare among Positive
    EXPECT_FALSE(a == b);
    EXPECT_TRUE(a != b);
    EXPECT_TRUE(a < b);
    EXPECT_FALSE(a > b);

    // calculate with Positive
    c *= a;
    EXPECT_TRUE(c == a*a);
    EXPECT_DOUBLE_EQ(log(c), log(a*a));
    c = a*a;
    EXPECT_TRUE(c == a*a);
    EXPECT_DOUBLE_EQ(log(c), log(a*a));

    c /= a;
    EXPECT_FLOAT_EQ(c, a);
    EXPECT_DOUBLE_EQ(log(c), log(a));
    c = a/b;
    EXPECT_EQ(c, 0.2);
    EXPECT_DOUBLE_EQ(log(c), log(0.2));

    c = a+b;
    EXPECT_EQ(c.get(), 0.6);
    EXPECT_DOUBLE_EQ(log(c), log(0.6));
    c += b;
    EXPECT_EQ(c.get(), 1.1);
    EXPECT_DOUBLE_EQ(log(c), log(1.1));

    c = b-a;
    EXPECT_EQ(c.get(), 0.4);
    EXPECT_DOUBLE_EQ(log(c), log(0.4));
    c -= a;
    EXPECT_DOUBLE_EQ(c.get(), 0.3);
    EXPECT_DOUBLE_EQ(log(c), log(0.3));
    EXPECT_ANY_THROW(c -= b);

    // compare with underlying type (double)
    EXPECT_FALSE(a == 1.5);
    EXPECT_TRUE(a != 1.5);
    EXPECT_TRUE(a < 1.5);
    EXPECT_FALSE(a > 1.5);

    // calculate with underlying type
    EXPECT_EQ(0.3 * a, 0.3*0.1);
    EXPECT_EQ(0.3 / a, 0.3/0.1);
    EXPECT_EQ(0.3 + a, 0.3 + 0.1);
    EXPECT_EQ(0.3 - a, 0.3 - 0.1);
    EXPECT_EQ(a * 0.3, 0.1*0.3);
    EXPECT_EQ(a / 0.3, 0.1/0.3);
    EXPECT_EQ(a + 0.3, 0.1 + 0.3);
    EXPECT_EQ(a - 0.3, 0.1 - 0.3);
}

TEST(WeakTypeWithExpTest, WeakTypeWithExp){
    // initialize < 0
    EXPECT_NO_THROW(WeakTypeWithExp<double>(-0.1));
    EXPECT_NO_THROW(WeakTypeWithExp<double>(0.));
    EXPECT_NO_THROW(WeakTypeWithExp<double>(10002.348321));

    // constructors
    WeakTypeWithExp<double> a(0.1);
    EXPECT_DOUBLE_EQ(a.get(), 0.1);
    EXPECT_DOUBLE_EQ(exp(a), exp(0.1));
    WeakTypeWithExp<double> b(0.5);
    EXPECT_DOUBLE_EQ(b.get(), 0.5);
    EXPECT_DOUBLE_EQ(exp(b), exp(0.5));

    WeakTypeWithExp<double> c(a);
    EXPECT_TRUE(c == a);
    EXPECT_TRUE(c != b);

    WeakTypeWithExp<double> d("14.31");
    EXPECT_DOUBLE_EQ(d.get(), 14.31);
    EXPECT_DOUBLE_EQ(exp(d), exp(14.31));

    // operator =
    WeakTypeWithExp<double> e = 0.;
    EXPECT_DOUBLE_EQ(e.get(), 0.);
    EXPECT_DOUBLE_EQ(exp(e), exp(0.));

    e = -9.92;
    EXPECT_DOUBLE_EQ(e.get(), -9.92);
    EXPECT_DOUBLE_EQ(exp(e), exp(-9.92));

    // compare among WeakTypeWithExp<double>
    EXPECT_FALSE(a == b);
    EXPECT_TRUE(a != b);
    EXPECT_TRUE(a < b);
    EXPECT_FALSE(a > b);

    // calculate with WeakTypeWithExp<double>
    c *= a;
    EXPECT_TRUE(c == a*a);
    EXPECT_DOUBLE_EQ(exp(c), exp(a*a));
    c = a*a;
    EXPECT_TRUE(c == a*a);
    EXPECT_DOUBLE_EQ(exp(c), exp(a*a));

    c /= a;
    EXPECT_FLOAT_EQ(c, a);
    EXPECT_DOUBLE_EQ(exp(c), exp(a));
    c = a/b;
    EXPECT_EQ(c, 0.2);
    EXPECT_DOUBLE_EQ(exp(c), exp(0.2));

    c = a+b;
    EXPECT_EQ(c.get(), 0.6);
    EXPECT_DOUBLE_EQ(exp(c), exp(0.6));
    c += b;
    EXPECT_EQ(c.get(), 1.1);
    EXPECT_DOUBLE_EQ(exp(c), exp(1.1));

    c = b-a;
    EXPECT_EQ(c.get(), 0.4);
    EXPECT_DOUBLE_EQ(exp(c), exp(0.4));
    c -= a;
    EXPECT_DOUBLE_EQ(c.get(), 0.3);
    EXPECT_DOUBLE_EQ(exp(c), exp(0.3));
    c -= b;
    EXPECT_DOUBLE_EQ(c.get(), -0.2);
    EXPECT_DOUBLE_EQ(exp(c), exp(-0.2));

    // compare with underlying type (double)
    EXPECT_FALSE(a == 1.5);
    EXPECT_TRUE(a != 1.5);
    EXPECT_TRUE(a < 1.5);
    EXPECT_FALSE(a > 1.5);

    // calculate with underlying type
    EXPECT_EQ(0.3 * a, 0.3*0.1);
    EXPECT_EQ(0.3 / a, 0.3/0.1);
    EXPECT_EQ(0.3 + a, 0.3 + 0.1);
    EXPECT_EQ(0.3 - a, 0.3 - 0.1);
    EXPECT_EQ(a * 0.3, 0.1*0.3);
    EXPECT_EQ(a / 0.3, 0.1/0.3);
    EXPECT_EQ(a + 0.3, 0.1 + 0.3);
    EXPECT_EQ(a - 0.3, 0.1 - 0.3);
}

TEST(WeakTypeWithLogAndExpTest, WeakTypeWithLogAndExp){
    // initialize < 0
    EXPECT_ANY_THROW(WeakTypeWithLogAndExp<double>(-0.1));
    EXPECT_NO_THROW(WeakTypeWithLogAndExp<double>(0.));
    EXPECT_NO_THROW(WeakTypeWithLogAndExp<double>(10002.348321));

    // constructors
    WeakTypeWithLogAndExp<double> a(0.1);
    EXPECT_DOUBLE_EQ(a.get(), 0.1);
    EXPECT_DOUBLE_EQ(log(a), log(0.1));
    EXPECT_DOUBLE_EQ(exp(a), exp(0.1));
    WeakTypeWithLogAndExp<double> b(0.5);
    EXPECT_DOUBLE_EQ(b.get(), 0.5);
    EXPECT_DOUBLE_EQ(log(b), log(0.5));
    EXPECT_DOUBLE_EQ(exp(b), exp(0.5));

    WeakTypeWithLogAndExp<double> c(a);
    EXPECT_TRUE(c == a);
    EXPECT_TRUE(c != b);

    WeakTypeWithLogAndExp<double> d("14.31");
    EXPECT_DOUBLE_EQ(d.get(), 14.31);
    EXPECT_DOUBLE_EQ(log(d), log(14.31));
    EXPECT_DOUBLE_EQ(exp(d), exp(14.31));

    // operator =
    WeakTypeWithLogAndExp<double> e = 0.;
    EXPECT_DOUBLE_EQ(e.get(), 0.);
    EXPECT_DOUBLE_EQ(log(e), log(0.));
    EXPECT_DOUBLE_EQ(exp(e), exp(0.));

    // compare among WeakTypeWithLogAndExp<double>
    EXPECT_FALSE(a == b);
    EXPECT_TRUE(a != b);
    EXPECT_TRUE(a < b);
    EXPECT_FALSE(a > b);

    // calculate with WeakTypeWithLogAndExp<double>
    c *= a;
    EXPECT_TRUE(c == a*a);
    EXPECT_DOUBLE_EQ(log(c), log(a*a));
    EXPECT_DOUBLE_EQ(exp(c), exp(a*a));
    c = a*a;
    EXPECT_TRUE(c == a*a);
    EXPECT_DOUBLE_EQ(log(c), log(a*a));
    EXPECT_DOUBLE_EQ(exp(c), exp(a*a));

    c /= a;
    EXPECT_FLOAT_EQ(c, a);
    EXPECT_DOUBLE_EQ(log(c), log(a));
    EXPECT_DOUBLE_EQ(exp(c), exp(a));
    c = a/b;
    EXPECT_EQ(c, 0.2);
    EXPECT_DOUBLE_EQ(log(c), log(0.2));
    EXPECT_DOUBLE_EQ(exp(c), exp(0.2));

    c = a+b;
    EXPECT_EQ(c.get(), 0.6);
    EXPECT_DOUBLE_EQ(log(c), log(0.6));
    EXPECT_DOUBLE_EQ(exp(c), exp(0.6));
    c += b;
    EXPECT_EQ(c.get(), 1.1);
    EXPECT_DOUBLE_EQ(log(c), log(1.1));
    EXPECT_DOUBLE_EQ(exp(c), exp(1.1));

    c = b-a;
    EXPECT_EQ(c.get(), 0.4);
    EXPECT_DOUBLE_EQ(log(c), log(0.4));
    EXPECT_DOUBLE_EQ(exp(c), exp(0.4));
    c -= a;
    EXPECT_DOUBLE_EQ(c.get(), 0.3);
    EXPECT_DOUBLE_EQ(log(c), log(0.3));
    EXPECT_DOUBLE_EQ(exp(c), exp(0.3));
    EXPECT_ANY_THROW(c -= b);

    // compare with underlying type (double)
    EXPECT_FALSE(a == 1.5);
    EXPECT_TRUE(a != 1.5);
    EXPECT_TRUE(a < 1.5);
    EXPECT_FALSE(a > 1.5);

    // calculate with underlying type
    EXPECT_EQ(0.3 * a, 0.3*0.1);
    EXPECT_EQ(0.3 / a, 0.3/0.1);
    EXPECT_EQ(0.3 + a, 0.3 + 0.1);
    EXPECT_EQ(0.3 - a, 0.3 - 0.1);
    EXPECT_EQ(a * 0.3, 0.1*0.3);
    EXPECT_EQ(a / 0.3, 0.1/0.3);
    EXPECT_EQ(a + 0.3, 0.1 + 0.3);
    EXPECT_EQ(a - 0.3, 0.1 - 0.3);
}

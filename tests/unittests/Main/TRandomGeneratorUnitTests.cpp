#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Main/TRandomPicker.h"
#include "coretools/Math/TMeanVar.h"
#include "coretools/Types/probability.h"
#include "coretools/algorithms.h"

using namespace testing;
using namespace coretools;

//------------------------------------------------
// Random Generator
//------------------------------------------------

TEST(TRandomGeneratorTests, seed) {
	constexpr long seed1 = 0;
	TRandomGenerator ran(seed1, true);
	EXPECT_EQ(ran.getSeed(), seed1);

	constexpr long seed2 = 0;
	ran.setSeed(seed2, true);
	EXPECT_EQ(ran.getSeed(), seed2);

	constexpr long seed3 = 3;
	ran.setSeed(seed3);
	EXPECT_NE(ran.getSeed(), seed3);
}

TEST(TRandomGeneratorTests, KolgomorovSmirnovTest) {
	// see https://dl.acm.org/doi/pdf/10.1145/355719.355724
	TRandomGenerator RG;
	std::vector<double> ds;

	for (size_t N = 10000; N < 1000000; N *= 10) {
		const double dN = static_cast<double>(N);
		std::vector<double> rans;
		std::generate_n(std::back_inserter(rans), N, [&RG]() { return RG.getRand(); });
		std::sort(rans.begin(), rans.end());

		EXPECT_GE(rans.front(), 0.);
		EXPECT_LE(rans.back(), 1.);

		double dmax = -1e6;
		for (size_t i = 0; i < N; ++i) {
			const double di = static_cast<double>(i);
			dmax            = std::max(dmax, (di + 1.) / dN - rans[i]);
			dmax            = std::max(dmax, rans[i] - di / dN);
		}
		ds.push_back(dmax);

		/* Too unpredictable
		constexpr double alpha = 0.3;
		const double d_alpha   = std::sqrt(-0.5*std::log(alpha/2.)/dN);
		EXPECT_LT(dmax, d_alpha);
		*/
	}
	EXPECT_TRUE(std::is_sorted(ds.begin(), ds.end(), std::greater<double>{}));
}

TEST(TRandomGeneratorTests, getRand) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr double min = -13.7;
	constexpr double max = 1185.3;
	constexpr int imin   = -77;
	constexpr int imax   = 33456;
	constexpr long lmin  = 3;
	constexpr long lmax  = 654334;

	TRandomGenerator RG;
	for (size_t i = 0; i < N; ++i) {
		const auto ran_01 = RG.getRand();
		EXPECT_GE(ran_01, 0);
		EXPECT_LE(ran_01, 1);

		const auto ran_d = RG.getRand(min, max);
		EXPECT_GE(ran_d, min);
		EXPECT_LE(ran_d, max);

		const auto ran_i = RG.getRand(imin, imax);
		static_assert(std::is_same<decltype(ran_i), decltype(imin)>::value);
		EXPECT_GE(ran_i, imin);
		EXPECT_LE(ran_i, imax);

		const auto ran_l = RG.getRand(lmin, lmax);
		static_assert(std::is_same<decltype(ran_l), decltype(lmin)>::value);
		EXPECT_GE(ran_l, lmin);
		EXPECT_LE(ran_l, lmax);
	}
}

TEST(TRandomGeneratorTests, getRand_integer) {
	// Test  interval
	constexpr size_t N = static_cast<size_t>(1e4);
	constexpr int min  = 0;
	constexpr int max  = 1;

	TRandomGenerator RG;
	int min_value = 10;
	int max_value = -10;

	for (size_t i = 0; i < N; ++i) {
		const auto ri = RG.getRand(min, max + 1);
		min_value     = std::min(min_value, ri);
		max_value     = std::max(max_value, ri);
	}
	EXPECT_EQ(min_value, min);
	EXPECT_EQ(max_value, max);
}

TEST(TRandomGeneratorTests, sample) {
	constexpr size_t N = static_cast<size_t>(1e4);
	constexpr int max  = 5;

	TRandomGenerator RG;
	int min_value = 100;
	int max_value = -100;

	EXPECT_EQ(RG.sample(1), 0);
#ifdef _DEBUG
	EXPECT_DEATH(RG.sample(0));
#endif

	for (size_t i = 0; i < N; ++i) {
		const auto ri = RG.sample(max);
		min_value     = std::min(min_value, ri);
		max_value     = std::max(max_value, ri);
	}
	EXPECT_EQ(min_value, 0);
	EXPECT_EQ(max_value, max - 1);
}

TEST(TRandomGeneratorTests, sampleIndexOfMaxima_singleMaximum) {
	// max is first element
	TRandomGenerator RG;
	EXPECT_EQ((RG.sampleIndexOfMaxima<std::vector<size_t>, size_t, 10>({10, 5, 1, 7, 9, 4, 3, 6, 2, 8})), 0);

	// max is somewhere in the middle
	EXPECT_EQ((RG.sampleIndexOfMaxima<std::vector<size_t>, size_t, 10>({3, 2, 1, 10, 5, 8, 6, 7, 9, 4})), 3);

	// max is last element
	EXPECT_EQ((RG.sampleIndexOfMaxima<std::vector<size_t>, size_t, 10>({9, 6, 5, 1, 7, 8, 4, 3, 2, 10})), 9);
}

TEST(TRandomGeneratorTests, sampleIndexOfMaxima_multipleMaxima) {
	constexpr auto N = static_cast<size_t>(1e4);
	TRandomGenerator RG;

	// some numbers, where 24 and 29 are the maxima
	constexpr std::array<size_t, 30> array = {4, 6, 3, 5, 8, 5, 7, 8, 1, 5,  5, 8, 7, 9, 9,
	                                          6, 9, 1, 3, 3, 8, 9, 1, 4, 10, 1, 7, 9, 1, 10};
	constexpr std::array trueIndices       = {24, 29};

	std::array<size_t, 30> counters = {};
	for (size_t i = 0; i < N; ++i) {
		const auto r = RG.sampleIndexOfMaxima<decltype(array), size_t, 10>(array);
		counters[r]++;
	}
	EXPECT_EQ(counters[trueIndices[0]] + counters[trueIndices[1]], N);
	EXPECT_TRUE(counters[trueIndices[0]] > 0);
	EXPECT_TRUE(counters[trueIndices[1]] > 0);
}

TEST(TRandomGeneratorTests, alphanum) {
	constexpr size_t N = static_cast<size_t>(1e4);

	TRandomGenerator RG;
	char min_value = 'z';
	char max_value = '0';

	for (size_t i = 0; i < N; ++i) {
		const char ch = RG.getRandomAlphaNumericCharacter();
		min_value     = std::min(min_value, ch);
		max_value     = std::max(max_value, ch);
		EXPECT_TRUE(std::isalnum(ch));
		// EXPECT_EQ(ch, 0);
	}
	EXPECT_EQ(min_value, '0');
	EXPECT_EQ(max_value, 'z');
}

TEST(TRandomGeneratorTests, alphanumstring) {
	constexpr size_t N = static_cast<size_t>(3e3);

	TRandomGenerator RG;
	char min_value = 'z';
	char max_value = '0';

	const auto rs = RG.getRandomAlphaNumericString(N);
	EXPECT_EQ(rs.size(), N);
	for (const auto ch : rs) {
		min_value = std::min(min_value, ch);
		max_value = std::max(max_value, ch);
		EXPECT_TRUE(std::isalnum(ch));
	}
	EXPECT_EQ(min_value, '0');
	EXPECT_EQ(max_value, 'z');
}

TEST(TRandomGeneratorTests, shuffle) {
	constexpr size_t N = static_cast<size_t>(3e3);

	TRandomGenerator RG;
	std::vector<int> vec(N);
	std::iota(vec.begin(), vec.end(), -123);
	const auto min = vec.front();
	const auto max = vec.back();

	RG.shuffle(vec);
	EXPECT_NE(vec.front(), min);
	EXPECT_NE(vec.back(), max);
}

TEST(TRandomGeneratorTests, randomOrderOfIndexes) {
	TRandomGenerator RG;
	auto v = RG.randomOrderOfIndexes(10);
	EXPECT_EQ(v.size(), 10);
}

TEST(TRandomGeneratorTests, fillRand) {
	constexpr size_t N = static_cast<size_t>(3e3);

	TRandomGenerator RG;
	std::vector<double> vec(N);

	RG.fillRand(vec);
	EXPECT_TRUE(std::all_of(vec.cbegin(), vec.cend(), [](double vi) { return (0 <= vi) && (vi < 1); }));
	EXPECT_EQ(std::find(vec.cbegin(), vec.cend(), 1.), vec.cend());
}

TEST(TRandomGeneratorTests, normal) {
	constexpr size_t N    = static_cast<size_t>(1e4);
	constexpr double mean = -13.7;
	constexpr double std  = 33.5;
	constexpr double err  = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) { mVar.add(RG.getNormalRandom(mean, std)); }
	EXPECT_EQ(mVar.counts(), N);
	EXPECT_NEAR(mVar.mean(), mean, err * std::abs(mean));
	EXPECT_NEAR(mVar.sd(), std, err * std::abs(mean));
}

TEST(TRandomGeneratorTests, binomial) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr auto p     = P(0.33);
	constexpr double n   = 5;
	constexpr double err = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	unsigned int min_value = 2 * n;
	unsigned int max_value = 0;
	std::vector<unsigned int> cnt(n + 1);

	for (size_t i = 0; i < N; ++i) {
		const auto ri = RG.getBinomialRand(p, n);
		min_value     = std::min(min_value, ri);
		max_value     = std::max(max_value, ri);
		++cnt[ri];
		mVar.add(ri);
	}
	EXPECT_EQ(min_value, 0);
	EXPECT_EQ(max_value, n);
	EXPECT_LT(cnt[0], cnt[1]);
	EXPECT_NEAR(cnt[2], cnt[1], 0.1 * N);
	EXPECT_LT(cnt[3], cnt[2]);
	EXPECT_LT(cnt[4], cnt[3]);
	EXPECT_LT(cnt[5], cnt[4]);

	const auto mean = n * p;
	const auto var  = n * p * (1 - p);
	EXPECT_NEAR(mVar.mean(), mean, err * std::abs(mean));
	EXPECT_NEAR(mVar.sd(), var, err * std::abs(var));
}

TEST(TRandomGeneratorTests, negativeBinomial) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr auto p     = P(0.33);
	constexpr double n   = 5;
	constexpr double err = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) {
		const auto ri = RG.getNegativeBinomialRand(p, n);
		mVar.add(ri);
	}

	const auto mean = n * (1. - p) / p;
	const auto var  = n * (1. - p) / (p * p);
	EXPECT_NEAR(mVar.mean(), mean, err * std::abs(mean));
	EXPECT_NEAR(mVar.variance(), var, err * std::abs(var));
}

TEST(TRandomGeneratorTests, multinomial) {
	constexpr size_t N1 = static_cast<size_t>(1e1);
	constexpr size_t N2 = static_cast<size_t>(1e4);

	TRandomGenerator RG;
	std::vector<double> rans;
	std::vector<int> cnts;

	std::generate_n(std::back_inserter(rans), N1, [&RG]() { return RG.getRand(); });
	std::sort(rans.begin(), rans.end());
	rans.back() = 1;
	EXPECT_EQ(rans.size(), N1);

	RG.fillMultinomialRandom(N2, rans, cnts);
	EXPECT_EQ(cnts.size(), N1);
	EXPECT_EQ(std::accumulate(cnts.begin(), cnts.end(), 0), N2);
}

TEST(TRandomGeneratorTests, gamma) {
	constexpr size_t N     = static_cast<size_t>(1e4);
	constexpr double alpha = 7.4;
	constexpr double beta  = 3.8;
	constexpr double err   = 0.2;

	TMeanVar<double> mVar1;
	TMeanVar<double> mVar2;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) {
		mVar1.add(RG.getGammaRand(alpha));
		mVar2.add(RG.getGammaRand(alpha, beta));
	}
	EXPECT_EQ(mVar1.counts(), N);
	EXPECT_EQ(mVar2.counts(), N);
	EXPECT_NEAR(mVar1.mean(), alpha, err * std::abs(alpha));
	EXPECT_NEAR(mVar1.variance(), alpha, err * std::abs(alpha));
	EXPECT_NEAR(mVar2.mean(), alpha / beta, err * std::abs(alpha / beta));
	EXPECT_NEAR(mVar2.variance(), alpha / (beta * beta), err * std::abs(alpha / (beta * beta)));
}

TEST(TRandomGeneratorTests, chisq) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr double k   = 7;
	constexpr double err = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) { mVar.add(RG.getChisqRand(k)); }
	EXPECT_EQ(mVar.counts(), N);
	EXPECT_NEAR(mVar.mean(), k, err * std::abs(k));
	EXPECT_NEAR(mVar.variance(), 2 * k, err * std::abs(2 * k));
}

TEST(TRandomGeneratorTests, poisson) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr double l   = 703.1;
	constexpr double err = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) { mVar.add(RG.getPoissonRandom(l)); }
	EXPECT_EQ(mVar.counts(), N);
	EXPECT_NEAR(mVar.mean(), l, err * l);
	EXPECT_NEAR(mVar.variance(), l, l);
}

TEST(TRandomGeneratorTests, exponential) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr double l   = 47.5;
	constexpr double err = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) { mVar.add(RG.getExponentialRandom(l)); }
	EXPECT_EQ(mVar.counts(), N);
	EXPECT_NEAR(mVar.mean(), 1. / l, err / l);
	EXPECT_NEAR(mVar.variance(), 1. / l / l, err / l / l);
}

TEST(TRandomGeneratorTests, geometric) {
	constexpr size_t N   = static_cast<size_t>(1e4);
	constexpr auto p     = P(0.1238);
	constexpr double err = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) { mVar.add(RG.getGeometricRandom(p)); }
	const auto mean = 1. / p;
	const auto var  = (1 - p) / p / p;
	EXPECT_EQ(mVar.counts(), N);
	EXPECT_NEAR(mVar.mean(), mean, err * mean);
	EXPECT_NEAR(mVar.variance(), var, err * var);
}

TEST(TRandomGeneratorTests, beta) {
	constexpr size_t N     = static_cast<size_t>(1e4);
	constexpr double alpha = 0.876;
	constexpr double beta1 = 0.33;
	constexpr double beta2 = 123.67;
	constexpr double err   = 0.2;

	TMeanVar<double> mVar1;
	TMeanVar<double> mVar2;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) {
		mVar1.add(RG.getBetaRandom(alpha, beta1));
		mVar2.add(RG.getBetaRandom(alpha, beta2));
	}
	EXPECT_EQ(mVar1.counts(), N);
	EXPECT_EQ(mVar2.counts(), N);

	const auto mean1 = alpha / (alpha + beta1);
	const auto var1  = alpha * beta1 / (std::pow(alpha + beta1, 2) * (alpha + beta1 + 1));
	const auto mean2 = alpha / (alpha + beta2);
	const auto var2  = alpha * beta2 / (std::pow(alpha + beta2, 2) * (alpha + beta2 + 1));

	EXPECT_NEAR(mVar1.mean(), mean1, err * mean1);
	EXPECT_NEAR(mVar2.mean(), mean2, err * mean2);
	EXPECT_NEAR(mVar1.variance(), var1, err * var1);
	EXPECT_NEAR(mVar2.variance(), var2, err * var2);
}

TEST(TRandomGeneratorTests, pareto) {
	constexpr size_t N     = static_cast<size_t>(1e4);
	constexpr double mu    = -11;
	constexpr double sigma = 1.5;
	constexpr double xi    = 0.15;
	constexpr double err   = 0.2;

	TMeanVar<double> mVar;
	TRandomGenerator RG;

	for (size_t i = 0; i < N; ++i) { mVar.add(RG.getGeneralizedParetoRand(mu, sigma, xi)); }
	EXPECT_EQ(mVar.counts(), N);
	const auto ximin = 1 - xi;
	const auto mean  = mu + sigma / ximin;
	const auto var   = sigma * sigma / (ximin * ximin * (1 - 2 * xi));

	EXPECT_NEAR(mVar.mean(), mean, err * std::abs(mean));
	EXPECT_NEAR(mVar.variance(), var, err * std::abs(var));
}

TEST(TRandomGeneratorTests, dirichlet) {
	constexpr size_t N = static_cast<size_t>(1e4);
	TRandomGenerator RG;

	std::vector<double> alphas(N);
	RG.fillRand(alphas);

	std::vector<double> dir;
	RG.fillDirichletRandom(alphas, dir);

	EXPECT_EQ(dir.size(), N);
	EXPECT_FLOAT_EQ(std::accumulate(dir.begin(), dir.end(), 0.), 1.);
	EXPECT_TRUE(std::all_of(dir.cbegin(), dir.cend(), [](double di) { return (0 <= di) && (di < 1); }));
}

void pickOne(size_t N, size_t NIt) {
	// OUT(N);
	using instances::randomGenerator;
	std::vector<double> ps(N);
	std::iota(ps.begin(), ps.end(), 1.);
	randomGenerator().setSeed(0, true);
	randomGenerator().shuffle(ps);
	normalize(ps);

	std::vector<double> cumul(N);
	std::partial_sum(ps.begin(), ps.end(), cumul.begin());
	cumul.back() = 1.;

	// Binary Search
	randomGenerator().setSeed(0, true);
	std::vector<double> histBinSearch(N);
	{
		// SCOPEDTIMER("BinSearch");
		for (size_t _ = 0; _ < NIt; ++_) { histBinSearch[randomGenerator().pickOne(cumul)] += 1.; }
	}

	// Picker
	const TRandomPicker picker(ps);

	randomGenerator().setSeed(0, true);
	std::vector<double> histPick(N);
	{
		// SCOPEDTIMER("Picker(rand)");
		for (size_t _ = 0; _ < NIt; ++_) {
			const size_t i = picker(randomGenerator().getRand());
			histPick[i] += 1.;
		}
	}

	EXPECT_TRUE(std::equal(histBinSearch.begin(), histBinSearch.end(), histPick.begin()));

	randomGenerator().setSeed(0, true);
	histPick.assign(N, 0);
	{
		// SCOPEDTIMER("Picker()");
		for (size_t _ = 0; _ < NIt; ++_) {
			const size_t i = picker();
			histPick[i] += 1.;
		}
	}

	EXPECT_TRUE(std::equal(histBinSearch.begin(), histBinSearch.end(), histPick.begin()));
}

template<size_t N>
void pickOne(size_t NIt) {
	using instances::randomGenerator;
	std::array<double, N> ps;
	std::iota(ps.begin(), ps.end(), 1.);
	randomGenerator().setSeed(0, true);
	randomGenerator().shuffle(ps);
	normalize(ps);
	std::array<double, N> cumul;
	std::partial_sum(ps.begin(), ps.end(), cumul.begin());

	randomGenerator().setSeed(0, true);
	std::array<double, N> histBinSearchArr{};
	{
		// SCOPEDTIMER("BinSearchArr");
		for (size_t _ = 0; _ < NIt; ++_) { histBinSearchArr[randomGenerator().pickOne(cumul)] += 1.; }
	}

	randomGenerator().setSeed(0, true);
	std::array<double, N> histBinSearchVec{};
	std::vector<double> cumulV(cumul.begin(), cumul.end());
	{
		// SCOPEDTIMER("BinSearchVec");
		for (size_t _ = 0; _ < NIt; ++_) { histBinSearchVec[randomGenerator().pickOne(cumulV)] += 1.; }
	}

	// Picker
	TRandomPicker picker(ps);
	randomGenerator().setSeed(0, true);
	std::vector<double> histPick(N);
	{
		// SCOPEDTIMER("Picker<N>");
		for (size_t _ = 0; _ < NIt; ++_) { histPick[picker(randomGenerator().getRand())] += 1.; }
	}

	EXPECT_TRUE(std::equal(histBinSearchArr.begin(), histBinSearchArr.end(), histPick.begin()));
}

TEST(TRandomGeneratorTests, pickOne) {
	using instances::randomGenerator;
	constexpr size_t NIt = 100'000;

	for (size_t N = 2; N < 2000; N *= 2) {
		pickOne(N, NIt);
	}
	pickOne<2>(NIt);
	pickOne<4>(NIt);
	pickOne<8>(NIt);
	pickOne<16>(NIt);
	pickOne<32>(NIt);
}

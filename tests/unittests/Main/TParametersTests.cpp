#include <deque>
#include <memory>
#include "coretools/Main/TParameters.h"

#include "gtest/gtest.h"


using namespace testing;
using namespace coretools;
using coretools::instances::parameters;

void get(TParameters& parameters) {
	EXPECT_TRUE(parameters.unusedParameters().empty());

	// get
	parameters.add("1", 1);
	EXPECT_FALSE(parameters.unusedParameters().empty());
	EXPECT_TRUE(parameters.exists("1"));
	EXPECT_TRUE(parameters.unusedParameters().empty());
	EXPECT_FALSE(parameters.exists("2"));
	EXPECT_EQ(parameters.get("1"), "1");
	EXPECT_EQ(parameters.get<int>("1"), 1);

	parameters.add("2", 2);
	EXPECT_FALSE(parameters.unusedParameters().empty());
	EXPECT_TRUE(parameters.exists("1"));
	EXPECT_FALSE(parameters.unusedParameters().empty());
	EXPECT_TRUE(parameters.exists("2"));
	EXPECT_TRUE(parameters.unusedParameters().empty());
	EXPECT_EQ(parameters.get<int>("1"), 1);
	EXPECT_EQ(parameters.get<int>("2"), 2);
}

void getVector(TParameters& parameters) {
	parameters.add("a1", "1,2,3");
	parameters.add("a2", "(4;5;6)");
	parameters.add("a3", "[7|8|9|]");
	EXPECT_FALSE(parameters.unusedParameters().empty());

	EXPECT_EQ(parameters.get("a1"), "1,2,3");
	EXPECT_EQ(parameters.get("a2"), "(4;5;6)");
	EXPECT_EQ(parameters.get("a3"), "[7|8|9|]");

	using T1 = std::vector<int>;
	using T2 = std::array<int,3>;
	using T3 = std::deque<int>;
	const auto r1 = T1{1,2,3};
	const auto r2 = T2{4,5,6};
	const auto r3 = T3{7,8,9};
	EXPECT_EQ(parameters.get<T1>("a1"), r1);
	EXPECT_EQ(parameters.get<T2>("a2"), r2);
	EXPECT_EQ(parameters.get<T3>("a3"), r3);
}

void fill(TParameters& parameters) {
	parameters.add("1", 1);
	parameters.add("2", 2);
	// fill
	int i1, i2;
	parameters.fill("1", i1);
	parameters.fill("2", i2);
	EXPECT_EQ(i1, 1);
	EXPECT_EQ(i2, 2);
}

void defaults(TParameters& parameters) {
	EXPECT_ANY_THROW(parameters.get("1"));
	EXPECT_ANY_THROW(parameters.get<int>("1"));
	EXPECT_EQ(parameters.get("1", "1"), "1");
	EXPECT_EQ(parameters.get<int>("1", 1), 1);
}


TEST(TParamterTests, get) {
	TParameters p;
	std::unique_ptr<TParameters> up = std::make_unique<TParameters>();
	parameters().clear();

	get(p);
	get(*up);
	get(parameters());
}
TEST(TParamterTests, getVector) {
	TParameters p;
	std::unique_ptr<TParameters> up = std::make_unique<TParameters>();
	parameters().clear();

	getVector(p);
	getVector(*up);
	getVector(parameters());
}

TEST(TParamterTests, fill) {
	TParameters p;
	std::unique_ptr<TParameters> up = std::make_unique<TParameters>();
	parameters().clear();

	fill(p);
	fill(*up);
	fill(parameters());
}


TEST(TParamterTests, defaults) {
	TParameters p;
	std::unique_ptr<TParameters> up = std::make_unique<TParameters>();
	parameters().clear();

	defaults(p);
	defaults(*up);
	defaults(parameters());
}

TEST(TParamterTests, readWrite) {
	constexpr auto filename = "parameters.io";
	parameters().clear();
	parameters().add("empty1", true);
	parameters().add("empty2", true);
	parameters().add("empty3", false); // will not be written

	parameters().add("one", 1, true);
	parameters().add("two", 2, true);
	parameters().add("three", 3, false); // will not be written

	parameters().writeFile(filename);
	parameters().clear();
	EXPECT_FALSE(parameters().exists("empty1"));
	parameters().readFile(filename);

	EXPECT_TRUE(parameters().exists("empty1"));
	EXPECT_TRUE(parameters().exists("empty2"));
	EXPECT_FALSE(parameters().exists("empty3"));
	EXPECT_EQ(parameters().get<int>("one"), 1);
	EXPECT_EQ(parameters().get<int>("two"), 2);
	EXPECT_ANY_THROW(parameters().get<int>("three"));
	remove(filename);
}

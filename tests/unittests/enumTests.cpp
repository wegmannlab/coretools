//
// Created by Andreas on 04.07.2022
//

#include "coretools/enum.h"
#include "gtest/gtest.h"

using namespace coretools;

enum class TestIndex: size_t {min=0, i0 = min, i1, i2, i3, max};

static_assert(index(TestIndex::min) == 0);
static_assert(index(TestIndex::i0) == 0);
static_assert(index(TestIndex::i1) == 1);
static_assert(index(TestIndex::i2) == 2);
static_assert(index(TestIndex::i3) == 3);
static_assert(index(TestIndex::max) == 4);

static_assert(index(5) == 5);

static_assert(next(TestIndex::min) == TestIndex::i1);
static_assert(next(TestIndex::i0) == TestIndex::i1);
static_assert(next(TestIndex::i2) == TestIndex::i3);

TEST(enumTests, enums) {
	auto i = TestIndex::min;
	EXPECT_EQ(i, TestIndex::i0);
	inc(i);
	EXPECT_EQ(i, TestIndex::i1);
	++i;
	EXPECT_EQ(i, TestIndex::i2);
}


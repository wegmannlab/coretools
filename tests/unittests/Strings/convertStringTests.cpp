
#include "gtest/gtest.h"
#include "coretools/Strings/convertString.h"
#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"

using namespace coretools;
using namespace str;

TEST(converStringTests, convertString_twoValidValues) {
	Unbounded val1;
	Unbounded val2;
	convertString("0,1", "Use format normal(mean,var)", val1, val2);
	EXPECT_EQ(val1, 0.0);
	EXPECT_EQ(val2, 1.0);
}

TEST(converStringTests, convertString_twoSignedValidValues) {
	Unbounded val1;
	Unbounded val2;
	convertString("-1,1", "Use format normal(mean,var)", val1, val2);
	EXPECT_EQ(val1, -1.0);
	EXPECT_EQ(val2, 1.0);
}

TEST(converStringTests, convertString_differentTypes) {
	Probability val1;
	StrictlyPositive val2;
	convertString("0.01,1", "Use format normal(mean,var)", val1, val2);
	EXPECT_EQ(val1, 0.01);
	EXPECT_EQ(val2, 1.0);

	// check for type
	EXPECT_ANY_THROW(convertString("0.01, 0", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("1.1, 1", "Use format normal(mean,var)", val1, val2));
}

TEST(converStringTests, convertString_twoInvalidValues) {
	Unbounded val1;
	Unbounded val2;

	EXPECT_ANY_THROW(convertString("", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("1", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("1/2", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("1.2", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("1,b", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("a,1", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("a,b", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("inf,b", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("true,b", "Use format normal(mean,var)", val1, val2));
	EXPECT_ANY_THROW(convertString("1,2,3", "Use format normal(mean,var)", val1, val2));
}

TEST(converStringTests, convertString_oneValidValue) {
	Unbounded val1;
	convertString("1", "Use format normal(mean,var)", val1);
	EXPECT_EQ(val1, 1.0);
}

TEST(converStringTests, convertString_oneSignedValidValue) {
	Unbounded val1;
	convertString("-1", "Use format normal(mean,var)", val1);
	EXPECT_EQ(val1, -1.0);
	convertString("1", "Use format normal(mean,var)", val1);
	EXPECT_EQ(val1, 1.0);
}

TEST(converStringTests, convertString_oneInvalidValue) {
	Unbounded val1;
	EXPECT_ANY_THROW(convertString("", "Use format normal(mean,var)", val1));
	EXPECT_ANY_THROW(convertString("1/2", "Use format normal(mean,var)", val1));
	EXPECT_ANY_THROW(convertString("a", "Use format normal(mean,var)", val1));
	EXPECT_ANY_THROW(convertString("inf", "Use format normal(mean,var)", val1));
	EXPECT_ANY_THROW(convertString("true", "Use format normal(mean,var)", val1));
	EXPECT_ANY_THROW(convertString("0,1", "Use format normal(mean,var)", val1));
}

TEST(converStringTests, convertString_noValue) {
	// string is also empty -> ok
	std::string s = "";
	EXPECT_NO_THROW(convertString("", "normal"));
	// string is not empty -> throw
	EXPECT_ANY_THROW(convertString("1", "normal"));
}

#include "gtest/gtest.h"

#include "coretools/Strings/stringConversions.h"

using namespace coretools;
using namespace str;

TEST(stringConversionTests, numericToUpperCaseAlphabetIndex) {
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(0), "A");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(1), "B");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(2), "C");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(25), "Z");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(26), "AA");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(27), "AB");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(28), "AC");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(52), "BA");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(53), "BB");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(54), "BC");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(78), "CA");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(701), "ZZ");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(702), "AAA");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(703), "AAB");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(704), "AAC");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(18277), "ZZZ");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(18278), "AAAA");
	EXPECT_EQ(numericToUpperCaseAlphabetIndex(18279), "AAAB");
	// ...
}

TEST(stringConversionTests, numericToLowerCaseAlphabetIndex) {
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(0), "a");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(1), "b");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(2), "c");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(25), "z");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(26), "aa");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(27), "ab");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(28), "ac");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(52), "ba");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(53), "bb");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(54), "bc");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(78), "ca");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(701), "zz");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(702), "aaa");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(703), "aab");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(704), "aac");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(18277), "zzz");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(18278), "aaaa");
	EXPECT_EQ(numericToLowerCaseAlphabetIndex(18279), "aaab");
	// ...
}

TEST(stringConversionTests, lowerCaseAlphabetIndexToNumeric) {
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("a"), 0);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("b"), 1);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("c"), 2);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("z"), 25);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aa"), 26);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ab"), 27);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ac"), 28);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ba"), 52);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("bb"), 53);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("bc"), 54);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ca"), 78);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("zz"), 701);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aaa"), 702);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aab"), 703);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aac"), 704);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("zzz"), 18277);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aaaa"), 18278);
	EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aaab"), 18279);
	// ...
}

TEST(stringConversionTests, upperCaseAlphabetIndexToNumeric) {
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("A"), 0);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("B"), 1);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("C"), 2);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("Z"), 25);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AA"), 26);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AB"), 27);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AC"), 28);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("BA"), 52);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("BB"), 53);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("BC"), 54);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("CA"), 78);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("ZZ"), 701);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAA"), 702);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAB"), 703);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAC"), 704);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("ZZZ"), 18277);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAAA"), 18278);
	EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAAB"), 18279);
	// ...
}

TEST(stringConversionTests, intToNumeralAdjective) {
	// valid
	intToNumeralAdjective(1);
	EXPECT_EQ(intToNumeralAdjective(1), "first");
	EXPECT_EQ(intToNumeralAdjective(2), "second");
	EXPECT_EQ(intToNumeralAdjective(3), "third");
	EXPECT_EQ(intToNumeralAdjective(4), "4th");
	EXPECT_EQ(intToNumeralAdjective(5), "5th");
	EXPECT_EQ(intToNumeralAdjective(100), "100th");
	EXPECT_EQ(intToNumeralAdjective(4), "4th");
}

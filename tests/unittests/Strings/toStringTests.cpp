#include "gtest/gtest.h"

#include "coretools/Strings/stringManipulations.h"
#include "coretools/Strings/toString.h"

using namespace coretools;
using namespace str;

TEST(toStringTests, toString) {
	EXPECT_EQ(toString((int)5), "5");
	EXPECT_EQ(readBefore(toString((float)75.3476), '0'), "75.3476");
	EXPECT_EQ(toString("This is a string!"), "This is a string!");
	EXPECT_EQ(toString(), "");
	EXPECT_EQ(toString(1), "1");
	EXPECT_EQ(toString(1, 2), "12");
	EXPECT_EQ(toString(1, 2, 3), "123");
	EXPECT_EQ(toString(1, 2, 3, 4), "1234");
	EXPECT_EQ(readBefore(toString((int)-5, "abc", (float)1.23456), '0'), "-5abc1.23456");
	EXPECT_EQ(toString(true), "true");
	EXPECT_EQ(toString(false), "false");
	std::string s = "This is a string";
	const char *c = s.c_str();
	EXPECT_EQ(toString(c), s);

	std::array<int, 4> as;
	std::iota(as.begin(), as.end(), 4);
	EXPECT_EQ(toString(as), "[4, 5, 6, 7]");
}


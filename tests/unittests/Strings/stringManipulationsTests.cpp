#include "gtest/gtest.h"

#include "coretools/Strings/stringManipulations.h"

using namespace coretools;
using namespace str;

TEST(stringManipulationsTests, stringReplaceChar) {
	// don't replace
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(stringReplace('j', "monster", s), s);
	EXPECT_EQ(stringReplace(' ', "monster", s), s);

	// do replace
	EXPECT_EQ(stringReplace('-', "monster", s), "007hay_stack123monsterblah");
	EXPECT_EQ(stringReplace('h', "monster", s), "007monsteray_stack123-blamonster");
	EXPECT_EQ(stringReplace('0', "-1", s), "-1-17hay_stack123-blah");
}

TEST(stringManipulationsTests, stringReplaceString) {
	// don't replace
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(stringReplace("j", "monster", s), s);
	EXPECT_EQ(stringReplace(" ", "monster", s), s);
	EXPECT_EQ(stringReplace("haya", "monster", s), s);

	// do replace
	EXPECT_EQ(stringReplace("-", "monster", s), "007hay_stack123monsterblah");
	EXPECT_EQ(stringReplace("h", "monster", s), "007monsteray_stack123-blamonster");
	EXPECT_EQ(stringReplace("0", "-1", s), "-1-17hay_stack123-blah");
	EXPECT_EQ(stringReplace("stack", "monster", s), "007hay_monster123-blah");
	EXPECT_EQ(stringReplace("007hay_stack123-blah", "monster", s), "monster");
}

TEST(stringManipulationsTests, strip) {
	// make sure nothing happens if there isn't anyting to strip
	EXPECT_EQ(lstrip("abc\tdef"), std::string_view("abc\tdef"));
	EXPECT_EQ(rstrip("abc\tdef"), std::string_view("abc\tdef"));
	EXPECT_EQ(strip("abc\tdef"), std::string_view("abc\tdef"));

	// now strip
	EXPECT_EQ(lstrip(" \r\t\v\nabc\tdef\v"), std::string_view("abc\tdef\v"));
	EXPECT_EQ(rstrip(" \r\t\v\nabc\tdef\v"), std::string_view(" \r\t\v\nabc\tdef"));
	EXPECT_EQ(strip(" \r\t\v\nabc\tdef\v"), std::string_view("abc\tdef"));
	EXPECT_EQ(strip(" \r\t\v\nabc\tdef\v"), lstrip(rstrip(" \r\t\v\nabc\tdef\v")));

	// strip everything
	EXPECT_EQ(lstrip("  \t\n  "), std::string_view(""));
	EXPECT_EQ(rstrip("  \t\n  "), std::string_view(""));
	EXPECT_EQ(strip("  \t\n  "), std::string_view(""));
}

TEST(stringManipulationsTests, eraseAllOccurencesExactly) {
	// don't erase
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	eraseAllOccurences(newS, "j", false);
	EXPECT_EQ(newS, s);
	newS = s;
	eraseAllOccurences(newS, " ", false);
	EXPECT_EQ(newS, s);
	newS = s;
	eraseAllOccurences(newS, "haya", false);
	EXPECT_EQ(newS, s);

	// do replace
	newS = s;
	eraseAllOccurences(newS, "-", false);
	EXPECT_EQ(newS, "007hay_stack123blah");
	newS = s;
	eraseAllOccurences(newS, "h", false);
	EXPECT_EQ(newS, "007ay_stack123-bla");
	newS = s;
	eraseAllOccurences(newS, "0", false);
	EXPECT_EQ(newS, "7hay_stack123-blah");
	newS = s;
	eraseAllOccurences(newS, "stack", false);
	EXPECT_EQ(newS, "007hay_123-blah");
	newS = s;
	eraseAllOccurences(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, eraseAllOccurencesAny) {
	// don't erase
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	eraseAllOccurences(newS, "j", true);
	EXPECT_EQ(newS, s);
	newS = s;
	eraseAllOccurences(newS, " ", true);
	EXPECT_EQ(newS, s);

	// do replace
	newS = s;
	eraseAllOccurences(newS, "-", true);
	EXPECT_EQ(newS, "007hay_stack123blah");
	newS = s;
	eraseAllOccurences(newS, "h", true);
	EXPECT_EQ(newS, "007ay_stack123-bla");
	newS = s;
	eraseAllOccurences(newS, "0", true);
	EXPECT_EQ(newS, "7hay_stack123-blah");
	newS = s;
	eraseAllOccurences(newS, "stack", true);
	EXPECT_EQ(newS, "007hy_123-blh");
	newS = s;
	eraseAllOccurences(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, eraseAllOccurencesChar) {
	// don't erase
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	eraseAllOccurences(newS, 'j');
	EXPECT_EQ(newS, s);
	newS = s;
	eraseAllOccurences(newS, ' ');
	EXPECT_EQ(newS, s);

	// do replace
	newS = s;
	eraseAllOccurences(newS, '-');
	EXPECT_EQ(newS, "007hay_stack123blah");
	newS = s;
	eraseAllOccurences(newS, 'h');
	EXPECT_EQ(newS, "007ay_stack123-bla");
	newS = s;
	eraseAllOccurences(newS, '0');
	EXPECT_EQ(newS, "7hay_stack123-blah");
}

TEST(stringManipulationsTests, eraseAllWhiteSpaces) {
	// don't erase
	std::string s = "007hay_stack123-blah";
	eraseAllWhiteSpaces(s);
	EXPECT_EQ(s, "007hay_stack123-blah");

	// do erase
	s = "007 hay    _st\tack\f123\n-blah\r\v";
	eraseAllWhiteSpaces(s);
	EXPECT_EQ(s, "007hay_stack123-blah");
}

TEST(stringManipulationsTests, eraseAllNonAlphabeticCharacters) {
	// don't erase
	std::string s = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜàéèÀÉÈ";
	eraseAllNonAlphabeticCharacters(s);
	EXPECT_EQ(s, "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

	// do erase
	s = "007123_ 47/1..,1872";
	eraseAllNonAlphabeticCharacters(s);
	EXPECT_EQ(s, "");
	s = "007hay_stack 123-blah";
	eraseAllNonAlphabeticCharacters(s);
	EXPECT_EQ(s, "haystackblah");
}

// read
/////////////////// read before ///////////////////

TEST(stringManipulationsTests, readBeforeExactly) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readBefore(s, "j"), s);
	EXPECT_EQ(readBefore(s, " "), s);
	EXPECT_EQ(readBefore(s, "haya"), s);

	// do read
	EXPECT_EQ(readBefore(s, "-"), "007hay_stack123");
	EXPECT_EQ(readBefore(s, "h"), "007");
	EXPECT_EQ(readBefore(s, "0"), "");
	EXPECT_EQ(readBefore(s, "stack"), "007hay_");
	EXPECT_EQ(readBefore(s, s), "");
}

TEST(stringManipulationsTests, readBeforeAny) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readBefore(s, "j", true), s);
	EXPECT_EQ(readBefore(s, " ", true), s);

	// do read
	EXPECT_EQ(readBefore(s, "haya", true), "007");
	EXPECT_EQ(readBefore(s, "-", true), "007hay_stack123");
	EXPECT_EQ(readBefore(s, "h", true), "007");
	EXPECT_EQ(readBefore(s, "0", true), "");
	EXPECT_EQ(readBefore(s, "stack", true), "007h");
	EXPECT_EQ(readBefore(s, s, true), "");
}

TEST(stringManipulationsTests, readBefore) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readBefore(s, 'j'), s);
	EXPECT_EQ(readBefore(s, ' '), s);

	// do read
	EXPECT_EQ(readBefore(s, '-'), "007hay_stack123");
	EXPECT_EQ(readBefore(s, 'h'), "007");
	EXPECT_EQ(readBefore(s, '0'), "");
}

/////////////////// read until ///////////////////

TEST(stringManipulationsTests, readUntilExactly) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readUntil(s, "j"), s);
	EXPECT_EQ(readUntil(s, " "), s);
	EXPECT_EQ(readUntil(s, "haya"), s);

	// do read
	EXPECT_EQ(readUntil(s, "-"), "007hay_stack123-");
	EXPECT_EQ(readUntil(s, "h"), "007h");
	EXPECT_EQ(readUntil(s, "0"), "0");
	EXPECT_EQ(readUntil(s, "stack"), "007hay_stack");
	EXPECT_EQ(readUntil(s, s), s);
}

TEST(stringManipulationsTests, readUntilAny) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readUntil(s, "j", true), s);
	EXPECT_EQ(readUntil(s, " ", true), s);

	// do read
	EXPECT_EQ(readUntil(s, "haya", true), "007h");
	EXPECT_EQ(readUntil(s, "-", true), "007hay_stack123-");
	EXPECT_EQ(readUntil(s, "h", true), "007h");
	EXPECT_EQ(readUntil(s, "0", true), "0");
	EXPECT_EQ(readUntil(s, "stack", true), "007ha");
	EXPECT_EQ(readUntil(s, s, true), "0");
}

TEST(stringManipulationsTests, readUntil) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readUntil(s, 'j'), s);
	EXPECT_EQ(readUntil(s, ' '), s);

	// do read
	EXPECT_EQ(readUntil(s, '-'), "007hay_stack123-");
	EXPECT_EQ(readUntil(s, 'h'), "007h");
	EXPECT_EQ(readUntil(s, '0'), "0");
}

/////////////////// read before last ///////////////////

TEST(stringManipulationsTests, readBeforeLastExactly) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readBeforeLast(s, "j"), s);
	EXPECT_EQ(readBeforeLast(s, " "), s);
	EXPECT_EQ(readBeforeLast(s, "haya"), s);

	// do read
	EXPECT_EQ(readBeforeLast(s, "-"), "007hay_stack123");
	EXPECT_EQ(readBeforeLast(s, "h"), "007hay_stack123-bla");
	EXPECT_EQ(readBeforeLast(s, "0"), "0");
	EXPECT_EQ(readBeforeLast(s, "stack"), "007hay_");
	EXPECT_EQ(readBeforeLast(s, s), "");
}

TEST(stringManipulationsTests, readBeforeLastAny) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readBeforeLast(s, "j", true), s);
	EXPECT_EQ(readBeforeLast(s, " ", true), s);

	// do read
	EXPECT_EQ(readBeforeLast(s, "haya", true), "007hay_stack123-bla");
	EXPECT_EQ(readBeforeLast(s, "-", true), "007hay_stack123");
	EXPECT_EQ(readBeforeLast(s, "h", true), "007hay_stack123-bla");
	EXPECT_EQ(readBeforeLast(s, "0", true), "0");
	EXPECT_EQ(readBeforeLast(s, "stack", true), "007hay_stack123-bl");
	EXPECT_EQ(readBeforeLast(s, s, true), "007hay_stack123-bla");
}

TEST(stringManipulationsTests, readBeforeLastChar) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readBeforeLast(s, 'j'), s);
	EXPECT_EQ(readBeforeLast(s, ' '), s);

	// do read
	EXPECT_EQ(readBeforeLast(s, '-'), "007hay_stack123");
	EXPECT_EQ(readBeforeLast(s, 'h'), "007hay_stack123-bla");
	EXPECT_EQ(readBeforeLast(s, '0'), "0");
}

/////////////////// read until last ///////////////////

TEST(stringManipulationsTests, readUntilLastExactly) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readUntilLast(s, "j"), s);
	EXPECT_EQ(readUntilLast(s, " "), s);
	EXPECT_EQ(readUntilLast(s, "haya"), s);

	// do read
	EXPECT_EQ(readUntilLast(s, "-"), "007hay_stack123-");
	EXPECT_EQ(readUntilLast(s, "h"), "007hay_stack123-blah");
	EXPECT_EQ(readUntilLast(s, "0"), "00");
	EXPECT_EQ(readUntilLast(s, "stack"), "007hay_stack");
	EXPECT_EQ(readUntilLast(s, s), s);
}

TEST(stringManipulationsTests, readUntilLastAny) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readUntilLast(s, "j", true), s);
	EXPECT_EQ(readUntilLast(s, " ", true), s);

	// do read
	EXPECT_EQ(readUntilLast(s, "haya", true), "007hay_stack123-blah");
	EXPECT_EQ(readUntilLast(s, "-", true), "007hay_stack123-");
	EXPECT_EQ(readUntilLast(s, "h", true), "007hay_stack123-blah");
	EXPECT_EQ(readUntilLast(s, "0", true), "00");
	EXPECT_EQ(readUntilLast(s, "stack", true), "007hay_stack123-bla");
	EXPECT_EQ(readUntilLast(s, s, true), s);
}

TEST(stringManipulationsTests, readUntilLastChar) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readUntilLast(s, 'j'), s);
	EXPECT_EQ(readUntilLast(s, ' '), s);

	// do read
	EXPECT_EQ(readUntilLast(s, '-'), "007hay_stack123-");
	EXPECT_EQ(readUntilLast(s, 'h'), "007hay_stack123-blah");
	EXPECT_EQ(readUntilLast(s, '0'), "00");
}

/////////////////// read after ///////////////////

TEST(stringManipulationsTests, readAfterExactly) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readAfter(s, "j"), "");
	EXPECT_EQ(readAfter(s, " "), "");
	EXPECT_EQ(readAfter(s, "haya"), "");

	// do read
	EXPECT_EQ(readAfter(s, "-"), "blah");
	EXPECT_EQ(readAfter(s, "h"), "ay_stack123-blah");
	EXPECT_EQ(readAfter(s, "0"), "07hay_stack123-blah");
	EXPECT_EQ(readAfter(s, "stack"), "123-blah");
	EXPECT_EQ(readAfter(s, s), "");
}

TEST(stringManipulationsTests, readAfterAny) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readAfter(s, "j", true), "");
	EXPECT_EQ(readAfter(s, " ", true), "");

	// do read
	EXPECT_EQ(readAfter(s, "haya", true), "ay_stack123-blah");
	EXPECT_EQ(readAfter(s, "-", true), "blah");
	EXPECT_EQ(readAfter(s, "h", true), "ay_stack123-blah");
	EXPECT_EQ(readAfter(s, "0", true), "07hay_stack123-blah");
	EXPECT_EQ(readAfter(s, "stack", true), "y_stack123-blah");
	EXPECT_EQ(readAfter(s, s, true), "07hay_stack123-blah");
}

TEST(stringManipulationsTests, readAfter) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readAfter(s, 'j'), "");
	EXPECT_EQ(readAfter(s, ' '), "");

	// do read
	EXPECT_EQ(readAfter(s, '-'), "blah");
	EXPECT_EQ(readAfter(s, 'h'), "ay_stack123-blah");
	EXPECT_EQ(readAfter(s, '0'), "07hay_stack123-blah");
}

/////////////////// read after last ///////////////////

TEST(stringManipulationsTests, readAfterLastExactly) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readAfterLast(s, "j"), "");
	EXPECT_EQ(readAfterLast(s, " "), "");
	EXPECT_EQ(readAfterLast(s, "haya"), "");

	// do read
	EXPECT_EQ(readAfterLast(s, "-"), "blah");
	EXPECT_EQ(readAfterLast(s, "h"), "");
	EXPECT_EQ(readAfterLast(s, "0"), "7hay_stack123-blah");
	EXPECT_EQ(readAfterLast(s, "stack"), "123-blah");
	EXPECT_EQ(readAfterLast(s, s), "");
}

TEST(stringManipulationsTests, readAfterLastAny) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readAfterLast(s, "j", true), "");
	EXPECT_EQ(readAfterLast(s, " ", true), "");

	// do read
	EXPECT_EQ(readAfterLast(s, "haya", true), "");
	EXPECT_EQ(readAfterLast(s, "-", true), "blah");
	EXPECT_EQ(readAfterLast(s, "h", true), "");
	EXPECT_EQ(readAfterLast(s, "0", true), "7hay_stack123-blah");
	EXPECT_EQ(readAfterLast(s, "stack", true), "h");
	EXPECT_EQ(readAfterLast(s, s, true), "");
}

TEST(stringManipulationsTests, readAfterLast) {
	// don't read
	std::string s = "007hay_stack123-blah";
	EXPECT_EQ(readAfterLast(s, 'j'), "");
	EXPECT_EQ(readAfterLast(s, ' '), "");

	// do read
	EXPECT_EQ(readAfterLast(s, '-'), "blah");
	EXPECT_EQ(readAfterLast(s, 'h'), "");
	EXPECT_EQ(readAfterLast(s, '0'), "7hay_stack123-blah");
}

// manipulations
/////////////////// extract before ///////////////////

TEST(stringManipulationsTests, extractBeforeExactly) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBefore(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBefore(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBefore(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractBefore(newS, "-", false);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "-blah");
	newS = s;
	p    = extractBefore(newS, "h", false);
	EXPECT_EQ(p, "007");
	EXPECT_EQ(newS, "hay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, "0", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "007hay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, "stack", false);
	EXPECT_EQ(p, "007hay_");
	EXPECT_EQ(newS, "stack123-blah");
	newS = s;
	p    = extractBefore(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
}

TEST(stringManipulationsTests, extractBeforeAny) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBefore(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBefore(newS, " ", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractBefore(newS, "haya", true);
	EXPECT_EQ(p, "007");
	EXPECT_EQ(newS, "hay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, "-", true);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "-blah");
	newS = s;
	p    = extractBefore(newS, "h", true);
	EXPECT_EQ(p, "007");
	EXPECT_EQ(newS, "hay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, "0", true);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "007hay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, "stack", true);
	EXPECT_EQ(p, "007h");
	EXPECT_EQ(newS, "ay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
}

TEST(stringManipulationsTests, extractBeforeChar) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBefore(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBefore(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractBefore(newS, '-');
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "-blah");
	newS = s;
	p    = extractBefore(newS, 'h');
	EXPECT_EQ(p, "007");
	EXPECT_EQ(newS, "hay_stack123-blah");
	newS = s;
	p    = extractBefore(newS, '0');
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "007hay_stack123-blah");
}

TEST(stringManipulationsTests, extractBeforeDoubleSlash) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBeforeDoubleSlash(newS);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	s    = "007hay_stac//k123-blah";
	newS = s;
	p    = extractBeforeDoubleSlash(newS);
	EXPECT_EQ(p, "007hay_stac");
	EXPECT_EQ(newS, "//k123-blah");
	s    = "//007hay_stack123-blah";
	newS = s;
	p    = extractBeforeDoubleSlash(newS);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "//007hay_stack123-blah");
	s    = "007hay_stack123-blah//";
	newS = s;
	p    = extractBeforeDoubleSlash(newS);
	EXPECT_EQ(p, "007hay_stack123-blah");
	EXPECT_EQ(newS, "//");
}

TEST(stringManipulationsTests, extractBeforeWhiteSpace) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBeforeWhiteSpace(newS);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	s    = "007hay_stac k123-blah";
	newS = s;
	p    = extractBeforeWhiteSpace(newS);
	EXPECT_EQ(p, "007hay_stac");
	EXPECT_EQ(newS, " k123-blah");
	s    = " 007hay_stack123- blah";
	newS = s;
	p    = extractBeforeWhiteSpace(newS);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, " 007hay_stack123- blah");
	s    = "007hay_stack123-blah   ";
	newS = s;
	p    = extractBeforeWhiteSpace(newS);
	EXPECT_EQ(p, "007hay_stack123-blah");
	EXPECT_EQ(newS, "   ");
}

/////////////////// extract until ///////////////////

TEST(stringManipulationsTests, extractUntilExactly) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractUntil(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntil(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntil(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractUntil(newS, "-", false);
	EXPECT_EQ(p, "007hay_stack123-");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = extractUntil(newS, "h", false);
	EXPECT_EQ(p, "007h");
	EXPECT_EQ(newS, "ay_stack123-blah");
	newS = s;
	p    = extractUntil(newS, "0", false);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
	newS = s;
	p    = extractUntil(newS, "stack", false);
	EXPECT_EQ(p, "007hay_stack");
	EXPECT_EQ(newS, "123-blah");
	newS = s;
	p    = extractUntil(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, extractUntilAny) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractUntil(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntil(newS, " ", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractUntil(newS, "haya", true);
	EXPECT_EQ(p, "007h");
	EXPECT_EQ(newS, "ay_stack123-blah");
	newS = s;
	p    = extractUntil(newS, "-", true);
	EXPECT_EQ(p, "007hay_stack123-");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = extractUntil(newS, "h", true);
	EXPECT_EQ(p, "007h");
	EXPECT_EQ(newS, "ay_stack123-blah");
	newS = s;
	p    = extractUntil(newS, "0", true);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
	newS = s;
	p    = extractUntil(newS, "stack", true);
	EXPECT_EQ(p, "007ha");
	EXPECT_EQ(newS, "y_stack123-blah");
	newS = s;
	p    = extractUntil(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
}

TEST(stringManipulationsTests, extractUntilChar) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractUntil(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntil(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractUntil(newS, '-');
	EXPECT_EQ(p, "007hay_stack123-");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = extractUntil(newS, 'h');
	EXPECT_EQ(p, "007h");
	EXPECT_EQ(newS, "ay_stack123-blah");
	newS = s;
	p    = extractUntil(newS, '0');
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
}
/////////////////// extract before last ///////////////////

TEST(stringManipulationsTests, extractBeforeLastExactly) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBeforeLast(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBeforeLast(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBeforeLast(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractBeforeLast(newS, "-", false);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "-blah");
	newS = s;
	p    = extractBeforeLast(newS, "h", false);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "h");
	newS = s;
	p    = extractBeforeLast(newS, "0", false);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
	newS = s;
	p    = extractBeforeLast(newS, "stack", false);
	EXPECT_EQ(p, "007hay_");
	EXPECT_EQ(newS, "stack123-blah");
	newS = s;
	p    = extractBeforeLast(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
}

TEST(stringManipulationsTests, extractBeforeLastAny) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBeforeLast(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBeforeLast(newS, " ", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractBeforeLast(newS, "haya", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "h");
	newS = s;
	p    = extractBeforeLast(newS, "-", true);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "-blah");
	newS = s;
	p    = extractBeforeLast(newS, "h", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "h");
	newS = s;
	p    = extractBeforeLast(newS, "0", true);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
	newS = s;
	p    = extractBeforeLast(newS, "stack", true);
	EXPECT_EQ(p, "007hay_stack123-bl");
	EXPECT_EQ(newS, "ah");
	newS = s;
	p    = extractBeforeLast(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "h");
}

TEST(stringManipulationsTests, extractBeforeLastChar) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractBeforeLast(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractBeforeLast(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractBeforeLast(newS, '-');
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "-blah");
	newS = s;
	p    = extractBeforeLast(newS, 'h');
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "h");
	newS = s;
	p    = extractBeforeLast(newS, '0');
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "07hay_stack123-blah");
}

/////////////////// extract until last ///////////////////

TEST(stringManipulationsTests, extractUntilLastExactly) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractUntilLast(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractUntilLast(newS, "-", false);
	EXPECT_EQ(p, "007hay_stack123-");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = extractUntilLast(newS, "h", false);
	EXPECT_EQ(p, "007hay_stack123-blah");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, "0", false);
	EXPECT_EQ(p, "00");
	EXPECT_EQ(newS, "7hay_stack123-blah");
	newS = s;
	p    = extractUntilLast(newS, "stack", false);
	EXPECT_EQ(p, "007hay_stack");
	EXPECT_EQ(newS, "123-blah");
	newS = s;
	p    = extractUntilLast(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, extractUntilLastAny) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractUntilLast(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, " ", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractUntilLast(newS, "haya", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, "-", true);
	EXPECT_EQ(p, "007hay_stack123-");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = extractUntilLast(newS, "h", true);
	EXPECT_EQ(p, "007hay_stack123-blah");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, "0", true);
	EXPECT_EQ(p, "00");
	EXPECT_EQ(newS, "7hay_stack123-blah");
	newS = s;
	p    = extractUntilLast(newS, "stack", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "h");
	newS = s;
	p    = extractUntilLast(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, extractUntilLastChar) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractUntilLast(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractUntilLast(newS, '-');
	EXPECT_EQ(p, "007hay_stack123-");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = extractUntilLast(newS, 'h');
	EXPECT_EQ(p, "007hay_stack123-blah");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractUntilLast(newS, '0');
	EXPECT_EQ(p, "00");
	EXPECT_EQ(newS, "7hay_stack123-blah");
}

/////////////////// extract after ///////////////////

TEST(stringManipulationsTests, extractAfterExactly) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractAfter(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfter(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfter(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractAfter(newS, "-", false);
	EXPECT_EQ(p, "blah");
	EXPECT_EQ(newS, "007hay_stack123-");
	newS = s;
	p    = extractAfter(newS, "h", false);
	EXPECT_EQ(p, "ay_stack123-blah");
	EXPECT_EQ(newS, "007h");
	newS = s;
	p    = extractAfter(newS, "0", false);
	EXPECT_EQ(p, "07hay_stack123-blah");
	EXPECT_EQ(newS, "0");
	newS = s;
	p    = extractAfter(newS, "stack", false);
	EXPECT_EQ(p, "123-blah");
	EXPECT_EQ(newS, "007hay_stack");
	newS = s;
	p    = extractAfter(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
}

TEST(stringManipulationsTests, extractAfterAny) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractAfter(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfter(newS, " ", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractAfter(newS, "haya", true);
	EXPECT_EQ(p, "ay_stack123-blah");
	EXPECT_EQ(newS, "007h");
	newS = s;
	p    = extractAfter(newS, "-", true);
	EXPECT_EQ(p, "blah");
	EXPECT_EQ(newS, "007hay_stack123-");
	newS = s;
	p    = extractAfter(newS, "h", true);
	EXPECT_EQ(p, "ay_stack123-blah");
	EXPECT_EQ(newS, "007h");
	newS = s;
	p    = extractAfter(newS, "0", true);
	EXPECT_EQ(p, "07hay_stack123-blah");
	EXPECT_EQ(newS, "0");
	newS = s;
	p    = extractAfter(newS, "stack", true);
	EXPECT_EQ(p, "y_stack123-blah");
	EXPECT_EQ(newS, "007ha");
	newS = s;
	p    = extractAfter(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, "07hay_stack123-blah");
	EXPECT_EQ(newS, "0");
}

TEST(stringManipulationsTests, extractAfterChar) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractAfter(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfter(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractAfter(newS, '-');
	EXPECT_EQ(p, "blah");
	EXPECT_EQ(newS, "007hay_stack123-");
	newS = s;
	p    = extractAfter(newS, 'h');
	EXPECT_EQ(p, "ay_stack123-blah");
	EXPECT_EQ(newS, "007h");
	newS = s;
	p    = extractAfter(newS, '0');
	EXPECT_EQ(p, "07hay_stack123-blah");
	EXPECT_EQ(newS, "0");
}

/////////////////// extract after last ///////////////////

TEST(stringManipulationsTests, extractAfterLastExactly) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractAfterLast(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfterLast(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfterLast(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractAfterLast(newS, "-", false);
	EXPECT_EQ(p, "blah");
	EXPECT_EQ(newS, "007hay_stack123-");
	newS = s;
	p    = extractAfterLast(newS, "h", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "007hay_stack123-blah");
	newS = s;
	p    = extractAfterLast(newS, "0", false);
	EXPECT_EQ(p, "7hay_stack123-blah");
	EXPECT_EQ(newS, "00");
	newS = s;
	p    = extractAfterLast(newS, "stack", false);
	EXPECT_EQ(p, "123-blah");
	EXPECT_EQ(newS, "007hay_stack");
	newS = s;
	p    = extractAfterLast(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
}

TEST(stringManipulationsTests, extractAfterLastAny) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractAfterLast(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfterLast(newS, " ", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractAfterLast(newS, "haya", true);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
	newS = s;
	p    = extractAfterLast(newS, "-", true);
	EXPECT_EQ(p, "blah");
	EXPECT_EQ(newS, "007hay_stack123-");
	newS = s;
	p    = extractAfterLast(newS, "h", true);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "007hay_stack123-blah");
	newS = s;
	p    = extractAfterLast(newS, "0", true);
	EXPECT_EQ(p, "7hay_stack123-blah");
	EXPECT_EQ(newS, "00");
	newS = s;
	p    = extractAfterLast(newS, "stack", true);
	EXPECT_EQ(p, "h");
	EXPECT_EQ(newS, "007hay_stack123-bla");
	newS = s;
	p    = extractAfterLast(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, s);
}

TEST(stringManipulationsTests, extractAfterLastChar) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractAfterLast(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = extractAfterLast(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	newS = s;
	p    = extractAfterLast(newS, '-');
	EXPECT_EQ(p, "blah");
	EXPECT_EQ(newS, "007hay_stack123-");
	newS = s;
	p    = extractAfterLast(newS, 'h');
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "007hay_stack123-blah");
	newS = s;
	p    = extractAfterLast(newS, '0');
	EXPECT_EQ(p, "7hay_stack123-blah");
	EXPECT_EQ(newS, "00");
}

TEST(stringManipulationsTests, extractPath) {
	// don't extract
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = extractPath(newS);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do extract
	s    = "/007hay_stack123-blah";
	newS = s;
	p    = extractPath(newS);
	EXPECT_EQ(p, "/");
	EXPECT_EQ(newS, "007hay_stack123-blah");
	s    = "007hay_sta/ck123-blah";
	newS = s;
	p    = extractPath(newS);
	EXPECT_EQ(p, "007hay_sta/");
	EXPECT_EQ(newS, "ck123-blah");
	s    = "007hay_stack123-blah/";
	newS = s;
	p    = extractPath(newS);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	s    = "/007/hay_stack/123-bla/h";
	newS = s;
	p    = extractPath(newS);
	EXPECT_EQ(p, "/007/hay_stack/123-bla/");
	EXPECT_EQ(newS, "h");
}

/////////////////// trim ///////////////////

TEST(stringManipulationsTests, trimString) {
	// don't trim
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	trimString(newS, "j");
	EXPECT_EQ(newS, s);
	newS = s;
	trimString(newS, " ");
	EXPECT_EQ(newS, s);
	newS = s;
	trimString(newS, "-");
	EXPECT_EQ(newS, s); // in the middle
	newS = s;
	trimString(newS, "stack");
	EXPECT_EQ(newS, s); // in the middle

	// do trim
	newS = s;
	trimString(newS, "h");
	EXPECT_EQ(newS, "007hay_stack123-bla"); // trim off end
	newS = s;
	trimString(newS, "0");
	EXPECT_EQ(newS, "7hay_stack123-blah"); // trim off beginning
	newS = s;
	trimString(newS, "007hay_stack123-blah");
	EXPECT_EQ(newS, ""); // trim everything
	s    = "007hay_stack123-blah00";
	newS = s;
	trimString(newS, "0");
	EXPECT_EQ(newS, "7hay_stack123-blah"); // trim off end and beginning
	s    = "007hay_stack123-blah0k0";
	newS = s;
	trimString(newS, "0");
	EXPECT_EQ(newS, "7hay_stack123-blah0k"); // trim off end and beginning
	newS = s;
	trimString(newS, "fd0dhw");
	EXPECT_EQ(newS, "7hay_stack123-blah0k"); // trim off end and beginning
}

TEST(stringManipulationsTests, trimStringWhiteSpace) {
	// don't trim
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	trimString(newS);
	EXPECT_EQ(newS, s);

	// do trim
	s    = "\n007 hay    _st\tack\f123\n-blah\r\v";
	newS = s;
	trimString(newS);
	EXPECT_EQ(newS, "007 hay    _st\tack\f123\n-blah");
}

TEST(stringManipulationsTests, trimStringEndlines) {
	// don't trim
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	trimEndlineString(newS);
	EXPECT_EQ(newS, s);
	s    = " 007 hay    _st\tack\f123\n-blah\r \t";
	newS = s;
	trimEndlineString(newS);
	EXPECT_EQ(newS, s);

	// do trim
	s    = "\n007 hay    _st\tack\f123\n-blah\r\v";
	newS = s;
	trimEndlineString(newS);
	EXPECT_EQ(newS, "007 hay    _st\tack\f123\n-blah");
}

/////////////////// split ///////////////////

TEST(stringManipulationsTests, splitAny) {
	// don't split
	std::string s        = "007hay_stack123-blah";
	auto [before, after] = split<true>(s, "j");
	EXPECT_EQ(before, s);
	EXPECT_EQ(after, "");

	// do split
	std::tie(before, after) = split<true>(s, "haya");
	EXPECT_EQ(before, "007");
	EXPECT_EQ(after, "ay_stack123-blah");

	std::tie(before, after) = split<true>(after, "-");
	EXPECT_EQ(before, "ay_stack123");
	EXPECT_EQ(after, "blah");

	std::tie(before, after) = split<true>(s, "h");
	EXPECT_EQ(before, "007");
	EXPECT_EQ(after, "ay_stack123-blah");

	std::tie(before, after) = split<true>(before, "0");
	EXPECT_EQ(before, "");
	EXPECT_EQ(after, "07");

	std::tie(before, after) = split<true>(s, "stack");
	EXPECT_EQ(before, "007h");
	EXPECT_EQ(after, "y_stack123-blah");

	std::tie(before, after) = split<true>(s, "007hay_stack123-blah");
	EXPECT_EQ(before, "");
	EXPECT_EQ(after, "07hay_stack123-blah");
}

TEST(stringManipulationsTests, splitExactly) {
	std::string s        = "007hay_stack123-blah";
	auto [before, after] = split<false>(s, "j");
	EXPECT_EQ(before, s);
	EXPECT_EQ(after, "");

	// do split
	std::tie(before, after) = split<false>(s, "007");
	EXPECT_EQ(before, "");
	EXPECT_EQ(after, "hay_stack123-blah");

	std::tie(before, after) = split<false>(s, "blah");
	EXPECT_EQ(before, "007hay_stack123-");
	EXPECT_EQ(after, "");

	std::tie(before, after) = split<false>(s, "-");
	EXPECT_EQ(before, "007hay_stack123");
	EXPECT_EQ(after, "blah");

	std::tie(before, after) = split<false>(s, "a");
	EXPECT_EQ(before, "007h");
	EXPECT_EQ(after, "y_stack123-blah");
}

/////////////////// split last ///////////////////

TEST(stringManipulationsTests, splitLastAny) {
	// don't split
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = splitAtLast(newS, "j", true);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, " ");
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do split
	newS = s;
	p    = splitAtLast(newS, "haya", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, "-", true);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = splitAtLast(newS, "h", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, "0", true);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "7hay_stack123-blah");
	newS = s;
	p    = splitAtLast(newS, "stack", true);
	EXPECT_EQ(p, "007hay_stack123-bl");
	EXPECT_EQ(newS, "h");
	newS = s;
	p    = splitAtLast(newS, "007hay_stack123-blah", true);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, splitLastExactly) {
	// don't split
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = splitAtLast(newS, "j", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, " ", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, "haya", false);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do split
	newS = s;
	p    = splitAtLast(newS, "-", false);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = splitAtLast(newS, "h", false);
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, "0", false);
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "7hay_stack123-blah");
	newS = s;
	p    = splitAtLast(newS, "stack", false);
	EXPECT_EQ(p, "007hay_");
	EXPECT_EQ(newS, "123-blah");
	newS = s;
	p    = splitAtLast(newS, "007hay_stack123-blah", false);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "");
}

TEST(stringManipulationsTests, splitLastChar) {
	// don't split
	std::string s    = "007hay_stack123-blah";
	std::string newS = s;
	std::string p    = splitAtLast(newS, 'j');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, ' ');
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do split
	newS = s;
	p    = splitAtLast(newS, '-');
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "blah");
	newS = s;
	p    = splitAtLast(newS, 'h');
	EXPECT_EQ(p, "007hay_stack123-bla");
	EXPECT_EQ(newS, "");
	newS = s;
	p    = splitAtLast(newS, '0');
	EXPECT_EQ(p, "0");
	EXPECT_EQ(newS, "7hay_stack123-blah");
}

TEST(stringManipulationsTests, splitAtPos) {
	// don't split
	std::string s            = "007hay_stack123-blah";
	std::string::size_type l = s.find_first_of('j');

	std::string newS = s;
	std::string p    = splitAtPos(newS, l);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");
	l    = s.find_first_of(' ');
	newS = s;
	p    = splitAtPos(newS, l);
	EXPECT_EQ(p, s);
	EXPECT_EQ(newS, "");

	// do split
	l    = s.find_first_of('h');
	newS = s;
	p    = splitAtPos(newS, l);
	EXPECT_EQ(p, "007");
	EXPECT_EQ(newS, "ay_stack123-blah");
	l    = s.find_first_of('-');
	newS = s;
	p    = splitAtPos(newS, l);
	EXPECT_EQ(p, "007hay_stack123");
	EXPECT_EQ(newS, "blah");
	l    = s.find_first_of('0');
	newS = s;
	p    = splitAtPos(newS, l);
	EXPECT_EQ(p, "");
	EXPECT_EQ(newS, "07hay_stack123-blah");
}

#include "gtest/gtest.h"

#include "coretools/Strings/toBuffer.h"

using namespace coretools;
using namespace str;

TEST(toBufferTests, toBuffer) {
	std::string s;
	toBuffer(std::back_inserter(s), "hallo");
	EXPECT_EQ(s, "hallo");
	s.clear();

	toBuffer(std::back_inserter(s), false);
	EXPECT_EQ(s, "false");
	s.clear();

	toBuffer(std::back_inserter(s), true);
	EXPECT_EQ(s, "true");
	s.clear();

	toBuffer(std::back_inserter(s), 5);
	EXPECT_EQ(s, "5");
	s.clear();

	toBuffer(std::back_inserter(s), 75.3476);
	EXPECT_EQ(s, "75.3476");
	s.clear();

	toBuffer(std::back_inserter(s), std::array<int, 3>({1, 2, 3}));
	EXPECT_EQ(s, "[1, 2, 3]");
	s.clear();

	toBuffer(std::back_inserter(s), 1, 2.2, true, 4, 'a', ",huhu");
	EXPECT_EQ(s, "12.2true4a,huhu");
}

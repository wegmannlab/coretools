#include "gtest/gtest.h"

#include "coretools/Strings/fromString.h"
#include "coretools/Types/commonWeakTypes.h"

using namespace coretools;
using namespace str;

TEST(fromStringTests, string) {
	std::string s;
	fromString("123", s);
	EXPECT_EQ(s, "123");
	fromString<true>("abc", s); // cannot really throw
	EXPECT_EQ(s, "abc");
}
TEST(fromStringTests, int) {
	int i;
	fromString("-123", i);
	EXPECT_EQ(i, -123);
	fromString<true>("3", i);
	EXPECT_EQ(i, 3);
	EXPECT_ANY_THROW(fromString<true>("3.5", i));
	EXPECT_ANY_THROW(fromString<true>("abc", i));
	EXPECT_ANY_THROW(fromString<true>("s3", i));
	EXPECT_ANY_THROW(fromString<true>("3s", i));
	EXPECT_ANY_THROW(fromString<true>("3   ", i));
	EXPECT_ANY_THROW(fromString<true>("12345678901234567890", i));

	unsigned int ui;
	fromString("4854", ui);
	EXPECT_EQ(ui, 4854);
	EXPECT_ANY_THROW(fromString<true>("-1", ui));
}
TEST(fromStringTests, float) {
	float f;
	fromString("-3.56", f);
	EXPECT_FLOAT_EQ(f, -3.56);
	EXPECT_ANY_THROW(fromString<true>("abc", f));
	EXPECT_ANY_THROW(fromString<true>("s3", f));
	EXPECT_ANY_THROW(fromString<true>("3s", f));
	EXPECT_ANY_THROW(fromString<true>("3   ", f));
	EXPECT_ANY_THROW(fromString<true>("nan", f));

	double d;
	fromString("35.88e10", d);
	EXPECT_FLOAT_EQ(d, 35.88e10);
}
TEST(fromStringTests, bool) {
	bool b;
	fromString("true", b);
	EXPECT_TRUE(b);
	fromString("1", b);
	EXPECT_TRUE(b);
	fromString("false", b);
	EXPECT_FALSE(b);
	EXPECT_ANY_THROW(fromString<true>("abc", b));
}
TEST(fromStringTests, vector) {
	std::vector<int> v;
	fromString("1",v);
	EXPECT_EQ(v.size(), 1);
	EXPECT_EQ(v.front(), 1);
	fromString("1,2,3,4",v);
	EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 10);
	fromString("(1 2 3 4)",v);
	EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 10);
	fromString("{1;2;3;4}",v);
	EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 10);
	fromString("1\t2\t3\t4",v);
	EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 10);
	EXPECT_ANY_THROW(fromString<true>("abc", v));

	//Repeated
	fromString("50000{2}", v);
	EXPECT_EQ(v.front(), 50000);
	EXPECT_EQ(v.back(), 50000);
	EXPECT_EQ(v.size(), 2);
	fromString("{10,200{5},3000,40000,500000{2}}", v);
	EXPECT_EQ(v.size(), 10);
	EXPECT_EQ(v.front(), 10);
	EXPECT_EQ(v.back(), 500000);
	EXPECT_EQ(v[1], 200);
	EXPECT_EQ(v[1], v[5]);
	EXPECT_EQ(v[9], v[8]);
}
TEST(fromStringTests, array) {
	std::array<int, 3> a;
	fromString("1,2,3",a);
	EXPECT_EQ(std::accumulate(a.begin(), a.end(), 0), 6);
	fromString("1|2|3",a);
	EXPECT_EQ(std::accumulate(a.begin(), a.end(), 0), 6);
	EXPECT_ANY_THROW(fromString<true>("1,2,3,4", a));

	fromString("5{3}", a);
	EXPECT_EQ(a.front(), 5);
	EXPECT_EQ(a.back(), 5);
	EXPECT_EQ(a.size(), 3);
}

TEST(fromStringTests, fillFromStringWithTypes) {
    coretools::StrictlyPositive val;
	fromString<true>("1.0", val);
}

TEST(fromStringTests, stringToInt) {
	// valid
	EXPECT_EQ(fromString<int>("0"), 0);
	EXPECT_EQ(fromString<int>("1"), 1);
	EXPECT_EQ(fromString<int>("-1"), -1);
	EXPECT_EQ(fromString<int>("10 823095"), 10);
	EXPECT_EQ(fromString<int>("930 goo"), 930);

	// fails
	EXPECT_NO_THROW(fromString<int>("1e+08"));
	EXPECT_NO_THROW(fromString<int>("foo 18"));
	EXPECT_NO_THROW(fromString<int>("1000000000000000000000000000000000"));
}

TEST(fromStringTests, stringToUnsignedInt) {
	// valid
	EXPECT_EQ(fromString<unsigned int>("0"), 0);
	EXPECT_EQ(fromString<unsigned int>("1"), 1);
	EXPECT_EQ(fromString<unsigned int>("10 823095"), 10);
	EXPECT_EQ(fromString<unsigned int>("930 goo"), 930);

	// fails (doesn't throw, but there is undefined behaviour)
	EXPECT_NO_THROW(fromString<unsigned int>("1e+08"));
	EXPECT_NO_THROW(fromString<unsigned int>("foo 18"));
	EXPECT_NO_THROW(fromString<unsigned int>("1000000000000000000000000000000000"));
}

TEST(fromStringTests, stringToLong) {
	// valid
	EXPECT_EQ(fromString<long>("0"), 0);
	EXPECT_EQ(fromString<long>("1"), 1);
	EXPECT_EQ(fromString<long>("-1"), -1);
	EXPECT_EQ(fromString<long>("10 823095"), 10);
	EXPECT_EQ(fromString<long>("930 goo"), 930);

	// fails
	EXPECT_NO_THROW(fromString<long>("1e+08"));
	EXPECT_NO_THROW(fromString<long>("foo 18"));
	EXPECT_NO_THROW(fromString<long>("1000000000000000000000000000000000"));
}

TEST(fromStringTests, stringToDouble) {
	// valid
	EXPECT_EQ(fromString<double>("1"), 1);
	EXPECT_EQ(fromString<double>("0.73"), 0.73);
	EXPECT_EQ(fromString<double>("1.2619"), 1.2619);
	EXPECT_EQ(fromString<double>("-1.32"), -1.32);
	EXPECT_EQ(fromString<double>("10.5782 823095."), 10.5782);
	EXPECT_EQ(fromString<double>("930.23 goo"), 930.23);

	// fails
	EXPECT_NO_THROW(fromString<double>("1e+08"));
	EXPECT_NO_THROW(fromString<double>("foo 18.1"));
	EXPECT_NO_THROW(fromString<double>("1000000000000000000000000000000000."));
}

TEST(fromStringTests, stringToFloat) {
	// valid
	EXPECT_FLOAT_EQ(fromString<float>("1"), 1);
	EXPECT_FLOAT_EQ(fromString<float>("0.73"), 0.73);
	EXPECT_FLOAT_EQ(fromString<float>("1.2619"), 1.2619);
	EXPECT_FLOAT_EQ(fromString<float>("-1.32"), -1.32);
	EXPECT_FLOAT_EQ(fromString<float>("10.5782 823095."), 10.5782);
	EXPECT_FLOAT_EQ(fromString<float>("930.23 goo"), 930.23);

	// fails
	EXPECT_NO_THROW(fromString<float>("1e+08"));
	EXPECT_NO_THROW(fromString<float>("foo 18.1"));
	EXPECT_NO_THROW(fromString<float>("1000000000000000000000000000000000."));
}

TEST(fromStringTests, stringToBool) {
	// valid
	EXPECT_TRUE(fromString<bool>("1"));
	EXPECT_TRUE(fromString<bool>("true"));
	EXPECT_FALSE(fromString<bool>("0"));
	EXPECT_FALSE(fromString<bool>("false"));

	// fails, but doesn't throw
	EXPECT_NO_THROW(fromString<bool>("0129"));
	EXPECT_NO_THROW(fromString<bool>("foo"));
	EXPECT_NO_THROW(fromString<bool>("1000000000000000000000000000000000."));
}

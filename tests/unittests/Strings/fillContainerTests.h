#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "coretools/Strings/fillContainer.h"

using namespace testing;
using namespace coretools;
using namespace str;

TEST(fillContainerTests, fillContainerFromString_Char) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromString(s, vec, ' ');
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromString(s, vec, ' ');
	EXPECT_TRUE(vec.empty());

	// correct string
	s = "0 72 364 -18 894";
	fillContainerFromString(s, vec, ' ');
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces
	s = "   0 72 364 \n -18 894    ";
	fillContainerFromString(s, vec, ' ');
	EXPECT_EQ(vec.size(), 12);

	// weird string
	s = "0 72 foo! -18 894";
	EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ')); // if you want throw, then check!

	// weird string with extra whitespaces
	s = "  0 72 \t  foo! -18  \n 894\n";
	EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ')); // if you want throw, then check!
}

TEST(fillContainerTests, fillContainerFromStringSkipEmpty_Char) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromString(s, vec, ' ', true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromString(s, vec, ' ', true);
	EXPECT_TRUE(vec.empty());

	// correct string
	s = "0 72 364 -18 894";
	fillContainerFromString(s, vec, ' ', true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces
	s = "   0 72 364 \n -18 894    ";
	fillContainerFromString(s, vec, ' ', true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// weird string
	s = "0 72 foo! -18 894";
	EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ', true)); // if you want throw, then check!

	// weird string with extra whitespaces
	s = "  0 72 \t  foo! -18  \n 894\n";
	EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ', true)); // if you want throw, then check!
}

TEST(fillContainerTests, fillContainerFromStringSkipEmptyCheck_Char) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromString(s, vec, ' ', true, true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromString(s, vec, ' ', true, true);
	EXPECT_TRUE(vec.empty());

	// correct string
	s = "0 72 364 -18 894";
	fillContainerFromString(s, vec, ' ', true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces
	s = "   0 72 364 \n -18 894    ";
	fillContainerFromString(s, vec, ' ', true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// weird string
	s = "0 72 foo! -18 894";
	EXPECT_ANY_THROW(fillContainerFromString(s, vec, ' ', true, true));

	// weird string with extra whitespaces
	s = "  0 72 \t  foo! -18  \n 894\n";
	EXPECT_ANY_THROW(fillContainerFromString(s, vec, ' ', true, true));
}

TEST(fillContainerTests, fillContainerFromString_String) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	fillContainerFromString(s, vec, " ");
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {"1", "2", "3"};
	fillContainerFromString(s, vec, " ");
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "0a72ab364ad-18dcab894bad";
	fillContainerFromString(s, vec, "ad");
	EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b"));

	// correct string, ending with non-delim
	s = "0a72ab364ad-18dcab894bad5";
	fillContainerFromString(s, vec, "ad");
	EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b", "5"));

	// string with extra whitespaces, ending with delim
	s = "   0a72ab364\nadad-18dca\tb894    bad";
	fillContainerFromString(s, vec, "ad");
	EXPECT_THAT(vec,
	            ElementsAre("0a72ab364", "", "-18dca\tb894    b")); // if you want correct result, then skip empty!

	// string with extra whitespaces, ending with non-delim
	s = "   0a72ab364\nadad-18dca\tb894    bad5";
	fillContainerFromString(s, vec, "ad");
	EXPECT_THAT(vec,
	            ElementsAre("0a72ab364", "", "-18dca\tb894    b", "5")); // if you want correct result, then skip empty!
}

TEST(fillContainerTests, fillContainerFromStringSkipEmpty_String) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	fillContainerFromString(s, vec, " ", true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {"1", "2", "3"};
	fillContainerFromString(s, vec, " ", true);
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "0a72ab364ad-18dcab894bad";
	fillContainerFromString(s, vec, "ad", true);
	EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b"));

	// correct string, ending with non-delim
	s = "0a72ab364ad-18dcab894bad5";
	fillContainerFromString(s, vec, "ad", true);
	EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b", "5"));

	// string with extra whitespaces, ending with delim
	s = "   0a72ab364\nadad-18dca\tb894    bad";
	fillContainerFromString(s, vec, "ad", true);
	EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dca\tb894    b"));

	// string with extra whitespaces, ending with non-delim
	s = "   0a72ab364\nadad-18dca\tb894    bad5";
	fillContainerFromString(s, vec, "ad", true);
	EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dca\tb894    b", "5"));
}

TEST(fillContainerTests, fillContainerFromString_Any) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringAny(s, vec, " ");
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromStringAny(s, vec, " ");
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "0a72ab364ad-18dcab894bad";
	fillContainerFromStringAny(s, vec, "abcd");
	EXPECT_EQ(vec.size(), 12);

	// correct string, ending with non-delim
	s = "0a72ab364ad-18dcab894bad5";
	fillContainerFromStringAny(s, vec, "abcd");
	EXPECT_EQ(vec.size(), 13);

	// string with extra whitespaces, ending with delim
	s = "   0a72ab364\nad-18dca\tb894    bad";
	fillContainerFromStringAny(s, vec, "abcd");
	EXPECT_EQ(vec.size(), 12);

	// string with extra whitespaces, ending with non-delim
	s = "   0a72ab364\nad-18dca\tb894    bad5";
	fillContainerFromStringAny(s, vec, "abcd");
	EXPECT_EQ(vec.size(), 13);

	// weird string
	s = "0a72ab364ad-18dcab894foo!bad";
	EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd")); // if you want throw, then check!

	// weird string with extra whitespaces
	s = "   0a72ab364\nad-18dca\tbfoo!894    bad";
	EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd")); // if you want throw, then check!
}

TEST(fillContainerTests, fillContainerFromStringSkipEmpty_Any) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringAny(s, vec, " ", true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromStringAny(s, vec, " ", true);
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "0a72ab364ad-18dcab894bad";
	fillContainerFromStringAny(s, vec, "abcd", true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// correct string, ending with non-delim
	s = "0a72ab364ad-18dcab894bad5";
	fillContainerFromStringAny(s, vec, "abcd", true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

	// string with extra whitespaces, ending with delim
	s = "   0a72ab364\nad-18dca\tb894    bad";
	fillContainerFromStringAny(s, vec, "abcd", true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces, ending with non-delim
	s = "   0a72ab364\nad-18dca\tb894    bad5";
	fillContainerFromStringAny(s, vec, "abcd", true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

	// weird string
	s = "0a72ab364ad-18dcab894foo!bad";
	EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd", true)); // if you want throw, then check!

	// weird string with extra whitespaces
	s = "   0a72ab364\nad-18dca\tbfoo!894    bad";
	EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd", true)); // if you want throw, then check!
}

TEST(fillContainerTests, fillContainerFromStringSkipEmptyCheck_Any) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringAny(s, vec, " ", true, true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromStringAny(s, vec, " ", true, true);
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "0a72ab364ad-18dcab894bad";
	fillContainerFromStringAny(s, vec, "abcd", true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// correct string, ending with non-delim
	s = "0a72ab364ad-18dcab894bad5";
	fillContainerFromStringAny(s, vec, "abcd", true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

	// string with extra whitespaces, ending with delim
	s = "   0a72ab364\nad-18dca\tb894    bad";
	fillContainerFromStringAny(s, vec, "abcd", true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces, ending with non-delim
	s = "   0a72ab364\nad-18dca\tb894    bad5";
	fillContainerFromStringAny(s, vec, "abcd", true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

	// weird string
	s = "0a72ab364ad-18dcab894foo!bad";
	EXPECT_ANY_THROW(fillContainerFromStringAny(s, vec, "abcd", true, true));

	// weird string with extra whitespaces
	s = "   0a72ab364\nad-18dca\tbfoo!894    bad";
	EXPECT_ANY_THROW(fillContainerFromStringAny(s, vec, "abcd", true, true));
}

// void fillContainerFromStringRepeatIndexes(std::string String, ContainerType<StorageType> & Container, const
// std::string & Delim, bool SkipEmpty = false, bool Check = false, bool Any = false){
TEST(fillContainerTests, fillContainerFromStringRepeatIndexes) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringRepeatIndexes(s, vec, ",", true, true, false);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 5, 10};
	fillContainerFromStringRepeatIndexes(s, vec, ",", true, true, false);
	EXPECT_TRUE(vec.empty());

	// indexes are repeated
	s = "10{2},5,,2{3},1{1}";
	fillContainerFromStringRepeatIndexes(s, vec, ",", true, true, false);
	EXPECT_THAT(vec, ElementsAre(10, 10, 5, 2, 2, 2, 1));

	// weird string
	s = "10{2},5b,,2{3},1{1}";
	EXPECT_ANY_THROW(
	    fillContainerFromStringRepeatIndexes(s, vec, ",", true, true, false)); // if you want throw, then check!
}

//////////////// whitespaces ////////////////
TEST(fillContainerTests, fillContainerFromStringWhiteSpace) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringWhiteSpace(s, vec);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromStringWhiteSpace(s, vec);
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "\r0\t72\t364 -18 894 ";
	fillContainerFromStringWhiteSpace(s, vec);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// correct string, ending with non-delim
	s = "\r0\t72\t364 -18 894 5";
	fillContainerFromStringWhiteSpace(s, vec);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

	// string with extra whitespaces, ending with delim
	s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894\n";
	fillContainerFromStringWhiteSpace(s, vec);
	EXPECT_EQ(vec.size(), 11);

	// string with extra whitespaces, ending with non-delim
	s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894";
	fillContainerFromStringWhiteSpace(s, vec);
	EXPECT_THAT(vec, ElementsAre(0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 894));

	// weird string
	s = "\r0\t72\rfoooo!364\v-18 894\n";
	EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec)); // if you want throw, then check!

	// weird string with extra whitespaces
	s = "\r 0 \t\t foooo!  \n 72\r364\n\v-18 \t 894";
	EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec)); // if you want throw, then check!
}

TEST(fillContainerTests, fillContainerFromStringWhiteSpaceSkipEmpty) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringWhiteSpace(s, vec, true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromStringWhiteSpace(s, vec, true);
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "\r0\t72\t364 -18 894\n";
	fillContainerFromStringWhiteSpace(s, vec, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// correct string, ending with non-delim
	s = "\r0\t72\t364 -18 894\n";
	fillContainerFromStringWhiteSpace(s, vec, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces, ending with delim
	s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894\n";
	fillContainerFromStringWhiteSpace(s, vec, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 894));

	// string with extra whitespaces, ending with non-delim
	s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894";
	fillContainerFromStringWhiteSpace(s, vec, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 894));

	// weird string
	s = "\r0\t72\rfoooo!364\v-18 894\n";
	EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec, true)); // if you want throw, then check!

	// weird string with extra whitespaces
	s = "\r 0 \t\t foooo!  \n 72\r364\n\v-18 \t 894";

	EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec, true)); // if you want throw, then check!
}

TEST(fillContainerTests, fillContainerFromStringWhiteSpaceSkipEmptyCheck) {
	// empty string
	std::string s;
	std::vector<int> vec;
	fillContainerFromStringWhiteSpace(s, vec, true, true);
	EXPECT_TRUE(vec.empty());

	// vec is cleared
	vec = {1, 2, 3};
	fillContainerFromStringWhiteSpace(s, vec, true, true);
	EXPECT_TRUE(vec.empty());

	// correct string, ending with delim
	s = "\r0\t72\t364 -18 894\n";
	fillContainerFromStringWhiteSpace(s, vec, true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// correct string, ending with non-delim
	s = "\r0\t72\t364 -18 894\n";
	fillContainerFromStringWhiteSpace(s, vec, true, true);
	EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

	// string with extra whitespaces, ending with delim
	s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894\n";
	EXPECT_ANY_THROW(fillContainerFromStringWhiteSpace(s, vec, true, true));

	// string with extra whitespaces, ending with non-delim
	s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894";
	EXPECT_ANY_THROW(fillContainerFromStringWhiteSpace(s, vec, true, true));

	// weird string
	s = "\r0\t72\rfoooo!364\v-18 894\n";
	EXPECT_ANY_THROW(fillContainerFromStringWhiteSpace(s, vec, true, true));

	// weird string with extra whitespaces
	s = "\r 0 \t\t foooo!  \n 72\r364\n\v-18 \t 894";
	EXPECT_ANY_THROW(fillContainerFromStringWhiteSpace(s, vec, true, true));
}

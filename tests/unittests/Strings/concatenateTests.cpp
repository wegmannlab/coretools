
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "coretools/Strings/concatenateString.h"

using namespace testing;

using namespace coretools;
using namespace str;


//-------------------------------------------------
// Concatenation
//-------------------------------------------------

TEST(concatenateTests, concatenateString) {
	std::vector<int> vec;
	EXPECT_EQ(concatenateString(vec, ", "), "");

	vec = {0, 72, 364, -18, 894};
	EXPECT_EQ(concatenateString(vec, ", "), "0, 72, 364, -18, 894");
	EXPECT_EQ(concatenateString(vec, "."), "0.72.364.-18.894");
	EXPECT_EQ(concatenateString(vec), "072364-18894");

	std::string s;
	fillConcatenatedString(vec, s);
	EXPECT_EQ(s, "072364-18894");

	s.clear();
	fillConcatenatedString(vec, s, ", ");
	EXPECT_EQ(s, "0, 72, 364, -18, 894");
}

TEST(concatenateTests, concatenateStringFrom) {
	std::vector<int> vec;
	EXPECT_EQ(concatenateString(vec, ", ", 100), "");

	vec = {0, 72, 364, -18, 894};
	EXPECT_EQ(concatenateString(vec, ", ", 0), "0, 72, 364, -18, 894");
	EXPECT_EQ(concatenateString(vec, ", ", 2), "364, -18, 894");
	EXPECT_EQ(concatenateString(vec, ", ", 4), "894");
	EXPECT_EQ(concatenateString(vec, ", ", 5), "");
	EXPECT_EQ(concatenateString(vec, ", ", 100), "");
	EXPECT_EQ(concatenateString(vec, 2), "364-18894");

	std::string s;
	fillConcatenatedString(vec, s, 2);
	EXPECT_EQ(s, "364-18894");

	s.clear();
	fillConcatenatedString(vec, s, ", ", 2);
	EXPECT_EQ(s, "364, -18, 894");
}

TEST(concatenateTests, paste_equalLength) {
	std::vector<int> vec1         = {1, 2, 3, 4, 5};
	std::vector<std::string> vec2 = {"A", "B", "C", "D", "E"};

	std::vector<std::string> result = paste(vec1, vec2, "_");

	EXPECT_THAT(result, ElementsAre("1_A", "2_B", "3_C", "4_D", "5_E"));
}

TEST(concatenateTests, paste_firstIsLonger) {
	std::vector<int> vec1         = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<std::string> vec2 = {"A", "B", "C", "D", "E"};

	std::vector<std::string> result = paste(vec1, vec2, "_");

	EXPECT_THAT(result, ElementsAre("1_A", "2_B", "3_C", "4_D", "5_E", "6_A", "7_B", "8_C", "9_D", "10_E"));
}

TEST(concatenateTests, paste_secondIsLonger) {
	std::vector<int> vec1         = {1, 2, 3, 4, 5};
	std::vector<std::string> vec2 = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

	std::vector<std::string> result = paste(vec1, vec2, "_");

	EXPECT_THAT(result, ElementsAre("1_A", "2_B", "3_C", "4_D", "5_E", "1_F", "2_G", "3_H", "4_I", "5_J"));
}

TEST(concatenateTests, paste_throw_notMultiple) {
	std::vector<int> vec1         = {1, 2, 3, 4, 5};
	std::vector<std::string> vec2 = {"A", "B", "C", "D", "E", "F"}; // 1 element longer

	EXPECT_ANY_THROW(paste(vec1, vec2, "_"));
}

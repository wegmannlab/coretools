#include "gtest/gtest.h"

#include <string>
#include "coretools/Strings/fromString.h"
#include "coretools/Strings/splitters.h"

using namespace coretools;
using namespace str;

TEST(splitterTests, TSplitter_char) {
	constexpr auto s = "ab c defg";
	TSplitter sp(s, ' ');

	EXPECT_EQ(sp.front(), "ab");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "c");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "defg");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_TRUE(sp.empty());

	constexpr auto e = "123";
	TSplitter spe(e, '\t');
	EXPECT_EQ(spe.front(), "123");
	EXPECT_FALSE(spe.empty());
	spe.popFront();
	EXPECT_TRUE(spe.empty());
}

TEST(splitterTests, TSplitter_str) {
	constexpr auto s = "AB...C...DEFG";
	TSplitter<std::string> sp(s, "...");
	EXPECT_EQ(sp.front(), "AB");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "C");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "DEFG");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_TRUE(sp.empty());

	constexpr auto e = "123";
	TSplitter<std::string> spe(e, "##");
	EXPECT_EQ(spe.front(), "123");
	EXPECT_FALSE(spe.empty());
	spe.popFront();
	EXPECT_TRUE(spe.empty());
}

TEST(splitterTests, TSplitter_str_any) {
	TSplitter<std::string, true> sp("A,B.C|D,.EFG", ",.|");
	EXPECT_EQ(sp.front(), "A");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "B");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "C");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "D");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "EFG");
	EXPECT_FALSE(sp.empty());


	sp.popFront();
	EXPECT_TRUE(sp.empty());
}

TEST(splitterTests, TReverseSplitter_char) {
	constexpr auto s1 = "ab c defg";
	TReverseSplitter sp1(s1, ' ');
	EXPECT_EQ(sp1.front(), "defg");
	EXPECT_FALSE(sp1.empty());
	sp1.popFront();
	EXPECT_EQ(sp1.front(), "c");
	EXPECT_FALSE(sp1.empty());
	sp1.popFront();
	EXPECT_EQ(sp1.front(), "ab");
	EXPECT_FALSE(sp1.empty());
	sp1.popFront();
	EXPECT_TRUE(sp1.empty());


	constexpr auto e = "123";
	TReverseSplitter spe(e, '\n');
	EXPECT_EQ(spe.front(), "123");
	EXPECT_FALSE(spe.empty());
	spe.popFront();
	EXPECT_TRUE(spe.empty());
}

TEST(splitterTests, TReverseSplitter_str) {
	constexpr auto s = "AB...C...DEFG";
	TReverseSplitter<std::string> sp(s, "...");
	EXPECT_EQ(sp.front(), "DEFG");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "C");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "AB");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_TRUE(sp.empty());

	constexpr auto e = "123";
	TReverseSplitter<std::string> spe(e, "END");
	EXPECT_EQ(spe.front(), "123");
	EXPECT_FALSE(spe.empty());
	spe.popFront();
	EXPECT_TRUE(spe.empty());
}

TEST(splitterTests, TReverseSplitter_str_any) {

	TReverseSplitter<std::string, true> sp("A,B.C|D,.EFG", ",.|");
	EXPECT_EQ(sp.front(), "EFG");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "D");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "C");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "B");
	EXPECT_FALSE(sp.empty());

	sp.popFront();
	EXPECT_EQ(sp.front(), "A");
	EXPECT_FALSE(sp.empty());


	sp.popFront();
	EXPECT_TRUE(sp.empty());
}

TEST(splitterTests, TWhitespaceSplitter) {
	TWhitespaceSplitter sp("  \t ab\tc defg");

	EXPECT_EQ(sp.front(), "ab");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "c");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_EQ(sp.front(), "defg");
	EXPECT_FALSE(sp.empty());
	sp.popFront();
	EXPECT_TRUE(sp.empty());

	TWhitespaceSplitter spe("123");
	EXPECT_EQ(spe.front(), "123");
	EXPECT_FALSE(spe.empty());
	spe.popFront();
	EXPECT_TRUE(spe.empty());
}

TEST(splitterTests, skip) {
	constexpr auto s = "1,2,3,4,5,6";
	TSplitter sp(s, ',');
	EXPECT_EQ(sp.front(), "1");
	skip(sp);
	EXPECT_EQ(sp.front(), "2");
	skip(sp, 2);
	EXPECT_EQ(sp.front(), "4");
	skip(sp, 3);
	EXPECT_TRUE(sp.empty());
}

TEST(splitterTests, rangeLoop) {
	constexpr auto s = "0,1,2,3,4,5,6";
	int i = 0;
	for (auto si: TSplitter(s, ',')) {
		int ii;
		fromString(si, ii);
		EXPECT_EQ(i, ii);
		++i;
	}
	for (auto si: TReverseSplitter(s, ',')) {
		--i;
		int ii;
		fromString(si, ii);
		EXPECT_EQ(i, ii);
	}

	i = 10;
	for (auto si: TSplitter<std::string>("10<|>20<|>30<|>40<|>50", "<|>")) {
		int ii;
		fromString(si, ii);
		EXPECT_EQ(i, ii);
		i += 10;
	}

	char c = 'a';
	for (auto si: TReverseSplitter<std::string>("e+++d+++c+++b+++a", "+++")) {
		EXPECT_EQ(c, si[0]);
		++c;
	}
}

TEST(splitterTest, algorithms) {
	auto spl = TSplitter("0,1,2,3,4,5", ',');

	std::array<std::string_view, 6> res{"0", "1", "2", "3", "4", "5"};
	EXPECT_TRUE(std::equal(spl.begin(), spl.end(), res.begin()));

	spl = TSplitter("a.b.c.d.e.f", '.');
	std::copy(spl.begin(), spl.end(), res.begin());
	EXPECT_EQ(res.front(), "a");

	spl = TSplitter("0,1,2,3,4,5", ',');
	std::array<int, 6> ar1;
	std::transform(spl.begin(), spl.end(), ar1.begin(), [](auto s) {
		int i;
		fromString(s, i);
		return i;
	});
	std::array<int, 6> ar2;
	auto rspl = TReverseSplitter("5|4|3|2|1|0", '|');
	std::transform(rspl.begin(), rspl.end(), ar2.begin(), [](auto s) {
		int i;
		fromString(s, i);
		return i;
	});
	EXPECT_TRUE(std::equal(ar1.begin(), ar1.end(), ar2.begin()));
}


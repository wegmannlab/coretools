//
// Created by madleina on 08.06.20.
//

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "coretools/Strings/repeatString.h"
#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"

using namespace testing;

using namespace coretools;
using namespace str;


//-------------------------------------------------
// Split into vector/set/array etc.
//-------------------------------------------------

// will test only with vectors, but behaviour is the same for std::set etc.


//-------------------------------------------
// Reading distribution parameters
//-------------------------------------------


//-------------------------------------------------
// Sequences and indexes
//-------------------------------------------------


TEST(repeatStringTests, repeat) {
    EXPECT_EQ(repeat("blub", 0), "");
    EXPECT_EQ(repeat("blub", 1), "blub");
    EXPECT_EQ(repeat("blub", 2), "blub,blub");
    EXPECT_EQ(repeat("blub", 3), "blub,blub,blub");

	// different delim
	EXPECT_EQ(repeat("blub", 0, '_'), "");
	EXPECT_EQ(repeat("blub", 1, '_'), "blub");
	EXPECT_EQ(repeat("blub", 2, '_'), "blub_blub");
	EXPECT_EQ(repeat("blub", 3, '_'), "blub_blub_blub");
}

TEST(repeatStringTests, addRepeatedIndexIfRepeated) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	EXPECT_FALSE(addRepeatedIndexIfRepeated(s, vec));

	// invalid strings
	s = "blub";
	EXPECT_FALSE(addRepeatedIndexIfRepeated(s, vec));
	s = "{4}blub";
	EXPECT_FALSE(addRepeatedIndexIfRepeated(s, vec));
	s = "b{lub{4}";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub}4{";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub{4}blub";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub{[4]}";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub{4]";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub{0}";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub{bar4}";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));
	s = "blub{4bar}";
	EXPECT_ANY_THROW(addRepeatedIndexIfRepeated(s, vec));

	// valid strings
	s = "blub{4}";
	addRepeatedIndexIfRepeated(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));
}

TEST(repeatStringTests, addExpandedIndexIfToExpand) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	EXPECT_FALSE(addExpandedIndexIfToExpand(s, vec));

	// invalid strings
	s = "blub";
	vec.clear();
	EXPECT_FALSE(addExpandedIndexIfToExpand(s, vec));
	s = "b[lub[4]";
	vec.clear();
	EXPECT_ANY_THROW(addExpandedIndexIfToExpand(s, vec));
	s = "blub[4";
	vec.clear();
	EXPECT_ANY_THROW(addExpandedIndexIfToExpand(s, vec));
	s = "blub[4]]";
	vec.clear();
	EXPECT_ANY_THROW(addExpandedIndexIfToExpand(s, vec));
	s = "blub[[4]]";
	vec.clear();
	EXPECT_ANY_THROW(addExpandedIndexIfToExpand(s, vec));
	s = "blub]4[";
	vec.clear();
	EXPECT_ANY_THROW(addExpandedIndexIfToExpand(s, vec));
	s = "blub{4]";
	vec.clear();
	EXPECT_FALSE(addExpandedIndexIfToExpand(s, vec));
	s = "blub[0]";
	vec.clear();
	EXPECT_ANY_THROW(addExpandedIndexIfToExpand(s, vec));
	s = "blub[bar4]";
	vec.clear();

	// valid strings
	s = "[4]blub";
	vec.clear();
	addExpandedIndexIfToExpand(s, vec);
	EXPECT_THAT(vec, ElementsAre("1blub", "2blub", "3blub", "4blub"));

	s = "blub[4]";
	vec.clear();
	addExpandedIndexIfToExpand(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));

	s = "blub[4]blub";
	vec.clear();
	addExpandedIndexIfToExpand(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub1blub", "blub2blub", "blub3blub", "blub4blub"));

	s = "blub{[4]}";
	vec.clear();
	addExpandedIndexIfToExpand(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub{1}", "blub{2}", "blub{3}", "blub{4}"));
}

TEST(repeatStringTests, addRepeatedIndex) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	addRepeatedIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre(""));

	// invalid strings
	s = "blub";
	vec.clear();
	addRepeatedIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub"));
	s = "{4}blub";
	vec.clear();
	addRepeatedIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("{4}blub"));

	// valid strings
	s = "blub{4}";
	vec.clear();
	addRepeatedIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));
}

TEST(repeatStringTests, addExpandIndex) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	addExpandIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre(""));

	// invalid strings
	s = "blub";
	vec.clear();
	addExpandIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub"));
	s = "blub{4]";
	vec.clear();
	addExpandIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub{4]"));

	// valid strings
	s = "blub[4]";
	vec.clear();
	addExpandIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));
}

TEST(repeatStringTests, addRepeatedAndExpandIndexes) {
	// empty string
	std::string s;
	std::vector<std::string> vec;
	addRepeatedAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre(""));

	// invalid strings
	s = "blub";
	vec.clear();
	addRepeatedAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub"));
	s = "{4}blub";
	vec.clear();
	addRepeatedIndex(s, vec);
	EXPECT_THAT(vec, ElementsAre("{4}blub"));

	// valid strings
	s = "blub{4}";
	vec.clear();
	addRepeatedAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));

	s = "blub[4]";
	vec.clear();
	addRepeatedAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));
}

TEST(repeatStringTests, repeatAndExpandIndexes) {
	// empty string
	std::vector<std::string> s;
	std::vector<std::string> vec;
	repeatAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre());

	// invalid strings
	s.clear();
	s.emplace_back("blub");
	vec.clear();
	repeatAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub"));
	s.clear();
	s.emplace_back("{4}blub");
	vec.clear();
	repeatAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("{4}blub"));

	// valid strings
	s.clear();
	s.emplace_back("blub{4}");
	vec.clear();
	repeatAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));

	s.clear();
	s.emplace_back("blub[4]");
	vec.clear();
	repeatAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));

	s.clear();
	s.emplace_back("blub[4]");
	s.emplace_back("bluh{3}");
	s.emplace_back("blu[1]");
	vec.clear();
	repeatAndExpandIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4", "bluh", "bluh", "bluh", "blu1"));
}

TEST(repeatStringTests, repeatIndexesStrings) {
	// empty string
	std::vector<std::string> s;
	std::vector<std::string> vec;
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre());

	// invalid strings
	s.clear();
	s.emplace_back("blub");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub"));
	s.clear();
	s.emplace_back("{4}blub");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("{4}blub"));

	// valid strings
	s.clear();
	s.emplace_back("blub{4}");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));

	s.clear();
	s.emplace_back("blab{2}");
	s.emplace_back("bluh{3}");
	s.emplace_back("blu{1}");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre("blab", "blab", "bluh", "bluh", "bluh", "blu"));
}

TEST(repeatStringTests, repeatIndexesInts) {
	// empty string
	std::vector<std::string> s;
	std::vector<int> vec;
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre());

	// invalid strings
	s.clear();
	s.emplace_back("3");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre(3));
	s.clear();
	s.emplace_back("{4}2");
	vec.clear();
	EXPECT_ANY_THROW(repeatIndexes(s, vec, true));

	// valid strings
	s.clear();
	s.emplace_back("4{4}");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre(4, 4, 4, 4));

	s.clear();
	s.emplace_back("6{2}");
	s.emplace_back("-102{3}");
	s.emplace_back("1047{1}");
	vec.clear();
	repeatIndexes(s, vec);
	EXPECT_THAT(vec, ElementsAre(6, 6, -102, -102, -102, 1047));
}

TEST(repeatStringTests, expandOverMultiDimensions) {
	std::vector<int> vec = {3};

	// 1D
	std::vector<std::string> res = expandOverMultiDimensions(vec);
	EXPECT_THAT(res, ElementsAre("0", "1", "2"));

	// 2D
	vec = {3, 2};
	res = expandOverMultiDimensions(vec);
	EXPECT_THAT(res, ElementsAre("0_0", "0_1", "1_0", "1_1", "2_0", "2_1"));

	// 3D
	vec = {3, 2, 2};
	res = expandOverMultiDimensions(vec);
	EXPECT_THAT(res, ElementsAre("0_0_0", "0_0_1", "0_1_0", "0_1_1", "1_0_0", "1_0_1", "1_1_0", "1_1_1", "2_0_0",
	                             "2_0_1", "2_1_0", "2_1_1"));
}

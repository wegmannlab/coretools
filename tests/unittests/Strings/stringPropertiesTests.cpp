#include "gtest/gtest.h"

#include "coretools/Strings/stringProperties.h"

using namespace coretools;
using namespace str;

//-------------------------------------------------
// String contains
//-------------------------------------------------

TEST(stringPropertiesTests, stringContainsString) {
	// evaluates true
	std::string s      = "foo12-3foo\tblah\n892 ";
	std::string needle = "f";
	EXPECT_TRUE(stringContains(s, needle));
	needle = "foo";
	EXPECT_TRUE(stringContains(s, needle));
	needle = "12-3";
	EXPECT_TRUE(stringContains(s, needle));
	needle = "\t";
	EXPECT_TRUE(stringContains(s, needle));
	needle = "\n";
	EXPECT_TRUE(stringContains(s, needle));
	needle = " ";
	EXPECT_TRUE(stringContains(s, needle));

	// evaluates false
	needle = "j";
	EXPECT_FALSE(stringContains(s, needle));
	needle = "123";
	EXPECT_FALSE(stringContains(s, needle));
	s = "";
	EXPECT_FALSE(stringContains(s, needle));
}

TEST(stringPropertiesTests, stringContainsAny) {
	// evaluates true
	std::string s      = "foo12-3foo\tblah\n892 ";
	std::string needle = "f";
	EXPECT_TRUE(stringContainsAny(s, needle));
	needle = "foo";
	EXPECT_TRUE(stringContainsAny(s, needle));
	needle = "12-3";
	EXPECT_TRUE(stringContainsAny(s, needle));
	needle = "\t";
	EXPECT_TRUE(stringContainsAny(s, needle));
	needle = "\n";
	EXPECT_TRUE(stringContainsAny(s, needle));
	needle = " ";
	EXPECT_TRUE(stringContainsAny(s, needle));
	needle = "12345678910";
	EXPECT_TRUE(stringContainsAny(s, needle));

	// evaluates false
	needle = "j";
	EXPECT_FALSE(stringContainsAny(s, needle));
	needle = "567";
	EXPECT_FALSE(stringContainsAny(s, needle));
	s = "";
	EXPECT_FALSE(stringContainsAny(s, needle));
}

TEST(stringPropertiesTests, stringContainsChar) {
	// evaluates true
	std::string s = "foo12-3foo\tblah\n892 ";
	char needle   = 'f';
	EXPECT_TRUE(stringContains(s, needle));
	needle = '1';
	EXPECT_TRUE(stringContains(s, needle));
	needle = '-';
	EXPECT_TRUE(stringContains(s, needle));
	needle = '\t';
	EXPECT_TRUE(stringContains(s, needle));
	needle = '\n';
	EXPECT_TRUE(stringContains(s, needle));
	needle = ' ';
	EXPECT_TRUE(stringContains(s, needle));

	// evaluates false
	needle = 'j';
	EXPECT_FALSE(stringContains(s, needle));
	needle = '5';
	EXPECT_FALSE(stringContains(s, needle));
	s = "";
	EXPECT_FALSE(stringContains(s, needle));
}

TEST(stringPropertiesTests, stringContainsOnly) {
	// evaluates true
	std::string s      = "aaaaaa";
	std::string needle = "a";
	EXPECT_TRUE(stringContainsOnly(s, needle));
	s      = "            ";
	needle = "  ";
	EXPECT_TRUE(stringContainsOnly(s, needle));
	s      = "123123123123";
	needle = "321";
	EXPECT_TRUE(stringContainsOnly(s, needle));
	s      = "1234827105749204828481946593272372917174901";
	needle = "1234567890";
	EXPECT_TRUE(stringContainsOnly(s, needle));

	// evaluates false
	s = "";
	EXPECT_FALSE(stringContainsOnly(s, needle));
	s      = "aaaaaaaaaabaaaaaaaaa";
	needle = "a";
	EXPECT_FALSE(stringContainsOnly(s, needle));
	s      = "123123123123";
	needle = "31";
	EXPECT_FALSE(stringContainsOnly(s, needle));
}

TEST(stringPropertiesTests, stringStartsWithString) {
	// evaluates true
	std::string s      = "foo12-3foo\tblah\n892 ";
	std::string needle = "f";
	EXPECT_TRUE(stringStartsWith(s, needle));
	needle = "foo12";
	EXPECT_TRUE(stringStartsWith(s, needle));

	// evaluates false
	needle = "a";
	EXPECT_FALSE(stringStartsWith(s, needle));
	needle = "foo123";
	EXPECT_FALSE(stringStartsWith(s, needle));
}

TEST(stringPropertiesTests, stringStartsWithChar) {
	// evaluates true
	std::string s = "foo12-3foo\tblah\n892 ";
	char needle   = 'f';
	EXPECT_TRUE(stringStartsWith(s, needle));

	// evaluates false
	needle = '1';
	EXPECT_FALSE(stringStartsWith(s, needle));
}

TEST(stringPropertiesTests, allEntriesAreUnique) {
	// evaluates true
	std::vector<double> vec = {1.1, -0.24, 10.372, 1.111};
	EXPECT_TRUE(allEntriesAreUnique(vec));
	std::vector<std::string> vec2 = {"one", "two", "three", "four"};
	EXPECT_TRUE(allEntriesAreUnique(vec2));

	// evaluates false
	vec.push_back(-0.24);
	EXPECT_FALSE(allEntriesAreUnique(vec));
	vec2.emplace_back("three");
	EXPECT_FALSE(allEntriesAreUnique(vec2));
}

TEST(stringPropertiesTests, getIteratorToFirstNotUniqueEntry) {
	// not found
	std::vector<double> vec = {1.1, -0.24, 10.372, 1.111};
	auto it                 = vec.end();
	EXPECT_EQ(getIteratorToFirstNonUniqueEntry(vec), it);

	std::vector<std::string> vec2 = {"one", "two", "three"};
	auto it2                      = vec2.end();
	EXPECT_EQ(getIteratorToFirstNonUniqueEntry(vec2), it2);

	// found
	vec.push_back(10.372);
	vec.push_back(7.1);
	vec.push_back(1.111);
	it = vec.begin();
	it += 4;
	EXPECT_EQ(*getIteratorToFirstNonUniqueEntry(vec), *it);

	vec2.emplace_back("two");
	vec2.emplace_back("one");
	it2 = vec2.begin();
	it += 3;
	EXPECT_EQ(*getIteratorToFirstNonUniqueEntry(vec2), *it2);
}

TEST(stringPropertiesTests, findNthInstanceInString_Char) {
	std::string haystack = "one,two,three\tfour,five\t,seven,";
	char needle          = ',';

	EXPECT_EQ(findNthInstanceInString(0, haystack, needle), std::string::npos);
	EXPECT_EQ(findNthInstanceInString(1, haystack, needle), 3);
	EXPECT_EQ(findNthInstanceInString(2, haystack, needle), 7);
	EXPECT_EQ(findNthInstanceInString(3, haystack, needle), 18);
	EXPECT_EQ(findNthInstanceInString(4, haystack, needle), 24);
	EXPECT_EQ(findNthInstanceInString(5, haystack, needle), 30);
	EXPECT_EQ(findNthInstanceInString(6, haystack, needle), std::string::npos);
}

TEST(stringPropertiesTests, findNthInstanceInString_String) {
	std::string haystack = "one,two,three\tfour,five\t,seven,";
	std::string needle   = ",";

	EXPECT_EQ(findNthInstanceInString(0, haystack, needle), std::string::npos);
	EXPECT_EQ(findNthInstanceInString(1, haystack, needle), 3);
	EXPECT_EQ(findNthInstanceInString(2, haystack, needle), 7);
	EXPECT_EQ(findNthInstanceInString(3, haystack, needle), 18);
	EXPECT_EQ(findNthInstanceInString(4, haystack, needle), 24);
	EXPECT_EQ(findNthInstanceInString(5, haystack, needle), 30);
	EXPECT_EQ(findNthInstanceInString(6, haystack, needle), std::string::npos);
}

TEST(stringPropertiesTests, levenshteinDistance) {
	EXPECT_EQ(levenshteinDistance("abc", ""), 3);
	EXPECT_EQ(levenshteinDistance("hello", "12345"), 5);
	EXPECT_EQ(levenshteinDistance("jein", "ja"), 3);
	EXPECT_EQ(levenshteinDistance("abcdefghijk", "0abcfghik1"), 5);

	// no substitutions
	EXPECT_EQ(levenshteinDistance<false>("abc", ""), 3);
	EXPECT_EQ(levenshteinDistance<false>("hello", "12345"), 10);
	EXPECT_EQ(levenshteinDistance<false>("jein", "ja"), 4);
	EXPECT_EQ(levenshteinDistance<false>("abcdefghijk", "0abcfghik1"), 5);

	// matching reward
	EXPECT_EQ(levenshteinDistance("abc", "", 0.5), 3);
	EXPECT_EQ(levenshteinDistance("hello", "12345", 0.5), 5);
	EXPECT_EQ(levenshteinDistance("jein", "ja", 0.5), 2.5);
	EXPECT_EQ(levenshteinDistance("abcdefghijk", "0abcfghik1", 0.5), 1);

	// no substitutions, matching reward
	EXPECT_EQ(levenshteinDistance<false>("abc", "", 0.5), 3);
	EXPECT_EQ(levenshteinDistance<false>("hello", "12345", 0.5), 10);
	EXPECT_EQ(levenshteinDistance<false>("jein", "ja", 0.5), 3.5);
	EXPECT_EQ(levenshteinDistance<false>("abcdefghijk", "0abcfghik1", 0.5), 1);
}

TEST(stringPropertiesTests, findClosestMatchLevenshtein) {
	std::string needle = "abcdef";
	std::vector<std::string> haystack = { "123456", "uvwxyz", "bcdefg", "LMNOPQ"};

	auto match = findClosestMatchLevenshtein(needle, haystack);

	EXPECT_EQ(match.first, haystack[2]);
	EXPECT_EQ(match.second, 2);
}

template<typename T> class EqualAs : public ::testing::Test {
public:
	bool areEqual_maxDiff0          = equalAs<T>("1", "1", static_cast<T>(0));
	bool areEqual_maxDiffSmall      = equalAs<T>("1", "1", static_cast<T>(0.000001));
	bool areEqual_maxDiffBig        = equalAs<T>("1", "1", static_cast<T>(100));
	bool areNotEqual_maxDiff0       = equalAs<T>("1", "2", static_cast<T>(0));
	bool areNotEqual_maxDiffSmall   = equalAs<T>("1", "2", static_cast<T>(0.000001));
	bool areNotEqual_maxDiffExactly = equalAs<T>("1", "2", static_cast<T>(1));
	bool areNotEqual_maxDiffBig     = equalAs<T>("1", "2", static_cast<T>(1.00001));
};

using MyTypes = ::testing::Types<double, float, int>;
TYPED_TEST_SUITE(EqualAs, MyTypes);

TYPED_TEST(EqualAs, equalAs_float_int_double) {
	EXPECT_TRUE(this->areEqual_maxDiff0);
	EXPECT_TRUE(this->areEqual_maxDiffSmall);
	EXPECT_TRUE(this->areEqual_maxDiffBig);
	EXPECT_FALSE(this->areNotEqual_maxDiff0);
	EXPECT_FALSE(this->areNotEqual_maxDiffSmall);
	EXPECT_TRUE(this->areNotEqual_maxDiffExactly);
	EXPECT_TRUE(this->areNotEqual_maxDiffBig);
}

TEST(EqualAs, equalAs_float_double) {
	// now test some things that only apply to float & double
	EXPECT_TRUE(equalAs<double>("1.7394", "1.7393", 0.001));
	EXPECT_TRUE(equalAs<float>("1.7394", "1.7393", 0.001));
	EXPECT_FALSE(equalAs<double>("1.7394", "1.7393", 0.000000001));
	EXPECT_FALSE(equalAs<float>("1.7394", "1.7393", 0.0000000001));
}

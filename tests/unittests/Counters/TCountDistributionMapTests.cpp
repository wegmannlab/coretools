#include "gtest/gtest.h"

#include "coretools/Counters/TCountDistributionMap.h"

using namespace coretools;

TEST(TCountDistributionMap, add) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	// ID = 0
	EXPECT_EQ(map[0][0], 1);
	EXPECT_EQ(map[0][1], 2);
	// ID = 10
	EXPECT_EQ(map[10][0], 0);
	EXPECT_EQ(map[10][1], 0);
	EXPECT_EQ(map[10][2], 0);
	EXPECT_EQ(map[10][3], 0);
	EXPECT_EQ(map[10][4], 0);
	EXPECT_EQ(map[10][5], 1);
}

TEST(TCountDistributionMap, size) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	EXPECT_EQ(map.size(), 2);
}

TEST(TCountDistributionMap, counts) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	EXPECT_EQ(map.counts(), 4);
}

TEST(TCountDistributionMap, sum) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	EXPECT_EQ(map.sum(), 7);
}

TEST(TCountDistributionMap, mean) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	EXPECT_EQ(map.mean(), 1.75);
}

TEST(TCountDistributionMap, exists) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	EXPECT_EQ(map.exists(0), true);
	EXPECT_EQ(map.exists(10), true);
	EXPECT_EQ(map.exists(1), false);
}

TEST(TCountDistributionMap, operator_square_brackets) {
	TCountDistributionMap<uint64_t> map;

	map.add(0, 0);
	map.add(0, 1);
	map.add(0, 1);
	map.add(10, 5);

	EXPECT_EQ(map[0][0], 1);
	EXPECT_EQ(map[0][1], 2);
	EXPECT_EQ(map[10][5], 1);
	EXPECT_ANY_THROW(map[1][0]);
}

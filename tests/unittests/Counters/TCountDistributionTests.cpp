#include "gtest/gtest.h"

#include "coretools/Counters/TCountDistribution.h"
#include "coretools/Files/TInputFile.h"

using namespace coretools;


TEST(TCountDistribution, constructor) {
	TCountDistribution vec(10U);

	for (size_t i = 0; i < 10; i++) EXPECT_EQ(vec[i], 0);
}

TEST(TCountDistribution, add) {
	TCountDistribution vec;
	vec.add(0);
	vec.add(0);
	vec.add(1);
	vec.add(10);

	EXPECT_EQ(vec[0], 2);
	EXPECT_EQ(vec[1], 1);
	EXPECT_EQ(vec[10], 1);
	for (size_t i = 2; i < 10; i++) EXPECT_EQ(vec[i], 0);
}

TEST(TCountDistribution, add_frequency) {
	TCountDistribution vec;
	vec.add(0, 10);
	vec.add(0, 1);
	vec.add(1, 100);
	vec.add(10, 200);

	EXPECT_EQ(vec[0], 11);
	EXPECT_EQ(vec[1], 100);
	EXPECT_EQ(vec[10], 200);
	for (size_t i = 2; i < 10; i++) EXPECT_EQ(vec[i], 0);
}

TEST(TCountDistribution, add_other) {
	TCountDistribution vec;
	vec.add(0, 10);
	vec.add(0, 1);
	vec.add(1, 100);
	vec.add(10, 200);

	TCountDistribution vec2;
	vec2.add(vec);

	EXPECT_EQ(vec2[0], 11);
	EXPECT_EQ(vec2[1], 100);
	EXPECT_EQ(vec2[10], 200);
	for (size_t i = 2; i < 10; i++) EXPECT_EQ(vec2[i], 0);
}

TEST(TCountDistribution, operator_square_brackets) {
	TCountDistribution vec;
	vec.add(0, 10);
	vec.add(1, 100);
	vec.add(10, 200);

	vec[0] = vec[0] + 10;
	EXPECT_EQ(vec[0], 20);

	// outside range: just resizes
	vec[20] = 150;
	EXPECT_EQ(vec[20], 150);
}

TEST(TCountDistribution, operator_square_brackets_const) {
	const TCountDistribution vec;
	// outside range
	EXPECT_ANY_THROW(vec[100]);
}

TEST(TCountDistribution, at) {
	TCountDistribution vec;
	vec.add(0, 10);
	vec.add(1, 100);
	vec.add(10, 200);

	EXPECT_EQ(vec.at(0), 10);
	EXPECT_EQ(vec.at(1), 100);
	EXPECT_EQ(vec.at(10), 200);
	for (size_t i = 2; i < 10; i++) EXPECT_EQ(vec.at(i), 0);

	// outside range: just return 0
	EXPECT_EQ(vec.at(100), 0);
}

TEST(TCountDistribution, empty_clear) {
	TCountDistribution vec;
	EXPECT_TRUE(vec.empty());
	vec.add(10);
	EXPECT_FALSE(vec.empty());
	vec.clear();
	EXPECT_TRUE(vec.empty());
}

TEST(TCountDistribution, counts) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(10, 200);
	EXPECT_EQ(vec.counts(), 301);
}

TEST(TCountDistribution, countsLarger) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_EQ(vec.countsLarger(1), 210);
}

TEST(TCountDistribution, countsLargerZero) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_EQ(vec.countsLargerZero(), 211);
}

TEST(TCountDistribution, sum) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_EQ(vec.sum(), 2021);
}

TEST(TCountDistribution, mean) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_FLOAT_EQ(vec.mean(), 6.498392);
}

TEST(TCountDistribution, mode) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_FLOAT_EQ(vec.mode(), 10);
}

TEST(TCountDistribution, mode_ties) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1, 100);
	EXPECT_FLOAT_EQ(vec.mode(), 0);
}

TEST(TCountDistribution, min) {
	TCountDistribution vec;

	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_EQ(vec.min(), 2);
}

TEST(TCountDistribution, max) {
	TCountDistribution vec;

	vec.add(2, 10);
	vec.add(10, 200);
	vec.add(20, 0);
	EXPECT_EQ(vec.max(), 10);
}

TEST(TCountDistribution, fracLarger) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_FLOAT_EQ(vec.fracLarger(1), 0.6752412);
}

TEST(TCountDistribution, fracLargerZero) {
	TCountDistribution vec;

	vec.add(0, 100);
	vec.add(1);
	vec.add(2, 10);
	vec.add(10, 200);
	EXPECT_FLOAT_EQ(vec.fracLargerZero(), 0.6784566);
}

TEST(TCountDistribution, write) {
	TCountDistribution vec;
	TOutputFile out("file.txt");

	vec.add(0, 0);
	vec.add(1, 1);
	vec.add(2, 4);
	vec.add(3, 9);

	vec.write(out);
	out.close();

	size_t c = 0;
	for (TInputFile in("file.txt", FileType::NoHeader); !in.empty(); in.popFront(), ++c) {
		EXPECT_EQ(in.get<size_t>(0), c);
		EXPECT_EQ(in.get<size_t>(1), c*c);
	}
	remove("file.txt");
}

TEST(TCountDistribution, write_name) {
	TCountDistribution vec;
	TOutputFile out("file.txt");

	vec.add(0, 0);
	vec.add(1, 1);
	vec.add(2, 2);
	vec.add(3, 3);

	vec.write(out, "ID");
	out.close();

	size_t c = 0;
	for (TInputFile in("file.txt", FileType::NoHeader); !in.empty(); in.popFront(), ++c) {
		EXPECT_EQ(in.get(0), "ID");
		EXPECT_EQ(in.get(1), str::toString(c));
		EXPECT_EQ(in.get(2), str::toString(c));
	}
	remove("file.txt");
}

TEST(TCountDistribution, write_label) {
	TCountDistribution vec;

	vec.add(0, 0);
	vec.add(1, 1);
	vec.add(2, 2);
	vec.add(3, 3);

	vec.write("file.txt", "ID");

	TInputFile in("file.txt", FileType::Header);
	EXPECT_EQ(in.index("ID"), 0);
	EXPECT_EQ(in.index("counts"), 1);

	size_t c = 0;
	for (; !in.empty(); in.popFront(), ++c) {
		EXPECT_EQ(in.get<size_t>(0), c);
		EXPECT_EQ(in.get<size_t>(1), c);
	}
	remove("file.txt");
}

#include "gtest/gtest.h"

#include "coretools/Counters/TCountDistributionMultiDimensional.h"

using namespace coretools;

TEST(TCountDistributionMultiDimensional, constructor) {
	TCountDistributionMultiDimensional<uint32_t, uint64_t, true, 3> vec({2,3,4});

	size_t sum = 0.0;
	for(size_t i = 0; i < 2; ++i){
		for(size_t j = 0; j < 3; ++j){
			for(size_t k = 0; k < 4; ++k){
				vec.add({i,j,k}, i*j*k);
				sum += i*j*k;
				auto m = vec[{i,j,k}].max();
				EXPECT_EQ(m, i*j*k);
			}
		}
	}

	EXPECT_EQ(vec.sum(), sum);
	EXPECT_EQ(vec.mean(), sum / (double) vec.size());
}



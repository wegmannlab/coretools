#include "gtest/gtest.h"

#include "coretools/Counters/TCountDistributionVector.h"
#include "coretools/Files/TInputFile.h"

using namespace coretools;
using str::toString;

void fillComplicatedDistr(TCountDistributionVector<> &vec) {
	vec.add(0, 0, 10);
	vec.add(0, 0, 1);
	vec.add(0, 1, 100);
	vec.add(10, 0, 400);
	vec.add(10, 1, 800);
	vec.add(10, 5, 200);
}

TEST(TCountDistributionVector, constructor) {
	TCountDistributionVector vec(10);

	for (size_t i = 0; i < 10; i++) EXPECT_EQ(vec[i].empty(), true);
}

TEST(TCountDistributionVector, add) {
	TCountDistributionVector vec(5);

	vec.add(0, 0);
	vec.add(0, 0);
	vec.add(0, 1);
	vec.add(10, 5);

	// ID = 0
	EXPECT_EQ(vec[0][0], 2);
	EXPECT_EQ(vec[0][1], 1);
	// ID = 1
	EXPECT_EQ(vec[1].empty(), true);
	// ID = 2
	EXPECT_EQ(vec[2].empty(), true);
	// ...
	// ID = 10
	EXPECT_EQ(vec[10][0], 0);
	EXPECT_EQ(vec[10][1], 0);
	EXPECT_EQ(vec[10][2], 0);
	EXPECT_EQ(vec[10][3], 0);
	EXPECT_EQ(vec[10][4], 0);
	EXPECT_EQ(vec[10][5], 1);
}

TEST(TCountDistributionVector, add_frequency) {
	TCountDistributionVector vec(5);

	vec.add(0, 0, 10);
	vec.add(0, 0, 1);
	vec.add(0, 1, 100);
	vec.add(10, 5, 200);

	// ID = 0
	EXPECT_EQ(vec[0][0], 11);
	EXPECT_EQ(vec[0][1], 100);
	// ID = 1
	EXPECT_EQ(vec[1].empty(), true);
	// ID = 2
	EXPECT_EQ(vec[2].empty(), true);
	// ...
	// ID = 10
	EXPECT_EQ(vec[10][0], 0);
	EXPECT_EQ(vec[10][1], 0);
	EXPECT_EQ(vec[10][2], 0);
	EXPECT_EQ(vec[10][3], 0);
	EXPECT_EQ(vec[10][4], 0);
	EXPECT_EQ(vec[10][5], 200);
}

TEST(TCountDistributionVector, add_id_other) {
	TCountDistribution<uint32_t, uint64_t, true> vec(5);

	vec.add(0, 10);
	vec.add(0, 1);
	vec.add(1, 100);

	TCountDistributionVector vec2;
	vec2.add(0, vec);
	vec2.add(10, 5, 200);

	// ID = 0
	EXPECT_EQ(vec2[0][0], 11);
	EXPECT_EQ(vec2[0][1], 100);
	// ID = 1
	EXPECT_EQ(vec2[1].empty(), true);
	// ID = 2
	EXPECT_EQ(vec2[2].empty(), true);
	// ...
	// ID = 10
	EXPECT_EQ(vec2[10][0], 0);
	EXPECT_EQ(vec2[10][1], 0);
	EXPECT_EQ(vec2[10][2], 0);
	EXPECT_EQ(vec2[10][3], 0);
	EXPECT_EQ(vec2[10][4], 0);
	EXPECT_EQ(vec2[10][5], 200);
}

TEST(TCountDistributionVector, add_other) {
	TCountDistributionVector vec(5);

	vec.add(0, 0, 10);
	vec.add(0, 0, 1);
	vec.add(0, 1, 100);
	vec.add(10, 5, 200);

	TCountDistributionVector vec2;
	vec2.add(vec);

	// ID = 0
	EXPECT_EQ(vec2[0][0], 11);
	EXPECT_EQ(vec2[0][1], 100);
	// ID = 1
	EXPECT_EQ(vec2[1].empty(), true);
	// ID = 2
	EXPECT_EQ(vec2[2].empty(), true);
	// ...
	// ID = 10
	EXPECT_EQ(vec2[10][0], 0);
	EXPECT_EQ(vec2[10][1], 0);
	EXPECT_EQ(vec2[10][2], 0);
	EXPECT_EQ(vec2[10][3], 0);
	EXPECT_EQ(vec2[10][4], 0);
	EXPECT_EQ(vec2[10][5], 200);
}

TEST(TCountDistributionVector, size_clear) {
	TCountDistributionVector vec(10);
	EXPECT_EQ(vec.size(), 10);
	vec.clear();
	EXPECT_EQ(vec.size(), 0);
}

TEST(TCountDistributionVector, counts) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.counts(), 1511);
}

TEST(TCountDistributionVector, countsLarger) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.countsLarger(1), 200);
}

TEST(TCountDistributionVector, countsLargerZero) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.countsLargerZero(), 1100);
}

TEST(TCountDistributionVector, sum) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.sum(), 1900);
}

TEST(TCountDistributionVector, mean) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_FLOAT_EQ(vec.mean(), 1.257445);
}

TEST(TCountDistributionVector, min) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.min(), 0);
}

TEST(TCountDistributionVector, max) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.max(), 5); // 5, because 5 is the largest non-zero value
}

TEST(TCountDistributionVector, fracLarger) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_FLOAT_EQ(vec.fracLarger(1), 0.1323627);
}

TEST(TCountDistributionVector, fracLargerZero) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_FLOAT_EQ(vec.fracLargerZero(), 0.7279947);
}

TEST(TCountDistributionVector, exists) {
	TCountDistributionVector vec(10);
	fillComplicatedDistr(vec);

	EXPECT_EQ(vec.exists(10), true);
	EXPECT_EQ(vec.exists(11), false);
}

TEST(TCountDistributionVector, operator_square_brackets_const) {
	const TCountDistributionVector vec(10);
	// outside range
	EXPECT_ANY_THROW(vec[100]);
}

TEST(TCountDistributionVector, fillCombinedDistribution) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// fill with stuff -> should be cleared afterwards
	TCountDistribution combined;
	combined.add(100, 28);

	vec.fillCombinedDistribution(combined);

	EXPECT_EQ(combined[0], 411);
	EXPECT_EQ(combined[1], 900);
	EXPECT_EQ(combined[2], 0);
	EXPECT_EQ(combined[3], 0);
	EXPECT_EQ(combined[4], 0);
	EXPECT_EQ(combined[5], 200);
}

TEST(TCountDistributionVector, RSquared) {
	TCountDistributionVector vec;

	vec.add(0, 0, 10);
	vec.add(0, 1, 20);
	vec.add(0, 2, 30);
	vec.add(1, 0, 40);
	vec.add(1, 1, 50);
	vec.add(1, 2, 60);
	vec.add(1, 3, 70);

	double RSquared = vec.RSquared();

	EXPECT_FLOAT_EQ(RSquared, 0.02404325);
}

TEST(TCountDistributionVector, writeCombined) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	TOutputFile out("file.txt");
	vec.writeCombined(out);
	out.close();


	// read and check if expected
	size_t c = 0;
	std::vector<uint64_t> expected = {411, 900, 0, 0, 0, 200};
	for (TInputFile in("file.txt", FileType::NoHeader); !in.empty(); in.popFront(), ++c) {
		EXPECT_EQ(in.get<size_t>(0), c);
		EXPECT_EQ(in.get<size_t>(1), expected[c]);
	}
}

TEST(TCountDistributionVector, writeCombined_name) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	TOutputFile out("file.txt");
	vec.writeCombined(out, "ID");
	out.close();

	// read and check if expected
	std::vector<uint64_t> expected = {411, 900, 0, 0, 0, 200};
	size_t c                          = 0;
	for (TInputFile in("file.txt", FileType::NoHeader); !in.empty(); in.popFront(), ++c) {
		EXPECT_EQ(in.get(0), "ID");
		EXPECT_EQ(in.get(1), str::toString(c));
		EXPECT_EQ(in.get(2), str::toString(expected[c]));
	}
	remove("file.txt");
}

TEST(TCountDistributionVector, write) {
	using str::toString;
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	TOutputFile out("file.txt");
	vec.write(out);
	out.close();

	// read and check if expected
	TInputFile in("file.txt", FileType::NoHeader);
	EXPECT_EQ(in.get(0), "0");
	EXPECT_EQ(in.get(1), "0");
	EXPECT_EQ(in.get(2), str::toString(11));

	in.popFront();
	EXPECT_EQ(in.get(0), "0");
	EXPECT_EQ(in.get(1), "1");
	EXPECT_EQ(in.get(2), toString(100));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "0");
	EXPECT_EQ(in.get(2), toString(400));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "1");
	EXPECT_EQ(in.get(2), toString(800));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "2");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "3");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "4");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "5");
	EXPECT_EQ(in.get(2), toString(200));

	in.close();
	remove("file.txt");
}

TEST(TCountDistributionVector, write_filename_labelID_label) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	vec.write("file.txt", "ID", "label");

	// read and check if expected
	TInputFile in("file.txt", FileType::Header);

	EXPECT_EQ(in.index("ID"), 0);
	EXPECT_EQ(in.index("label"), 1);
	EXPECT_EQ(in.index("counts"), 2);

	EXPECT_EQ(in.get(0), "0");
	EXPECT_EQ(in.get(1), "0");
	EXPECT_EQ(in.get(2), toString(11));

	in.popFront();
	EXPECT_EQ(in.get(0), "0");
	EXPECT_EQ(in.get(1), "1");
	EXPECT_EQ(in.get(2), toString(100));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "0");
	EXPECT_EQ(in.get(2), toString(400));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "1");
	EXPECT_EQ(in.get(2), toString(800));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "2");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "3");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "4");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), "5");
	EXPECT_EQ(in.get(2), toString(200));

	in.close();
	remove("file.txt");
}

TEST(TCountDistributionVector, write_names) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	TOutputFile out("file.txt");
	std::vector<std::string> names = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"};
	vec.write(out, names);
	out.close();

	// read and check if expected
	TInputFile in("file.txt", FileType::NoHeader);

	EXPECT_EQ(in.get(0), "a");
	EXPECT_EQ(in.get(1), "0");
	EXPECT_EQ(in.get(2), toString(11));

	in.popFront();
	EXPECT_EQ(in.get(0), "a");
	EXPECT_EQ(in.get(1), "1");
	EXPECT_EQ(in.get(2), toString(100));

	in.popFront();
	EXPECT_EQ(in.get(0), "k");
	EXPECT_EQ(in.get(1), "0");
	EXPECT_EQ(in.get(2), toString(400));

	in.popFront();
	EXPECT_EQ(in.get(0), "k");
	EXPECT_EQ(in.get(1), "1");
	EXPECT_EQ(in.get(2), toString(800));

	in.popFront();
	EXPECT_EQ(in.get(0), "k");
	EXPECT_EQ(in.get(1), "2");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "k");
	EXPECT_EQ(in.get(1), "3");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "k");
	EXPECT_EQ(in.get(1), "4");
	EXPECT_EQ(in.get(2), toString(0));

	in.popFront();
	EXPECT_EQ(in.get(0), "k");
	EXPECT_EQ(in.get(1), "5");
	EXPECT_EQ(in.get(2), toString(200));

	in.close();
	remove("file.txt");
}

TEST(TCountDistributionVector, writeAsMatrix_filename_labelID_label) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	vec.writeAsMatrix("file.txt", "ID", "label");

	// read and check if expected
	TInputFile in("file.txt", FileType::Header);
	EXPECT_EQ(in.index("ID/label"), 0);
	EXPECT_EQ(in.index("0"), 1);
	EXPECT_EQ(in.index("1"), 2);
	EXPECT_EQ(in.index("2"), 3);
	EXPECT_EQ(in.index("3"), 4);
	EXPECT_EQ(in.index("4"), 5);
	EXPECT_EQ(in.index("5"), 6);

	EXPECT_EQ(in.get(0), "0");
	EXPECT_EQ(in.get(1), toString(11));
	EXPECT_EQ(in.get(2), toString(100));
	EXPECT_EQ(in.get(3), toString(0));
	EXPECT_EQ(in.get(4), toString(0));
	EXPECT_EQ(in.get(5), toString(0));
	EXPECT_EQ(in.get(6), toString(0));

	for (size_t i = 1; i < 10; i++) {
		in.popFront();
		EXPECT_EQ(in.get<size_t>(0), i);
		EXPECT_EQ(in.get<size_t>(1), 0);
		EXPECT_EQ(in.get<size_t>(2), 0);
		EXPECT_EQ(in.get<size_t>(3), 0);
		EXPECT_EQ(in.get<size_t>(4), 0);
		EXPECT_EQ(in.get<size_t>(5), 0);
		EXPECT_EQ(in.get<size_t>(6), 0);
	}

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), toString(400));
	EXPECT_EQ(in.get(2), toString(800));
	EXPECT_EQ(in.get(3), toString(0));
	EXPECT_EQ(in.get(4), toString(0));
	EXPECT_EQ(in.get(5), toString(0));
	EXPECT_EQ(in.get(6), toString(200));

	in.close();
	remove("file.txt");
}

TEST(TCountDistributionVector, writeAsMatrix_filename_labelID_valueNames) {
	TCountDistributionVector vec(5);
	fillComplicatedDistr(vec);

	// open output file and write
	std::vector<std::string> valNames = {"a", "b", "c", "d", "e", "f"};
	vec.writeAsMatrix("file.txt", "ID", valNames);

	// read and check if expected
	TInputFile in("file.txt", FileType::Header);
	EXPECT_EQ(in.index("ID"), 0);
	EXPECT_EQ(in.index("a"), 1);
	EXPECT_EQ(in.index("b"), 2);
	EXPECT_EQ(in.index("c"), 3);
	EXPECT_EQ(in.index("d"), 4);
	EXPECT_EQ(in.index("e"), 5);
	EXPECT_EQ(in.index("f"), 6);

	EXPECT_EQ(in.get(0), "0");
	EXPECT_EQ(in.get(1), toString(11));
	EXPECT_EQ(in.get(2), toString(100));
	EXPECT_EQ(in.get(3), toString(0));
	EXPECT_EQ(in.get(4), toString(0));
	EXPECT_EQ(in.get(5), toString(0));
	EXPECT_EQ(in.get(6), toString(0));

	for (size_t i = 1; i < 10; i++) {
		in.popFront();
		EXPECT_EQ(in.get(0), toString(i));
		EXPECT_EQ(in.get(1), toString(0));
		EXPECT_EQ(in.get(2), toString(0));
		EXPECT_EQ(in.get(3), toString(0));
		EXPECT_EQ(in.get(4), toString(0));
		EXPECT_EQ(in.get(5), toString(0));
		EXPECT_EQ(in.get(6), toString(0));
	}

	in.popFront();
	EXPECT_EQ(in.get(0), "10");
	EXPECT_EQ(in.get(1), toString(400));
	EXPECT_EQ(in.get(2), toString(800));
	EXPECT_EQ(in.get(3), toString(0));
	EXPECT_EQ(in.get(4), toString(0));
	EXPECT_EQ(in.get(5), toString(0));
	EXPECT_EQ(in.get(6), toString(200));

	in.close();
	remove("file.txt");
}

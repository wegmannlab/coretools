#include "coretools/Containers/TStrongArray.h"
#include "coretools/Containers/TView.h"
#include "coretools/Types/probability.h"
#include "coretools/traits.h"
#include <map>
#include <set>
#include <vector>

using namespace coretools;

//argumentType
void f1(size_t, double, int);
static_assert(std::is_same_v<argumentType_t<0, decltype(f1)>, size_t>);
static_assert(std::is_same_v<argumentType_t<1, decltype(f1)>, double>);
static_assert(std::is_same_v<argumentType_t<2, decltype(f1)>, int>);

// isIterable
static_assert(isIterable_v<std::vector<double>>);
static_assert(isIterable_v<std::array<int, 4>>);
static_assert(isIterable_v<std::set<int>>);
static_assert(isIterable_v<std::map<int, double>>);
static_assert(isIterable_v<std::initializer_list<int>>);

static_assert(!isIterable_v<double>);
static_assert(!isIterable_v<int>);
static_assert(!isIterable_v<char>);
static_assert(!isIterable_v<std::vector<double>::iterator>);

// indexType
static_assert(std::is_same_v<indexType_t<std::vector<double>>, size_t>);
static_assert(std::is_same_v<indexType_t<double>, void>);
enum class iType : size_t {min, A=min, B, max};
static_assert(std::is_same_v<indexType_t<TStrongArray<double, iType>>, iType>);

//isView
static_assert(isView_v<TView<double>>);
static_assert(isView_v<TView<std::string>>);
static_assert(isView_v<TConstView<double>>);
static_assert(isView_v<TConstView<Probability>>);
static_assert(!isView_v<std::vector<double>>);

//isResizable
static_assert(isResizable_v<std::vector<double>>);
static_assert(!isResizable_v<std::array<double, 4>>);

// isCastable
static_assert(isCastable_v<int, double>);
static_assert(!isCastable_v<std::vector<double>, double>);
static_assert(isCastable_v<Probability, double>);
static_assert(!isCastable_v<Probability, std::array<Probability, 4>>);

// hasGetMember
static_assert(hasGetMember_v<Probability>);
static_assert(!hasGetMember_v<double>);

// underlying
enum class Test : size_t;
static_assert(std::is_same_v<double, decltype(underlying(Probability{}))>);
static_assert(std::is_same_v<double, decltype(underlying(double{}))>);
static_assert(std::is_same_v<size_t, decltype(underlying(Test{}))>);
static_assert(!std::is_same_v<double, decltype(underlying(int{}))>);

// underlyingType
static_assert(std::is_same_v<double, underlyingType_t<Probability>>);
static_assert(std::is_same_v<double, underlyingType_t<double>>);
static_assert(std::is_same_v<size_t, underlyingType_t<Test>>);
static_assert(!std::is_same_v<double, underlyingType_t<int>>);

// underlyingType
static_assert(isSameUnderlyingType_v<double, double>);
static_assert(isSameUnderlyingType_v<double, Probability>);
static_assert(!isSameUnderlyingType_v<double, int>);

// SmallestInteger
static_assert(sizeof(SmallestInteger<1>::type) == 1);
static_assert(sizeof(SmallestInteger<8>::type) == 1);
static_assert(sizeof(SmallestInteger<9>::type) == 2);
static_assert(sizeof(SmallestInteger<16>::type) == 2);
static_assert(sizeof(SmallestInteger<17>::type) == 4);
static_assert(sizeof(SmallestInteger<32>::type) == 4);
static_assert(sizeof(SmallestInteger<33>::type) == 8);
static_assert(sizeof(SmallestInteger<64>::type) == 8);

static_assert(std::is_same<SmallestInteger<8>::type, std::uint8_t>::value);
static_assert(std::is_same<SmallestInteger<16>::type, std::uint16_t>::value);
static_assert(std::is_same<SmallestInteger<32>::type, std::uint32_t>::value);
static_assert(std::is_same<SmallestInteger<64>::type, std::uint64_t>::value);
